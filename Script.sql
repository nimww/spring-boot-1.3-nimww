--<ScriptOptions statementTerminator=";"/>

ALTER TABLE "public"."tnim3_userpracticelu" DROP CONSTRAINT "tnim3_userpracticelu_pkey";

ALTER TABLE "public"."tpromocodeli" DROP CONSTRAINT "tpromocodeli_pkey";

ALTER TABLE "public"."tpracticecontractstatusli" DROP CONSTRAINT "tpracticecontractstatusli_pkey";

ALTER TABLE "public"."tnim3_caseaccountuseraccountlu" DROP CONSTRAINT "tnim3_caseaccountuseraccountlu_pkey";

ALTER TABLE "public"."tspecialtystatusli" DROP CONSTRAINT "tspecialtystatusli_pkey";

ALTER TABLE "public"."tsalesstatusli" DROP CONSTRAINT "tsalesstatusli_pkey";

ALTER TABLE "public"."tccardtransaction" DROP CONSTRAINT "tccardtransaction_pkey";

ALTER TABLE "public"."thcoprivilegecategorytypelu" DROP CONSTRAINT "thcoprivilegecategorytypelu_pkey";

ALTER TABLE "public"."tnim2_feeschedule" DROP CONSTRAINT "tnim2_feeschedule_pkey";

ALTER TABLE "public"."tbillingtransaction" DROP CONSTRAINT "tbillingtransaction_pkey";

ALTER TABLE "public"."teventchild" DROP CONSTRAINT "teventchild_pkey";

ALTER TABLE "public"."tdrugmedicaredata" DROP CONSTRAINT "tdrugmedicaredata_pkey";

ALTER TABLE "public"."tsalesmansaleslu" DROP CONSTRAINT "tsalesmansaleslu_pkey";

ALTER TABLE "public"."nidnamecontest" DROP CONSTRAINT "nidnamecontest_pkey";

ALTER TABLE "public"."tcommtrackalertstatuscodeli" DROP CONSTRAINT "tcommtrackalertstatuscodeli_pkey";

ALTER TABLE "public"."tmodalitytypeli" DROP CONSTRAINT "tmodalitytypeli_pkey";

ALTER TABLE "public"."tsalesaccount" DROP CONSTRAINT "tsalesaccount_pkey";

ALTER TABLE "public"."tappointmenttypeli" DROP CONSTRAINT "tappointmenttypeli_pkey";

ALTER TABLE "public"."tuseraccount" DROP CONSTRAINT "tuseraccount_pkey";

ALTER TABLE "public"."tlicenseboardli" DROP CONSTRAINT "tlicenseboardli_pkey";

ALTER TABLE "public"."tnim3_commreference" DROP CONSTRAINT "tnim3_commreference_pkey";

ALTER TABLE "public"."tdocumentmanagement" DROP CONSTRAINT "tdocumentmanagement_pkey";

ALTER TABLE "public"."tnim2_feescheduleref" DROP CONSTRAINT "tnim2_feescheduleref_pkey";

ALTER TABLE "public"."tct_modelli" DROP CONSTRAINT "tct_modelli_pkey";

ALTER TABLE "public"."tbillingaccount" DROP CONSTRAINT "tbillingaccount_pkey";

ALTER TABLE "public"."tfacilityaffiliation" DROP CONSTRAINT "tfacilityaffiliation_pkey";

ALTER TABLE "public"."tnim2_appointment" DROP CONSTRAINT "tnim2_appointment_pkey";

ALTER TABLE "public"."tnim3_netdevdatatablesupport" DROP CONSTRAINT "tnim3_netdevdatatablesupport_pkey";

ALTER TABLE "public"."atpa_compare" DROP CONSTRAINT "atpa_compare_pkey";

ALTER TABLE "public"."tactivityreference" DROP CONSTRAINT "tactivityreference_pkey";

ALTER TABLE "public"."tresourcetypeli" DROP CONSTRAINT "tresourcetypeli_pkey";

ALTER TABLE "public"."teventeventlu" DROP CONSTRAINT "teventeventlu_pkey";

ALTER TABLE "public"."tactivity" DROP CONSTRAINT "tactivity_pkey";

ALTER TABLE "public"."tcommunityforum" DROP CONSTRAINT "tcommunityforum_pkey";

ALTER TABLE "public"."tactivityfilereference" DROP CONSTRAINT "tactivityfilereference_pkey";

ALTER TABLE "public"."thospitalli" DROP CONSTRAINT "thospitalli_pkey";

ALTER TABLE "public"."tnim2_salescontact" DROP CONSTRAINT "tnim2_salescontact_pkey";

ALTER TABLE "public"."tprofessionalliability" DROP CONSTRAINT "tprofessionalliability_pkey";

ALTER TABLE "public"."thcodepartmentli" DROP CONSTRAINT "thcodepartmentli_pkey";

ALTER TABLE "public"."tadminpracticelu_copy" DROP CONSTRAINT "tadminpracticelu_copy_pkey";

ALTER TABLE "public"."thcouseraccountlu" DROP CONSTRAINT "thcouseraccountlu_pkey";

ALTER TABLE "public"."tnim3_weblinktracker" DROP CONSTRAINT "tnim3_weblinktracker_pkey";

ALTER TABLE "public"."tmri_modelli" DROP CONSTRAINT "tmri_modelli_pkey";

ALTER TABLE "public"."thcophysicianlu" DROP CONSTRAINT "thcophysicianlu_pkey";

ALTER TABLE "public"."tcompanysecuritygrouplu" DROP CONSTRAINT "tcompanysecuritygrouplu_pkey";

ALTER TABLE "public"."tnim3_directreferralsource" DROP CONSTRAINT "tnim3_directreferralsource_pkey";

ALTER TABLE "public"."tnim3_caseaccount" DROP CONSTRAINT "tnim3_caseaccount_pkey";

ALTER TABLE "public"."treadphysicianlu" DROP CONSTRAINT "treadphysicianlu_pkey";

ALTER TABLE "public"."tnim3_nidpromo" DROP CONSTRAINT "tnim3_nidpromo_pkey";

ALTER TABLE "public"."tstandardaccesstypeli" DROP CONSTRAINT "tstandardaccesstypeli_pkey";

ALTER TABLE "public"."tcoveringphysicians" DROP CONSTRAINT "tcoveringphysicians_pkey";

ALTER TABLE "public"."tnim2_payermaster" DROP CONSTRAINT "tnim2_payermaster_pkey";

ALTER TABLE "public"."thcodistrictli" DROP CONSTRAINT "thcodistrictli_pkey";

ALTER TABLE "public"."ttemp" DROP CONSTRAINT "ttemp_pkey";

ALTER TABLE "public"."tvoidleakreasonli" DROP CONSTRAINT "tvoidleakreasonli_pkey";

ALTER TABLE "public"."rest_api_token" DROP CONSTRAINT "nid_api_token_copy_pkey1";

ALTER TABLE "public"."tuserstatusli" DROP CONSTRAINT "tuserstatusli_pkey";

ALTER TABLE "public"."tnim3_jobtype" DROP CONSTRAINT "tnim3_jobtype_pkey";

ALTER TABLE "public"."tmcmc_retros" DROP CONSTRAINT "tmcmc_retros_pkey";

ALTER TABLE "public"."tnim3_modality" DROP CONSTRAINT "tnim3_modality_pkey";

ALTER TABLE "public"."tnim3_salesportal" DROP CONSTRAINT "tnim3_salesportal_pkey";

ALTER TABLE "public"."tencounterstatusli" DROP CONSTRAINT "tencounterstatusli_pkey";

ALTER TABLE "public"."tmalpractice" DROP CONSTRAINT "tmalpractice_pkey";

ALTER TABLE "public"."tupdateitem" DROP CONSTRAINT "tupdateitem_pkey";

ALTER TABLE "public"."tnim2_billingentity" DROP CONSTRAINT "tnim2_billingentity_pkey";

ALTER TABLE "public"."terrorlog" DROP CONSTRAINT "terrorlog_pkey";

ALTER TABLE "public"."thcoprivilegequestionlu" DROP CONSTRAINT "thcoprivilegequestionlu_pkey";

ALTER TABLE "public"."tapprovaltypeli" DROP CONSTRAINT "tapprovaltypeli_pkey";

ALTER TABLE "public"."texperience" DROP CONSTRAINT "texperience_pkey";

ALTER TABLE "public"."tnim2_salestransaction" DROP CONSTRAINT "tnim2_salestransaction_pkey";

ALTER TABLE "public"."bunchcare_eligibility" DROP CONSTRAINT "bunchcare_eligibility_pkey";

ALTER TABLE "public"."tsuppuecn" DROP CONSTRAINT "tsuppuecn_pkey";

ALTER TABLE "public"."tphysiciancategoryli" DROP CONSTRAINT "tphysiciancategoryli_pkey";

ALTER TABLE "public"."tgroupsecurityitemsli" DROP CONSTRAINT "tgroupsecurityitemsli_pkey";

ALTER TABLE "public"."tnid_widgettrack" DROP CONSTRAINT "tnid_widgettrack_pkey";

ALTER TABLE "public"."thcophysicianstatusli" DROP CONSTRAINT "thcophysicianstatusli_pkey";

ALTER TABLE "public"."tnim3_netdevcommtrack" DROP CONSTRAINT "tnim3_netdevcommtrack_pkey";

ALTER TABLE "public"."tformli" DROP CONSTRAINT "tformli_pkey";

ALTER TABLE "public"."tphysicianeventlu" DROP CONSTRAINT "tphysicianeventlu_pkey";

ALTER TABLE "public"."tnim3_document" DROP CONSTRAINT "tnim3_document_pkey";

ALTER TABLE "public"."thcomaster" DROP CONSTRAINT "thcomaster_pkey";

ALTER TABLE "public"."tcommtracktypeli" DROP CONSTRAINT "tcommtracktypeli_pkey";

ALTER TABLE "public"."tnim3_billingentity" DROP CONSTRAINT "tnim3_billingentity_pkey";

ALTER TABLE "public"."tnim3_cptgroup" DROP CONSTRAINT "tnim3_cptgroup_pkey";

ALTER TABLE "public"."talliedhealthprofessional" DROP CONSTRAINT "talliedhealthprofessional_pkey";

ALTER TABLE "public"."thcocontractstatusli" DROP CONSTRAINT "thcocontractstatusli_pkey";

ALTER TABLE "public"."audittemp" DROP CONSTRAINT "taudit_copy_pkey";

ALTER TABLE "public"."tadminphysicianlu" DROP CONSTRAINT "tadminphysicianlu_pkey";

ALTER TABLE "public"."tsalesmanmaster" DROP CONSTRAINT "tsalesmanmaster_pkey";

ALTER TABLE "public"."tcvohcolu" DROP CONSTRAINT "tcvohcolu_pkey";

ALTER TABLE "public"."tpracticetypeli" DROP CONSTRAINT "tpracticetypeli_pkey";

ALTER TABLE "public"."talertdatetrack" DROP CONSTRAINT "talertdatetrack_pkey";

ALTER TABLE "public"."tfieldsecurity" DROP CONSTRAINT "tfieldsecurity_pkey";

ALTER TABLE "public"."tnim_report" DROP CONSTRAINT "tnim_report_pkey";

ALTER TABLE "public"."tscheduledappointmentstatusli" DROP CONSTRAINT "tscheduledappointmentstatusli_pkey";

ALTER TABLE "public"."tyesnoli" DROP CONSTRAINT "tyesnoli_pkey";

ALTER TABLE "public"."tnim3_payermaster_copy" DROP CONSTRAINT "tnim3_payermaster_copy1_pkey";

ALTER TABLE "public"."tphdbhospitalli" DROP CONSTRAINT "tphdbhospitalli_pkey";

ALTER TABLE "public"."tmanagedcareplan" DROP CONSTRAINT "tmanagedcareplan_pkey";

ALTER TABLE "public"."tdegreetypeli" DROP CONSTRAINT "tdegreetypeli_pkey";

ALTER TABLE "public"."thcophysicianstanding" DROP CONSTRAINT "thcophysicianstanding_pkey";

ALTER TABLE "public"."tnim2_caseaccountuseraccountlu" DROP CONSTRAINT "tnim2_caseaccountuseraccountlu_pkey";

ALTER TABLE "public"."tnim3_cptgrouplist" DROP CONSTRAINT "tnim3_cptgrouplist_pkey";

ALTER TABLE "public"."tnim3_referralintake" DROP CONSTRAINT "tnim3_referralintake_pkey";

ALTER TABLE "public"."tplcli" DROP CONSTRAINT "tplcli_pkey";

ALTER TABLE "public"."ttimetrack_rcodeli" DROP CONSTRAINT "ttimetrack_rcodeli_pkey";

ALTER TABLE "public"."tcallschedule" DROP CONSTRAINT "tcallschedule_pkey";

ALTER TABLE "public"."tupdateitemstatusli" DROP CONSTRAINT "tupdateitemstatusli_pkey";

ALTER TABLE "public"."tnpdbfoltypeli" DROP CONSTRAINT "tnpdbfoltypeli_pkey";

ALTER TABLE "public"."tothercertification" DROP CONSTRAINT "tothercertification_pkey";

ALTER TABLE "public"."tstateli" DROP CONSTRAINT "tstateli_pkey";

ALTER TABLE "public"."tnim3_commtrack" DROP CONSTRAINT "tnim3_commtrack_pkey";

ALTER TABLE "public"."tprivilegecategorytypeli" DROP CONSTRAINT "tprivilegecategorytypeli_pkey";

ALTER TABLE "public"."tdruglist" DROP CONSTRAINT "tdruglist_pkey";

ALTER TABLE "public"."tsalutationli" DROP CONSTRAINT "tsalutationli_pkey";

ALTER TABLE "public"."fce_referral2" DROP CONSTRAINT "nid_api_token_copy_pkey";

ALTER TABLE "public"."tnim3_servicebillingtransaction" DROP CONSTRAINT "tnim3_servicebillingtransaction_pkey";

ALTER TABLE "public"."tpeerreference" DROP CONSTRAINT "tpeerreference_pkey";

ALTER TABLE "public"."tnim2_document" DROP CONSTRAINT "tnim2_document_pkey";

ALTER TABLE "public"."ttransaction" DROP CONSTRAINT "ttransaction_pkey";

ALTER TABLE "public"."tnim3_netdevdatatable" DROP CONSTRAINT "tnim3_netdevdatatable_pkey";

ALTER TABLE "public"."tprofessionalsociety" DROP CONSTRAINT "tprofessionalsociety_pkey";

ALTER TABLE "public"."tnim_payermaster" DROP CONSTRAINT "tnim_payermaster_pkey";

ALTER TABLE "public"."tattestr" DROP CONSTRAINT "tattestr_pkey";

ALTER TABLE "public"."tpracticeactivity" DROP CONSTRAINT "tpracticeactivity_pkey";

ALTER TABLE "public"."tschoolli" DROP CONSTRAINT "tschoolli_pkey";

ALTER TABLE "public"."tphysicianuseraccountlu" DROP CONSTRAINT "tphysicianuseraccountlu_pkey";

ALTER TABLE "public"."tsupppractoptometry" DROP CONSTRAINT "tsupppractoptometry_pkey";

ALTER TABLE "public"."tcaseaccountstatusli" DROP CONSTRAINT "tcaseaccountstatusli_pkey";

ALTER TABLE "public"."temailtransaction" DROP CONSTRAINT "temailtransaction_pkey";

ALTER TABLE "public"."tphysicianpracticelu" DROP CONSTRAINT "tphysicianpracticelu_pkey";

ALTER TABLE "public"."tboardcertification" DROP CONSTRAINT "tboardcertification_pkey";

ALTER TABLE "public"."thcodivisionli" DROP CONSTRAINT "thcodivisionli_pkey";

ALTER TABLE "public"."tnim3_feeschedule" DROP CONSTRAINT "tnim3_feeschedule_pkey";

ALTER TABLE "public"."tcvomaster" DROP CONSTRAINT "tcvomaster_pkey";

ALTER TABLE "public"."tnim3_directpatientcard" DROP CONSTRAINT "tnim3_directpatientcard_pkey";

ALTER TABLE "public"."treadstatusli" DROP CONSTRAINT "treadstatusli_pkey";

ALTER TABLE "public"."thcodisclosurequestionlu" DROP CONSTRAINT "thcodisclosurequestionlu_pkey";

ALTER TABLE "public"."tcompanyuseraccountlu" DROP CONSTRAINT "tcompanyuseraccountlu_pkey";

ALTER TABLE "public"."tnim2_salesprospect" DROP CONSTRAINT "tnim2_salesprospect_pkey";

ALTER TABLE "public"."texperiencetypeli" DROP CONSTRAINT "texperiencetypeli_pkey";

ALTER TABLE "public"."tgroupsecurity" DROP CONSTRAINT "tgroupsecurity_pkey";

ALTER TABLE "public"."tworkhistorytypeli" DROP CONSTRAINT "tworkhistorytypeli_pkey";

ALTER TABLE "public"."thcophysiciantransaction" DROP CONSTRAINT "thcophysiciantransaction_pkey";

ALTER TABLE "public"."tprofessionaleducation" DROP CONSTRAINT "tprofessionaleducation_pkey";

ALTER TABLE "public"."tdisclosurequestionli" DROP CONSTRAINT "tdisclosurequestionli_pkey";

ALTER TABLE "public"."tadminpracticerelationshiptypeli" DROP CONSTRAINT "tadminpracticerelationshiptypeli_pkey";

ALTER TABLE "public"."tsupportbase" DROP CONSTRAINT "tsupportbase_pkey";

ALTER TABLE "public"."tuseraccount_copy" DROP CONSTRAINT "tuseraccount_copy_pkey";

ALTER TABLE "public"."treadphysicianstatusli" DROP CONSTRAINT "treadphysicianstatusli_pkey";

ALTER TABLE "public"."tdocumenttypeli" DROP CONSTRAINT "tdocumenttypeli_pkey";

ALTER TABLE "public"."tnim3_appointment" DROP CONSTRAINT "tnim3_appointment_pkey";

ALTER TABLE "public"."tpracticemaster_copy" DROP CONSTRAINT "tpracticemaster_copy_pkey";

ALTER TABLE "public"."tnim_patientaccount" DROP CONSTRAINT "tnim_patientaccount_pkey";

ALTER TABLE "public"."tnim3_schedulingtransaction" DROP CONSTRAINT "tnim3_schedulingtransaction_pkey";

ALTER TABLE "public"."tuaalertsli" DROP CONSTRAINT "tuaalertsli_pkey";

ALTER TABLE "public"."tcountryli" DROP CONSTRAINT "tcountryli_pkey";

ALTER TABLE "public"."tnim2_commtrack" DROP CONSTRAINT "tnim2_commtrack_pkey";

ALTER TABLE "public"."nid_referral_transaction" DROP CONSTRAINT "nid_referral_transaction_pkey";

ALTER TABLE "public"."nid_api_token" DROP CONSTRAINT "nid_api_token_pkey";

ALTER TABLE "public"."trislinqtransaction" DROP CONSTRAINT "trislinqtransaction_pkey";

ALTER TABLE "public"."thcotransaction" DROP CONSTRAINT "thcotransaction_pkey";

ALTER TABLE "public"."tdefendantstatusli" DROP CONSTRAINT "tdefendantstatusli_pkey";

ALTER TABLE "public"."payerexceptionlist" DROP CONSTRAINT "payerexceptionlist_pkey";

ALTER TABLE "public"."tcompanymaster" DROP CONSTRAINT "tcompanymaster_pkey";

ALTER TABLE "public"."tnim_authorization" DROP CONSTRAINT "tnim_authorization_pkey";

ALTER TABLE "public"."tprivileger" DROP CONSTRAINT "tprivileger_pkey";

ALTER TABLE "public"."tphysicianformlu" DROP CONSTRAINT "tphysicianformlu_pkey";

ALTER TABLE "public"."tadminpracticelu" DROP CONSTRAINT "tadminpracticelu_pkey";

ALTER TABLE "public"."tsuppinterplan" DROP CONSTRAINT "tsuppinterplan_pkey";

ALTER TABLE "public"."tprivilegequestionli" DROP CONSTRAINT "tprivilegequestionli_pkey";

ALTER TABLE "public"."tnim3_service" DROP CONSTRAINT "tnim3_service_pkey";

ALTER TABLE "public"."tboardnameli" DROP CONSTRAINT "tboardnameli_pkey";

ALTER TABLE "public"."tadminmaster" DROP CONSTRAINT "tadminmaster_pkey";

ALTER TABLE "public"."tcasestatusli" DROP CONSTRAINT "tcasestatusli_pkey";

ALTER TABLE "public"."tcoveragetypeli" DROP CONSTRAINT "tcoveragetypeli_pkey";

ALTER TABLE "public"."tnim3_feescheduleref" DROP CONSTRAINT "tnim3_feescheduleref_pkey";

ALTER TABLE "public"."tnim_icmaster" DROP CONSTRAINT "tnim_icmaster_pkey";

ALTER TABLE "public"."tnim2_salescontactsalesprospectlu" DROP CONSTRAINT "tnim2_salescontactsalesprospectlu_pkey";

ALTER TABLE "public"."tnim_appointmentlu" DROP CONSTRAINT "tnim_appointmentlu_pkey";

ALTER TABLE "public"."tlicensetypeli" DROP CONSTRAINT "tlicensetypeli_pkey";

ALTER TABLE "public"."tnim3_payermaster" DROP CONSTRAINT "tnim3_payermaster_pkey";

ALTER TABLE "public"."tservicestatusli" DROP CONSTRAINT "tservicestatusli_pkey";

ALTER TABLE "public"."tnim2_cptgroup" DROP CONSTRAINT "tnim2_cptgroup_pkey";

ALTER TABLE "public"."tencountertypeli" DROP CONSTRAINT "tencountertypeli_pkey";

ALTER TABLE "public"."teventmaster" DROP CONSTRAINT "teventmaster_pkey";

ALTER TABLE "public"."tnim2_icmaster" DROP CONSTRAINT "tnim2_icmaster_pkey";

ALTER TABLE "public"."tcompanyadminlu" DROP CONSTRAINT "tcompanyadminlu_pkey";

ALTER TABLE "public"."tdisclosurecategorytypeli" DROP CONSTRAINT "tdisclosurecategorytypeli_pkey";

ALTER TABLE "public"."tuseraccesstypeli" DROP CONSTRAINT "tuseraccesstypeli_pkey";

ALTER TABLE "public"."faxreceive" DROP CONSTRAINT "faxreceive_pkey";

ALTER TABLE "public"."tadditionalinformation" DROP CONSTRAINT "tadditionalinformation_pkey";

ALTER TABLE "public"."tcourtesyli" DROP CONSTRAINT "tcourtesyli_pkey";

ALTER TABLE "public"."tgenderli" DROP CONSTRAINT "tgenderli_pkey";

ALTER TABLE "public"."tysonbilling" DROP CONSTRAINT "tysonbilling_pkey";

ALTER TABLE "public"."tnim3_patientaccount" DROP CONSTRAINT "tnim3_patientaccount_pkey";

ALTER TABLE "public"."treadmaster" DROP CONSTRAINT "treadmaster_pkey";

ALTER TABLE "public"."tnim2_cptgrouplist" DROP CONSTRAINT "tnim2_cptgrouplist_pkey";

ALTER TABLE "public"."tsupporttypeli" DROP CONSTRAINT "tsupporttypeli_pkey";

ALTER TABLE "public"."tformdisclosurequestionlu" DROP CONSTRAINT "tformdisclosurequestionlu_pkey";

ALTER TABLE "public"."zip_code_old1" DROP CONSTRAINT "zip_code_pkey";

ALTER TABLE "public"."tcontinuingeducation" DROP CONSTRAINT "tcontinuingeducation_pkey";

ALTER TABLE "public"."tnim2_authorization" DROP CONSTRAINT "tnim2_authorization_pkey";

ALTER TABLE "public"."tsalestransaction" DROP CONSTRAINT "tsalestransaction_pkey";

ALTER TABLE "public"."tcompanyhcolu" DROP CONSTRAINT "tcompanyhcolu_pkey";

ALTER TABLE "public"."thcoeventattesttransaction" DROP CONSTRAINT "thcoeventattesttransaction_pkey";

ALTER TABLE "public"."tnim3_referral" DROP CONSTRAINT "tnim3_referral_pkey";

ALTER TABLE "public"."tresource" DROP CONSTRAINT "tresource_pkey";

ALTER TABLE "public"."tnim2_caseaccount" DROP CONSTRAINT "tnim2_caseaccount_pkey";

ALTER TABLE "public"."taudit" DROP CONSTRAINT "taudit_pkey";

ALTER TABLE "public"."tlicenseregistration" DROP CONSTRAINT "tlicenseregistration_pkey";

ALTER TABLE "public"."nidvote" DROP CONSTRAINT "nidvote_pkey";

ALTER TABLE "public"."tnim3_documentblob" DROP CONSTRAINT "tnim3_documentblob_pkey";

ALTER TABLE "public"."tservicebillingtransactiontypeli" DROP CONSTRAINT "tservicebillingtransactiontypeli_pkey";

ALTER TABLE "public"."tupdateitemcategoryli" DROP CONSTRAINT "tupdateitemcategoryli_pkey";

ALTER TABLE "public"."tnim3_encounter" DROP CONSTRAINT "tnim3_encounter_pkey";

ALTER TABLE "public"."tcommunitytopic" DROP CONSTRAINT "tcommunitytopic_pkey";

ALTER TABLE "public"."tprofessionalassociationsli" DROP CONSTRAINT "tprofessionalassociationsli_pkey";

ALTER TABLE "public"."tmessage" DROP CONSTRAINT "tmessage_pkey";

ALTER TABLE "public"."tnim3_eligibilityreference" DROP CONSTRAINT "tnim3_eligibilityreference_pkey";

ALTER TABLE "public"."tpracticemaster" DROP CONSTRAINT "tpracticemaster_pkey";

ALTER TABLE "public"."tphysicianmaster" DROP CONSTRAINT "tphysicianmaster_pkey";

ALTER TABLE "public"."tcptwizard" DROP CONSTRAINT "tcptwizard_pkey";

ALTER TABLE "public"."tsecuritygroupmaster" DROP CONSTRAINT "tsecuritygroupmaster_pkey";

ALTER TABLE "public"."tnim3_employeraccount" DROP CONSTRAINT "tnim3_employeraccount_pkey";

ALTER TABLE "public"."tprofessionalmisconduct" DROP CONSTRAINT "tprofessionalmisconduct_pkey";

ALTER TABLE "public"."tcptli" DROP CONSTRAINT "tcptli_pkey";

ALTER TABLE "public"."tinsuranceli" DROP CONSTRAINT "tinsuranceli_pkey";

ALTER TABLE "public"."tworkhistory" DROP CONSTRAINT "tworkhistory_pkey";

ALTER TABLE "public"."tspecialtyli" DROP CONSTRAINT "tspecialtyli_pkey";

ALTER TABLE "public"."tappointmentstatusli" DROP CONSTRAINT "tappointmentstatusli_pkey";

DROP INDEX "public"."tcompanysecuritygrouplu_pkey";

DROP INDEX "public"."thcouseraccountlu_pkey";

DROP INDEX "public"."tnim3_patient_zip";

DROP INDEX "public"."nid_api_token_copy_pkey1";

DROP INDEX "public"."tadminmaster_pkey";

DROP INDEX "public"."tnim3_feeschedule_pkey";

DROP INDEX "public"."tnim3_referralintake_pkey";

DROP INDEX "public"."tnim3_referral_pkey";

DROP INDEX "public"."practice_zip";

DROP INDEX "public"."tnim2_icmaster_pkey";

DROP INDEX "public"."tnim3_service_pkey";

DROP INDEX "public"."tdruglist_pkey";

DROP INDEX "public"."treadstatusli_pkey";

DROP INDEX "public"."tnim2_salesprospect_pkey";

DROP INDEX "public"."vmq4_services_index";

DROP INDEX "public"."tencounterstatusli_pkey";

DROP INDEX "public"."tuseraccount_pkey";

DROP INDEX "public"."tspecialtystatusli_pkey";

DROP INDEX "public"."tphysiciancategoryli_pkey";

DROP INDEX "public"."tnim3_referral_case";

DROP INDEX "public"."tnim3_netdevdatatable_pkey";

DROP INDEX "public"."tattestr_pkey";

DROP INDEX "public"."nim3_appointment_providerid_istatus_appointment_time";

DROP INDEX "public"."treadmaster_pkey";

DROP INDEX "public"."tnim3_servicebillingtransaction_pkey";

DROP INDEX "public"."talliedhealthprofessional_pkey";

DROP INDEX "public"."tsecuritygroupmaster_pkey";

DROP INDEX "public"."tprofessionalmisconduct_pkey";

DROP INDEX "public"."tprofessionalliability_pkey";

DROP INDEX "public"."tsalutationli_pkey";

DROP INDEX "public"."tfacilityaffiliation_pkey";

DROP INDEX "public"."taudit_copy_pkey";

DROP INDEX "public"."tupdateitem_pkey";

DROP INDEX "public"."tprivileger_pkey";

DROP INDEX "public"."tpracticeactivity_pkey";

DROP INDEX "public"."tnim3_jobtype_pkey";

DROP INDEX "public"."fce_referral2_id";

DROP INDEX "public"."terrorlog_pkey";

DROP INDEX "public"."tnim3_netdevdatatablesupport_pkey";

DROP INDEX "public"."tcompanyadminlu_pkey";

DROP INDEX "public"."trislinqtransaction_pkey";

DROP INDEX "public"."ttimetrack_rcodeli_pkey";

DROP INDEX "public"."tcompanyuseraccountlu_pkey";

DROP INDEX "public"."talertdatetrack_pkey";

DROP INDEX "public"."tnim3_case_payer";

DROP INDEX "public"."tphysicianmaster_pkey";

DROP INDEX "public"."tgenderli_pkey";

DROP INDEX "public"."tnim3_employeraccount_pkey";

DROP INDEX "public"."practice_zip_copy";

DROP INDEX "public"."tnim2_salestransaction_pkey";

DROP INDEX "public"."tencountertypeli_pkey";

DROP INDEX "public"."tnim3_caseaccountuseraccountlu_pkey";

DROP INDEX "public"."thcomaster_pkey";

DROP INDEX "public"."tnim2_authorization_pkey";

DROP INDEX "public"."tothercertification_pkey";

DROP INDEX "public"."tuserstatusli_pkey";

DROP INDEX "public"."tadditionalinformation_pkey";

DROP INDEX "public"."tcompanymaster_pkey";

DROP INDEX "public"."tnim3_patient_lastname";

DROP INDEX "public"."tnim2_salescontactsalesprospectlu_pkey";

DROP INDEX "public"."tpeerreference_pkey";

DROP INDEX "public"."tnim3_case_adjusterid";

DROP INDEX "public"."thcocontractstatusli_pkey";

DROP INDEX "public"."thcoprivilegequestionlu_pkey";

DROP INDEX "public"."tnim3_patient_firstname";

DROP INDEX "public"."tboardcertification_pkey";

DROP INDEX "public"."tsupporttypeli_pkey";

DROP INDEX "public"."tnim3_eligibilityreference_pkey";

DROP INDEX "public"."tcptwizard_pkey";

DROP INDEX "public"."tphysicianuseraccountlu_pkey";

DROP INDEX "public"."tstateli_pkey";

DROP INDEX "public"."zip_code_pkey";

DROP INDEX "public"."tadminphysicianlu_pkey";

DROP INDEX "public"."tspecialtyli_pkey";

DROP INDEX "public"."atpa_compare_pkey";

DROP INDEX "public"."payerexceptionlist_pkey";

DROP INDEX "public"."thospitalli_pkey";

DROP INDEX "public"."practice_zip_contractstatus";

DROP INDEX "public"."tnim2_salescontact_pkey";

DROP INDEX "public"."tformdisclosurequestionlu_pkey";

DROP INDEX "public"."tcommtrackalertstatuscodeli_pkey";

DROP INDEX "public"."tmodalitytypeli_pkey";

DROP INDEX "public"."tpracticecontractstatusli_pkey";

DROP INDEX "public"."tcasestatusli_pkey";

DROP INDEX "public"."tupdateitemcategoryli_pkey";

DROP INDEX "public"."tnim3_billingentity_pkey";

DROP INDEX "public"."thcophysicianstanding_pkey";

DROP INDEX "public"."tworkhistorytypeli_pkey";

DROP INDEX "public"."tplcli_pkey";

DROP INDEX "public"."thcodepartmentli_pkey";

DROP INDEX "public"."tnim2_payermaster_pkey";

DROP INDEX "public"."tgroupsecurity_pkey";

DROP INDEX "public"."tnim3_schedulingtransaction_pkey";

DROP INDEX "public"."tnim3_directreferralsource_pkey";

DROP INDEX "public"."tmri_modelli_pkey";

DROP INDEX "public"."tcoveringphysicians_pkey";

DROP INDEX "public"."tcaseaccountstatusli_pkey";

DROP INDEX "public"."tccardtransaction_pkey";

DROP INDEX "public"."tnim2_billingentity_pkey";

DROP INDEX "public"."tnim2_caseaccount_pkey";

DROP INDEX "public"."taudit_pkey";

DROP INDEX "public"."nidvote_pkey";

DROP INDEX "public"."thcophysiciantransaction_pkey";

DROP INDEX "public"."tnim3_directpatientcard_pkey";

DROP INDEX "public"."thcoprivilegecategorytypelu_pkey";

DROP INDEX "public"."tyesnoli_pkey";

DROP INDEX "public"."tpracticetypeli_pkey";

DROP INDEX "public"."ttemp_pkey";

DROP INDEX "public"."tmalpractice_pkey";

DROP INDEX "public"."tnim3_salesportal_pkey";

DROP INDEX "public"."tnim_authorization_pkey";

DROP INDEX "public"."practice_zip_contractstatus_copy";

DROP INDEX "public"."tsuppuecn_pkey";

DROP INDEX "public"."thcophysicianlu_pkey";

DROP INDEX "public"."treadphysicianstatusli_pkey";

DROP INDEX "public"."tinsuranceli_pkey";

DROP INDEX "public"."tschoolli_pkey";

DROP INDEX "public"."tadminpracticelu_pkey";

DROP INDEX "public"."tphdbhospitalli_pkey";

DROP INDEX "public"."temailtransaction_pkey";

DROP INDEX "public"."tnim3_appointment_pkey";

DROP INDEX "public"."tgroupsecurityitemsli_pkey";

DROP INDEX "public"."tnim3_payermaster_pkey";

DROP INDEX "public"."tuseraccesstypeli_pkey";

DROP INDEX "public"."tworkhistory_pkey";

DROP INDEX "public"."teventmaster_pkey";

DROP INDEX "public"."tcallschedule_pkey";

DROP INDEX "public"."tprofessionalsociety_pkey";

DROP INDEX "public"."tlicenseboardli_pkey";

DROP INDEX "public"."tbillingaccount_pkey";

DROP INDEX "public"."tuaalertsli_pkey";

DROP INDEX "public"."tprivilegequestionli_pkey";

DROP INDEX "public"."tuseraccount_copy_pkey";

DROP INDEX "public"."nidnamecontest_pkey";

DROP INDEX "public"."tcourtesyli_pkey";

DROP INDEX "public"."tpromocodeli_pkey";

DROP INDEX "public"."nid_referral_transaction_pkey";

DROP INDEX "public"."tprofessionaleducation_pkey";

DROP INDEX "public"."tcoveragetypeli_pkey";

DROP INDEX "public"."tcommunitytopic_pkey";

DROP INDEX "public"."tlicensetypeli_pkey";

DROP INDEX "public"."tnim2_appointment_pkey";

DROP INDEX "public"."nid_api_token_pkey";

DROP INDEX "public"."tnim3_documentblob_pkey";

DROP INDEX "public"."tcvohcolu_pkey";

DROP INDEX "public"."tnim3_payermaster_copy1_pkey";

DROP INDEX "public"."tnim_report_pkey";

DROP INDEX "public"."tappointmenttypeli_pkey";

DROP INDEX "public"."thcodivisionli_pkey";

DROP INDEX "public"."tnim2_document_pkey";

DROP INDEX "public"."tsuppinterplan_pkey";

DROP INDEX "public"."tcontinuingeducation_pkey";

DROP INDEX "public"."tnim3_cptgroup_pkey";

DROP INDEX "public"."tnim2_feescheduleref_pkey";

DROP INDEX "public"."thcotransaction_pkey";

DROP INDEX "public"."tnim2_cptgroup_pkey";

DROP INDEX "public"."tprivilegecategorytypeli_pkey";

DROP INDEX "public"."tnim_icmaster_pkey";

DROP INDEX "public"."tdisclosurequestionli_pkey";

DROP INDEX "public"."tnim_appointmentlu_pkey";

DROP INDEX "public"."thcodistrictli_pkey";

DROP INDEX "public"."tcommunityforum_pkey";

DROP INDEX "public"."tnim3_userpracticelu_pkey";

DROP INDEX "public"."tnim3_nidpromo_pkey";

DROP INDEX "public"."tresourcetypeli_pkey";

DROP INDEX "public"."tactivityreference_pkey";

DROP INDEX "public"."tnim3_case_caseclaimnumber";

DROP INDEX "public"."tnid_widgettrack_pkey";

DROP INDEX "public"."nid_api_token_copy_pkey";

DROP INDEX "public"."tnim2_caseaccountuseraccountlu_pkey";

DROP INDEX "public"."nim3_modality_practiceid";

DROP INDEX "public"."tmessage_pkey";

DROP INDEX "public"."tactivityfilereference_pkey";

DROP INDEX "public"."faxreceive_pkey";

DROP INDEX "public"."bunchcare_eligibility_pkey";

DROP INDEX "public"."tnim3_patientaccount_pkey";

DROP INDEX "public"."tsalesmanmaster_pkey";

DROP INDEX "public"."tdefendantstatusli_pkey";

DROP INDEX "public"."tscheduledappointmentstatusli_pkey";

DROP INDEX "public"."tdocumentmanagement_pkey";

DROP INDEX "public"."teventeventlu_pkey";

DROP INDEX "public"."thcodisclosurequestionlu_pkey";

DROP INDEX "public"."tcountryli_pkey";

DROP INDEX "public"."tformli_pkey";

DROP INDEX "public"."tprofessionalassociationsli_pkey";

DROP INDEX "public"."texperiencetypeli_pkey";

DROP INDEX "public"."tphysicianformlu_pkey";

DROP INDEX "public"."tdegreetypeli_pkey";

DROP INDEX "public"."tupdateitemstatusli_pkey";

DROP INDEX "public"."thcoeventattesttransaction_pkey";

DROP INDEX "public"."tdrugmedicaredata_pkey";

DROP INDEX "public"."tmanagedcareplan_pkey";

DROP INDEX "public"."teventchild_pkey";

DROP INDEX "public"."tboardnameli_pkey";

DROP INDEX "public"."tnim_patientaccount_pkey";

DROP INDEX "public"."tstandardaccesstypeli_pkey";

DROP INDEX "public"."tnim3_caseaccount_pkey";

DROP INDEX "public"."tadminpracticerelationshiptypeli_pkey";

DROP INDEX "public"."tnim3_encounter_referral";

DROP INDEX "public"."tfieldsecurity_pkey";

DROP INDEX "public"."zipcode_county";

DROP INDEX "public"."tcptli_pkey";

DROP INDEX "public"."tphysicianpracticelu_pkey";

DROP INDEX "public"."tpracticemaster_pkey";

DROP INDEX "public"."tnim3_encounter_pkey";

DROP INDEX "public"."tnim3_service_encounter";

DROP INDEX "public"."tnim3_document_pkey";

DROP INDEX "public"."tsalesaccount_pkey";

DROP INDEX "public"."tsalestransaction_pkey";

DROP INDEX "public"."tdisclosurecategorytypeli_pkey";

DROP INDEX "public"."tsalesstatusli_pkey";

DROP INDEX "public"."ttransaction_pkey";

DROP INDEX "public"."tnim3_weblinktracker_pkey";

DROP INDEX "public"."tnim3_commtrack_pkey";

DROP INDEX "public"."tvoidleakreasonli_pkey";

DROP INDEX "public"."tnim3_feescheduleref_pkey";

DROP INDEX "public"."tnim2_commtrack_pkey";

DROP INDEX "public"."tappointmentstatusli_pkey";

DROP INDEX "public"."texperience_pkey";

DROP INDEX "public"."tnim2_feeschedule_pkey";

DROP INDEX "public"."tnim2_cptgrouplist_pkey";

DROP INDEX "public"."tnim_payermaster_pkey";

DROP INDEX "public"."tnim3_modality_pkey";

DROP INDEX "public"."tapprovaltypeli_pkey";

DROP INDEX "public"."tnim3_cptgrouplist_pkey";

DROP INDEX "public"."tpracticemaster_copy_pkey";

DROP INDEX "public"."tnim3_netdevcommtrack_pkey";

DROP INDEX "public"."tbillingtransaction_pkey";

DROP INDEX "public"."thcophysicianstatusli_pkey";

DROP INDEX "public"."tmcmc_retros_pkey";

DROP INDEX "public"."tdocumenttypeli_pkey";

DROP INDEX "public"."tnpdbfoltypeli_pkey";

DROP INDEX "public"."tnim3_commreference_pkey";

DROP INDEX "public"."zipcode_zip";

DROP INDEX "public"."tphysicianeventlu_pkey";

DROP INDEX "public"."tsupportbase_pkey";

DROP INDEX "public"."tlicenseregistration_pkey";

DROP INDEX "public"."tct_modelli_pkey";

DROP INDEX "public"."tactivity_pkey";

DROP INDEX "public"."treadphysicianlu_pkey";

DROP INDEX "public"."tresource_pkey";

DROP INDEX "public"."tadminpracticelu_copy_pkey";

DROP INDEX "public"."tservicebillingtransactiontypeli_pkey";

DROP INDEX "public"."tcvomaster_pkey";

DROP INDEX "public"."cid";

DROP INDEX "public"."tnim3_case_patientaccountid";

DROP INDEX "public"."tservicestatusli_pkey";

DROP INDEX "public"."tcompanyhcolu_pkey";

DROP INDEX "public"."tysonbilling_pkey";

DROP INDEX "public"."tcommtracktypeli_pkey";

DROP INDEX "public"."tnim3_patient_payerid";

DROP INDEX "public"."tsalesmansaleslu_pkey";

DROP INDEX "public"."tsupppractoptometry_pkey";

DROP VIEW "public"."vMQ_Services-NoVoid";

DROP VIEW "public"."testview";

DROP VIEW "public"."jan_sp";

DROP VIEW "public"."getRawPowerBi";

DROP VIEW "public"."Active_NOW_Terminated_Practices";

DROP VIEW "public"."ttop40";

DROP VIEW "public"."needs rx review";

DROP VIEW "public"."vNIM_Daily_Division";

DROP VIEW "public"."vMQ_B2A";

DROP VIEW "public"."incomplete data";

DROP VIEW "public"."vMQ5_Services_Transactions";

DROP VIEW "public"."vMQ_Practices_TIN_List";

DROP VIEW "public"."vSummary of Referrals6";

DROP VIEW "public"."vMQ_Services";

DROP VIEW "public"."vMQ2_Practice_Services";

DROP VIEW "public"."missing patient availability";

DROP VIEW "public"."getCustomFGA";

DROP VIEW "public"."missing patient homephone";

DROP VIEW "public"."monthlyschedulernumbers";

DROP VIEW "public"."report review";

DROP VIEW "public"."vEmail_Report1";

DROP VIEW "public"."TysonReadyToBill";

DROP VIEW "public"."vMQ4_Encounter_Void_BP";

DROP VIEW "public"."n4_worklist";

DROP VIEW "public"."vRP_Sch_CaseWorkload";

DROP VIEW "public"."all_payers";

DROP VIEW "public"."test";

DROP VIEW "public"."vMQ4_Encounter_NoVoid_BP";

DROP VIEW "public"."vMQ4_Services";

DROP VIEW "public"."SchedulerStats";

DROP VIEW "public"."vBilling_Report";

DROP VIEW "public"."vND_Overview";

DROP VIEW "public"."mQ5";

DROP VIEW "public"."vRP_Sch_EncounterAnalysis_Missing_TT_RRec";

DROP VIEW "public"."feb_sp";

DROP VIEW "public"."tEmailT_Errors";

DROP VIEW "public"."vMQ_Encounters-NoVoid";

DROP VIEW "public"."vCaseDetails_1";

DROP VIEW "public"."Active_New_Practices";

DROP VIEW "public"."getBusinessDataSum";

DROP VIEW "public"."tTop40_java";

DROP VIEW "public"."vMQ_Services2-NoVoid";

DROP VIEW "public"."vMQ4_Filter_Service_BP";

DROP VIEW "public"."util_report";

DROP VIEW "public"."vSummary of Referrals7 by Patient Name";

DROP VIEW "public"."provider_list";

DROP VIEW "public"."nim_network";

DROP VIEW "public"."getPowerBiResults";

DROP VIEW "public"."vRP_NIM-Daily";

DROP VIEW "public"."billing_report_no_cs_with_adjustments_pp";

DROP VIEW "public"."marty_query";

DROP VIEW "public"."unpaid_or_underpaid_cliams_by_adjuster";

DROP VIEW "public"."vMQ4_Services_NoVoid_BP";

DROP VIEW "public"."vRP_ByPayerByDayByMod";

DROP VIEW "public"."tnim3_dualprovider_v_payer";

DROP VIEW "public"."vActivity All Payers April 2010";

DROP VIEW "public"."nim_monthly_numbers";

DROP VIEW "public"."vActivity View All Payers March 2010";

DROP VIEW "public"."next action alerts";

DROP VIEW "public"."vSummary of Referrals7 by Create Date";

DROP VIEW "public"."ReferralByContact";

DROP VIEW "public"."vRP_Sch_EncounterAnalysis";

DROP VIEW "public"."rising_HCFA_View";

DROP VIEW "public"."vCaseCount_AdjusterID";

DROP VIEW "public"."abcd";

DROP VIEW "public"."vCaseDetails_2";

DROP VIEW "public"."ActivePractices_In_Network";

DROP VIEW "public"."vMQ_NIMCRM_CountByUser";

DROP VIEW "public"."vRP_Total_ByPayer_ByMod";

DROP VIEW "public"."getPbi";

DROP VIEW "public"."vMQ2_Practices";

DROP VIEW "public"."missing rx";

DROP VIEW "public"."vEmail_Pending";

DROP VIEW "public"."vMQ4_Services_Void_BP";

DROP VIEW "public"."vMQ_Practices";

DROP VIEW "public"."vRP_Total_ByDrPayer";

DROP VIEW "public"."amerisys_performance";

DROP VIEW "public"."vMQ4_EncounterService_PayoutMax_Date";

DROP VIEW "public"."ota_at_a_glance";

DROP VIEW "public"."ready to schedule";

DROP VIEW "public"."vMQ_Cases_NoVoid";

DROP VIEW "public"."vMQ4_NID_Parter_Payouts";

DROP VIEW "public"."all_active_needs_to_be_scheduled";

DROP VIEW "public"."vLinkedUsersCount_CaseAccount";

DROP VIEW "public"."dailyNumbers";

DROP VIEW "public"."vMQ4_Encounter_NoVoid_BP_Plus_Checks";

DROP VIEW "public"."vMQ3_Practice_Services";

DROP VIEW "public"."vMQ4_Services_with_FS";

DROP VIEW "public"."monthYearSideView";

DROP VIEW "public"."vMQ_Practice_Services";

DROP VIEW "public"."referral not approved";

DROP VIEW "public"."vMQ4_Encounter_NoVoid_BP_InteractionsCount";

DROP VIEW "public"."vRP_Sch_TotalCasesByMonth2010";

DROP VIEW "public"."vMQ_Services2";

DROP VIEW "public"."getBusinessData";

DROP VIEW "public"."vMQ_Encounters";

DROP VIEW "public"."vRP_CaseSummary";

DROP VIEW "public"."no report";

DROP TABLE "public"."tpeerreference";

DROP TABLE "public"."tnim3_feescheduleref";

DROP TABLE "public"."tupdateitem";

DROP TABLE "public"."tspecialtyli";

DROP TABLE "public"."in_fs_feeschedule_nh";

DROP TABLE "public"."tnim_authorization";

DROP TABLE "public"."in_fs_feeschedule_wa";

DROP TABLE "public"."tuseraccesstypeli";

DROP TABLE "public"."faxreceive";

DROP TABLE "public"."tsuppuecn";

DROP TABLE "public"."tgenderli";

DROP TABLE "public"."bunchcare_eligibility";

DROP TABLE "public"."in_fs_feeschedule_ri";

DROP TABLE "public"."in_fs_feeschedule_or";

DROP TABLE "public"."cs_audit";

DROP TABLE "public"."fce_referral2";

DROP TABLE "public"."vmq4_services_temp";

DROP TABLE "public"."tnim3_referralregistrationform";

DROP TABLE "public"."tnim3_netdevcommtrack";

DROP TABLE "public"."tnim2_payermaster";

DROP TABLE "public"."tnim3_caseaccountuseraccountlu";

DROP TABLE "public"."in_fs_feeschedule_ne";

DROP TABLE "public"."tnim3_jobtype";

DROP TABLE "public"."SelectMRI_FULL_facilities";

DROP TABLE "public"."tyesnoli";

DROP TABLE "public"."tnim2_salescontactsalesprospectlu";

DROP TABLE "public"."qSelectMRI_IC";

DROP TABLE "public"."tnim2_billingentity";

DROP TABLE "public"."tnim2_cptgroup";

DROP TABLE "public"."in_fs_feeschedule_al";

DROP TABLE "public"."tcompanyuseraccountlu";

DROP TABLE "public"."tsupportbase";

DROP TABLE "public"."thcomaster";

DROP TABLE "public"."tcompanyadminlu";

DROP TABLE "public"."tlicenseregistration";

DROP TABLE "public"."in_fs_feeschedule_nc";

DROP TABLE "public"."tnim3_weblinktracker";

DROP TABLE "public"."tprivileger";

DROP TABLE "public"."tdrugmedicaredata";

DROP TABLE "public"."in_fs_feeschedule_ms";

DROP TABLE "public"."tsalesmanmaster";

DROP TABLE "public"."tnim3_documentblob";

DROP TABLE "public"."tappointmenttypeli";

DROP TABLE "public"."tcptwizard";

DROP TABLE "public"."in_fs_feeschedule_ia";

DROP TABLE "public"."tphdbhospitalli";

DROP TABLE "public"."tmodalitytypeli";

DROP TABLE "public"."GEOCODES";

DROP TABLE "public"."in_fs_feeschedule_mn";

DROP TABLE "public"."export2_temp";

DROP TABLE "public"."nid_api_token";

DROP TABLE "public"."tgroupsecurity";

DROP TABLE "public"."teventmaster";

DROP TABLE "public"."tcallschedule";

DROP TABLE "public"."tuatransaction";

DROP TABLE "public"."tuserstatusli";

DROP TABLE "public"."tnim3_modality";

DROP TABLE "public"."tcptli";

DROP TABLE "public"."rest_api_token";

DROP TABLE "public"."tnim_payermaster";

DROP TABLE "public"."tformdisclosurequestionlu";

DROP TABLE "public"."ttimetrack_rcodeli";

DROP TABLE "public"."thcodisclosurequestionlu";

DROP TABLE "public"."tencountertypeli";

DROP TABLE "public"."tcaseaccountstatusli";

DROP TABLE "public"."tnim2_appointment";

DROP TABLE "public"."in_fs_feeschedule_ak";

DROP TABLE "public"."tservicebillingtransactiontypeli";

DROP TABLE "public"."tcoveragetypeli";

DROP TABLE "public"."teventeventlu";

DROP TABLE "public"."tnim2_salescontact";

DROP TABLE "public"."tnim3_encounter";

DROP TABLE "public"."vtemp";

DROP TABLE "public"."tadminphysicianlu";

DROP TABLE "public"."tccardtransaction";

DROP TABLE "public"."tapprovaltypeli";

DROP TABLE "public"."in_fs_feeschedule_wy";

DROP TABLE "public"."thcouseraccountlu";

DROP TABLE "public"."in_fs_feeschedule_nv";

DROP TABLE "public"."SelectMRI_qPayerMaster_Branches";

DROP TABLE "public"."ticd9li";

DROP TABLE "public"."tcvohcolu";

DROP TABLE "public"."tnim2_salesprospect";

DROP TABLE "public"."in_fs_regioncode";

DROP TABLE "public"."in_fs_feeschedule_mt";

DROP TABLE "public"."thcophysicianlu";

DROP TABLE "public"."tnim3_payermaster_copy";

DROP TABLE "public"."thcodepartmentli";

DROP TABLE "public"."in_fs_feeschedule_az";

DROP TABLE "public"."tworkhistory";

DROP TABLE "public"."tnpdbfoltypeli";

DROP TABLE "public"."in_fs_feeschedule_co";

DROP TABLE "public"."tmessage";

DROP TABLE "public"."tphysicianmaster";

DROP TABLE "public"."tphysicianpracticelu";

DROP TABLE "public"."tphysicianuseraccountlu";

DROP TABLE "public"."in_fs_feeschedule_ut";

DROP TABLE "public"."in_fs_feeschedule_tn";

DROP TABLE "public"."tcoveringphysicians";

DROP TABLE "public"."in_fs_feeschedule_pa";

DROP TABLE "public"."treadmaster";

DROP TABLE "public"."tappointmentstatusli";

DROP TABLE "public"."tprofessionalsociety";

DROP TABLE "public"."tnim2_cptgrouplist";

DROP TABLE "public"."tsecuritygroupmaster";

DROP TABLE "public"."tprofessionaleducation";

DROP TABLE "public"."tnim2_salestransaction";

DROP TABLE "public"."tcourtesyli";

DROP TABLE "public"."in_fs_feeschedule_id";

DROP TABLE "public"."treadphysicianlu";

DROP TABLE "public"."vmq4_services";

DROP TABLE "public"."in_fs_feeschedule_fl";

DROP TABLE "public"."audittemp";

DROP TABLE "public"."tcvomaster";

DROP TABLE "public"."in_fs_feeschedule_oh";

DROP TABLE "public"."tpracticetypeli";

DROP TABLE "public"."tsuppinterplan";

DROP TABLE "public"."tnim3_feeschedule";

DROP TABLE "public"."in_fs_feeschedule_in";

DROP TABLE "public"."tdefendantstatusli";

DROP TABLE "public"."tnim3_referralintake";

DROP TABLE "public"."tnim2_authorization";

DROP TABLE "public"."in_fs_feeschedule_ny";

DROP TABLE "public"."export2";

DROP TABLE "public"."thcoprivilegequestionlu";

DROP TABLE "public"."tcommunityforum";

DROP TABLE "public"."tnim3_commreference";

DROP TABLE "public"."tphysicianeventlu";

DROP TABLE "public"."in_fs_feeschedule_wv";

DROP TABLE "public"."tnim3_cptgroup";

DROP TABLE "public"."in_fs_feeschedule_nj";

DROP TABLE "public"."tnim3_crawford";

DROP TABLE "public"."tnim3_document";

DROP TABLE "public"."tnim_report";

DROP TABLE "public"."tnim3_directpatientcard";

DROP TABLE "public"."in_fs_feeschedule_ok";

DROP TABLE "public"."thcodistrictli";

DROP TABLE "public"."thcodivisionli";

DROP TABLE "public"."texperiencetypeli";

DROP TABLE "public"."tnim2_caseaccountuseraccountlu";

DROP TABLE "public"."talertdatetrack";

DROP TABLE "public"."SelectMRI_qPayerMaster_Clients";

DROP TABLE "public"."tdruglist";

DROP TABLE "public"."tsalesmansaleslu";

DROP TABLE "public"."atpa_compare";

DROP TABLE "public"."tbillingaccount";

DROP TABLE "public"."in_fs_feeschedule_ga";

DROP TABLE "public"."tsupppractoptometry";

DROP TABLE "public"."tnim3_schedulingtransaction";

DROP TABLE "public"."tinsuranceli";

DROP TABLE "public"."in_fs_feeschedule_mo";

DROP TABLE "public"."tencounterstatusli";

DROP TABLE "public"."in_fs_feeschedule_la";

DROP TABLE "public"."in_fs_feeschedule";

DROP TABLE "public"."in_fs_feeschedule_ky";

DROP TABLE "public"."tcommtracktypeli";

DROP TABLE "public"."tresourcetypeli";

DROP TABLE "public"."tphysiciancategoryli";

DROP TABLE "public"."thcoprivilegecategorytypelu";

DROP TABLE "public"."icd9";

DROP TABLE "public"."thospitalli";

DROP TABLE "public"."treadstatusli";

DROP TABLE "public"."tcompanyhcolu";

DROP TABLE "public"."schedulingstatsdata";

DROP TABLE "public"."tnim3_userpracticelu";

DROP TABLE "public"."taudit";

DROP TABLE "public"."tnim3_billingentity";

DROP TABLE "public"."tnim3_nidpromo";

DROP TABLE "public"."in_fs_feeschedule_me";

DROP TABLE "public"."tnim3_directreferralsource";

DROP TABLE "public"."cmi_walmart_rc";

DROP TABLE "public"."thcophysicianstatusli";

DROP TABLE "public"."tmanagedcareplan";

DROP TABLE "public"."in_fs_feeschedule_ca";

DROP TABLE "public"."tnim3_appointment";

DROP TABLE "public"."in_fs_feeschedule_nd";

DROP TABLE "public"."tdocumenttypeli";

DROP TABLE "public"."tmri_modelli";

DROP TABLE "public"."in_fs_feeschedule_va";

DROP TABLE "public"."tsalesstatusli";

DROP TABLE "public"."tICD9_Codes";

DROP TABLE "public"."tnimwwretro";

DROP TABLE "public"."tnim3_patientaccount";

DROP TABLE "public"."qRegionCode_MC_2012";

DROP TABLE "public"."tuseraccount_copy";

DROP TABLE "public"."tprofessionalassociationsli";

DROP TABLE "public"."tnim2_commtrack";

DROP TABLE "public"."tdisclosurequestionli";

DROP TABLE "public"."zip_code";

DROP TABLE "public"."in_fs_regioncode_backup";

DROP TABLE "public"."fce_referral";

DROP TABLE "public"."thcophysicianstanding";

DROP TABLE "public"."tnim2_icmaster";

DROP TABLE "public"."tpromocodeli";

DROP TABLE "public"."tnim_icmaster";

DROP TABLE "public"."in_fs_feeschedule_dc";

DROP TABLE "public"."tattestr";

DROP TABLE "public"."tvoidleakreasonli";

DROP TABLE "public"."nid_referral_transaction";

DROP TABLE "public"."tnim3_employeraccount";

DROP TABLE "public"."CPTGroup";

DROP TABLE "public"."tlicensetypeli";

DROP TABLE "public"."texperience";

DROP TABLE "public"."tnim3_eligibilityreference";

DROP TABLE "public"."tservicestatusli";

DROP TABLE "public"."in_fs_feeschedule_ks";

DROP TABLE "public"."tnim3_commtrack";

DROP TABLE "public"."tprivilegecategorytypeli";

DROP TABLE "public"."tnim_appointmentlu";

DROP TABLE "public"."tadminpracticerelationshiptypeli";

DROP TABLE "public"."thcocontractstatusli";

DROP TABLE "public"."in_fs_feeschedule_sd";

DROP TABLE "public"."tphysicianformlu";

DROP TABLE "public"."ticdli";

DROP TABLE "public"."in_fs_feeschedule_backup";

DROP TABLE "public"."tcommunitytopic";

DROP TABLE "public"."tlicenseboardli";

DROP TABLE "public"."trislinqtransaction";

DROP TABLE "public"."tresource";

DROP TABLE "public"."tnim_patientaccount";

DROP TABLE "public"."tuaalertsli";

DROP TABLE "public"."tpracticeactivity";

DROP TABLE "public"."tspecialtystatusli";

DROP TABLE "public"."nidnamecontest";

DROP TABLE "public"."tnim3_caseaccount";

DROP TABLE "public"."in_fs_feeschedule_sc";

DROP TABLE "public"."qExport_RegionCode";

DROP TABLE "public"."tupdateitemstatusli";

DROP TABLE "public"."nidvote";

DROP TABLE "public"."zip_code_old1";

DROP TABLE "public"."tdocumentmanagement";

DROP TABLE "public"."thcoeventattesttransaction";

DROP TABLE "public"."tnim2_feescheduleref";

DROP TABLE "public"."in_fs_feeschedule_nm";

DROP TABLE "public"."tprofessionalmisconduct";

DROP TABLE "public"."in_fs_feeschedule_hi";

DROP TABLE "public"."ACR_Pivot_Table";

DROP TABLE "public"."tysonbilling";

DROP TABLE "public"."tfacilityaffiliation";

DROP TABLE "public"."in_fs_feeschedule_ct";

DROP TABLE "public"."tschoolli";

DROP TABLE "public"."talliedhealthprofessional";

DROP TABLE "public"."payerexceptionlist";

DROP TABLE "public"."tsalesaccount";

DROP TABLE "public"."tnim3_service";

DROP TABLE "public"."in_fs_feeschedule_de";

DROP TABLE "public"."tupdateitemcategoryli";

DROP TABLE "public"."tprivilegequestionli";

DROP TABLE "public"."tnim3_servicebillingtransaction";

DROP TABLE "public"."tnim3_salesportal";

DROP TABLE "public"."tnim3_payermaster";

DROP TABLE "public"."thcotransaction";

DROP TABLE "public"."tbillingtransaction";

DROP TABLE "public"."tstandardaccesstypeli";

DROP TABLE "public"."tpracticecontractstatusli";

DROP TABLE "public"."tmcmc_retros";

DROP TABLE "public"."tnid_widgettrack";

DROP TABLE "public"."ttemp";

DROP TABLE "public"."tdisclosurecategorytypeli";

DROP TABLE "public"."treadphysicianstatusli";

DROP TABLE "public"."tnim2_feeschedule";

DROP TABLE "public"."tcompanysecuritygrouplu";

DROP TABLE "public"."tscheduledappointmentstatusli";

DROP TABLE "public"."in_fs_feeschedule_il";

DROP TABLE "public"."tnim3_referral";

DROP TABLE "public"."SelectMRI_qUserUnion";

DROP TABLE "public"."tcountryli";

DROP TABLE "public"."tothercertification";

DROP TABLE "public"."in_fs_feeschedule_ma";

DROP TABLE "public"."tdegreetypeli";

DROP TABLE "public"."tcasestatusli";

DROP TABLE "public"."in_fs_feeschedule_ar";

DROP TABLE "public"."tformli";

DROP TABLE "public"."tpracticemaster_copy";

DROP TABLE "public"."terrorlog";

DROP TABLE "public"."in_fs_feeschedule_backup_may15";

DROP TABLE "public"."tsupporttypeli";

DROP TABLE "public"."tsalestransaction";

DROP TABLE "public"."tnim3_netdevdatatable";

DROP TABLE "public"."in_fs_feeschedule_md";

DROP TABLE "public"."tfieldsecurity";

DROP TABLE "public"."tworkhistorytypeli";

DROP TABLE "public"."tstateli";

DROP TABLE "public"."temailtransaction";

DROP TABLE "public"."tcontinuingeducation";

DROP TABLE "public"."tboardcertification";

DROP TABLE "public"."tboardnameli";

DROP TABLE "public"."tadminpracticelu_copy";

DROP TABLE "public"."tmalpractice";

DROP TABLE "public"."tactivity";

DROP TABLE "public"."CPTGroup_BefPT";

DROP TABLE "public"."tplcli";

DROP TABLE "public"."in_fs_feeschedule_mi";

DROP TABLE "public"."tactivityreference";

DROP TABLE "public"."tsalutationli";

DROP TABLE "public"."tnim3_netdevdatatablesupport";

DROP TABLE "public"."tadminmaster";

DROP TABLE "public"."tprofessionalliability";

DROP TABLE "public"."ttransaction";

DROP TABLE "public"."teventchild";

DROP TABLE "public"."tct_modelli";

DROP TABLE "public"."in_fs_feeschedule_wi";

DROP TABLE "public"."in_fs_feeschedule_tx";

DROP TABLE "public"."tcompanymaster";

DROP TABLE "public"."tcommtrackalertstatuscodeli";

DROP TABLE "public"."tnim3_cptgrouplist";

DROP TABLE "public"."tactivityfilereference";

DROP TABLE "public"."thcophysiciantransaction";

DROP TABLE "public"."tadditionalinformation";

DROP TABLE "public"."in_fs_feeschedule_vt";

DROP TABLE "public"."tnim2_caseaccount";

DROP TABLE "public"."tuseraccount";

DROP TABLE "public"."tnim2_document";

DROP TABLE "public"."qMedicare_Fee_2011_RC";

DROP TABLE "public"."tpracticemaster";

DROP TABLE "public"."tgroupsecurityitemsli";

DROP TABLE "public"."tadminpracticelu";

CREATE TABLE "public"."tpeerreference" (
		"referenceid" SERIAL DEFAULT nextval('tpeerreference_referenceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"salutation" INT4 DEFAULT 0,
		"firstname" VARCHAR(50) DEFAULT ''::character varying,
		"lastname" VARCHAR(50) DEFAULT ''::character varying,
		"specialty" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"yearsassociated" VARCHAR(50) DEFAULT ''::character varying,
		"doculinkid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"title" VARCHAR(50) DEFAULT ''::character varying,
		"hospitalaffiliation" VARCHAR(100) DEFAULT ''::character varying,
		"hospitaldepartment" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_feescheduleref" (
		"feeschedulerefid" SERIAL DEFAULT nextval('tnim3_feescheduleref_feeschedulerefid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"refname" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tupdateitem" (
		"updateitemid" SERIAL DEFAULT nextval('tupdateitem_updateitemid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"submissiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"releaseversion" VARCHAR(200) DEFAULT ''::character varying,
		"categoryid" INT4 DEFAULT 0,
		"priorityid" INT4 DEFAULT 0,
		"statusid" INT4 DEFAULT 0,
		"itemtitle" VARCHAR(200) DEFAULT ''::character varying,
		"itemdescription" VARCHAR(15000) DEFAULT ''::character varying,
		"developercomments" VARCHAR(15000) DEFAULT ''::character varying,
		"testercomments" VARCHAR(15000) DEFAULT ''::character varying,
		"datetestedondevelopment" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"datetestedonqa" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"datetestedonproduction" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"requesterinitials" VARCHAR(15000) DEFAULT ''::character varying,
		"remoteip" VARCHAR(100) DEFAULT ''::character varying,
		"howtotestcomments" VARCHAR(15000) DEFAULT ''::character varying,
		"comments" VARCHAR(15000) DEFAULT ''::character varying,
		"plcsecid" INT4 DEFAULT 0,
		"ticketnumber" VARCHAR(50) DEFAULT ''::character varying,
		"assignedtoid" INT4 DEFAULT 0,
		"assignedtoroleid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tspecialtyli" (
		"specialtyid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"specialtylong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_nh" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim_authorization" (
		"authorizationid" SERIAL DEFAULT nextval('tnim_authorization_authorizationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"patientid" INT4 DEFAULT 0,
		"authorizationconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"authorizationname" VARCHAR(100),
		"cpt" VARCHAR(100) DEFAULT ''::character varying,
		"cpttext" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysician" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianid" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(100),
		"cpt4" VARCHAR(10) DEFAULT ''::character varying,
		"cpt1" VARCHAR(10) DEFAULT ''::character varying,
		"cpt2" VARCHAR(10) DEFAULT ''::character varying,
		"cpt3" VARCHAR(10) DEFAULT ''::character varying,
		"cpt5" VARCHAR(200) DEFAULT ''::character varying,
		"adjusterid" INT4 DEFAULT 0,
		"claimnumber" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt1" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt2" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt3" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt4" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianphone" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianfax" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianemail" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanager" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanagerphone" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanagerfax" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanageremail" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanagerid" VARCHAR(100) DEFAULT ''::character varying,
		"rxfilename" VARCHAR(100) DEFAULT ''::character varying,
		"reportfilename" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_wa" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tuseraccesstypeli" (
		"useraccesstypeid" INT4 NOT NULL,
		"useraccesstypelong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."faxreceive" (
		"faxid" INT4 NOT NULL,
		"sentdate" TIMESTAMP,
		"src" VARCHAR(255),
		"dest" VARCHAR(255),
		"status" INT4 DEFAULT 0,
		"filename" VARCHAR(255),
		"log" TEXT(2147483647)
	);

CREATE TABLE "public"."tsuppuecn" (
		"suppuecnid" SERIAL DEFAULT nextval('tsuppuecn_suppuecnid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"stateid" INT4 DEFAULT 0,
		"disclosureanswer1" INT4 DEFAULT 0,
		"disclosureanswer2" INT4 DEFAULT 0,
		"disclosureanswer3" INT4 DEFAULT 0,
		"disclosureanswer4" INT4 DEFAULT 0,
		"disclosureanswer5" INT4 DEFAULT 0,
		"disclosureanswer6" INT4 DEFAULT 0,
		"disclosureanswer7" INT4 DEFAULT 0,
		"disclosureanswer8" INT4 DEFAULT 0,
		"disclosureanswer9" INT4 DEFAULT 0,
		"disclosureanswer10" INT4 DEFAULT 0,
		"disclosureanswer11" INT4 DEFAULT 0,
		"disclosureanswer12" INT4 DEFAULT 0,
		"disclosureanswer13" INT4 DEFAULT 0,
		"disclosureanswer14" INT4 DEFAULT 0,
		"disclosureanswer15" INT4 DEFAULT 0,
		"disclosureanswer16" INT4 DEFAULT 0,
		"disclosureanswer17" INT4 DEFAULT 0,
		"disclosureanswer18" INT4 DEFAULT 0,
		"disclosurecomments" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tgenderli" (
		"genderid" INT4 NOT NULL,
		"gendershort" VARCHAR(10) DEFAULT ''::character varying,
		"genderlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."bunchcare_eligibility" (
		"caseid" VARCHAR(255) NOT NULL,
		"carriercaseid" VARCHAR(255) NOT NULL,
		"patientname" VARCHAR(255),
		"adjustername" VARCHAR(255),
		"ncmname" VARCHAR(255),
		"data" TEXT(2147483647),
		"createdate" TIMESTAMP DEFAULT now()
	);

CREATE TABLE "public"."in_fs_feeschedule_ri" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."in_fs_feeschedule_or" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."cs_audit" (
		"e_id" INT4,
		"status" BOOL,
		"createdate" TIMESTAMP DEFAULT now()
	);

CREATE TABLE "public"."fce_referral2" (
		"id" VARCHAR(50) DEFAULT md5((random())::text) NOT NULL,
		"createdate" TIMESTAMP DEFAULT now() NOT NULL,
		"fce" VARCHAR(20000) DEFAULT ''::character varying,
		"status" INT4 DEFAULT 0
	);

CREATE TABLE "public"."vmq4_services_temp" (
		"CaseID" INT4,
		"CaseCode" VARCHAR(100),
		"CaseClaimNumber" VARCHAR(100),
		"PayerID" INT4,
		"PayerName" VARCHAR(200),
		"Payer_TypeID" INT4,
		"Payer_SalesDivision" VARCHAR(10),
		"Payer_AcquisitionDivision" VARCHAR(10),
		"PayerCity" VARCHAR(100),
		"PayerState" VARCHAR(4),
		"Parent_PayerID" INT4,
		"Parent_PayerName" VARCHAR(200),
		"ParentPayer_TypeID" INT4,
		"ParentPayer_SalesDivision" VARCHAR(10),
		"ParentPayer_AcquisitionDivsion" VARCHAR(10),
		"UA_AssignedTo_UserID" INT4,
		"UA_AssignedTo_UserName" VARCHAR(200),
		"UA_AssignedTo_FirstName" VARCHAR(50),
		"UA_AssignedTo_LastName" VARCHAR(50),
		"UA_AssignedTo_Email" VARCHAR(200),
		"UA_Adjuster_UserID" INT4,
		"UA_Adjuster_FirstName" VARCHAR(50),
		"UA_Adjuster_LastName" VARCHAR(50),
		"UA_Adjuster_Email" VARCHAR(200),
		"UA_NCM_UserID" INT4,
		"UA_NCM_FirstName" VARCHAR(50),
		"UA_NCM_LastName" VARCHAR(50),
		"UA_NCM_Email" VARCHAR(200),
		"UA_ReferralSource_UserID" INT4,
		"UA_ReferralSource_FirstName" VARCHAR(50),
		"UA_ReferralSource_LastName" VARCHAR(50),
		"UA_ReferralSource_Email" VARCHAR(200),
		"EmployerName" VARCHAR(500),
		"PatientFirstName" VARCHAR(100),
		"PatientLastName" VARCHAR(100),
		"DateOfInjury" TIMESTAMP,
		"DateOfInjury_display" TIMESTAMP,
		"DateOfInjury_iMonth" FLOAT8,
		"DateOfInjury_iDay" FLOAT8,
		"DateOfInjury_iDOW" FLOAT8,
		"DateOfInjury_iYear" FLOAT8,
		"PatientDOB" TIMESTAMP,
		"PatientDOB_display" TIMESTAMP,
		"PatientDOB_iMonth" FLOAT8,
		"PatientDOB_iDay" FLOAT8,
		"PatientDOB_iDOW" FLOAT8,
		"PatientDOB_iYear" FLOAT8,
		"PatientAddress1" VARCHAR(200),
		"PatientAddress2" VARCHAR(200),
		"PatientCity" VARCHAR(100),
		"PatientState" VARCHAR(4),
		"PatientHomePhone" VARCHAR(50),
		"PatientCellPhone" VARCHAR(50),
		"PreScreen_IsClaus" INT4,
		"PreScreen_ReqOpenModality" INT4,
		"PreScreen_HasImplants" INT4,
		"PreScreen_HasMetal" INT4,
		"PreScreen_Allergies" INT4,
		"PreScreen_HasRecentSurgery" INT4,
		"PreScreen_PreviousMRIs" INT4,
		"PreScreen_OtherCondition" INT4,
		"PreScreen_IsPregnant" INT4,
		"PreScreen_Height" VARCHAR(10),
		"PreScreen_Weight" INT4,
		"ReferralID" INT4,
		"ReferralStatusID" INT4,
		"Referral_ReceiveDate" TIMESTAMP,
		"Referral_ReceiveDate_display" TIMESTAMP,
		"Referral_ReceiveDate_iMonth" FLOAT8,
		"Referral_ReceiveDate_iDay" FLOAT8,
		"Referral_ReceiveDate_iDOW" FLOAT8,
		"Referral_ReceiveDate_iYear" FLOAT8,
		"Referral_OrderFileID" INT4,
		"Referral_RxFileID" INT4,
		"UA_ReferringDr_UserID" INT4,
		"UA_ReferringDr_FirstName" VARCHAR(50),
		"UA_ReferringDr_LastName" VARCHAR(50),
		"UA_ReferringDr_Email" VARCHAR(200),
		"EncounterID" INT4,
		"Encounter_StatusID" INT4,
		"Encounter_Status" VARCHAR(100),
		"Encounter_Type" VARCHAR(10),
		"Encounter_ScanPass" VARCHAR(100),
		"Encounter_DateOfService" TIMESTAMP,
		"Encounter_DateOfService_display" TIMESTAMP,
		"Encounter_DateOfService_iMonth" FLOAT8,
		"Encounter_DateOfService_iDay" FLOAT8,
		"Encounter_DateOfService_iDOW" FLOAT8,
		"Encounter_DateOfService_iYear" FLOAT8,
		"Encounter_ReportFileID" INT4,
		"Encounter_VoidLeakReason" VARCHAR(10),
		"Encounter_ExportPaymentToQB" TIMESTAMP,
		"Encounter_IsSTAT" INT4,
		"Encounter_IsRetro" INT4,
		"Encounter_ReqFilms" INT4,
		"Encounter_HasBeenRescheduled" INT4,
		"Encounter_ReqAging" INT4,
		"Encounter_IsCourtesy" INT4,
		"Encounter_HCFA_ToPayerFileID" INT4,
		"Encounter_HCFA_FromProvider_FileID" INT4,
		"Encounter_SentTo_Bill_Pay" TIMESTAMP,
		"Encounter_SentTo_Bill_Pay_display" TIMESTAMP,
		"Encounter_SentTo_Bill_Pay_iMonth" FLOAT8,
		"Encounter_SentTo_Bill_Pay_iDay" FLOAT8,
		"Encounter_SentTo_Bill_Pay_iDOW" FLOAT8,
		"Encounter_SentTo_Bill_Pay_iYear" FLOAT8,
		"Encounter_Rec_Bill_Pro" TIMESTAMP,
		"Encounter_Rec_Bill_Pro_display" TIMESTAMP,
		"Encounter_Rec_Bill_Pro_iMonth" FLOAT8,
		"Encounter_Rec_Bill_Pro_iDay" FLOAT8,
		"Encounter_Rec_Bill_Pro_iDOW" FLOAT8,
		"Encounter_Rec_Bill_Pro_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqRec" TIMESTAMP,
		"Encounter_TimeTrack_ReqRec_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqRec_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqRec_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqRec_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqRec_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqCreated" TIMESTAMP,
		"Encounter_TimeTrack_ReqCreated_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqCreated_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqCreated_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqCreated_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqCreated_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqProc" TIMESTAMP,
		"Encounter_TimeTrack_ReqProc_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqProc_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqProc_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqProc_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqProc_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqSched" TIMESTAMP,
		"Encounter_TimeTrack_ReqSched_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqSched_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqSched_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqSched_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqSched_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered" TIMESTAMP,
		"Encounter_TimeTrack_ReqDelivered_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqDelivered_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidIN_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidIN_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidOut_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidOut_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqApproved" TIMESTAMP,
		"Encounter_TimeTrack_ReqApproved_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqApproved_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqApproved_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqApproved_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqApproved_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment" TIMESTAMP,
		"Encounter_TimeTrack_ReqInitialAppointment_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqInitialAppointment_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview" TIMESTAMP,
		"Encounter_TimeTrack_ReqRxReview_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqRxReview_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview" TIMESTAMP,
		"Encounter_TimeTrack_ReqRpReview_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqRpReview_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview_iYear" FLOAT8,
		"AppointmentID" INT4,
		"Encounter_PaidToProvider_CheckCalc" NUMERIC(131089 , 0),
		"Appointment_ProviderID" INT4,
		"Appointment_AppointmentTime" TIMESTAMP,
		"Appointment_AppointmentTime_display" TIMESTAMP,
		"Appointment_AppointmentTime_iMonth" FLOAT8,
		"Appointment_AppointmentTime_iDay" FLOAT8,
		"Appointment_AppointmentTime_iDOW" FLOAT8,
		"Appointment_AppointmentTime_iYear" FLOAT8,
		"Appointment_StatusID" INT4,
		"PracticeID" INT4,
		"Practice_Name" VARCHAR(500),
		"Practice_City" VARCHAR(100),
		"Practice_State" VARCHAR(4),
		"Practice_IsBlueStar" INT4,
		"Practice_PetLinQID" VARCHAR(10),
		"Practice_SelectMRI_ID" INT4,
		"ServiceID" INT4,
		"Service_StatusID" INT4,
		"Service_Status" VARCHAR(10),
		"Service_TypeID" INT4,
		"Service_CPT" VARCHAR(100),
		"Service_CPT_Qty" INT4,
		"Service_CPTModifier" VARCHAR(100),
		"Service_CPTBodyPart" VARCHAR(100),
		"Service_ICD_1" VARCHAR(100),
		"Service_ICD_2" VARCHAR(100),
		"Service_ICD_3" VARCHAR(100),
		"Service_ICD_4" VARCHAR(100),
		"Service_CPTText" VARCHAR(1000),
		"Service_BillAmount" NUMERIC(9 , 2),
		"Service_AllowAmount" NUMERIC(9 , 2),
		"Service_AllowAmountAdjustment" NUMERIC(9 , 2),
		"Service_ReceivedAmount" NUMERIC(9 , 2),
		"Service_PaidOutAmount" NUMERIC(9 , 2),
		"Service_ReceivedAmount_CheckCalc" NUMERIC(131089 , 0),
		"UA_CaseAdmin_FirstName" VARCHAR(50),
		"UA_CaseAdmin_LastName" VARCHAR(50),
		"Service_modality" TEXT(2147483647),
		"Patient_Zip" VARCHAR(50),
		"Practice_Zip" VARCHAR(50),
		"StateFS" FLOAT8,
		"Medicare" FLOAT8,
		"Walmart" FLOAT8,
		"NSP" FLOAT8
	);

CREATE TABLE "public"."tnim3_referralregistrationform" (
		"portalregisterid" INT4,
		"uniquecreatedate" VARCHAR(30),
		"uniquemodifydate" DATE,
		"uniquemodifycomments" VARCHAR(30),
		"statusid" INT4,
		"firstname" VARCHAR(30),
		"lastname" VARCHAR(30),
		"phone" VARCHAR(15),
		"fax" VARCHAR(15),
		"email" VARCHAR(30),
		"messagetext" VARCHAR(500),
		"encounterid" INT4,
		"linktype" VARCHAR(30)
	);

CREATE TABLE "public"."tnim3_netdevcommtrack" (
		"netdevcommtrackid" SERIAL DEFAULT nextval('tnim3_netdevcommtrack_netdevcommtrackid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"accountid" INT4 DEFAULT 0,
		"commtypeid" INT4 DEFAULT 0,
		"commstart" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"commend" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"messagetext" VARCHAR(4000) DEFAULT ''::character varying,
		"messagename" VARCHAR(20) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_payermaster" (
		"payerid" SERIAL DEFAULT nextval('tnim2_payermaster_payerid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"payername" VARCHAR(100) DEFAULT ''::character varying,
		"contactname" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(100) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(100) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"price_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"payertypeid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tnim3_caseaccountuseraccountlu" (
		"lookupid" SERIAL DEFAULT nextval('tnim3_caseaccountuseraccountlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"statuscode" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ne" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_jobtype" (
		"jobtypeid" SERIAL DEFAULT nextval('tnim3_jobtype_jobtypeid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"employerid" INT4 DEFAULT 0,
		"modifiedworkid" INT4 DEFAULT 0,
		"jobtitle" VARCHAR(100) DEFAULT ''::character varying,
		"jobdesc" VARCHAR(100) DEFAULT ''::character varying,
		"joblinkpdf" VARCHAR(100) DEFAULT ''::character varying,
		"joblinkvideo" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"auditnotes" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."SelectMRI_FULL_facilities" (
		"FacilityID" VARCHAR(255),
		"VendorID" VARCHAR(255),
		"Active" VARCHAR(255),
		"FacilityName" VARCHAR(255),
		"Address1" VARCHAR(255),
		"Address2" VARCHAR(255),
		"City" VARCHAR(255),
		"State" VARCHAR(255),
		"ZipCode" VARCHAR(255),
		"Phone" VARCHAR(255),
		"Fax" VARCHAR(255),
		"TaxID" VARCHAR(255),
		"JACHO" VARCHAR(255),
		"ACR" VARCHAR(255),
		"BCRadiologist" VARCHAR(255),
		"MRIConnectionAccredited" VARCHAR(255),
		"FacUnderReview" VARCHAR(255),
		"NumViolations" VARCHAR(255),
		"ScreenOrbits" VARCHAR(255),
		"InjuryAging" VARCHAR(255),
		"ContactName" VARCHAR(255),
		"ContactPhone" VARCHAR(255),
		"ContactPhoneExt" VARCHAR(255),
		"ContactEMail" VARCHAR(255),
		"SoftwareUpgrades" VARCHAR(255),
		"MRIPlain" VARCHAR(255),
		"MRIWContrast" VARCHAR(255),
		"MRIManufacturer" VARCHAR(255),
		"MRIMake" VARCHAR(255),
		"MRIModel" VARCHAR(255),
		"MRITelsa" VARCHAR(255),
		"MRISoftwareUpdates" VARCHAR(255),
		"OMRIPlain" VARCHAR(255),
		"OMRIWContrast" VARCHAR(255),
		"OMRIManufacturer" VARCHAR(255),
		"OMRIMake" VARCHAR(255),
		"OMRIModel" VARCHAR(255),
		"OMRITelsa" VARCHAR(255),
		"OMRISoftwareUpdates" VARCHAR(255),
		"CTPlain" VARCHAR(255),
		"CTWContrast" VARCHAR(255),
		"CTManufacturer" VARCHAR(255),
		"CTMake" VARCHAR(255),
		"CTModel" VARCHAR(255),
		"CTTelsa" VARCHAR(255),
		"CTSoftwareUpdates" VARCHAR(255),
		"MRAngiography" VARCHAR(255),
		"MRAManufacturer" VARCHAR(255),
		"MRAMake" VARCHAR(255),
		"MRAModel" VARCHAR(255),
		"MRASoftwareUpdates" VARCHAR(255),
		"NuclearMedicine" VARCHAR(255),
		"NMManufacturer" VARCHAR(255),
		"NMMake" VARCHAR(255),
		"NMModel" VARCHAR(255),
		"NMSoftwareUpdates" VARCHAR(255),
		"BoneScans" VARCHAR(255),
		"BSManufacturer" VARCHAR(255),
		"BSMake" VARCHAR(255),
		"BSModel" VARCHAR(255),
		"BSSoftwareUpdates" VARCHAR(255),
		"BoneDensitometry" VARCHAR(255),
		"BDManufacturer" VARCHAR(255),
		"BDMake" VARCHAR(255),
		"BDModel" VARCHAR(255),
		"BDSoftwareUpdates" VARCHAR(255),
		"XRay" VARCHAR(255),
		"XRManufacturer" VARCHAR(255),
		"XRMake" VARCHAR(255),
		"XRModel" VARCHAR(255),
		"XRSoftwareUpdates" VARCHAR(255),
		"Ultrasound" VARCHAR(255),
		"USManufacturer" VARCHAR(255),
		"USMake" VARCHAR(255),
		"USModel" VARCHAR(255),
		"USSoftwareUpdates" VARCHAR(255),
		"Fluoroscopy" VARCHAR(255),
		"FLManufacturer" VARCHAR(255),
		"FLMake" VARCHAR(255),
		"FLModel" VARCHAR(255),
		"FLSoftwareUpdates" VARCHAR(255),
		"Angiography" VARCHAR(255),
		"ANGManufacturer" VARCHAR(255),
		"ANGMake" VARCHAR(255),
		"ANGModel" VARCHAR(255),
		"ANGSoftwareUpdates" VARCHAR(255),
		"Arthrogram" VARCHAR(255),
		"ATGManufacturer" VARCHAR(255),
		"ATGMake" VARCHAR(255),
		"ATGModel" VARCHAR(255),
		"ATGSoftwareUpdates" VARCHAR(255),
		"Mammography" VARCHAR(255),
		"MAMManufacturer" VARCHAR(255),
		"MAMMake" VARCHAR(255),
		"MAMModel" VARCHAR(255),
		"MAMSoftwareUpdates" VARCHAR(255),
		"Myelography" VARCHAR(255),
		"MYGManufacturer" VARCHAR(255),
		"MYGMake" VARCHAR(255),
		"MYGModel" VARCHAR(255),
		"MYGSoftwareUpdates" VARCHAR(255),
		"EMGNCV" VARCHAR(255),
		"EMGManufacturer" VARCHAR(255),
		"EMGMake" VARCHAR(255),
		"EMGModel" VARCHAR(255),
		"EMGSoftwareUpdates" VARCHAR(255),
		"IVPGIGU" VARCHAR(255),
		"IVPManufacturer" VARCHAR(255),
		"IVPMake" VARCHAR(255),
		"IVPModel" VARCHAR(255),
		"IVPSoftwareUpdates" VARCHAR(255),
		"DopplerStudies" VARCHAR(255),
		"DOPManufacturer" VARCHAR(255),
		"DOPMake" VARCHAR(255),
		"DOPModel" VARCHAR(255),
		"DOPSoftwareUpdates" VARCHAR(255),
		"PetScan" VARCHAR(255),
		"PETManufacturer" VARCHAR(255),
		"PETMake" VARCHAR(255),
		"PETModel" VARCHAR(255),
		"PETSoftwareUpdates" VARCHAR(255),
		"NuclearCardiology" VARCHAR(255),
		"NCManufacturer" VARCHAR(255),
		"NCMake" VARCHAR(255),
		"NCModel" VARCHAR(255),
		"NCSoftwareUpdates" VARCHAR(255),
		"EchoCardiography" VARCHAR(255),
		"ECManufacturer" VARCHAR(255),
		"ECMake" VARCHAR(255),
		"ECModel" VARCHAR(255),
		"ECSoftwareUpdates" VARCHAR(255),
		"EKG" VARCHAR(255),
		"EKGManufacturer" VARCHAR(255),
		"EKGMake" VARCHAR(255),
		"EKGModel" VARCHAR(255),
		"EKGSoftwareUpdates" VARCHAR(255),
		"Notes" VARCHAR(255),
		"QuickBooksID" VARCHAR(255),
		"AddressValidated" VARCHAR(255),
		"BillingAddress1" VARCHAR(255),
		"BillingAddress2" VARCHAR(255),
		"BillingCity" VARCHAR(255),
		"BillingState" VARCHAR(255),
		"BillingZipCode" VARCHAR(255),
		"BillingPhone" VARCHAR(255),
		"BillingFax" VARCHAR(255),
		"BillingAddressValidated" VARCHAR(255),
		"RatePlanID" VARCHAR(255),
		"Longitude" VARCHAR(255),
		"Latitude" VARCHAR(255)
	);

CREATE TABLE "public"."tyesnoli" (
		"yesnoid" INT4 NOT NULL,
		"ynshort" VARCHAR(10) DEFAULT ''::character varying,
		"ynlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_salescontactsalesprospectlu" (
		"lookupid" SERIAL DEFAULT nextval('tnim2_salescontactsalesprospectlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"reftypeid" INT4 DEFAULT 0,
		"salescontactid" INT4 DEFAULT 0,
		"salesprospectid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"accountnotes" VARCHAR(2000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."qSelectMRI_IC" (
		"SelectMRI_ID" VARCHAR(255),
		"PracticeName" VARCHAR(255),
		"mergeme" VARCHAR(255),
		"officeaddress1" VARCHAR(255),
		"officeaddress2" VARCHAR(255),
		"officecity" VARCHAR(255),
		"officestateid" VARCHAR(255),
		"officezip" VARCHAR(255),
		"officephone" VARCHAR(255),
		"officefederaltaxid" VARCHAR(255),
		"price_mod_mri_wo" VARCHAR(255),
		"price_mod_mri_w" VARCHAR(255),
		"price_mod_mri_wwo" VARCHAR(255),
		"price_mod_ct_wo" VARCHAR(255),
		"price_mod_ct_w" VARCHAR(255),
		"price_mod_ct_wwo" VARCHAR(255),
		"feepercentage" VARCHAR(255),
		"selectmri_notes" VARCHAR(255),
		"contractingstatusid" VARCHAR(255)
	);

CREATE TABLE "public"."tnim2_billingentity" (
		"billingentityid" SERIAL DEFAULT nextval('tnim2_billingentity_billingentityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"entityname" VARCHAR(100) DEFAULT ''::character varying,
		"billtypeid" INT4 DEFAULT 0,
		"billfeeschedulerefid" INT4 DEFAULT 0,
		"billpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"allowtypeid" INT4 DEFAULT 0,
		"allowfeeschedulerefid" INT4 DEFAULT 0,
		"allowpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"overridetypeid" INT4 DEFAULT 0,
		"overridefeeschedulerefid" INT4 DEFAULT 0,
		"overridepercentage" NUMERIC(9 , 2) DEFAULT 0.0
	);

CREATE TABLE "public"."tnim2_cptgroup" (
		"cptgroupid" SERIAL DEFAULT nextval('tnim2_cptgroup_cptgroupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"groupname" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_al" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tcompanyuseraccountlu" (
		"lookupid" SERIAL DEFAULT nextval('tcompanyuseraccountlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"companyid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tsupportbase" (
		"supportbaseid" SERIAL DEFAULT nextval('tsupportbase_supportbaseid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"companyid" INT4 DEFAULT 0,
		"iscomplete" INT4 DEFAULT 0,
		"physicianid" INT4 DEFAULT 0,
		"plcid" INT4 DEFAULT 0,
		"adminid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"contactname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(90) DEFAULT ''::character varying,
		"companyname" VARCHAR(100) DEFAULT ''::character varying,
		"phone" VARCHAR(90) DEFAULT ''::character varying,
		"fax" VARCHAR(90) DEFAULT ''::character varying,
		"address1" VARCHAR(90) DEFAULT ''::character varying,
		"address2" VARCHAR(90) DEFAULT ''::character varying,
		"city" VARCHAR(90) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(90) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"supporttypeid" INT4 DEFAULT 0,
		"subject" VARCHAR(100) DEFAULT ''::character varying,
		"problem" VARCHAR(200) DEFAULT ''::character varying,
		"solution" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcomaster" (
		"hcoid" SERIAL DEFAULT nextval('thcomaster_hcoid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"alertemail" VARCHAR(50) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"contactfirstname" VARCHAR(90) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"contactaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"contactaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"contactcity" VARCHAR(30) DEFAULT ''::character varying,
		"contactstateid" INT4 DEFAULT 0,
		"contactprovince" VARCHAR(100) DEFAULT ''::character varying,
		"contactzip" VARCHAR(50) DEFAULT ''::character varying,
		"contactcountryid" INT4 DEFAULT 0,
		"contactphone" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"masterformid" INT4 DEFAULT 0,
		"hcotypeid" INT4 DEFAULT 0,
		"hcobillingid" INT4 DEFAULT 0,
		"isonprofile" INT4 DEFAULT 0,
		"phdbtemplate1" VARCHAR(200) DEFAULT ''::character varying,
		"phdbtemplate2" VARCHAR(200) DEFAULT ''::character varying,
		"phdbtemplate3" VARCHAR(200) DEFAULT ''::character varying,
		"phdbtemplate4" VARCHAR(200) DEFAULT ''::character varying,
		"phdbtemplate5" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcompanyadminlu" (
		"lookupid" SERIAL DEFAULT nextval('tcompanyadminlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"companyid" INT4 DEFAULT 0,
		"adminid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tlicenseregistration" (
		"licenseregistrationid" SERIAL DEFAULT nextval('tlicenseregistration_licenseregistrationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"iscurrent" INT4 DEFAULT 0,
		"licensetype" INT4 DEFAULT 0,
		"subtype" VARCHAR(90) DEFAULT ''::character varying,
		"npdbfol" INT4 DEFAULT 0,
		"stateid" INT4 DEFAULT 0,
		"ispractice" INT4 DEFAULT 0,
		"licensedocumentnumber" VARCHAR(60) DEFAULT ''::character varying,
		"issuedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"expirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"orgstateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"doculinkid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_nc" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_weblinktracker" (
		"weblinktrackerid" SERIAL DEFAULT nextval('tnim3_weblinktracker_weblinktrackerid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"remoteip" VARCHAR(100) DEFAULT ''::character varying,
		"weblink" VARCHAR(100) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"payerid" INT4,
		"useragent" VARCHAR(2000),
		"userhash" VARCHAR(2000)
	);

CREATE TABLE "public"."tprivileger" (
		"privilegerid" SERIAL DEFAULT nextval('tprivileger_privilegerid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"eventid" INT4 DEFAULT 0,
		"questionid" INT4 DEFAULT 0,
		"requestanswer" INT4 DEFAULT 0,
		"requestdetails" VARCHAR(200) DEFAULT ''::character varying,
		"responseanswer" INT4 DEFAULT 0,
		"responsedetails" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tdrugmedicaredata" (
		"drugmedicaredataid" SERIAL DEFAULT nextval('tdrugmedicaredata_drugmedicaredataid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"drugid" INT4 DEFAULT 0,
		"druglistid" INT4 DEFAULT 0,
		"drugname" VARCHAR(100) DEFAULT ''::character varying,
		"drugtype" VARCHAR(10) DEFAULT ''::character varying,
		"drugsubtype" VARCHAR(10) DEFAULT ''::character varying,
		"ndc11" VARCHAR(50) DEFAULT ''::character varying,
		"dosagetext" VARCHAR(50) DEFAULT ''::character varying,
		"dosagecode" VARCHAR(50) DEFAULT ''::character varying,
		"searchzip" VARCHAR(50) DEFAULT ''::character varying,
		"drugqty" INT4 DEFAULT 0,
		"programcode" VARCHAR(200) DEFAULT ''::character varying,
		"programname" VARCHAR(200) DEFAULT ''::character varying,
		"pharminarea" VARCHAR(50) DEFAULT ''::character varying,
		"pricelow" NUMERIC(9 , 2) DEFAULT 0.0,
		"pricehigh" NUMERIC(9 , 2) DEFAULT 0.0,
		"fee" NUMERIC(9 , 2) DEFAULT 0.0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ms" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tsalesmanmaster" (
		"salesmanid" SERIAL DEFAULT nextval('tsalesmanmaster_salesmanid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"firstname" VARCHAR(50) DEFAULT ''::character varying,
		"lastname" VARCHAR(50) DEFAULT ''::character varying,
		"address1" VARCHAR(100) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(100) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"alertemail" VARCHAR(50) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_documentblob" (
		"documentid" SERIAL DEFAULT nextval('documentid_seq'::regclass) NOT NULL,
		"filename" VARCHAR(200),
		"document" BYTEA(2147483647)
	);

CREATE TABLE "public"."tappointmenttypeli" (
		"appointmenttypeid" INT4 NOT NULL,
		"appointmenttypelong" VARCHAR(50) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcptwizard" (
		"cptwizardid" SERIAL DEFAULT nextval('tcptwizard_cptwizardid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"modality" VARCHAR(100) DEFAULT ''::character varying,
		"special" VARCHAR(100) DEFAULT ''::character varying,
		"contrast" VARCHAR(100) DEFAULT ''::character varying,
		"bodypart" VARCHAR(100) DEFAULT ''::character varying,
		"orientation" VARCHAR(100) DEFAULT ''::character varying,
		"cpt1" VARCHAR(100) DEFAULT ''::character varying,
		"bp1" VARCHAR(100) DEFAULT ''::character varying,
		"cpt2" VARCHAR(100) DEFAULT ''::character varying,
		"bp2" VARCHAR(100) DEFAULT ''::character varying,
		"cpt3" VARCHAR(100) DEFAULT ''::character varying,
		"bp3" VARCHAR(100) DEFAULT ''::character varying,
		"cpt4" VARCHAR(100) DEFAULT ''::character varying,
		"bp4" VARCHAR(100) DEFAULT ''::character varying,
		"cpt5" VARCHAR(100) DEFAULT ''::character varying,
		"bp5" VARCHAR(100) DEFAULT ''::character varying,
		"allownim" INT4,
		"allownid" INT4
	);

CREATE TABLE "public"."in_fs_feeschedule_ia" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tphdbhospitalli" (
		"hospitalid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"status" INT4 DEFAULT 0,
		"name" VARCHAR(200) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"state" VARCHAR(5) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"contactname" VARCHAR(200) DEFAULT ''::character varying,
		"email" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tmodalitytypeli" (
		"modalitytypeid" INT4 NOT NULL,
		"statusshort" VARCHAR(10) DEFAULT ''::character varying,
		"statuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."GEOCODES" (
		"address" VARCHAR(255),
		"lon" VARCHAR(255),
		"lat" VARCHAR(255)
	);

CREATE TABLE "public"."in_fs_feeschedule_mn" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."export2_temp" (
		"ScanPass" VARCHAR(255),
		"EncounterID" INT4,
		"ParentPayer" VARCHAR(255),
		"PayerBranch" VARCHAR(255),
		"PayerID" VARCHAR(255),
		"DateOfService" VARCHAR(255),
		"DateOfService_iMonth" VARCHAR(255),
		"DateOfService_iYear" VARCHAR(255),
		"ClaimNumber" VARCHAR(255),
		"Adjuster_FullName" VARCHAR(255),
		"AdjusterName_BranchName" VARCHAR(255),
		"AdjusterID" VARCHAR(255),
		"EncounterType" VARCHAR(255),
		"IsCourtesy_Status" VARCHAR(255),
		"UniqueCreateDate" TIMESTAMP,
		"ReceiveDate" VARCHAR(255),
		"TT_Scheduled" VARCHAR(255),
		"TA_Scheduled_Days" VARCHAR(255),
		"TA_Scheduled_Hours" VARCHAR(255),
		"TA_Init_App_Days" VARCHAR(255),
		"TA_Init_App_Hours" VARCHAR(255),
		"Practice_Name" VARCHAR(255),
		"Practice_City" VARCHAR(255),
		"Practice_State" VARCHAR(255),
		"Practice_Zip" VARCHAR(255),
		"Payer_BillDate" VARCHAR(255),
		"Provider_BilledAmount" VARCHAR(255),
		"First_CPT" VARCHAR(255),
		"Service_modality" VARCHAR(255),
		"First_CPT_Qty" VARCHAR(255),
		"Total_BillAmount" VARCHAR(255),
		"Total_AllowAmount" VARCHAR(255),
		"Total_AllowAmountAdjustment" VARCHAR(255),
		"Total_AllowAmount_Net" VARCHAR(255),
		"Total_AllowAmount_Unit_C2" VARCHAR(255),
		"Total_AllowAmount_Total_C2" VARCHAR(255),
		"Total_FeeSchedule_Unit_C2" VARCHAR(255),
		"Total_FeeSchedule_Total_C2" VARCHAR(255),
		"Total_Savings_FromFS_Net" VARCHAR(255),
		"Total_Savings_FromFS_Percentage" VARCHAR(255),
		"Total_PaidIn_Amount" VARCHAR(255),
		"Total_PaidIn_Amount_CheckCalc" VARCHAR(255),
		"Total_PaidOut_Amount_Expected" VARCHAR(255),
		"Total_Profit_Net" VARCHAR(255),
		"Client_Savings_Warning" VARCHAR(255),
		"Profit_Warning" VARCHAR(255),
		"Payment_Warning" VARCHAR(255),
		"FS Zero" VARCHAR(255),
		"UC" VARCHAR(255),
		"Medicare" VARCHAR(255),
		"serviceid" INT4,
		"service_uniquemodifydate" TIMESTAMP,
		"encounter_uniquemodifydate" TIMESTAMP
	);

CREATE TABLE "public"."nid_api_token" (
		"token" VARCHAR(50) DEFAULT md5((random())::text) NOT NULL,
		"uniquecreatedate" DATE DEFAULT now() NOT NULL,
		"pid" INT4,
		"status" BOOL DEFAULT true NOT NULL
	);

CREATE TABLE "public"."tgroupsecurity" (
		"groupsecurityid" SERIAL DEFAULT nextval('tgroupsecurity_groupsecurityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"itemname" VARCHAR(100) DEFAULT ''::character varying,
		"accesslevel" INT4 DEFAULT 0
	);

CREATE TABLE "public"."teventmaster" (
		"eventid" SERIAL DEFAULT nextval('teventmaster_eventid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"name" VARCHAR(20) DEFAULT ''::character varying,
		"eventtype" INT4 DEFAULT 0,
		"completed" INT4 DEFAULT 0,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"reminddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"summary" VARCHAR(1000) DEFAULT ''::character varying,
		"itemrefid" INT4 DEFAULT 0,
		"itemreftype" VARCHAR(200) DEFAULT ''::character varying,
		"contactname" VARCHAR(90) DEFAULT ''::character varying,
		"email" VARCHAR(90) DEFAULT ''::character varying,
		"address1" VARCHAR(90) DEFAULT ''::character varying,
		"address2" VARCHAR(90) DEFAULT ''::character varying,
		"city" VARCHAR(90) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(90) DEFAULT ''::character varying,
		"zip" VARCHAR(15) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcallschedule" (
		"callscheduleid" SERIAL DEFAULT nextval('tcallschedule_callscheduleid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"iscurrent" INT4 DEFAULT 0,
		"callstart" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"callend" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timestatus" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tuatransaction" (
		"uatransactionid" SERIAL DEFAULT nextval('tuatransaction_uatransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"remoteip" VARCHAR(100) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"comments" VARCHAR(1000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tuserstatusli" (
		"userstatusid" INT4 NOT NULL,
		"userstatuslong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_modality" (
		"modalityid" SERIAL DEFAULT nextval('tnim3_modality_modalityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"practiceid" INT4 DEFAULT 0,
		"modalitytypeid" INT4 DEFAULT 0,
		"modalitymodelid" INT4 DEFAULT 0,
		"softwareversion" VARCHAR(400) DEFAULT ''::character varying,
		"lastservicedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"coilwrist" INT4 DEFAULT 0,
		"coilelbow" INT4 DEFAULT 0,
		"coilknee" INT4 DEFAULT 0,
		"coilankle" INT4 DEFAULT 0,
		"coilshoulder" INT4 DEFAULT 0,
		"coilspine" INT4 DEFAULT 0,
		"isacr" INT4 DEFAULT 0,
		"acrexpiration" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"isacrverified" INT4 DEFAULT 0,
		"acrverifieddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"comments" VARCHAR(500) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcptli" (
		"lookupid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"cpt" VARCHAR(10) DEFAULT ''::character varying,
		"mod" VARCHAR(10) DEFAULT ''::character varying,
		"description" VARCHAR(100) DEFAULT ''::character varying,
		"cptcommon1" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."rest_api_token" (
		"token" VARCHAR(50) NOT NULL,
		"createdate" DATE DEFAULT now() NOT NULL,
		"userid" INT4 DEFAULT 0 NOT NULL,
		"active" BOOL DEFAULT false NOT NULL,
		"hostip" VARCHAR(30)
	);

CREATE TABLE "public"."tnim_payermaster" (
		"payerid" SERIAL DEFAULT nextval('tnim_payermaster_payerid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"payername" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(100) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(100) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"mrirate" NUMERIC(9 , 2) DEFAULT 0.0,
		"ctrate" NUMERIC(9 , 2) DEFAULT 0.0
	);

CREATE TABLE "public"."tformdisclosurequestionlu" (
		"lookupid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"formid" INT4 DEFAULT 0,
		"questionid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."ttimetrack_rcodeli" (
		"timetrack_rcodeid" INT4 NOT NULL,
		"descriptionlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcodisclosurequestionlu" (
		"lookupid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"hcoid" INT4 DEFAULT 0,
		"questionid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tencountertypeli" (
		"encountertypeid" INT4 NOT NULL,
		"encountertypeshort" VARCHAR(10) DEFAULT ''::character varying,
		"encountertypelong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcaseaccountstatusli" (
		"casestatusid" INT4 NOT NULL,
		"casestatusshort" VARCHAR(10) DEFAULT ''::character varying,
		"casestatuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_appointment" (
		"appointmentid" SERIAL DEFAULT nextval('tnim2_appointment_appointmentid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"icid" INT4 DEFAULT 0,
		"istatus" INT4 DEFAULT 0,
		"appointmenttime" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"appointmentconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"scheduler" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ak" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tservicebillingtransactiontypeli" (
		"servicebillingtransactiontypeid" INT4 NOT NULL,
		"statusshort" VARCHAR(4) DEFAULT ''::character varying,
		"statuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcoveragetypeli" (
		"coverageid" INT4 NOT NULL,
		"coveragelong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."teventeventlu" (
		"lookupid" SERIAL DEFAULT nextval('teventeventlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"parenteventid" INT4 DEFAULT 0,
		"childeventid" INT4 DEFAULT 0,
		"eventtypeid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_salescontact" (
		"salescontactid" SERIAL DEFAULT nextval('tnim2_salescontact_salescontactid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"firstname" VARCHAR(90) DEFAULT ''::character varying,
		"lastname" VARCHAR(90) DEFAULT ''::character varying,
		"title" VARCHAR(90) DEFAULT ''::character varying,
		"companyname" VARCHAR(90) DEFAULT ''::character varying,
		"email" VARCHAR(95) DEFAULT ''::character varying,
		"address1" VARCHAR(90) DEFAULT ''::character varying,
		"address2" VARCHAR(90) DEFAULT ''::character varying,
		"city" VARCHAR(90) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"mobile" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(2000) DEFAULT ''::character varying,
		"nextactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"planofaction" VARCHAR(5000) DEFAULT ''::character varying,
		"accountnotes" VARCHAR(5000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_encounter" (
		"encounterid" SERIAL DEFAULT nextval('tnim3_encounter_encounterid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"referralid" INT4 DEFAULT 0,
		"referringphysicianid" INT4 DEFAULT 0,
		"attendingphysicianid" INT4 DEFAULT 0,
		"encountertypeid" INT4 DEFAULT 0,
		"appointmentid" INT4 DEFAULT 0,
		"scanpass" VARCHAR(100) DEFAULT ''::character varying,
		"dateofservice" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nextactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nextactiontaskid" INT4 DEFAULT 0,
		"reportfileid" INT4 DEFAULT 0,
		"dicomfileid" INT4 DEFAULT 0,
		"comments" VARCHAR(9000) DEFAULT ''::character varying,
		"auditnotes" VARCHAR(9000) DEFAULT ''::character varying,
		"capabilityreportfileid" INT4 DEFAULT 0,
		"lodid" INT4 DEFAULT 0,
		"senttorefdr" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"senttoadj" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"senttoic" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_refdr" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_adj" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_ic" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_bill_pay" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"rec_bill_pay" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_bill_pro" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"rec_bill_pro" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"encounterstatusid" INT4 DEFAULT 0,
		"senttorefdr_crid" INT4 DEFAULT 0,
		"senttoadj_crid" INT4 DEFAULT 0,
		"senttoic_crid" INT4 DEFAULT 0,
		"sentto_appt_conf_pat" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_appt_conf_pat_crid" INT4 DEFAULT 0,
		"sentto_sp_pat" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_pat_crid" INT4 DEFAULT 0,
		"sentto_sp_refdr_crid" INT4 DEFAULT 0,
		"sentto_sp_adj_crid" INT4 DEFAULT 0,
		"sentto_sp_ic_crid" INT4 DEFAULT 0,
		"sentto_dataproc_refdr" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_dataproc_refdr_crid" INT4 DEFAULT 0,
		"sentto_dataproc_adj" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_dataproc_adj_crid" INT4 DEFAULT 0,
		"sentto_bill_pay_crid" INT4 DEFAULT 0,
		"rec_bill_pay_crid" INT4 DEFAULT 0,
		"sentto_bill_pro_crid" INT4 DEFAULT 0,
		"rec_bill_pro_crid" INT4 DEFAULT 0,
		"paidtoprovidercheck1number" VARCHAR(20) DEFAULT ''::character varying,
		"paidtoprovidercheck1amount" NUMERIC(9 , 2) DEFAULT 0.0,
		"paidtoprovidercheck1date" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"paidtoprovidercheck2number" VARCHAR(20) DEFAULT ''::character varying,
		"paidtoprovidercheck2amount" NUMERIC(9 , 2) DEFAULT 0.0,
		"paidtoprovidercheck2date" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"paidtoprovidercheck3number" VARCHAR(20) DEFAULT ''::character varying,
		"paidtoprovidercheck3amount" NUMERIC(9 , 2) DEFAULT 0.0,
		"paidtoprovidercheck3date" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_refdr" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_refdr_crid" INT4 DEFAULT 0,
		"sentto_reqrec_adj" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_adj_crid" INT4 DEFAULT 0,
		"timetrack_reqrec" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqrec_userid" INT4 DEFAULT 0,
		"timetrack_reqcreated" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqcreated_userid" INT4 DEFAULT 0,
		"timetrack_reqproc" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqproc_userid" INT4 DEFAULT 0,
		"timetrack_reqsched" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqsched_userid" INT4 DEFAULT 0,
		"timetrack_reqdelivered" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqdelivered_userid" INT4 DEFAULT 0,
		"timetrack_reqpaidin" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqpaidin_userid" INT4 DEFAULT 0,
		"timetrack_reqpaidout" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqpaidout_userid" INT4 DEFAULT 0,
		"providerinvoiceid" INT4 DEFAULT 0,
		"timetrack_reqapproved_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqapproved" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqapproved_userid" INT4 DEFAULT 0,
		"timetrack_reqrec_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqcreated_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqproc_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqsched_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqdelivered_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqpaidin_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqpaidout_rcodeid" INT4 DEFAULT 0,
		"sentto_reqrec_adm" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_adm_crid" INT4 DEFAULT 0,
		"sentto_sp_adm" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_adm_crid" INT4 DEFAULT 0,
		"senttoadm" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"senttoadm_crid" INT4 DEFAULT 0,
		"timetrack_reqinitialappointment_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqinitialappointment" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqinitialappointment_userid" INT4 DEFAULT 0,
		"timetrack_reqrxreview_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqrxreview" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqrxreview_userid" INT4 DEFAULT 0,
		"nextactionnotes" VARCHAR(2000) DEFAULT ''::character varying,
		"voidleakreasonid" INT4 DEFAULT 0,
		"exportpaymenttoqb" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqrpreview_rcodeid" INT4 DEFAULT 0,
		"timetrack_reqrpreview" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"timetrack_reqrpreview_userid" INT4 DEFAULT 0,
		"sentto_sp_ncm" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_ncm_crid" INT4 DEFAULT 0,
		"sentto_rp_ncm" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_rp_ncm_crid" INT4 DEFAULT 0,
		"sentto_reqrec_ncm" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_ncm_crid" INT4 DEFAULT 0,
		"isstat" INT4 DEFAULT 0,
		"requiresfilms" INT4 DEFAULT 0,
		"hasbeenrescheduled" INT4 DEFAULT 0,
		"sentto_sp_adm2" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_adm2_crid" INT4 DEFAULT 0,
		"sentto_rp_adm2" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_rp_adm2_crid" INT4 DEFAULT 0,
		"sentto_reqrec_adm2" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_adm2_crid" INT4 DEFAULT 0,
		"sentto_sp_adm3" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_adm3_crid" INT4 DEFAULT 0,
		"sentto_rp_adm3" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_rp_adm3_crid" INT4 DEFAULT 0,
		"sentto_reqrec_adm3" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_adm3_crid" INT4 DEFAULT 0,
		"sentto_sp_adm4" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_adm4_crid" INT4 DEFAULT 0,
		"sentto_rp_adm4" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_rp_adm4_crid" INT4 DEFAULT 0,
		"sentto_reqrec_adm4" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_reqrec_adm4_crid" INT4 DEFAULT 0,
		"requiresageinjury" INT4 DEFAULT 0,
		"iscourtesy" INT4 DEFAULT 0,
		"billinghcfa_topayer_fileid" INT4 DEFAULT 0,
		"billinghcfa_fromprovider_fileid" INT4 DEFAULT 0,
		"isretro" INT4 DEFAULT 0,
		"exportaptoqb" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"exportartoqb" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"amountbilledbyprovider" NUMERIC(9 , 2) DEFAULT 0.0,
		"seenetdev_flagged" INT4 DEFAULT 0,
		"seenetdev_reqsenttonetdev" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"seenetdev_reqsenttonetdev_userid" INT4 DEFAULT 0,
		"seenetdev_waiting" INT4 DEFAULT 0,
		"patientavailability" VARCHAR(2000) DEFAULT ''::character varying,
		"requiresonlineimage" INT4 DEFAULT 0,
		"pricing_structure" INT4 DEFAULT 1,
		"seenetdev_selectedpracticeid" INT4 DEFAULT 0,
		"actiondate_onlineimagerequest" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actiondate_reportrequest" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"void_userid" INT4 DEFAULT 0,
		"requireshandcarrycd" INT4 DEFAULT 0,
		"reportfollowupcounter" INT4 DEFAULT 0,
		"problemcategory" VARCHAR(100) DEFAULT ''::character varying,
		"problemdescription" VARCHAR(500) DEFAULT ''::character varying,
		"agecommentlock" INT4 DEFAULT 0,
		"sentto_reqrec_pt" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"senttopt" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sentto_sp_pt" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nidreferralsource" VARCHAR(200) DEFAULT ''::character varying,
		"nidreferralcode" VARCHAR(200) DEFAULT ''::character varying,
		"cctransactionid" INT4 DEFAULT 0,
		"isvip" INT4 DEFAULT 0,
		"hcfainvoice" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."vtemp" (
		"cpt" VARCHAR(10),
		"state" VARCHAR(2),
		"price" NUMERIC(6 , 2)
	);

CREATE TABLE "public"."tadminphysicianlu" (
		"lookupid" SERIAL DEFAULT nextval('tadminphysicianlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"adminid" INT4 DEFAULT 0,
		"physicianid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tccardtransaction" (
		"ccardtransactionid" SERIAL DEFAULT nextval('tccardtransaction_ccardtransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"cname" VARCHAR(200) DEFAULT ''::character varying,
		"caddress" VARCHAR(200) DEFAULT ''::character varying,
		"ccity" VARCHAR(200) DEFAULT ''::character varying,
		"cstate" VARCHAR(2) DEFAULT ''::character varying,
		"czip" VARCHAR(11) DEFAULT ''::character varying,
		"ccnumber" INT4 DEFAULT 0,
		"cexp" VARCHAR(10) DEFAULT ''::character varying,
		"ordernumber" VARCHAR(200) DEFAULT ''::character varying,
		"authnumber" VARCHAR(200) DEFAULT ''::character varying,
		"amountcharged" VARCHAR(200) DEFAULT ''::character varying,
		"encounterid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tapprovaltypeli" (
		"approvaltypeid" INT4 NOT NULL,
		"approvaltypelong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_wy" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."thcouseraccountlu" (
		"lookupid" SERIAL DEFAULT nextval('thcouseraccountlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"hcoid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"hcousertypeid" INT4 DEFAULT 0,
		"notificationstatusid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_nv" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."SelectMRI_qPayerMaster_Branches" (
		"ClientID" VARCHAR(255),
		"SelectMRI_ID" VARCHAR(255),
		"PayerName" VARCHAR(255),
		"OfficePhone" VARCHAR(255),
		"OfficeFax" VARCHAR(255),
		"OfficeContact" VARCHAR(255),
		"SelectMRI_Notes" VARCHAR(255),
		"ContactName" VARCHAR(255),
		"OfficeAddress1" VARCHAR(255),
		"OfficeAddress2" VARCHAR(255),
		"OfficeCity" VARCHAR(255),
		"OfficeStateID" VARCHAR(255),
		"OfficeState" VARCHAR(255),
		"OfficeZIP" VARCHAR(255),
		"SelectMRI_QBID" VARCHAR(255)
	);

CREATE TABLE "public"."ticd9li" (
		"icd9codeid" VARCHAR(30) NOT NULL,
		"longdescription" VARCHAR(100) NOT NULL,
		"shortdescription" VARCHAR(30) NOT NULL
	);

CREATE TABLE "public"."tcvohcolu" (
		"lookupid" SERIAL DEFAULT nextval('tcvohcolu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"cvoid" INT4 DEFAULT 0,
		"hcoid" INT4 DEFAULT 0,
		"controltype" INT4 DEFAULT 0,
		"prioritytype" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_salesprospect" (
		"salesprospectid" SERIAL DEFAULT nextval('tnim2_salesprospect_salesprospectid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"nimcontactid" INT4 DEFAULT 0,
		"maincontactid" INT4 DEFAULT 0,
		"companyname" VARCHAR(90) DEFAULT ''::character varying,
		"weburl" VARCHAR(95) DEFAULT ''::character varying,
		"address1" VARCHAR(90) DEFAULT ''::character varying,
		"address2" VARCHAR(90) DEFAULT ''::character varying,
		"city" VARCHAR(90) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(5000) DEFAULT ''::character varying,
		"nextactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"planofaction" VARCHAR(5000) DEFAULT ''::character varying,
		"accountnotes" VARCHAR(5000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_regioncode" (
		"fsid" VARCHAR(255),
		"rc" VARCHAR(255),
		"zip" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."in_fs_feeschedule_mt" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."thcophysicianlu" (
		"lookupid" SERIAL DEFAULT nextval('thcophysicianlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"hcoid" INT4 DEFAULT 0,
		"physicianid" INT4 DEFAULT 0,
		"controltypeid" INT4 DEFAULT 0,
		"credentialingstatus" INT4 DEFAULT 0,
		"applicationreceivedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"applicationsigndate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"credentialingrequestdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"initialcredentialdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"lastcredentialdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nextrecredentialdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"effectivedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"terminationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"credentialer" INT4 DEFAULT 0,
		"reviewer" INT4 DEFAULT 0,
		"division" VARCHAR(100) DEFAULT ''::character varying,
		"district" VARCHAR(100) DEFAULT ''::character varying,
		"contractmanager" INT4 DEFAULT 0,
		"statustype" VARCHAR(100) DEFAULT ''::character varying,
		"uniqueprovidernumber" VARCHAR(100) DEFAULT ''::character varying,
		"laborandindustrynumber" VARCHAR(100) DEFAULT ''::character varying,
		"uniquebaselinenumber" VARCHAR(100) DEFAULT ''::character varying,
		"honeywellnumber" VARCHAR(100) DEFAULT ''::character varying,
		"contdepartment" VARCHAR(100) DEFAULT ''::character varying,
		"certificationnotes" VARCHAR(2000) DEFAULT ''::character varying,
		"targetcpcdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"msofilerequestdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"medicarepasdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"userfield1" VARCHAR(200) DEFAULT ''::character varying,
		"userfield2" VARCHAR(200) DEFAULT ''::character varying,
		"userfield3" VARCHAR(200) DEFAULT ''::character varying,
		"userfield4" VARCHAR(200) DEFAULT ''::character varying,
		"userfield5" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"sponsoringpractitionerprimary" VARCHAR(99) DEFAULT ''::character varying,
		"sponsoringpractitionersecondary" VARCHAR(99) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_payermaster_copy" (
		"payerid" SERIAL DEFAULT nextval('tnim3_payermaster_payerid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT now(),
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"payername" VARCHAR(200) DEFAULT ''::character varying,
		"contactname" VARCHAR(100) DEFAULT ''::character varying,
		"billingaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"billingaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"billingcity" VARCHAR(100) DEFAULT ''::character varying,
		"billingstateid" INT4 DEFAULT 0,
		"billingzip" VARCHAR(50) DEFAULT ''::character varying,
		"billingphone" VARCHAR(50) DEFAULT ''::character varying,
		"billingfax" VARCHAR(50) DEFAULT ''::character varying,
		"billingemail" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"billingentityid" INT4 DEFAULT 0,
		"payertypeid" INT4 DEFAULT 0,
		"importantnotes" VARCHAR(9000) DEFAULT ''::character varying,
		"payableto" VARCHAR(100) DEFAULT ''::character varying,
		"price_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"feeschedulerefid" INT4 DEFAULT 0,
		"feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"feeschedule2refid" INT4 DEFAULT 0,
		"feepercentage2" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"feepercentagebill" NUMERIC(9 , 2) DEFAULT 0.0,
		"feepercentage2bill" NUMERIC(9 , 2) DEFAULT 0.0,
		"billingtypeid" INT4 DEFAULT 0,
		"acceptsemgcaserate" INT4 DEFAULT 0,
		"parentpayerid" INT4 DEFAULT 0,
		"selectmri_id" INT4 DEFAULT 0,
		"selectmri_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"selectmri_qbid" VARCHAR(50) DEFAULT ''::character varying,
		"billingname" VARCHAR(100) DEFAULT ''::character varying,
		"officeaddress1" VARCHAR(100) DEFAULT ''::character varying,
		"officeaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"officecity" VARCHAR(100) DEFAULT ''::character varying,
		"officestateid" INT4 DEFAULT 0,
		"officezip" VARCHAR(50) DEFAULT ''::character varying,
		"officephone" VARCHAR(50) DEFAULT ''::character varying,
		"officefax" VARCHAR(50) DEFAULT ''::character varying,
		"officeemail" VARCHAR(50) DEFAULT ''::character varying,
		"salesdivision" VARCHAR(10) DEFAULT ''::character varying,
		"acquisitiondivision" VARCHAR(10) DEFAULT ''::character varying,
		"contract1_feescheduleid" INT4 DEFAULT 0,
		"contract1_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"contract2_feescheduleid" INT4 DEFAULT 0,
		"contract2_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"contract3_feescheduleid" INT4 DEFAULT 0,
		"contract3_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"bill1_feescheduleid" INT4 DEFAULT 0,
		"bill1_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"bill2_feescheduleid" INT4 DEFAULT 0,
		"bill2_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"bill3_feescheduleid" INT4 DEFAULT 0,
		"bill3_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"importantnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"emailalertnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"requiresonlineimage" INT4 DEFAULT 0,
		"requiresaging" INT4 DEFAULT 0,
		"sales_monthly_target" INT4 DEFAULT 0,
		"acceptstier2" INT4 DEFAULT 0,
		"acceptstier3" INT4 DEFAULT 0,
		"tier2_minsavings" NUMERIC(9 , 2) DEFAULT 0.0,
		"tier2_targetsavings" NUMERIC(9 , 2) DEFAULT 0.0,
		"tier3_fee" NUMERIC(9 , 2) DEFAULT 0.0,
		"autoauth" VARCHAR(1) DEFAULT ''::character varying,
		"petlinqready" INT4 DEFAULT 0,
		"isnid" INT4 DEFAULT 0,
		"nid_webcode" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showlogo" INT4 DEFAULT 0,
		"nid_weblogo" VARCHAR(200) DEFAULT ''::character varying,
		"nid_referralsource" INT4 DEFAULT 0,
		"commissionhighpercentageself" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionlowpercentageself" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionhighpercentageparent" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionlowpercentageparent" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionnotes" VARCHAR(5000) DEFAULT ''::character varying,
		"discounthighamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"discounthighpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"discountlowamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"discountlowpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"customerdisplayname" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showcode" INT4 DEFAULT 0,
		"nid_codedisplayname" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showsource" INT4 DEFAULT 0,
		"nid_sourcedisplayname" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showbanner" INT4 DEFAULT 0,
		"nid_welcomemessage" VARCHAR(200) DEFAULT ''::character varying,
		"nid_discountpercentagemessage" VARCHAR(200) DEFAULT ''::character varying,
		"insidesalesdivision" VARCHAR(30) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcodepartmentli" (
		"hcodepartmentid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"hcodepartment" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_az" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tworkhistory" (
		"workhistoryid" SERIAL DEFAULT nextval('tworkhistory_workhistoryid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"workhistorytypeid" INT4 DEFAULT 0,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"workdescription" VARCHAR(200) DEFAULT ''::character varying,
		"fromdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"todate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"reasonforleaving" VARCHAR(50) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(95) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnpdbfoltypeli" (
		"npdbfolid" INT4 NOT NULL,
		"npdbfollong" VARCHAR(200) DEFAULT ''::character varying,
		"npdbfolshort" VARCHAR(10) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_co" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tmessage" (
		"messageid" SERIAL DEFAULT nextval('tmessage_messageid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"senderuserid" INT4 DEFAULT 0,
		"referencemessageid" INT4 DEFAULT 0,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"emailto" VARCHAR(200) DEFAULT ''::character varying,
		"emailfrom" VARCHAR(200) DEFAULT ''::character varying,
		"emailbody" VARCHAR(200) DEFAULT ''::character varying,
		"emailbodytype" VARCHAR(90) DEFAULT ''::character varying,
		"emailsubject" VARCHAR(200) DEFAULT ''::character varying,
		"emailimportance" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"emailcategory" INT4 DEFAULT 0,
		"isprivate" INT4 DEFAULT 0,
		"isread" INT4 DEFAULT 0,
		"readpassword" VARCHAR(200) DEFAULT ''::character varying,
		"relatedlink" VARCHAR(1000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tphysicianmaster" (
		"physicianid" SERIAL DEFAULT nextval('tphysicianmaster_physicianid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"salutation" INT4 DEFAULT 0,
		"title" VARCHAR(50) DEFAULT ''::character varying,
		"firstname" VARCHAR(50) DEFAULT ''::character varying,
		"middlename" VARCHAR(50) DEFAULT ''::character varying,
		"lastname" VARCHAR(50) DEFAULT ''::character varying,
		"suffix" VARCHAR(50) DEFAULT ''::character varying,
		"homeemail" VARCHAR(75) DEFAULT ''::character varying,
		"homeaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"homeaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"homecity" VARCHAR(30) DEFAULT ''::character varying,
		"homestateid" INT4 DEFAULT 0,
		"homeprovince" VARCHAR(100) DEFAULT ''::character varying,
		"homezip" VARCHAR(50) DEFAULT ''::character varying,
		"homecountryid" INT4 DEFAULT 0,
		"homephone" VARCHAR(50) DEFAULT ''::character varying,
		"homefax" VARCHAR(50) DEFAULT ''::character varying,
		"homemobile" VARCHAR(50) DEFAULT ''::character varying,
		"homepager" VARCHAR(50) DEFAULT ''::character varying,
		"alertemail" VARCHAR(50) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"othername1" VARCHAR(100) DEFAULT ''::character varying,
		"othername1start" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"othername1end" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"othername2" VARCHAR(100) DEFAULT ''::character varying,
		"othername2start" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"othername2end" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"dateofbirth" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"placeofbirth" VARCHAR(100) DEFAULT ''::character varying,
		"spouse" VARCHAR(100) DEFAULT ''::character varying,
		"citizenshipyn" INT4 DEFAULT 0,
		"citizenship" VARCHAR(100) DEFAULT ''::character varying,
		"visanumber" VARCHAR(100) DEFAULT ''::character varying,
		"visastatus" VARCHAR(100) DEFAULT ''::character varying,
		"eligibletoworkinus" INT4 DEFAULT 0,
		"ssn" VARCHAR(50) DEFAULT ''::character varying,
		"gender" INT4 DEFAULT 0,
		"militaryactive" INT4 DEFAULT 0,
		"militarybranch" VARCHAR(40) DEFAULT ''::character varying,
		"militaryreserve" INT4 DEFAULT 0,
		"hospitaladmitingprivileges" INT4 DEFAULT 0,
		"hospitaladmitingprivilegesno" VARCHAR(400) DEFAULT ''::character varying,
		"physiciancategoryid" INT4 DEFAULT 0,
		"ipamedicalaffiliation" INT4 DEFAULT 0,
		"affiliationdesc1" VARCHAR(200) DEFAULT ''::character varying,
		"physicianlanguages" VARCHAR(100) DEFAULT ''::character varying,
		"ecfmgno" VARCHAR(100) DEFAULT ''::character varying,
		"ecfmgdateissued" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"ecfmgdateexpires" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"medicareupin" VARCHAR(100) DEFAULT ''::character varying,
		"uniquenpi" VARCHAR(100) DEFAULT ''::character varying,
		"medicareparticipation" INT4 DEFAULT 0,
		"medicareno" VARCHAR(100) DEFAULT ''::character varying,
		"medicaidparticipation" INT4 DEFAULT 0,
		"medicaidno" VARCHAR(100) DEFAULT ''::character varying,
		"supplementalidnumber1" VARCHAR(100) DEFAULT ''::character varying,
		"supplementalidnumber2" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"attestconsent" INT4 DEFAULT 0,
		"attestationcomments" VARCHAR(200) DEFAULT ''::character varying,
		"isviewable" INT4 DEFAULT 0,
		"isattested" INT4 DEFAULT 0,
		"attestdatepending" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"isattestedpending" INT4 DEFAULT 0,
		"attestdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"deattestdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"lastmodifieddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"questionvalidation" VARCHAR(100) DEFAULT ''::character varying,
		"usmledatepassedstep1" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"usmledatepassedstep2" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"usmledatepassedstep3" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"haveflex" INT4 DEFAULT 0,
		"flexdatepassed" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"visasponsor" VARCHAR(100) DEFAULT ''::character varying,
		"visaexpiration" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"currentvisatemporary" INT4 DEFAULT 0,
		"currentvisaextended" INT4 DEFAULT 0,
		"havegreencard" INT4 DEFAULT 0,
		"countryofissueid" INT4 DEFAULT 0,
		"visatemporary5years" INT4 DEFAULT 0,
		"pastvisa1datefrom" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pastvisa1dateto" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pastvisa1type" VARCHAR(100) DEFAULT ''::character varying,
		"pastvisa1sponsor" VARCHAR(100) DEFAULT ''::character varying,
		"pastvisa2datefrom" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pastvisa2dateto" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pastvisa2type" VARCHAR(100) DEFAULT ''::character varying,
		"pastvisa2sponsor" VARCHAR(100) DEFAULT ''::character varying,
		"militaryrank" VARCHAR(100) DEFAULT ''::character varying,
		"militarydutystatus" VARCHAR(100) DEFAULT ''::character varying,
		"militarycurrentassignment" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tphysicianpracticelu" (
		"lookupid" SERIAL DEFAULT nextval('tphysicianpracticelu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"practiceid" INT4 DEFAULT 0,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"isprimaryoffice" INT4 DEFAULT 0,
		"isadministrativeoffice" INT4 DEFAULT 0,
		"coveragehours" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tphysicianuseraccountlu" (
		"lookupid" SERIAL DEFAULT nextval('tphysicianuseraccountlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ut" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."in_fs_feeschedule_tn" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tcoveringphysicians" (
		"coveringphysicianid" SERIAL DEFAULT nextval('tcoveringphysicians_coveringphysicianid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"firstname" VARCHAR(50) DEFAULT ''::character varying,
		"lastname" VARCHAR(50) DEFAULT ''::character varying,
		"specialty" VARCHAR(80) DEFAULT ''::character varying,
		"licensenumber" VARCHAR(50) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"providertype" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_pa" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."treadmaster" (
		"readid" SERIAL DEFAULT nextval('treadmaster_readid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"readfilename" VARCHAR(100) DEFAULT ''::character varying,
		"readsource" VARCHAR(200) DEFAULT ''::character varying,
		"referringphysician" VARCHAR(200) DEFAULT ''::character varying,
		"readtype" INT4 DEFAULT 0,
		"specialty1" VARCHAR(50) DEFAULT ''::character varying,
		"specialtyrequired1" INT4 DEFAULT 0,
		"specialty2" VARCHAR(50) DEFAULT ''::character varying,
		"specialtyrequired2" INT4 DEFAULT 0,
		"readstatusid" INT4 DEFAULT 0,
		"duedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tappointmentstatusli" (
		"appointmentstatusid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"appointmentstatuslong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tprofessionalsociety" (
		"professionalsocietyid" SERIAL DEFAULT nextval('tprofessionalsociety_professionalsocietyid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"membershipstatus" VARCHAR(50) DEFAULT ''::character varying,
		"fromdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"todate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(95) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_cptgrouplist" (
		"lookupid" SERIAL DEFAULT nextval('tnim2_cptgrouplist_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"cptgroupid" INT4 DEFAULT 0,
		"cpt" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tsecuritygroupmaster" (
		"securitygroupid" SERIAL DEFAULT nextval('tsecuritygroupmaster_securitygroupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"groupname" VARCHAR(100) DEFAULT ''::character varying,
		"runfieldsecurityid" INT4 DEFAULT 0,
		"showauditid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tprofessionaleducation" (
		"professionaleducationid" SERIAL DEFAULT nextval('tprofessionaleducation_professionaleducationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"degreeid" INT4 DEFAULT 0,
		"other" VARCHAR(50) DEFAULT ''::character varying,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"dateofgraduation" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"focus" VARCHAR(50) DEFAULT ''::character varying,
		"schoolname" VARCHAR(100) DEFAULT ''::character varying,
		"schooladdress1" VARCHAR(50) DEFAULT ''::character varying,
		"schooladdress2" VARCHAR(20) DEFAULT ''::character varying,
		"schoolcity" VARCHAR(30) DEFAULT ''::character varying,
		"schoolstateid" INT4 DEFAULT 0,
		"schoolprovince" VARCHAR(100) DEFAULT ''::character varying,
		"schoolzip" VARCHAR(50) DEFAULT ''::character varying,
		"schoolcountryid" INT4 DEFAULT 0,
		"schoolphone" VARCHAR(50) DEFAULT ''::character varying,
		"schoolfax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_salestransaction" (
		"salestransactionid" SERIAL DEFAULT nextval('tnim2_salestransaction_salestransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"salesprospectid" INT4 DEFAULT 0,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"typedescription" VARCHAR(90) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"refid" INT4 DEFAULT 0,
		"transactioncomments" VARCHAR(2000) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcourtesyli" (
		"courtesyid" INT4 NOT NULL,
		"courtesyshort" VARCHAR(10) DEFAULT ''::character varying,
		"courtesylong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_id" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."treadphysicianlu" (
		"lookupid" SERIAL DEFAULT nextval('treadphysicianlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"readid" INT4 DEFAULT 0,
		"physicianid" INT4 DEFAULT 0,
		"reportstatus" INT4 DEFAULT 0,
		"reporttext" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."vmq4_services" (
		"CaseID" INT4,
		"CaseCode" VARCHAR(100),
		"CaseClaimNumber" VARCHAR(100),
		"PayerID" INT4,
		"PayerName" VARCHAR(200),
		"Payer_TypeID" INT4,
		"Payer_SalesDivision" VARCHAR(10),
		"Payer_AcquisitionDivision" VARCHAR(10),
		"PayerCity" VARCHAR(100),
		"PayerState" VARCHAR(4),
		"Parent_PayerID" INT4,
		"Parent_PayerName" VARCHAR(200),
		"ParentPayer_TypeID" INT4,
		"ParentPayer_SalesDivision" VARCHAR(10),
		"ParentPayer_AcquisitionDivsion" VARCHAR(10),
		"UA_AssignedTo_UserID" INT4,
		"UA_AssignedTo_UserName" VARCHAR(200),
		"UA_AssignedTo_FirstName" VARCHAR(50),
		"UA_AssignedTo_LastName" VARCHAR(50),
		"UA_AssignedTo_Email" VARCHAR(200),
		"UA_Adjuster_UserID" INT4,
		"UA_Adjuster_FirstName" VARCHAR(50),
		"UA_Adjuster_LastName" VARCHAR(50),
		"UA_Adjuster_Email" VARCHAR(200),
		"UA_NCM_UserID" INT4,
		"UA_NCM_FirstName" VARCHAR(50),
		"UA_NCM_LastName" VARCHAR(50),
		"UA_NCM_Email" VARCHAR(200),
		"UA_ReferralSource_UserID" INT4,
		"UA_ReferralSource_FirstName" VARCHAR(50),
		"UA_ReferralSource_LastName" VARCHAR(50),
		"UA_ReferralSource_Email" VARCHAR(200),
		"EmployerName" VARCHAR(500),
		"PatientFirstName" VARCHAR(100),
		"PatientLastName" VARCHAR(100),
		"DateOfInjury" TIMESTAMP,
		"DateOfInjury_display" TIMESTAMP,
		"DateOfInjury_iMonth" FLOAT8,
		"DateOfInjury_iDay" FLOAT8,
		"DateOfInjury_iDOW" FLOAT8,
		"DateOfInjury_iYear" FLOAT8,
		"PatientDOB" TIMESTAMP,
		"PatientDOB_display" TIMESTAMP,
		"PatientDOB_iMonth" FLOAT8,
		"PatientDOB_iDay" FLOAT8,
		"PatientDOB_iDOW" FLOAT8,
		"PatientDOB_iYear" FLOAT8,
		"PatientAddress1" VARCHAR(200),
		"PatientAddress2" VARCHAR(200),
		"PatientCity" VARCHAR(100),
		"PatientState" VARCHAR(4),
		"PatientHomePhone" VARCHAR(50),
		"PatientCellPhone" VARCHAR(50),
		"PreScreen_IsClaus" INT4,
		"PreScreen_ReqOpenModality" INT4,
		"PreScreen_HasImplants" INT4,
		"PreScreen_HasMetal" INT4,
		"PreScreen_Allergies" INT4,
		"PreScreen_HasRecentSurgery" INT4,
		"PreScreen_PreviousMRIs" INT4,
		"PreScreen_OtherCondition" INT4,
		"PreScreen_IsPregnant" INT4,
		"PreScreen_Height" VARCHAR(10),
		"PreScreen_Weight" INT4,
		"ReferralID" INT4,
		"ReferralStatusID" INT4,
		"Referral_ReceiveDate" TIMESTAMP,
		"Referral_ReceiveDate_display" TIMESTAMP,
		"Referral_ReceiveDate_iMonth" FLOAT8,
		"Referral_ReceiveDate_iDay" FLOAT8,
		"Referral_ReceiveDate_iDOW" FLOAT8,
		"Referral_ReceiveDate_iYear" FLOAT8,
		"Referral_OrderFileID" INT4,
		"Referral_RxFileID" INT4,
		"UA_ReferringDr_UserID" INT4,
		"UA_ReferringDr_FirstName" VARCHAR(50),
		"UA_ReferringDr_LastName" VARCHAR(50),
		"UA_ReferringDr_Email" VARCHAR(200),
		"EncounterID" INT4,
		"Encounter_StatusID" INT4,
		"Encounter_Status" VARCHAR(100),
		"Encounter_Type" VARCHAR(10),
		"Encounter_ScanPass" VARCHAR(100),
		"Encounter_DateOfService" TIMESTAMP,
		"Encounter_DateOfService_display" TIMESTAMP,
		"Encounter_DateOfService_iMonth" FLOAT8,
		"Encounter_DateOfService_iDay" FLOAT8,
		"Encounter_DateOfService_iDOW" FLOAT8,
		"Encounter_DateOfService_iYear" FLOAT8,
		"Encounter_ReportFileID" INT4,
		"Encounter_VoidLeakReason" VARCHAR(10),
		"Encounter_ExportPaymentToQB" TIMESTAMP,
		"Encounter_IsSTAT" INT4,
		"Encounter_IsRetro" INT4,
		"Encounter_ReqFilms" INT4,
		"Encounter_HasBeenRescheduled" INT4,
		"Encounter_ReqAging" INT4,
		"Encounter_IsCourtesy" INT4,
		"Encounter_HCFA_ToPayerFileID" INT4,
		"Encounter_HCFA_FromProvider_FileID" INT4,
		"Encounter_SentTo_Bill_Pay" TIMESTAMP,
		"Encounter_SentTo_Bill_Pay_display" TIMESTAMP,
		"Encounter_SentTo_Bill_Pay_iMonth" FLOAT8,
		"Encounter_SentTo_Bill_Pay_iDay" FLOAT8,
		"Encounter_SentTo_Bill_Pay_iDOW" FLOAT8,
		"Encounter_SentTo_Bill_Pay_iYear" FLOAT8,
		"Encounter_Rec_Bill_Pro" TIMESTAMP,
		"Encounter_Rec_Bill_Pro_display" TIMESTAMP,
		"Encounter_Rec_Bill_Pro_iMonth" FLOAT8,
		"Encounter_Rec_Bill_Pro_iDay" FLOAT8,
		"Encounter_Rec_Bill_Pro_iDOW" FLOAT8,
		"Encounter_Rec_Bill_Pro_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqRec" TIMESTAMP,
		"Encounter_TimeTrack_ReqRec_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqRec_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqRec_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqRec_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqRec_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqCreated" TIMESTAMP,
		"Encounter_TimeTrack_ReqCreated_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqCreated_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqCreated_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqCreated_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqCreated_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqProc" TIMESTAMP,
		"Encounter_TimeTrack_ReqProc_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqProc_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqProc_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqProc_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqProc_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqSched" TIMESTAMP,
		"Encounter_TimeTrack_ReqSched_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqSched_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqSched_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqSched_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqSched_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered" TIMESTAMP,
		"Encounter_TimeTrack_ReqDelivered_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqDelivered_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqDelivered_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidIN_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidIN_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqPaidIN_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidOut_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqPaidOut_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqPaidOut_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqApproved" TIMESTAMP,
		"Encounter_TimeTrack_ReqApproved_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqApproved_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqApproved_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqApproved_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqApproved_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment" TIMESTAMP,
		"Encounter_TimeTrack_ReqInitialAppointment_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqInitialAppointment_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqInitialAppointment_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview" TIMESTAMP,
		"Encounter_TimeTrack_ReqRxReview_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqRxReview_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqRxReview_iYear" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview" TIMESTAMP,
		"Encounter_TimeTrack_ReqRpReview_display" TIMESTAMP,
		"Encounter_TimeTrack_ReqRpReview_iMonth" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview_iDay" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview_iDOW" FLOAT8,
		"Encounter_TimeTrack_ReqRpReview_iYear" FLOAT8,
		"AppointmentID" INT4,
		"Encounter_PaidToProvider_CheckCalc" NUMERIC(131089 , 0),
		"Appointment_ProviderID" INT4,
		"Appointment_AppointmentTime" TIMESTAMP,
		"Appointment_AppointmentTime_display" TIMESTAMP,
		"Appointment_AppointmentTime_iMonth" FLOAT8,
		"Appointment_AppointmentTime_iDay" FLOAT8,
		"Appointment_AppointmentTime_iDOW" FLOAT8,
		"Appointment_AppointmentTime_iYear" FLOAT8,
		"Appointment_StatusID" INT4,
		"PracticeID" INT4,
		"Practice_Name" VARCHAR(500),
		"Practice_City" VARCHAR(100),
		"Practice_State" VARCHAR(4),
		"Practice_IsBlueStar" INT4,
		"Practice_PetLinQID" VARCHAR(10),
		"Practice_SelectMRI_ID" INT4,
		"ServiceID" INT4,
		"Service_StatusID" INT4,
		"Service_Status" VARCHAR(10),
		"Service_TypeID" INT4,
		"Service_CPT" VARCHAR(100),
		"Service_CPT_Qty" INT4,
		"Service_CPTModifier" VARCHAR(100),
		"Service_CPTBodyPart" VARCHAR(100),
		"Service_ICD_1" VARCHAR(100),
		"Service_ICD_2" VARCHAR(100),
		"Service_ICD_3" VARCHAR(100),
		"Service_ICD_4" VARCHAR(100),
		"Service_CPTText" VARCHAR(1000),
		"Service_BillAmount" NUMERIC(9 , 2),
		"Service_AllowAmount" NUMERIC(9 , 2),
		"Service_AllowAmountAdjustment" NUMERIC(9 , 2),
		"Service_ReceivedAmount" NUMERIC(9 , 2),
		"Service_PaidOutAmount" NUMERIC(9 , 2),
		"Service_ReceivedAmount_CheckCalc" NUMERIC(131089 , 0),
		"UA_CaseAdmin_FirstName" VARCHAR(50),
		"UA_CaseAdmin_LastName" VARCHAR(50),
		"Service_modality" TEXT(2147483647),
		"Patient_Zip" VARCHAR(50),
		"Practice_Zip" VARCHAR(50),
		"StateFS" FLOAT8,
		"Medicare" FLOAT8,
		"Walmart" FLOAT8,
		"NSP" FLOAT8
	);

CREATE TABLE "public"."in_fs_feeschedule_fl" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."audittemp" (
		"auditid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP,
		"uniquemodifydate" TIMESTAMP,
		"uniquemodifycomments" VARCHAR(400),
		"auditdate" TIMESTAMP,
		"tablename" VARCHAR(100),
		"fieldname" VARCHAR(100),
		"refid" VARCHAR(100),
		"valuechange" VARCHAR(50000),
		"modifier" VARCHAR(400),
		"comments" VARCHAR(500)
	);

CREATE TABLE "public"."tcvomaster" (
		"cvoid" SERIAL DEFAULT nextval('tcvomaster_cvoid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"cvoname" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(100) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"alertemail" VARCHAR(50) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"contactfirstname" VARCHAR(90) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"contactaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"contactaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"contactcity" VARCHAR(30) DEFAULT ''::character varying,
		"contactstateid" INT4 DEFAULT 0,
		"contactprovince" VARCHAR(100) DEFAULT ''::character varying,
		"contactzip" VARCHAR(50) DEFAULT ''::character varying,
		"contactcountryid" INT4 DEFAULT 0,
		"contactphone" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"masteradminid" INT4 DEFAULT 0,
		"cvotypeid" INT4 DEFAULT 0,
		"cvobillingid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."in_fs_feeschedule_oh" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tpracticetypeli" (
		"practicetypeid" INT4 NOT NULL,
		"practicetypelong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tsuppinterplan" (
		"suppinterplanid" SERIAL DEFAULT nextval('tsuppinterplan_suppinterplanid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(200) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"question1" INT4 DEFAULT 0,
		"question2" INT4 DEFAULT 0,
		"question3" INT4 DEFAULT 0,
		"question4" INT4 DEFAULT 0,
		"question5" INT4 DEFAULT 0,
		"question6" INT4 DEFAULT 0,
		"question7" INT4 DEFAULT 0,
		"question8" INT4 DEFAULT 0,
		"question9" INT4 DEFAULT 0,
		"question10" INT4 DEFAULT 0,
		"question11" INT4 DEFAULT 0,
		"question12" INT4 DEFAULT 0,
		"question13" INT4 DEFAULT 0,
		"question14" INT4 DEFAULT 0,
		"question15" INT4 DEFAULT 0,
		"question16" INT4 DEFAULT 0,
		"question17" INT4 DEFAULT 0,
		"question18" INT4 DEFAULT 0,
		"question19" INT4 DEFAULT 0,
		"question19a" VARCHAR(50) DEFAULT ''::character varying,
		"question20" INT4 DEFAULT 0,
		"question21" INT4 DEFAULT 0,
		"question22" VARCHAR(200) DEFAULT ''::character varying,
		"iagree" INT4 DEFAULT 0,
		"question23" INT4 DEFAULT 0,
		"question24" INT4 DEFAULT 0,
		"question25" INT4 DEFAULT 0,
		"question26" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tnim3_feeschedule" (
		"feescheduleid" SERIAL DEFAULT nextval('tnim3_feeschedule_feescheduleid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"feeschedulerefid" INT4 DEFAULT 0,
		"cpt" VARCHAR(100) DEFAULT ''::character varying,
		"cptmodifier" VARCHAR(100) DEFAULT ''::character varying,
		"billamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"allowamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"stateid" INT4 DEFAULT 0,
		"feescheduleregionid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."in_fs_feeschedule_in" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tdefendantstatusli" (
		"defendantstatusid" INT4 NOT NULL,
		"defendantstatusshort" VARCHAR(10) DEFAULT ''::character varying,
		"defendantstatuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_referralintake" (
		"referralintakeid" SERIAL DEFAULT nextval('tnim3_referralintake_referralintakeid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"actionid" INT4 DEFAULT 0,
		"virtualfilename" VARCHAR(200) DEFAULT ''::character varying,
		"referralsource" VARCHAR(20) DEFAULT ''::character varying,
		"adjname" VARCHAR(20) DEFAULT ''::character varying,
		"adjemail" VARCHAR(20) DEFAULT ''::character varying,
		"adjphone" VARCHAR(20) DEFAULT ''::character varying,
		"adjfax" VARCHAR(20) DEFAULT ''::character varying,
		"insurance" VARCHAR(20) DEFAULT ''::character varying,
		"claimnumber" VARCHAR(20) DEFAULT ''::character varying,
		"ncmname" VARCHAR(20) DEFAULT ''::character varying,
		"ncmemail" VARCHAR(20) DEFAULT ''::character varying,
		"ncmphone" VARCHAR(20) DEFAULT ''::character varying,
		"ptname" VARCHAR(20) DEFAULT ''::character varying,
		"ptdob" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"ptdoi" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"ptgender" VARCHAR(20) DEFAULT ''::character varying,
		"ptemployer" VARCHAR(20) DEFAULT ''::character varying,
		"ptaddress" VARCHAR(20) DEFAULT ''::character varying,
		"ptcity" VARCHAR(20) DEFAULT ''::character varying,
		"ptstate" VARCHAR(20) DEFAULT ''::character varying,
		"ptzip" VARCHAR(20) DEFAULT ''::character varying,
		"ptphone" VARCHAR(20) DEFAULT ''::character varying,
		"ptcell" VARCHAR(20) DEFAULT ''::character varying,
		"ptwork" VARCHAR(20) DEFAULT ''::character varying,
		"ptemail" VARCHAR(20) DEFAULT ''::character varying,
		"mdname" VARCHAR(20) DEFAULT ''::character varying,
		"mdphone" VARCHAR(20) DEFAULT ''::character varying,
		"mdfax" VARCHAR(20) DEFAULT ''::character varying,
		"followupappt" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"proceduretype" VARCHAR(20) DEFAULT ''::character varying,
		"bp" VARCHAR(20) DEFAULT ''::character varying,
		"ruleout" VARCHAR(20) DEFAULT ''::character varying,
		"flims" VARCHAR(20) DEFAULT ''::character varying,
		"cd" VARCHAR(20) DEFAULT ''::character varying,
		"age" VARCHAR(20) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_authorization" (
		"authorizationid" SERIAL DEFAULT nextval('tnim2_authorization_authorizationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"appointmentid" INT4 DEFAULT 0,
		"scanpass" VARCHAR(100) DEFAULT ''::character varying,
		"payerpreauthorizationconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"payerfinalauthorizationconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"payerclaimnumber" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianname" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianid" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianpreparedby" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianphone" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianfax" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianemail" VARCHAR(200) DEFAULT ''::character varying,
		"cpt1" VARCHAR(100) DEFAULT ''::character varying,
		"cpt2" VARCHAR(100) DEFAULT ''::character varying,
		"cpt3" VARCHAR(100) DEFAULT ''::character varying,
		"cpt4" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt1" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt2" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt3" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt4" VARCHAR(100) DEFAULT ''::character varying,
		"cpttext" VARCHAR(2000) DEFAULT ''::character varying,
		"rxfileid" INT4 DEFAULT 0,
		"reportfileid" INT4 DEFAULT 0,
		"comments" VARCHAR(2000) DEFAULT ''::character varying,
		"authorizationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"authorizationstatusid" INT4 DEFAULT 0,
		"orderfileid" INT4 DEFAULT 0,
		"nextactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nextactiontaskid" INT4 DEFAULT 0,
		"cpt1bodypart" VARCHAR(100) DEFAULT ''::character varying,
		"cpt2bodypart" VARCHAR(100) DEFAULT ''::character varying,
		"cpt3bodypart" VARCHAR(100) DEFAULT ''::character varying,
		"cpt4bodypart" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt1b" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt1c" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt1d" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt2b" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt2c" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt2d" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt3b" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt3c" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt3d" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt4b" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt4c" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt4d" VARCHAR(100) DEFAULT ''::character varying,
		"dicomid" INT4 DEFAULT 0,
		"auditnotes" VARCHAR(5000) DEFAULT ''::character varying,
		"isbilledid" INT4 DEFAULT 0,
		"receivedate" TIMESTAMP,
		"sentdaterefdoctor" TIMESTAMP
	);

CREATE TABLE "public"."in_fs_feeschedule_ny" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."export2" (
		"ScanPass" VARCHAR(255),
		"EncounterID" INT4,
		"ParentPayer" VARCHAR(255),
		"PayerBranch" VARCHAR(255),
		"PayerID" VARCHAR(255),
		"DateOfService" VARCHAR(255),
		"DateOfService_iMonth" VARCHAR(255),
		"DateOfService_iYear" VARCHAR(255),
		"ClaimNumber" VARCHAR(255),
		"Adjuster_FullName" VARCHAR(255),
		"AdjusterName_BranchName" VARCHAR(255),
		"AdjusterID" VARCHAR(255),
		"EncounterType" VARCHAR(255),
		"IsCourtesy_Status" VARCHAR(255),
		"UniqueCreateDate" TIMESTAMP,
		"ReceiveDate" VARCHAR(255),
		"TT_Scheduled" VARCHAR(255),
		"TA_Scheduled_Days" VARCHAR(255),
		"TA_Scheduled_Hours" VARCHAR(255),
		"TA_Init_App_Days" VARCHAR(255),
		"TA_Init_App_Hours" VARCHAR(255),
		"Practice_Name" VARCHAR(255),
		"Practice_City" VARCHAR(255),
		"Practice_State" VARCHAR(255),
		"Practice_Zip" VARCHAR(255),
		"Payer_BillDate" VARCHAR(255),
		"Provider_BilledAmount" VARCHAR(255),
		"First_CPT" VARCHAR(255),
		"Service_modality" VARCHAR(255),
		"First_CPT_Qty" VARCHAR(255),
		"Total_BillAmount" VARCHAR(255),
		"Total_AllowAmount" VARCHAR(255),
		"Total_AllowAmountAdjustment" VARCHAR(255),
		"Total_AllowAmount_Net" VARCHAR(255),
		"Total_AllowAmount_Unit_C2" VARCHAR(255),
		"Total_AllowAmount_Total_C2" VARCHAR(255),
		"Total_FeeSchedule_Unit_C2" VARCHAR(255),
		"Total_FeeSchedule_Total_C2" VARCHAR(255),
		"Total_Savings_FromFS_Net" VARCHAR(255),
		"Total_Savings_FromFS_Percentage" VARCHAR(255),
		"Total_PaidIn_Amount" VARCHAR(255),
		"Total_PaidIn_Amount_CheckCalc" VARCHAR(255),
		"Total_PaidOut_Amount_Expected" VARCHAR(255),
		"Total_Profit_Net" VARCHAR(255),
		"Client_Savings_Warning" VARCHAR(255),
		"Profit_Warning" VARCHAR(255),
		"Payment_Warning" VARCHAR(255),
		"FS Zero" VARCHAR(255),
		"UC" VARCHAR(255),
		"Medicare" VARCHAR(255),
		"serviceid" INT4,
		"service_uniquemodifydate" TIMESTAMP,
		"encounter_uniquemodifydate" TIMESTAMP
	);

CREATE TABLE "public"."thcoprivilegequestionlu" (
		"lookupid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"hcoid" INT4 DEFAULT 0,
		"questionid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcommunityforum" (
		"communityforumid" SERIAL DEFAULT nextval('tcommunityforum_communityforumid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"priorityid" INT4 DEFAULT 0,
		"forumtitle" VARCHAR(200) DEFAULT ''::character varying,
		"forumdescription" VARCHAR(4000) DEFAULT ''::character varying,
		"isviewable" INT4 DEFAULT 0,
		"ispostable" INT4 DEFAULT 0,
		"readpassword" VARCHAR(200) DEFAULT ''::character varying,
		"relatedlink" VARCHAR(1000) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_commreference" (
		"commreferenceid" SERIAL DEFAULT nextval('tnim3_commreference_commreferenceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"crid" INT4 DEFAULT 0,
		"refid" INT4 DEFAULT 0,
		"crtypeid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tphysicianeventlu" (
		"lookupid" SERIAL DEFAULT nextval('tphysicianeventlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"eventid" INT4 DEFAULT 0,
		"hcoid" INT4 DEFAULT 0,
		"eventtypeid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_wv" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_cptgroup" (
		"cptgroupid" SERIAL DEFAULT nextval('tnim3_cptgroup_cptgroupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"groupname" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_nj" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_crawford" (
		"claim_id" VARCHAR(20),
		"patient_number" VARCHAR(18),
		"claim_system" VARCHAR(50),
		"patient_abbr" VARCHAR(12),
		"patient_address_line1" VARCHAR(30),
		"patient_address_line2" VARCHAR(30),
		"patient_aww" FLOAT8,
		"patient_city" VARCHAR(20),
		"patient_comp_rate" FLOAT8,
		"patient_country" VARCHAR(30),
		"patient_first_name" VARCHAR(30),
		"patient_gender" VARCHAR(1),
		"patient_last_name" VARCHAR(30),
		"patient_middle_initial" VARCHAR(1),
		"patient_misc" VARCHAR(6),
		"patient_occupation" VARCHAR(50),
		"patient_phone" VARCHAR(10),
		"patient_phone_ext" VARCHAR(4),
		"patient_ssn" VARCHAR(9),
		"patient_state" VARCHAR(2),
		"patient_suffix" VARCHAR(3),
		"patient_weekly_rate" FLOAT8,
		"patient_work_phone" VARCHAR(10),
		"patient_work_phone_ext" VARCHAR(4),
		"patient_zip_code" VARCHAR(9),
		"patient_zip_ext" VARCHAR(4),
		"date_of_birth" DATE,
		"date_of_injury" DATE,
		"first_lost_date" DATE,
		"benefit_state" VARCHAR(2),
		"accident_address_line1" VARCHAR(30),
		"accident_address_line2" VARCHAR(30),
		"accident_cause" VARCHAR(40),
		"accident_city" VARCHAR(20),
		"accident_desc1" VARCHAR(40),
		"accident_desc2" VARCHAR(40),
		"accident_state" VARCHAR(2),
		"accident_zip_code" VARCHAR(10),
		"claim_accepted" VARCHAR(1),
		"claim_close_date" DATE,
		"claim_open_date" DATE,
		"claim_received_date" DATE,
		"claim_reclose_date" DATE,
		"claim_reopen_date" DATE,
		"claim_report_date" DATE,
		"adjuster_assigned_date" DATE,
		"adjuster_email_address" VARCHAR(80),
		"adjuster_first_name" VARCHAR(30),
		"adjuster_id" VARCHAR(10),
		"adjuster_last_name" VARCHAR(64),
		"adjuster_phone" VARCHAR(10),
		"adjuster_phone_ext" VARCHAR(5),
		"branch_address_line1" VARCHAR(30),
		"branch_address_line2" VARCHAR(30),
		"branch_city" VARCHAR(20),
		"branch_fax" VARCHAR(10),
		"branch_manager_name" VARCHAR(30),
		"branch_name" VARCHAR(40),
		"branch_phone" VARCHAR(10),
		"branch_state" VARCHAR(2),
		"branch_zip_code" VARCHAR(10),
		"date_of_hire" DATE,
		"employee_rpt_location_name" VARCHAR(30),
		"employee_rpt_wc_cntct_name" VARCHAR(30),
		"employee_rpt_wc_cntct_phone" VARCHAR(30),
		"employer_address_line1" VARCHAR(30),
		"employer_address_line2" VARCHAR(30),
		"employer_city" VARCHAR(20),
		"employer_federal_tax_id" VARCHAR(9),
		"employer_geo_address" VARCHAR(30),
		"employer_id_number" VARCHAR(10),
		"employer_name" VARCHAR(40),
		"employer_state" VARCHAR(2),
		"employer_zip_code" VARCHAR(9),
		"employment_status" VARCHAR(40),
		"equipment_in_use" VARCHAR(30),
		"fatality_ind" VARCHAR(1),
		"lob_code" VARCHAR(5),
		"loss_body_part_code" VARCHAR(16),
		"loss_cause_code" VARCHAR(16),
		"loss_coverage_code" VARCHAR(16),
		"loss_nature_code" VARCHAR(16),
		"loss_time" INT4,
		"result_of_accident" VARCHAR(40),
		"result_of_accident_code" VARCHAR(4),
		"return_to_work_date" DATE,
		"kind" VARCHAR(1),
		"vendor_name" VARCHAR(50)
	);

CREATE TABLE "public"."tnim3_document" (
		"documentid" SERIAL DEFAULT nextval('tnim3_document_documentid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"referralid" INT4 DEFAULT 0,
		"encounterid" INT4 DEFAULT 0,
		"serviceid" INT4 DEFAULT 0,
		"uploaduserid" INT4 DEFAULT 0,
		"referencedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"doctype" INT4 DEFAULT 0,
		"filename" VARCHAR(200) DEFAULT ''::character varying,
		"dcmfile" VARCHAR(200) DEFAULT ''::character varying,
		"reportsummary" VARCHAR(200) DEFAULT ''::character varying,
		"reporttext" VARCHAR(9000) DEFAULT ''::character varying,
		"reportdictationfile" VARCHAR(200) DEFAULT ''::character varying,
		"treatingphysician" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosticpacs" VARCHAR(100) DEFAULT ''::character varying,
		"emr" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysician" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"docname" VARCHAR(100),
		"document" BYTEA(2147483647)
	);

CREATE TABLE "public"."tnim_report" (
		"reportid" SERIAL DEFAULT nextval('tnim_report_reportid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"authorizationid" INT4 DEFAULT 0,
		"dcmfile" VARCHAR(100) DEFAULT ''::character varying,
		"reporttext" VARCHAR(100) DEFAULT ''::character varying,
		"reportdictationfile" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosticphysician" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosticpacs" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianid" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_directpatientcard" (
		"directpatientcardid" SERIAL DEFAULT nextval('tnim3_directpatientcard_directpatientcardid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"directreferralsourceid" INT4 DEFAULT 0,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"patientfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"patientlastname" VARCHAR(200) DEFAULT ''::character varying,
		"signupdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"sourcewebsite" VARCHAR(200) DEFAULT ''::character varying,
		"patientmrn" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ok" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."thcodistrictli" (
		"hcodistrictid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"hcodistrict" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcodivisionli" (
		"hcodivisionid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"hcodivision" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."texperiencetypeli" (
		"experienceid" INT4 NOT NULL,
		"experiencelong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_caseaccountuseraccountlu" (
		"lookupid" SERIAL DEFAULT nextval('tnim2_caseaccountuseraccountlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"statuscode" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."talertdatetrack" (
		"alertdateid" SERIAL DEFAULT nextval('talertdatetrack_alertdateid_seq'::regclass) NOT NULL,
		"alertdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."SelectMRI_qPayerMaster_Clients" (
		"ClientID" VARCHAR(255),
		"PayerName" VARCHAR(255),
		"QuickBooksID" VARCHAR(255),
		"BillingName" VARCHAR(255),
		"BillingAddress1" VARCHAR(255),
		"BillingAddress2" VARCHAR(255),
		"BillingCity" VARCHAR(255),
		"BillingStateID" VARCHAR(255),
		"BillingZIP" VARCHAR(255),
		"UniqueModifyDate" VARCHAR(255),
		"UniqueCreateDate" VARCHAR(255),
		"BillingPhone" VARCHAR(255),
		"BillingFax" VARCHAR(255),
		"SelectMRI_Notes" VARCHAR(255)
	);

CREATE TABLE "public"."tdruglist" (
		"druglistid" SERIAL DEFAULT nextval('tdruglist_druglistid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"drugid" INT4 DEFAULT 0,
		"drugname" VARCHAR(100) DEFAULT ''::character varying,
		"drugtype" VARCHAR(10) DEFAULT ''::character varying,
		"drugsubtype" VARCHAR(10) DEFAULT ''::character varying,
		"ndc11" VARCHAR(50) DEFAULT ''::character varying,
		"dosagetext" VARCHAR(50) DEFAULT ''::character varying,
		"dosagecode" VARCHAR(50) DEFAULT ''::character varying,
		"drugqty" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tsalesmansaleslu" (
		"lookupid" SERIAL DEFAULT nextval('tsalesmansaleslu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"salesmanid" INT4 DEFAULT 0,
		"salesid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."atpa_compare" (
		"Refno" VARCHAR(255),
		"FUND" VARCHAR(255),
		"Billing Provider" INT4,
		"BILL PROV ALPHA" VARCHAR(255),
		"BILL PROV ADD1" VARCHAR(255),
		"BILL PROV ADD2" VARCHAR(255),
		"BILL PROV ADD3" VARCHAR(255),
		"SVC PROV ALPHA" VARCHAR(255),
		"SVC PROV ADD1" VARCHAR(255),
		"SVC PROV ADD2" VARCHAR(255),
		"SVC PROV ADD3" VARCHAR(255),
		"Procedure" VARCHAR(255),
		"POS" INT4,
		"Bill Type" VARCHAR(255),
		"Billed Amount" VARCHAR(255),
		"Quantity" INT4,
		"id" INT4 NOT NULL,
		"mod" VARCHAR(255),
		"pid" VARCHAR(255),
		"nim_price" VARCHAR(255),
		"atpa_zip" VARCHAR(10)
	);

CREATE TABLE "public"."tbillingaccount" (
		"billingid" SERIAL DEFAULT nextval('tbillingaccount_billingid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"billingtype" VARCHAR(20) DEFAULT ''::character varying,
		"status" INT4 DEFAULT 0,
		"wsaccountnumber" VARCHAR(20) DEFAULT ''::character varying,
		"contactfirstname" VARCHAR(50) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"contactaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"contactaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"contactcity" VARCHAR(30) DEFAULT ''::character varying,
		"contactstateid" INT4 DEFAULT 0,
		"contactprovince" VARCHAR(100) DEFAULT ''::character varying,
		"contactzip" VARCHAR(10) DEFAULT ''::character varying,
		"contactcountryid" INT4 DEFAULT 0,
		"contactphone" VARCHAR(20) DEFAULT ''::character varying,
		"accountfullname" VARCHAR(100) DEFAULT ''::character varying,
		"accountnumber1" VARCHAR(20) DEFAULT ''::character varying,
		"accountnumber2" VARCHAR(20) DEFAULT ''::character varying,
		"accounttype" VARCHAR(20) DEFAULT ''::character varying,
		"accountexpmonth" VARCHAR(4) DEFAULT ''::character varying,
		"accountexpyear" VARCHAR(6) DEFAULT ''::character varying,
		"accountchargedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"accountpostdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"alertemail" VARCHAR(50) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"billingcomments" VARCHAR(200) DEFAULT ''::character varying,
		"debitamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"promocode" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ga" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tsupppractoptometry" (
		"supppractoptometryid" SERIAL DEFAULT nextval('tsupppractoptometry_supppractoptometryid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"practiceid" INT4 DEFAULT 0,
		"owndispensary" INT4 DEFAULT 0,
		"dispensecontactlenses" INT4 DEFAULT 0,
		"framesinstock" VARCHAR(20) DEFAULT ''::character varying,
		"specialtylowvision" INT4 DEFAULT 0,
		"specialtyrefracticesurgerycomanagement" INT4 DEFAULT 0,
		"specialtycontactlenses" INT4 DEFAULT 0,
		"specialtyvisiontherapy" INT4 DEFAULT 0,
		"specialtyprosthetics" INT4 DEFAULT 0,
		"specialtyother" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_schedulingtransaction" (
		"schedulingtransactionid" SERIAL DEFAULT nextval('tnim3_schedulingtransaction_schedulingtransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"practiceid" INT4 DEFAULT 0,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"compareaddress1" VARCHAR(100) DEFAULT ''::character varying,
		"compareaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"comparecity" VARCHAR(100) DEFAULT ''::character varying,
		"comparestateli" VARCHAR(100) DEFAULT ''::character varying,
		"comparestatecode" VARCHAR(100) DEFAULT ''::character varying,
		"comparezip" VARCHAR(100) DEFAULT ''::character varying,
		"comparecounty" VARCHAR(100) DEFAULT ''::character varying,
		"compareencounterrefid" INT4 DEFAULT 0,
		"totalresults" INT4 DEFAULT 0,
		"proceduredescription" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tinsuranceli" (
		"insuranceid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"name" VARCHAR(200) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"contactname" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_mo" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tencounterstatusli" (
		"encounterstatusid" INT4 NOT NULL,
		"descriptionlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_la" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."in_fs_feeschedule" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."in_fs_feeschedule_ky" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tcommtracktypeli" (
		"commtracktypeid" INT4 NOT NULL,
		"commtracktypeshort" VARCHAR(10) DEFAULT ''::character varying,
		"commtracktypelong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tresourcetypeli" (
		"resourcetypeid" INT4 NOT NULL,
		"resourcetypelong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tphysiciancategoryli" (
		"physiciancategoryid" INT4 NOT NULL,
		"categoryshort" VARCHAR(10) DEFAULT ''::character varying,
		"categorylong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcoprivilegecategorytypelu" (
		"lookupid" SERIAL DEFAULT nextval('thcoprivilegecategorytypelu_lookupid_seq'::regclass) NOT NULL,
		"hcoid" INT4 DEFAULT 0,
		"privilegecategoryid" INT4 DEFAULT 0,
		"privilegecategorydocument" VARCHAR(500) DEFAULT ''::character varying
	);

CREATE TABLE "public"."icd9" (
		"icd9ID" VARCHAR(255) NOT NULL,
		"icd9Desc" VARCHAR(255) NOT NULL
	);

CREATE TABLE "public"."thospitalli" (
		"hospitalid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"name" VARCHAR(200) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"contactname" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."treadstatusli" (
		"readstatusid" INT4 NOT NULL,
		"readstatuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcompanyhcolu" (
		"lookupid" SERIAL DEFAULT nextval('tcompanyhcolu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"companyid" INT4 DEFAULT 0,
		"hcoid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."schedulingstatsdata" (
		"createdate" TIMESTAMP DEFAULT now() NOT NULL,
		"category" VARCHAR(30) DEFAULT 'unknown'::character varying NOT NULL,
		"count" INT4 DEFAULT 0 NOT NULL
	);

CREATE TABLE "public"."tnim3_userpracticelu" (
		"lookupid" SERIAL DEFAULT nextval('tnim3_userpracticelu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"practiceid" INT4 DEFAULT 0,
		"ispreferred" INT4 DEFAULT 0,
		"isblocked" INT4 DEFAULT 0,
		"comments" VARCHAR(1000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."taudit" (
		"auditid" SERIAL DEFAULT nextval('taudit_auditid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"auditdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"tablename" VARCHAR(100) DEFAULT ''::character varying,
		"fieldname" VARCHAR(100) DEFAULT ''::character varying,
		"refid" VARCHAR(100) DEFAULT ''::character varying,
		"valuechange" VARCHAR(50000) DEFAULT ''::character varying,
		"modifier" VARCHAR(400) DEFAULT ''::character varying,
		"comments" VARCHAR(500) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_billingentity" (
		"billingentityid" SERIAL DEFAULT nextval('tnim3_billingentity_billingentityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(5000) DEFAULT ''::character varying,
		"entityname" VARCHAR(100) DEFAULT ''::character varying,
		"feeschedulerefid" INT4 DEFAULT 0,
		"feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"overridetypeid" INT4 DEFAULT 0,
		"overrideschedulerefid" INT4 DEFAULT 0,
		"overridepercentage" NUMERIC(9 , 2) DEFAULT 0.0
	);

CREATE TABLE "public"."tnim3_nidpromo" (
		"nidpromoid" SERIAL DEFAULT nextval('tnim3_nidpromo_nidpromoid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"weblink" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"isactive" INT4,
		"discounthighamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"discounthighpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"discountlowamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"discountlowpercentage" NUMERIC(9 , 2) DEFAULT 0.0
	);

CREATE TABLE "public"."in_fs_feeschedule_me" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_directreferralsource" (
		"directreferralsourceid" SERIAL DEFAULT nextval('tnim3_directreferralsource_directreferralsourceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"sourcename" VARCHAR(200) DEFAULT ''::character varying,
		"sourcephone" VARCHAR(200) DEFAULT ''::character varying,
		"sourcefax" VARCHAR(200) DEFAULT ''::character varying,
		"sourcelandingweb" VARCHAR(200) DEFAULT ''::character varying,
		"sourcewebsite" VARCHAR(200) DEFAULT ''::character varying,
		"sourcereferralcode" VARCHAR(200) DEFAULT ''::character varying,
		"externalid" VARCHAR(200) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."cmi_walmart_rc" (
		"fsid" VARCHAR(255),
		"rc" VARCHAR(255),
		"zip" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."thcophysicianstatusli" (
		"hcophysicianstatusid" INT4 NOT NULL,
		"hcophysicianstatus" VARCHAR(80) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tmanagedcareplan" (
		"planid" SERIAL DEFAULT nextval('tmanagedcareplan_planid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"plannumber" VARCHAR(100) DEFAULT ''::character varying,
		"status" VARCHAR(50) DEFAULT ''::character varying,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pendingdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"reasonforleaving" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ca" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_appointment" (
		"appointmentid" SERIAL DEFAULT nextval('tnim3_appointment_appointmentid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(5000) DEFAULT ''::character varying,
		"providerid" INT4 DEFAULT 0,
		"icid" INT4 DEFAULT 0,
		"istatus" INT4 DEFAULT 0,
		"appointmenttime" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actualtime" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"appointmentconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"scheduler" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(5000) DEFAULT ''::character varying,
		"initialencounterid" INT4 DEFAULT 0,
		"scheduler_userid" INT4 DEFAULT 0,
		"providerconfirmset" INT4 DEFAULT 0,
		"providerconfirmset_userid" INT4 DEFAULT 0,
		"providerappointmentconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"providerconfirmshow" INT4 DEFAULT 0,
		"providerconfirmshow_userid" INT4 DEFAULT 0,
		"providernotes" VARCHAR(5000) DEFAULT ''::character varying,
		"voidnotes" VARCHAR(5000) DEFAULT ''::character varying,
		"providerconfirmdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"providerconfirmshowdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientconfirmset" INT4 DEFAULT 0,
		"patientconfirmset_userid" INT4 DEFAULT 0,
		"patientconfirmdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone
	);

CREATE TABLE "public"."in_fs_feeschedule_nd" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tdocumenttypeli" (
		"documenttypeid" INT4 NOT NULL,
		"documenttypelong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tmri_modelli" (
		"mri_modelid" INT4 NOT NULL,
		"descriptionlong" VARCHAR(100) DEFAULT ''::character varying,
		"isopen" INT4 DEFAULT 0,
		"accomodatesclaus" INT4 DEFAULT 0,
		"teslastrength" VARCHAR(100) DEFAULT ''::character varying,
		"manufacturer" VARCHAR(100) DEFAULT ''::character varying,
		"model" VARCHAR(100) DEFAULT ''::character varying,
		"classlevel" INT4 DEFAULT 0
	);

CREATE TABLE "public"."in_fs_feeschedule_va" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tsalesstatusli" (
		"salesstatusid" INT4 NOT NULL,
		"salesstatuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tICD9_Codes" (
		"procedurecode" VARCHAR(30) NOT NULL,
		"longdescription" VARCHAR(100) NOT NULL,
		"shortdescription" VARCHAR(30) NOT NULL
	);

CREATE TABLE "public"."tnimwwretro" (
		"nimwwretroid" SERIAL DEFAULT nextval('tnimwwretro_nimwwretroid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP,
		"uniquemodifydate" TIMESTAMP,
		"uniquemodifycomments" VARCHAR(100),
		"caseid" INT4,
		"refid" INT4,
		"enid" INT4,
		"physicianid" INT4,
		"payerid" INT4,
		"providerid" INT4,
		"cptwizardid1" INT4,
		"cptwizardid2" INT4,
		"cptwizardid3" INT4,
		"cptwizardid4" INT4,
		"cpt1" VARCHAR(10),
		"cpt2" VARCHAR(10),
		"cpt3" VARCHAR(10),
		"cpt4" VARCHAR(10),
		"cpt5" VARCHAR(10),
		"cpt6" VARCHAR(10),
		"cpt7" VARCHAR(10),
		"cpt8" VARCHAR(10),
		"cpt9" VARCHAR(10),
		"cpt10" VARCHAR(10),
		"diagnosiscode1" VARCHAR(20),
		"diagnosiscode2" VARCHAR(20),
		"diagnosiscode3" VARCHAR(20),
		"diagnosiscode4" VARCHAR(20),
		"diagnosiscode5" VARCHAR(20),
		"diagnosiscode6" VARCHAR(20),
		"diagnosiscode7" VARCHAR(20),
		"diagnosiscode8" VARCHAR(20),
		"diagnosiscode9" VARCHAR(20),
		"diagnosiscode10" VARCHAR(20),
		"cptqty1" INT4,
		"cptqty2" INT4,
		"cptqty3" INT4,
		"cptqty4" INT4,
		"cptqty5" INT4,
		"cptqty6" INT4,
		"cptqty7" INT4,
		"cptqty8" INT4,
		"cptqty9" INT4,
		"cptqty10" INT4,
		"fstype" VARCHAR(20),
		"stateallowance" NUMERIC(9 , 2),
		"providercharge" NUMERIC(9 , 2),
		"networkallowance" NUMERIC(9 , 2),
		"networksavings" NUMERIC(9 , 2),
		"reasoncode" VARCHAR(20),
		"rejectdescription" VARCHAR(600),
		"statusid" INT4,
		"billingprovidertaxid" VARCHAR(50),
		"billingprovidername" VARCHAR(100),
		"renderingprovidertaxid" VARCHAR(50),
		"lineofbusiness" INT4,
		"margin" NUMERIC(9 , 2),
		"payerallow" NUMERIC(9 , 2),
		"origpdfname" VARCHAR(50)
	);

CREATE TABLE "public"."tnim3_patientaccount" (
		"patientid" SERIAL DEFAULT nextval('tnim3_patientaccount_patientid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"payeraccountidnumber" VARCHAR(100) DEFAULT ''::character varying,
		"patientfirstname" VARCHAR(100) DEFAULT ''::character varying,
		"patientlastname" VARCHAR(100) DEFAULT ''::character varying,
		"patientaccountnumber" VARCHAR(100) DEFAULT ''::character varying,
		"patientexpirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientisactive" INT4 DEFAULT 0,
		"patientdob" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientssn" VARCHAR(200) DEFAULT ''::character varying,
		"patientgender" VARCHAR(10) DEFAULT ''::character varying,
		"patientaddress1" VARCHAR(100) DEFAULT ''::character varying,
		"patientaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"patientcity" VARCHAR(100) DEFAULT ''::character varying,
		"patientstateid" INT4 DEFAULT 0,
		"patientzip" VARCHAR(50) DEFAULT ''::character varying,
		"patienthomephone" VARCHAR(50) DEFAULT ''::character varying,
		"patientworkphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientcellphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientccaccountfullname" VARCHAR(100) DEFAULT ''::character varying,
		"patientccaccountnumber1" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccountnumber2" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccounttype" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccountexpmonth" VARCHAR(4) DEFAULT ''::character varying,
		"patientccaccountexpyear" VARCHAR(6) DEFAULT ''::character varying,
		"comments" TEXT(2147483647) DEFAULT ''::character varying,
		"auditnotes" TEXT(2147483647) DEFAULT ''::character varying,
		"employerid" INT4 DEFAULT 0,
		"jobid" INT4 DEFAULT 0,
		"jobnotes" TEXT(2147483647) DEFAULT ''::character varying,
		"supervisorname" VARCHAR(100) DEFAULT ''::character varying,
		"supervisorphone" VARCHAR(50) DEFAULT ''::character varying,
		"supervisorfax" VARCHAR(50) DEFAULT ''::character varying
	);

CREATE TABLE "public"."qRegionCode_MC_2012" (
		"fsid" INT4 NOT NULL,
		"rc" VARCHAR(255) NOT NULL,
		"zip" INT4 NOT NULL,
		"effdate" DATE NOT NULL
	);

CREATE TABLE "public"."tuseraccount_copy" (
		"userid" SERIAL DEFAULT nextval('tuseraccount_userid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(500) DEFAULT ''::character varying,
		"plcid" INT4 DEFAULT 0,
		"startpage" VARCHAR(100) DEFAULT ''::character varying,
		"accounttype" VARCHAR(20) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"genericsecuritygroupid" INT4 DEFAULT 0,
		"referenceid" INT4 DEFAULT 0,
		"accesstype" INT4 DEFAULT 0,
		"status" INT4 DEFAULT 0,
		"logonusername" VARCHAR(200) DEFAULT ''::character varying,
		"logonuserpassword" VARCHAR(100) DEFAULT ''::character varying,
		"attestkeyword1" VARCHAR(50) DEFAULT ''::character varying,
		"attestkeyword2" VARCHAR(50) DEFAULT ''::character varying,
		"attestkeywordtemp1" VARCHAR(50) DEFAULT ''::character varying,
		"attestkeywordtemp2" VARCHAR(50) DEFAULT ''::character varying,
		"contactfirstname" VARCHAR(50) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(200) DEFAULT ''::character varying,
		"contactaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"contactaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"contactcity" VARCHAR(30) DEFAULT ''::character varying,
		"contactstateid" INT4 DEFAULT 0,
		"contactprovince" VARCHAR(100) DEFAULT ''::character varying,
		"contactzip" VARCHAR(50) DEFAULT ''::character varying,
		"contactcountryid" INT4 DEFAULT 0,
		"contactphone" VARCHAR(50) DEFAULT ''::character varying,
		"secretquestion" VARCHAR(50) DEFAULT ''::character varying,
		"secretanswer" VARCHAR(50) DEFAULT ''::character varying,
		"creditcardfullname" VARCHAR(100) DEFAULT ''::character varying,
		"creditcardnumber" VARCHAR(20) DEFAULT ''::character varying,
		"creditcardtype" VARCHAR(20) DEFAULT ''::character varying,
		"creditcardexpmonth" VARCHAR(4) DEFAULT ''::character varying,
		"creditcardexpyear" VARCHAR(6) DEFAULT ''::character varying,
		"creditcardchargedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"creditcardpostdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"alertemail" VARCHAR(50) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"billingcomments" VARCHAR(200) DEFAULT ''::character varying,
		"promocode" VARCHAR(20) DEFAULT ''::character varying,
		"companytype" INT4 DEFAULT 0,
		"companyname" VARCHAR(200) DEFAULT ''::character varying,
		"companyaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"companyaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"companycity" VARCHAR(30) DEFAULT ''::character varying,
		"companystateid" INT4 DEFAULT 0,
		"companyzip" VARCHAR(50) DEFAULT ''::character varying,
		"companyphone" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"billingid" INT4 DEFAULT 0,
		"phdbacknowledgementstatus" INT4 DEFAULT 0,
		"phdbletterheadstatus" INT4 DEFAULT 0,
		"taxid" VARCHAR(50) DEFAULT ''::character varying,
		"notusingphdbid" INT4 DEFAULT 0,
		"notusingphdbcomments" VARCHAR(4000) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"contactmobile" VARCHAR(60) DEFAULT ''::character varying,
		"contactfax" VARCHAR(60) DEFAULT ''::character varying,
		"companyfax" VARCHAR(60) DEFAULT ''::character varying,
		"managerid" INT4 DEFAULT 0,
		"comm_email_requiresattachement" INT4 DEFAULT 0,
		"comm_alerts_leveluser" INT4 DEFAULT 0,
		"comm_alerts_levelmanager" INT4 DEFAULT 0,
		"comm_alerts_levelbranch" INT4 DEFAULT 0,
		"comm_report_leveluser" INT4 DEFAULT 0,
		"comm_report_levelmanager" INT4 DEFAULT 0,
		"comm_report_levelbranch" INT4 DEFAULT 0,
		"comm_alerts2_leveluser" INT4 DEFAULT 0,
		"comm_alerts2_levelmanager" INT4 DEFAULT 0,
		"comm_alerts2_levelbranch" INT4 DEFAULT 0,
		"usernpi" VARCHAR(50) DEFAULT ''::character varying,
		"userstatelicense" VARCHAR(50) DEFAULT ''::character varying,
		"userstatelicensedesc" VARCHAR(10) DEFAULT ''::character varying,
		"selectmri_id" INT4 DEFAULT 0,
		"selectmri_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"melicense" VARCHAR(50) DEFAULT ''::character varying,
		"selectmri_usertype" VARCHAR(20) DEFAULT ''::character varying,
		"importantnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"emailalertnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"importantnotes" VARCHAR(2000) DEFAULT ''::character varying,
		"nim_usertype" VARCHAR(20) DEFAULT ''::character varying,
		"totalmonthlymris" INT4 DEFAULT 0,
		"expectedmonthlymris" INT4 DEFAULT 0,
		"data_rank" INT4 DEFAULT 0,
		"data_referrals_3monthaverage" INT4 DEFAULT 0,
		"data_referrals_alltime" INT4 DEFAULT 0,
		"data_referrals_analysistotal" INT4 DEFAULT 0,
		"data_referrals_analysisturnaroundaverage_tosched" NUMERIC(9 , 2) DEFAULT 0.0,
		"data_referrals_analysiscostsavings" NUMERIC(9 , 2) DEFAULT 0.0,
		"internal_primarycontact" INT4 DEFAULT 0,
		"lastcontacted" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"dateofbirth" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"personalinsights" VARCHAR(5000) DEFAULT ''::character varying,
		"allowasprimarycontact" INT4 DEFAULT 0,
		"contactemail2" VARCHAR(75) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tprofessionalassociationsli" (
		"professionalassociationsid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"societyname" VARCHAR(200) DEFAULT ''::character varying,
		"name" VARCHAR(200) DEFAULT ''::character varying,
		"address1" VARCHAR(200) DEFAULT ''::character varying,
		"address2" VARCHAR(90) DEFAULT ''::character varying,
		"city" VARCHAR(200) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"email" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_commtrack" (
		"commtrackid" SERIAL DEFAULT nextval('tnim2_commtrack_commtrackid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"authorizationid" INT4 DEFAULT 0,
		"intuserid" INT4 DEFAULT 0,
		"extuserid" INT4 DEFAULT 0,
		"commtypeid" INT4 DEFAULT 0,
		"commstart" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"commend" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"messagetext" VARCHAR(4000) DEFAULT ''::character varying,
		"messagename" VARCHAR(80) DEFAULT ''::character varying,
		"messagecompany" VARCHAR(80) DEFAULT ''::character varying,
		"messageemail" VARCHAR(200) DEFAULT ''::character varying,
		"messagephone" VARCHAR(50) DEFAULT ''::character varying,
		"messagefax" VARCHAR(50) DEFAULT ''::character varying,
		"alertstatuscode" INT4 DEFAULT 0,
		"comments" VARCHAR(2000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tdisclosurequestionli" (
		"questionid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"disclosurecategoryid" INT4 DEFAULT 0,
		"questiontext" VARCHAR(2000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."zip_code" (
		"zip_code" VARCHAR(5),
		"city" VARCHAR(200),
		"county" VARCHAR(200),
		"state_prefix" VARCHAR(2),
		"lat" FLOAT8,
		"lon" FLOAT8
	);

CREATE TABLE "public"."in_fs_regioncode_backup" (
		"fsid" VARCHAR(255),
		"rc" VARCHAR(255),
		"zip" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."fce_referral" (
		"refreferredbyfirst" VARCHAR(255),
		"refreferredbylast" VARCHAR(255),
		"refemail" VARCHAR(255),
		"refcompanyname" VARCHAR(255),
		"refaddress" VARCHAR(255),
		"refaddress2" VARCHAR(255),
		"refcity" VARCHAR(255),
		"refstate" VARCHAR(255),
		"refzipcode" VARCHAR(255),
		"refphone" VARCHAR(255),
		"reftollfreephone" VARCHAR(255),
		"refdiagnosis" VARCHAR(255),
		"refisadj" VARCHAR(255),
		"adjfirstname" VARCHAR(255),
		"adjlastname" VARCHAR(255),
		"adjaddress" VARCHAR(255),
		"adjaddress2" VARCHAR(255),
		"adjcity" VARCHAR(255),
		"adjstate" VARCHAR(255),
		"adjzipcode" VARCHAR(255),
		"adjemail" VARCHAR(255),
		"adjphone" VARCHAR(255),
		"ptfirstname" VARCHAR(255),
		"ptlastname" VARCHAR(255),
		"ptemail" VARCHAR(255),
		"ptaddress" VARCHAR(255),
		"ptcity" VARCHAR(255),
		"ptstate" VARCHAR(255),
		"ptzipcode" VARCHAR(255),
		"ptclaimnumber" VARCHAR(255),
		"ptdateofbirth" VARCHAR(255),
		"ptssn" VARCHAR(255),
		"ptphone" VARCHAR(255),
		"ptoccupation" VARCHAR(255),
		"ptjobdesc" VARCHAR(255),
		"pttypeofinjury" VARCHAR(255),
		"ptdateofinjury" VARCHAR(255),
		"pthowinjuryoccurred" VARCHAR(255),
		"pttypeofclaim" VARCHAR(255),
		"ptdiagnosiscode" VARCHAR(255),
		"ptdiagnosisdesc" VARCHAR(255),
		"ptgender" VARCHAR(255),
		"srisfunctionalcapacityevaluation" VARCHAR(255),
		"srisonedayreturntoworkfce" VARCHAR(255),
		"srisjobspecificfce" VARCHAR(255),
		"jobdescription" VARCHAR(255),
		"emcompanyname" VARCHAR(255),
		"ememail" VARCHAR(255),
		"emaddress" VARCHAR(255),
		"emcity" VARCHAR(255),
		"emstate" VARCHAR(255),
		"emzipcode" VARCHAR(255),
		"emphone" VARCHAR(255),
		"emcontactperson" VARCHAR(255),
		"osneedstransportation" VARCHAR(255),
		"oshavepreferredtransportationprovider" VARCHAR(255),
		"osisrequiretranslation" VARCHAR(255),
		"oshavepreferredtranslationprovider" VARCHAR(255),
		"oiireporttoreferralsource" VARCHAR(255),
		"oiireporttoadjuster" VARCHAR(255),
		"oiiinvoicetoadjuster" VARCHAR(255),
		"oiirx" VARCHAR(255),
		"oiimdinfo" VARCHAR(255),
		"oiiattorneyinfo" VARCHAR(255),
		"oiiapproxdateassessment" VARCHAR(255),
		"notes" VARCHAR(255)
	);

CREATE TABLE "public"."thcophysicianstanding" (
		"lookupid" SERIAL DEFAULT nextval('thcophysicianstanding_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(200) DEFAULT ''::character varying,
		"hcoid" INT4 DEFAULT 0,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"firstname" VARCHAR(200) DEFAULT ''::character varying,
		"middlename" VARCHAR(200) DEFAULT ''::character varying,
		"lastname" VARCHAR(200) DEFAULT ''::character varying,
		"licensenumber" VARCHAR(200) DEFAULT ''::character varying,
		"licensestate" VARCHAR(10) DEFAULT ''::character varying,
		"department" VARCHAR(200) DEFAULT ''::character varying,
		"fromdate" VARCHAR(100) DEFAULT ''::character varying,
		"todate" VARCHAR(100) DEFAULT ''::character varying,
		"status" VARCHAR(200) DEFAULT ''::character varying,
		"priviledgecomments" VARCHAR(4000) DEFAULT ''::character varying,
		"isarchived" INT4 DEFAULT 0,
		"specialty" VARCHAR(200) DEFAULT ''::character varying,
		"suffix" VARCHAR(100) DEFAULT ''::character varying,
		"title" VARCHAR(100) DEFAULT ''::character varying,
		"photofilename" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_icmaster" (
		"icid" SERIAL DEFAULT nextval('tnim2_icmaster_icid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"icname" VARCHAR(100) DEFAULT ''::character varying,
		"contactname" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(100) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(100) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"url" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"price_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0
	);

CREATE TABLE "public"."tpromocodeli" (
		"promocodeid" SERIAL DEFAULT nextval('tpromocodeli_promocodeid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"promocode" VARCHAR(50) DEFAULT ''::character varying,
		"price50" NUMERIC(9 , 2) DEFAULT 0.0,
		"price100" NUMERIC(9 , 2) DEFAULT 0.0,
		"price250" NUMERIC(9 , 2) DEFAULT 0.0,
		"price500" NUMERIC(9 , 2) DEFAULT 0.0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim_icmaster" (
		"icid" SERIAL DEFAULT nextval('tnim_icmaster_icid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"icname" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(100) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(100) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"url" VARCHAR(200) DEFAULT ''::character varying,
		"mrirate" NUMERIC(9 , 2) DEFAULT 0.0,
		"ctrate" NUMERIC(9 , 2) DEFAULT 0.0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"contactfirstname" VARCHAR(100),
		"contactlastname" VARCHAR(100),
		"contacttitle" VARCHAR(100),
		"contactowner" VARCHAR(100),
		"zip2" VARCHAR(100)
	);

CREATE TABLE "public"."in_fs_feeschedule_dc" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tattestr" (
		"attestrid" SERIAL DEFAULT nextval('tattestr_attestrid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"questionid" INT4 DEFAULT 0,
		"answer" INT4 DEFAULT 0,
		"details" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tvoidleakreasonli" (
		"voidleakreasonid" INT4 NOT NULL,
		"voidleakreasonshort" VARCHAR(10) DEFAULT ''::character varying,
		"voidleakreasonlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."nid_referral_transaction" (
		"referraltoken" VARCHAR(1000) DEFAULT ''::character varying NOT NULL,
		"createdate" TIMESTAMP DEFAULT now() NOT NULL,
		"modifydate" TIMESTAMP DEFAULT now() NOT NULL,
		"referral" VARCHAR(1000) DEFAULT ''::character varying NOT NULL,
		"cctransactionlist" VARCHAR(1000) DEFAULT ''::character varying NOT NULL,
		"typidid" INT4 DEFAULT 0 NOT NULL
	);

CREATE TABLE "public"."tnim3_employeraccount" (
		"employerid" SERIAL DEFAULT nextval('tnim3_employeraccount_employerid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"employername" VARCHAR(100) DEFAULT ''::character varying,
		"employeraddress1" VARCHAR(100) DEFAULT ''::character varying,
		"employeraddress2" VARCHAR(100) DEFAULT ''::character varying,
		"employercity" VARCHAR(100) DEFAULT ''::character varying,
		"employerstateid" INT4 DEFAULT 0,
		"employerzip" VARCHAR(50) DEFAULT ''::character varying,
		"employerfax" VARCHAR(50) DEFAULT ''::character varying,
		"employerworkphone" VARCHAR(50) DEFAULT ''::character varying,
		"employercellphone" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"auditnotes" TEXT(2147483647) DEFAULT ''::character varying
	);

CREATE TABLE "public"."CPTGroup" (
		"CPTGroup" VARCHAR(255) NOT NULL,
		"CPT" INT4 NOT NULL
	);

CREATE TABLE "public"."tlicensetypeli" (
		"licensetypeid" INT4 NOT NULL,
		"licensetypeshort" VARCHAR(10) DEFAULT ''::character varying,
		"licensetypelong" VARCHAR(50) DEFAULT ''::character varying
	);

CREATE TABLE "public"."texperience" (
		"experienceid" SERIAL DEFAULT nextval('texperience_experienceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"typeofexperience" INT4 DEFAULT 0,
		"other" VARCHAR(100) DEFAULT ''::character varying,
		"specialty" VARCHAR(100) DEFAULT ''::character varying,
		"datefrom" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"dateto" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nocomplete" VARCHAR(200) DEFAULT ''::character varying,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_eligibilityreference" (
		"eligibilityreferenceid" SERIAL DEFAULT nextval('tnim3_eligibilityreference_eligibilityreferenceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(200) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"caseclaimnumber" VARCHAR(100) DEFAULT ''::character varying,
		"clientpayerid" VARCHAR(32) DEFAULT ''::character varying,
		"payerindicator" VARCHAR(8) DEFAULT ''::character varying,
		"employername" VARCHAR(200) DEFAULT ''::character varying,
		"employerphone" VARCHAR(50) DEFAULT ''::character varying,
		"employeraddress1" VARCHAR(200) DEFAULT ''::character varying,
		"employeraddress2" VARCHAR(200) DEFAULT ''::character varying,
		"employercity" VARCHAR(200) DEFAULT ''::character varying,
		"employercounty" VARCHAR(200) DEFAULT ''::character varying,
		"employerstate" VARCHAR(2) DEFAULT ''::character varying,
		"employerzip" VARCHAR(20) DEFAULT ''::character varying,
		"datecaseopened" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"dateofinjury" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"attorneyfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"attorneylastname" VARCHAR(200) DEFAULT ''::character varying,
		"attorneyworkphone" VARCHAR(50) DEFAULT ''::character varying,
		"attorneyfax" VARCHAR(50) DEFAULT ''::character varying,
		"patientaccountnumber" VARCHAR(100) DEFAULT ''::character varying,
		"patientfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"patientlastname" VARCHAR(200) DEFAULT ''::character varying,
		"patientdob" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientssn" VARCHAR(20) DEFAULT ''::character varying,
		"patientgender" VARCHAR(100) DEFAULT ''::character varying,
		"patientaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"patientaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"patientcity" VARCHAR(200) DEFAULT ''::character varying,
		"patientstate" VARCHAR(2) DEFAULT ''::character varying,
		"patientzip" VARCHAR(20) DEFAULT ''::character varying,
		"patienthomephone" VARCHAR(50) DEFAULT ''::character varying,
		"patientworkphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientcellphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientemail" VARCHAR(200) DEFAULT ''::character varying,
		"corrusclaimid" VARCHAR(8) DEFAULT ''::character varying,
		"claimdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"clientclaimnum" VARCHAR(32) DEFAULT ''::character varying,
		"primarydiagcode" VARCHAR(8) DEFAULT ''::character varying,
		"diagcode2" VARCHAR(8) DEFAULT ''::character varying,
		"diagcode3" VARCHAR(8) DEFAULT ''::character varying,
		"claimstatus" VARCHAR(1) DEFAULT ''::character varying,
		"casemanagerfirstname" VARCHAR(64) DEFAULT ''::character varying,
		"casemanagerlastname" VARCHAR(64) DEFAULT ''::character varying,
		"accountreference" VARCHAR(4) DEFAULT ''::character varying,
		"controverted" VARCHAR(1) DEFAULT ''::character varying,
		"mmi" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"databaseid" VARCHAR(1) DEFAULT ''::character varying,
		"jurisdicationstate" VARCHAR(2) DEFAULT ''::character varying,
		"adjusterreferenceid" VARCHAR(200) DEFAULT ''::character varying,
		"adjusterfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"adjusterlastname" VARCHAR(200) DEFAULT ''::character varying,
		"adjusterphonenumber" VARCHAR(50) DEFAULT ''::character varying,
		"adjusterfaxnumber" VARCHAR(50) DEFAULT ''::character varying,
		"adjusteremail" VARCHAR(200) DEFAULT ''::character varying,
		"adjustercompany" VARCHAR(200) DEFAULT ''::character varying,
		"adjusteraddress1" VARCHAR(200) DEFAULT ''::character varying,
		"adjusteraddress2" VARCHAR(200) DEFAULT ''::character varying,
		"adjustercity" VARCHAR(200) DEFAULT ''::character varying,
		"adjusterstate" VARCHAR(2) DEFAULT ''::character varying,
		"adjusterzip" VARCHAR(20) DEFAULT ''::character varying,
		"referringdoctorreferenceid" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctornpi" VARCHAR(50) DEFAULT ''::character varying,
		"referringdoctormedicallicense" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctorfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctorlastname" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctorphonenumber" VARCHAR(50) DEFAULT ''::character varying,
		"referringdoctorfaxnumber" VARCHAR(50) DEFAULT ''::character varying,
		"referringdoctoremail" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctorcompany" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctoraddress1" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctoraddress2" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctorcity" VARCHAR(200) DEFAULT ''::character varying,
		"referringdoctorstate" VARCHAR(2) DEFAULT ''::character varying,
		"referringdoctorzip" VARCHAR(20) DEFAULT ''::character varying,
		"notes" VARCHAR(5000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tservicestatusli" (
		"servicestatusid" INT4 NOT NULL,
		"servicestatusshort" VARCHAR(10) DEFAULT ''::character varying,
		"servicestatuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ks" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_commtrack" (
		"commtrackid" SERIAL DEFAULT nextval('tnim3_commtrack_commtrackid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"authorizationid" INT4 DEFAULT 0,
		"intuserid" INT4 DEFAULT 0,
		"extuserid" INT4 DEFAULT 0,
		"commtypeid" INT4 DEFAULT 0,
		"commstart" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"commend" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"messagetext" VARCHAR(9000) DEFAULT ''::character varying,
		"messagename" VARCHAR(200) DEFAULT ''::character varying,
		"messagecompany" VARCHAR(200) DEFAULT ''::character varying,
		"messageemail" VARCHAR(200) DEFAULT ''::character varying,
		"messagephone" VARCHAR(50) DEFAULT ''::character varying,
		"messagefax" VARCHAR(50) DEFAULT ''::character varying,
		"alertstatuscode" INT4 DEFAULT 0,
		"comments" VARCHAR(9000) DEFAULT ''::character varying,
		"encounterid" INT4 DEFAULT 0,
		"referralid" INT4 DEFAULT 0,
		"serviceid" INT4 DEFAULT 0,
		"commreferenceid" INT4 DEFAULT 0,
		"link_userid" INT4 DEFAULT 0,
		"messagesubject" VARCHAR(500) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tprivilegecategorytypeli" (
		"privilegecategoryid" INT4 NOT NULL,
		"privilegecategoryshort" VARCHAR(100) DEFAULT ''::character varying,
		"privilegecategorylong" VARCHAR(1000) DEFAULT ''::character varying,
		"privilegecategorydocument" VARCHAR(500) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim_appointmentlu" (
		"lookupid" SERIAL DEFAULT nextval('tnim_appointmentlu_appointmentid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"appointmenttime" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"icid" INT4 DEFAULT 0,
		"authorizationid" INT4 DEFAULT 0,
		"appointmentconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"scheduler" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tadminpracticerelationshiptypeli" (
		"adminpracticerelationshiptypeid" INT4 NOT NULL,
		"descriptionlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcocontractstatusli" (
		"hcocontractstatusid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"hcocontractstatus" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_sd" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tphysicianformlu" (
		"lookupid" SERIAL DEFAULT nextval('tphysicianformlu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"formid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."ticdli" (
		"icdcodeid" VARCHAR(30) NOT NULL,
		"longdescription" VARCHAR(100) NOT NULL,
		"shortdescription" VARCHAR(30) NOT NULL
	);

CREATE TABLE "public"."in_fs_feeschedule_backup" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tcommunitytopic" (
		"communitytopicid" SERIAL DEFAULT nextval('tcommunitytopic_communitytopicid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"communityforumid" INT4 DEFAULT 0,
		"refcommunitytopicid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"topictype" INT4 DEFAULT 0,
		"priorityid" INT4 DEFAULT 0,
		"lastreplydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"includelink" VARCHAR(1000) DEFAULT ''::character varying,
		"topictitle" VARCHAR(200) DEFAULT ''::character varying,
		"topicbody" VARCHAR(4000) DEFAULT ''::character varying,
		"isviewable" INT4 DEFAULT 0,
		"ispostable" INT4 DEFAULT 0,
		"readpassword" VARCHAR(200) DEFAULT ''::character varying,
		"relatedlink" VARCHAR(1000) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tlicenseboardli" (
		"licenseboardid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"name" VARCHAR(200) DEFAULT ''::character varying,
		"address1" VARCHAR(70) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."trislinqtransaction" (
		"rislinqtransactionid" SERIAL DEFAULT nextval('trislinqtransaction_rislinqtransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"scanpass" VARCHAR(200) DEFAULT ''::character varying,
		"virtualfilename" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tresource" (
		"resourceid" SERIAL DEFAULT nextval('tresource_resourceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"resourcetypeid" INT4 DEFAULT 0,
		"resourcename" VARCHAR(90) DEFAULT ''::character varying,
		"resourcelink" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim_patientaccount" (
		"patientid" SERIAL DEFAULT nextval('tnim_patientaccount_patientid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"patientname" VARCHAR(100) DEFAULT ''::character varying,
		"accountnumber" VARCHAR(100) DEFAULT ''::character varying,
		"expirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"isactive" INT4 DEFAULT 0,
		"dob" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"address1" VARCHAR(100) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(100) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"accountfullname" VARCHAR(100) DEFAULT ''::character varying,
		"accountnumber1" VARCHAR(20) DEFAULT ''::character varying,
		"accountnumber2" VARCHAR(20) DEFAULT '0'::character varying,
		"accounttype" VARCHAR(20) DEFAULT ''::character varying,
		"accountexpmonth" VARCHAR(4) DEFAULT ''::character varying,
		"accountexpyear" VARCHAR(6) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"patientfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"patientlastname" VARCHAR(200) DEFAULT ''::character varying,
		"ssn" VARCHAR(20) DEFAULT ''::character varying,
		"gender" VARCHAR(20) DEFAULT ''::character varying,
		"homephone" VARCHAR(50) DEFAULT ''::character varying,
		"workphone" VARCHAR(50) DEFAULT ''::character varying,
		"cellphone" VARCHAR(50) DEFAULT ''::character varying,
		"injurycomments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tuaalertsli" (
		"uaalertsid" INT4 NOT NULL,
		"uaalertslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tpracticeactivity" (
		"practiceactivityid" SERIAL DEFAULT nextval('tpracticeactivity_practiceactivityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"practiceid" INT4 DEFAULT 0,
		"adminid" INT4 DEFAULT 0,
		"name" VARCHAR(200) DEFAULT ''::character varying,
		"activitytype" VARCHAR(200) DEFAULT ''::character varying,
		"iscompleted" INT4 DEFAULT 0,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"reminddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"description" VARCHAR(200) DEFAULT ''::character varying,
		"taskfilereference" VARCHAR(90) DEFAULT ''::character varying,
		"tasklogicreference" VARCHAR(90) DEFAULT ''::character varying,
		"summary" VARCHAR(5000) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"referenceid" INT4 DEFAULT 0,
		"doculinkid" INT4 DEFAULT 0,
		"assignedtoid" INT4
	);

CREATE TABLE "public"."tspecialtystatusli" (
		"specialtystatusid" INT4 NOT NULL,
		"longstatus" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."nidnamecontest" (
		"nidid" SERIAL DEFAULT nextval('nidnamecontest_nidid_seq'::regclass) NOT NULL,
		"nidname" VARCHAR(255)
	);

CREATE TABLE "public"."tnim3_caseaccount" (
		"caseid" SERIAL DEFAULT nextval('tnim3_caseaccount_caseid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(2000) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 23,
		"casecode" VARCHAR(100) DEFAULT ''::character varying,
		"caseclaimnumber" VARCHAR(100) DEFAULT ''::character varying,
		"adjusterid" INT4 DEFAULT 0,
		"employername" VARCHAR(500) DEFAULT ''::character varying,
		"employerphone" VARCHAR(100) DEFAULT ''::character varying,
		"dateofinjury" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"injurydescription" VARCHAR(9000) DEFAULT ''::character varying,
		"nursecasemanagerid" INT4 DEFAULT 0,
		"patientaccountid" INT4 DEFAULT 0,
		"comments" VARCHAR(9000) DEFAULT ''::character varying,
		"auditnotes" VARCHAR(9000) DEFAULT ''::character varying,
		"attorneyfirstname" VARCHAR(100) DEFAULT ''::character varying,
		"attorneylastname" VARCHAR(100) DEFAULT ''::character varying,
		"attorneyaddress1" VARCHAR(100) DEFAULT ''::character varying,
		"attorneyaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"attorneycity" VARCHAR(100) DEFAULT ''::character varying,
		"attorneystateid" INT4 DEFAULT 0,
		"attorneyzip" VARCHAR(50) DEFAULT ''::character varying,
		"attorneyfax" VARCHAR(50) DEFAULT ''::character varying,
		"attorneyworkphone" VARCHAR(50) DEFAULT ''::character varying,
		"attorneycellphone" VARCHAR(50) DEFAULT ''::character varying,
		"employerfax" VARCHAR(100) DEFAULT ''::character varying,
		"casestatusid" INT4 DEFAULT 0,
		"occmedpatientid" VARCHAR(100) DEFAULT ''::character varying,
		"wcbnumber" VARCHAR(100),
		"lodid" INT4 DEFAULT 0,
		"jobid" INT4 DEFAULT 0,
		"jobnotes" VARCHAR(1000) DEFAULT ''::character varying,
		"payeraccountidnumber" VARCHAR(100) DEFAULT ''::character varying,
		"patientfirstname" VARCHAR(100) DEFAULT ''::character varying,
		"patientlastname" VARCHAR(100) DEFAULT ''::character varying,
		"patientaccountnumber" VARCHAR(100) DEFAULT ''::character varying,
		"patientexpirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientisactive" INT4 DEFAULT 0,
		"patientdob" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientssn" VARCHAR(50) DEFAULT ''::character varying,
		"patientgender" VARCHAR(10) DEFAULT ''::character varying,
		"patientaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"patientaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"patientcity" VARCHAR(100) DEFAULT ''::character varying,
		"patientstateid" INT4 DEFAULT 0,
		"patientzip" VARCHAR(50) DEFAULT ''::character varying,
		"patienthomephone" VARCHAR(50) DEFAULT ''::character varying,
		"patientworkphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientcellphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientccaccountfullname" VARCHAR(100) DEFAULT ''::character varying,
		"patientccaccountnumber1" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccountnumber2" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccounttype" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccountexpmonth" VARCHAR(4) DEFAULT ''::character varying,
		"patientccaccountexpyear" VARCHAR(6) DEFAULT ''::character varying,
		"patientisclaus" INT4 DEFAULT 0,
		"patientishwratio" INT4 DEFAULT 0,
		"caseadministratorid" INT4 DEFAULT 0,
		"billtocontactid" INT4 DEFAULT 0,
		"urcontactid" INT4 DEFAULT 0,
		"referredbycontactid" INT4 DEFAULT 0,
		"assignedtoid" INT4 DEFAULT 0,
		"assignedtoroleid" INT4 DEFAULT 0,
		"patienthasimplants" INT4 DEFAULT 0,
		"patienthasmetalinbody" INT4 DEFAULT 0,
		"patienthasallergies" INT4 DEFAULT 0,
		"patienthasrecentsurgery" INT4 DEFAULT 0,
		"patienthaspreviousmris" INT4 DEFAULT 0,
		"patienthaskidneyliverhypertensiondiabeticconditions" INT4 DEFAULT 0,
		"patientispregnant" INT4 DEFAULT 0,
		"patientheight" VARCHAR(10) DEFAULT ''::character varying,
		"patientweight" INT4 DEFAULT 0,
		"patienthasimplantsdesc" VARCHAR(80) DEFAULT ''::character varying,
		"patienthasmetalinbodydesc" VARCHAR(80) DEFAULT ''::character varying,
		"patienthasallergiesdesc" VARCHAR(80) DEFAULT ''::character varying,
		"patienthasrecentsurgerydesc" VARCHAR(80) DEFAULT ''::character varying,
		"patienthaspreviousmrisdesc" VARCHAR(80) DEFAULT ''::character varying,
		"patienthaskidneyliverhypertensiondiabeticconditionsdesc" VARCHAR(80) DEFAULT ''::character varying,
		"patientispregnantdesc" VARCHAR(80) DEFAULT ''::character varying,
		"caseadministrator2id" INT4 DEFAULT 0,
		"caseadministrator3id" INT4 DEFAULT 0,
		"caseadministrator4id" INT4 DEFAULT 0,
		"employerlocationid" INT4 DEFAULT 0,
		"patientemail" VARCHAR(250) DEFAULT ''::character varying,
		"mobilephonecarrier" VARCHAR(50) DEFAULT ''::character varying,
		"iscurrentlyonmeds" INT4 DEFAULT 0,
		"iscurrentlyonmedsdesc" VARCHAR(80) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_sc" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."qExport_RegionCode" (
		"FSID" VARCHAR(255),
		"RC" VARCHAR(255),
		"zip_code" VARCHAR(255),
		"effdate" VARCHAR(255)
	);

CREATE TABLE "public"."tupdateitemstatusli" (
		"updateitemstatusid" INT4 NOT NULL,
		"updateitemstatustext" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."nidvote" (
		"nidvote" SERIAL DEFAULT nextval('nidvote_nidvote_seq'::regclass) NOT NULL,
		"schid" INT4
	);

CREATE TABLE "public"."zip_code_old1" (
		"id" SERIAL DEFAULT nextval('zip_code_id_seq'::regclass) NOT NULL,
		"zip_code" VARCHAR(5),
		"city" VARCHAR(50),
		"county" VARCHAR(50),
		"state_name" VARCHAR(50),
		"state_prefix" VARCHAR(2),
		"area_code" VARCHAR(3),
		"time_zone" VARCHAR(50),
		"lat" FLOAT8 NOT NULL,
		"lon" FLOAT8 NOT NULL
	);

CREATE TABLE "public"."tdocumentmanagement" (
		"documentid" SERIAL DEFAULT nextval('tdocumentmanagement_documentid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"documenttypeid" INT4 DEFAULT 0,
		"documentname" VARCHAR(200) DEFAULT ''::character varying,
		"documentfilename" VARCHAR(100) DEFAULT ''::character varying,
		"datereceived" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"dateofexpiration" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"datealertsent" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"archived" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcoeventattesttransaction" (
		"hcoeventattesttransactionid" SERIAL DEFAULT nextval('thcoeventattesttransaction_hcoeventattesttransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"eventid" INT4 DEFAULT 0,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"attestcomments" VARCHAR(200) DEFAULT ''::character varying,
		"approvalstatusid" INT4 DEFAULT 0,
		"approvalcomments" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_feescheduleref" (
		"feeschedulerefid" SERIAL DEFAULT nextval('tnim2_feescheduleref_feeschedulerefid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"refname" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_nm" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tprofessionalmisconduct" (
		"professionalmisconductid" SERIAL DEFAULT nextval('tprofessionalmisconduct_professionalmisconductid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"reviewboardname" VARCHAR(90) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(95) DEFAULT ''::character varying,
		"inicidentdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"suitdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"inicidentlocation" VARCHAR(100) DEFAULT ''::character varying,
		"patientrelationship" VARCHAR(100) DEFAULT ''::character varying,
		"allegation" VARCHAR(200) DEFAULT ''::character varying,
		"findings" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_hi" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."ACR_Pivot_Table" (
		"state" VARCHAR(255),
		"Facility Name" VARCHAR(255),
		"Phone Number" VARCHAR(255),
		"Street 1" VARCHAR(255),
		"city" VARCHAR(255),
		"Zip Code" VARCHAR(255),
		"Expiration Date" VARCHAR(255),
		"Facility NPI List" VARCHAR(255)
	);

CREATE TABLE "public"."tysonbilling" (
		"tysonbillingid" SERIAL DEFAULT nextval('tysonbilling_tysonbillingid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT now(),
		"uniquemodifydate" TIMESTAMP DEFAULT now(),
		"uniquemoadifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"actionid" INT4 DEFAULT 0,
		"referralid" INT4 DEFAULT 0,
		"userapproveid" INT4 DEFAULT 0,
		"generatedbillname" VARCHAR(1000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tfacilityaffiliation" (
		"affiliationid" SERIAL DEFAULT nextval('tfacilityaffiliation_affiliationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"isprimary" INT4 DEFAULT 0,
		"appointmentlevel" VARCHAR(50) DEFAULT ''::character varying,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pendingdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"facilityname" VARCHAR(100) DEFAULT ''::character varying,
		"facilitydepartment" VARCHAR(50) DEFAULT ''::character varying,
		"facilityaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"facilityaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"facilitycity" VARCHAR(30) DEFAULT ''::character varying,
		"facilitystateid" INT4 DEFAULT 0,
		"facilityprovince" VARCHAR(100) DEFAULT ''::character varying,
		"facilityzip" VARCHAR(50) DEFAULT ''::character varying,
		"facilitycountryid" INT4 DEFAULT 0,
		"facilityphone" VARCHAR(50) DEFAULT ''::character varying,
		"facilityfax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"reasonforleaving" VARCHAR(200) DEFAULT ''::character varying,
		"admissionpriviledges" INT4 DEFAULT 0,
		"admissionarrangements" VARCHAR(100) DEFAULT ''::character varying,
		"unrestrictedadmission" INT4 DEFAULT 0,
		"temppriviledges" INT4 DEFAULT 0,
		"inpatientcare" INT4 DEFAULT 0,
		"percentadmissions" VARCHAR(15) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ct" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tschoolli" (
		"schoolid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"name" VARCHAR(200) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."talliedhealthprofessional" (
		"alliedid" SERIAL DEFAULT nextval('talliedhealthprofessional_alliedid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"practiceid" INT4 DEFAULT 0,
		"firstname" VARCHAR(90) DEFAULT ''::character varying,
		"lastname" VARCHAR(90) DEFAULT ''::character varying,
		"specialtyid" VARCHAR(50) DEFAULT ''::character varying,
		"licensenumber" VARCHAR(50) DEFAULT ''::character varying,
		"expirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."payerexceptionlist" (
		"payerexceptionlistid" SERIAL DEFAULT nextval('payerexceptionlist_payerexceptionlistid_seq'::regclass) NOT NULL,
		"createdate" TIMESTAMP DEFAULT now(),
		"modifydate" TIMESTAMP DEFAULT now(),
		"payerid" INT4 DEFAULT 0,
		"practiceid" INT4 DEFAULT 0,
		"cpts" TEXT(2147483647),
		"desccpt" INT4,
		"fsid" VARCHAR(10),
		"fsidpercentage" NUMERIC(9 , 2) DEFAULT 0,
		"feetype" VARCHAR(20) DEFAULT 0
	);

CREATE TABLE "public"."tsalesaccount" (
		"salesid" SERIAL DEFAULT nextval('tsalesaccount_salesid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"salesmanid" INT4 DEFAULT 0,
		"accounttype" VARCHAR(20) DEFAULT ''::character varying,
		"status" INT4 DEFAULT 0,
		"firstname" VARCHAR(90) DEFAULT ''::character varying,
		"lastname" VARCHAR(90) DEFAULT ''::character varying,
		"companyname" VARCHAR(90) DEFAULT ''::character varying,
		"totalactivepractitioners" INT4 DEFAULT 0,
		"estimatedletter" INT4 DEFAULT 0,
		"email" VARCHAR(95) DEFAULT ''::character varying,
		"address1" VARCHAR(90) DEFAULT ''::character varying,
		"address2" VARCHAR(90) DEFAULT ''::character varying,
		"city" VARCHAR(90) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(2000) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"hcoid" INT4 DEFAULT 0,
		"billingid" INT4 DEFAULT 0,
		"nextactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone
	);

CREATE TABLE "public"."tnim3_service" (
		"serviceid" SERIAL DEFAULT nextval('tnim3_service_serviceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"encounterid" INT4 DEFAULT 0,
		"referringphysicianid" INT4 DEFAULT 0,
		"attendingphysicianid" INT4 DEFAULT 0,
		"servicetypeid" INT4 DEFAULT 0,
		"dateofservice" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"cpt" VARCHAR(100) DEFAULT ''::character varying,
		"cptmodifier" VARCHAR(100) DEFAULT ''::character varying,
		"cptbodypart" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt1" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt2" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt3" VARCHAR(100) DEFAULT ''::character varying,
		"dcpt4" VARCHAR(100) DEFAULT ''::character varying,
		"cpttext" VARCHAR(1000) DEFAULT ''::character varying,
		"billamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"allowamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"receivedamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"paidoutamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"reportfileid" INT4 DEFAULT 0,
		"comments" VARCHAR(9000) DEFAULT ''::character varying,
		"auditnotes" VARCHAR(9000) DEFAULT ''::character varying,
		"cptwizardid" INT4 DEFAULT 0,
		"receivedcheck1number" VARCHAR(20) DEFAULT ''::character varying,
		"receivedcheck1amount" NUMERIC(9 , 2) DEFAULT 0.0,
		"receivedcheck1date" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"receivedcheck2number" VARCHAR(20) DEFAULT ''::character varying,
		"receivedcheck2amount" NUMERIC(9 , 2) DEFAULT 0.0,
		"receivedcheck2date" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"receivedcheck3number" VARCHAR(20) DEFAULT ''::character varying,
		"receivedcheck3amount" NUMERIC(9 , 2) DEFAULT 0.0,
		"receivedcheck3date" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"cptqty" INT4 DEFAULT 0,
		"allowamountadjustment" NUMERIC(9 , 2) DEFAULT 0.0,
		"servicestatusid" INT4 DEFAULT 0,
		"dicom_accessionnumber" VARCHAR(200) DEFAULT ''::character varying,
		"dicom_showimage" INT4 DEFAULT 0,
		"dicom_mrn" VARCHAR(50) DEFAULT ''::character varying,
		"dicom_pn" VARCHAR(90) DEFAULT ''::character varying,
		"dicom_studyid" VARCHAR(50) DEFAULT ''::character varying,
		"readingphysicianid" INT4 DEFAULT 0,
		"reference_providercontractamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"reference_providerbilledamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"reference_feescheduleamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"reference_ucamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"reference_payerbillamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"reference_payercontractamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"billingnotes" VARCHAR(500) DEFAULT ''::character varying,
		"invoicenotes_payer" VARCHAR(500) DEFAULT ''::character varying,
		"invoicenotes_provider" VARCHAR(500) DEFAULT ''::character varying,
		"cptqty_bill" INT4 DEFAULT 0,
		"cptqty_pay" INT4 DEFAULT 0,
		"state_fs" NUMERIC(9 , 2) DEFAULT 0,
		"payer_fs" NUMERIC(9 , 2) DEFAULT 0,
		"uandc_fs" NUMERIC(9 , 2) DEFAULT 0
	);

CREATE TABLE "public"."in_fs_feeschedule_de" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tupdateitemcategoryli" (
		"updateitemcategoryid" INT4 NOT NULL,
		"updateitemcategorytext" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tprivilegequestionli" (
		"questionid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"privilegecategoryid" INT4 DEFAULT 0,
		"questiontext" VARCHAR(2000) DEFAULT ''::character varying,
		"initialcriteria" VARCHAR(2000) DEFAULT ''::character varying,
		"recredcriteria" VARCHAR(2000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_servicebillingtransaction" (
		"servicebillingtransactionid" SERIAL DEFAULT nextval('tnim3_servicebillingtransaction_servicebillingtransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"serviceid" INT4 DEFAULT 0,
		"servicebillingtransactiontypeid" INT4 DEFAULT 0,
		"exporteddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"transactiontitle" VARCHAR(90) DEFAULT ''::character varying,
		"transactionnote" VARCHAR(90) DEFAULT ''::character varying,
		"transactionamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"referencenumber" VARCHAR(60) DEFAULT ''::character varying,
		"referencedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"referencenotes" VARCHAR(200) DEFAULT ''::character varying,
		"detail_item_qty" INT4 DEFAULT 0,
		"generated_userid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"exportcomments" VARCHAR(200) DEFAULT ''::character varying,
		"billeddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"billcomments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_salesportal" (
		"salesportalid" SERIAL DEFAULT nextval('tnim3_salesportal_salesportalid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"statusid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"trackingid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tnim3_payermaster" (
		"payerid" SERIAL DEFAULT nextval('tnim3_payermaster_payerid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT now(),
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"payername" VARCHAR(200) DEFAULT ''::character varying,
		"contactname" VARCHAR(100) DEFAULT ''::character varying,
		"billingaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"billingaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"billingcity" VARCHAR(100) DEFAULT ''::character varying,
		"billingstateid" INT4 DEFAULT 0,
		"billingzip" VARCHAR(50) DEFAULT ''::character varying,
		"billingphone" VARCHAR(50) DEFAULT ''::character varying,
		"billingfax" VARCHAR(50) DEFAULT ''::character varying,
		"billingemail" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"billingentityid" INT4 DEFAULT 0,
		"payertypeid" INT4 DEFAULT 0,
		"importantnotes" VARCHAR(9000) DEFAULT ''::character varying,
		"payableto" VARCHAR(100) DEFAULT ''::character varying,
		"price_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"feeschedulerefid" INT4 DEFAULT 0,
		"feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"feeschedule2refid" INT4 DEFAULT 0,
		"feepercentage2" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"billprice_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"feepercentagebill" NUMERIC(9 , 2) DEFAULT 0.0,
		"feepercentage2bill" NUMERIC(9 , 2) DEFAULT 0.0,
		"billingtypeid" INT4 DEFAULT 0,
		"acceptsemgcaserate" INT4 DEFAULT 0,
		"parentpayerid" INT4 DEFAULT 0,
		"selectmri_id" INT4 DEFAULT 0,
		"selectmri_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"selectmri_qbid" VARCHAR(50) DEFAULT ''::character varying,
		"billingname" VARCHAR(100) DEFAULT ''::character varying,
		"officeaddress1" VARCHAR(100) DEFAULT ''::character varying,
		"officeaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"officecity" VARCHAR(100) DEFAULT ''::character varying,
		"officestateid" INT4 DEFAULT 0,
		"officezip" VARCHAR(50) DEFAULT ''::character varying,
		"officephone" VARCHAR(50) DEFAULT ''::character varying,
		"officefax" VARCHAR(50) DEFAULT ''::character varying,
		"officeemail" VARCHAR(50) DEFAULT ''::character varying,
		"salesdivision" VARCHAR(10) DEFAULT ''::character varying,
		"acquisitiondivision" VARCHAR(10) DEFAULT ''::character varying,
		"contract1_feescheduleid" INT4 DEFAULT 0,
		"contract1_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"contract2_feescheduleid" INT4 DEFAULT 0,
		"contract2_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"contract3_feescheduleid" INT4 DEFAULT 0,
		"contract3_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"bill1_feescheduleid" INT4 DEFAULT 0,
		"bill1_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"bill2_feescheduleid" INT4 DEFAULT 0,
		"bill2_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"bill3_feescheduleid" INT4 DEFAULT 0,
		"bill3_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"importantnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"emailalertnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"requiresonlineimage" INT4 DEFAULT 0,
		"requiresaging" INT4 DEFAULT 0,
		"sales_monthly_target" INT4 DEFAULT 0,
		"acceptstier2" INT4 DEFAULT 0,
		"acceptstier3" INT4 DEFAULT 0,
		"tier2_minsavings" NUMERIC(9 , 2) DEFAULT 0.0,
		"tier2_targetsavings" NUMERIC(9 , 2) DEFAULT 0.0,
		"tier3_fee" NUMERIC(9 , 2) DEFAULT 0.0,
		"autoauth" VARCHAR(1) DEFAULT ''::character varying,
		"petlinqready" INT4 DEFAULT 0,
		"isnid" INT4 DEFAULT 0,
		"nid_webcode" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showlogo" INT4 DEFAULT 0,
		"nid_weblogo" VARCHAR(200) DEFAULT ''::character varying,
		"nid_referralsource" INT4 DEFAULT 0,
		"commissionhighpercentageself" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionlowpercentageself" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionhighpercentageparent" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionlowpercentageparent" NUMERIC(9 , 2) DEFAULT 0.0,
		"commissionnotes" VARCHAR(5000) DEFAULT ''::character varying,
		"discounthighamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"discounthighpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"discountlowamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"discountlowpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"customerdisplayname" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showcode" INT4 DEFAULT 0,
		"nid_codedisplayname" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showsource" INT4 DEFAULT 0,
		"nid_sourcedisplayname" VARCHAR(200) DEFAULT ''::character varying,
		"nid_showbanner" INT4 DEFAULT 0,
		"nid_welcomemessage" VARCHAR(200) DEFAULT ''::character varying,
		"nid_discountpercentagemessage" VARCHAR(200) DEFAULT ''::character varying,
		"insidesalesdivision" VARCHAR(30) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcotransaction" (
		"hcotransactionid" SERIAL DEFAULT nextval('thcotransaction_hcotransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"physicianid" INT4 DEFAULT 0,
		"hcoid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tbillingtransaction" (
		"billingtransactionid" SERIAL DEFAULT nextval('tbillingtransaction_billingtransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"billingid" INT4 DEFAULT 0,
		"status" INT4 DEFAULT 0,
		"settlementdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"settlementamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tstandardaccesstypeli" (
		"standardaccesstypeid" INT4 NOT NULL,
		"standardaccesstypelong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tpracticecontractstatusli" (
		"practicecontractstatusid" INT4 NOT NULL,
		"descriptionlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tmcmc_retros" (
		"mcmcid" SERIAL DEFAULT nextval('tmcmc_retros_mcmcid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"mcmcbillid" INT4 DEFAULT 0,
		"networkbillid" INT4 DEFAULT 0,
		"mcmclocation" VARCHAR(100) DEFAULT ''::character varying,
		"renderingprovidertaxid" VARCHAR(100) DEFAULT ''::character varying,
		"renderingprovidername" VARCHAR(100) DEFAULT ''::character varying,
		"renderingprovideraddress" VARCHAR(100) DEFAULT ''::character varying,
		"renderingprovidercity" VARCHAR(100) DEFAULT ''::character varying,
		"renderingproviderstate" VARCHAR(100) DEFAULT ''::character varying,
		"renderingproviderzip" VARCHAR(100) DEFAULT ''::character varying,
		"billingprovidertaxid" VARCHAR(100) DEFAULT ''::character varying,
		"billingprovidername" VARCHAR(100) DEFAULT ''::character varying,
		"billingprovideraddress" VARCHAR(100) DEFAULT ''::character varying,
		"billingprovidercity" VARCHAR(100) DEFAULT ''::character varying,
		"billingproviderstate" VARCHAR(100) DEFAULT ''::character varying,
		"billingproviderzip" VARCHAR(100) DEFAULT ''::character varying,
		"clientname" VARCHAR(100) DEFAULT ''::character varying,
		"claimnumber" VARCHAR(100) DEFAULT ''::character varying,
		"patientssn" VARCHAR(100) DEFAULT ''::character varying,
		"patientfirstname" VARCHAR(100) DEFAULT ''::character varying,
		"patientlastname" VARCHAR(100) DEFAULT ''::character varying,
		"patientaddress1" VARCHAR(100) DEFAULT ''::character varying,
		"patientaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"patientcity" VARCHAR(100) DEFAULT ''::character varying,
		"patientstate" VARCHAR(100) DEFAULT ''::character varying,
		"patientzip" VARCHAR(100) DEFAULT ''::character varying,
		"employername" VARCHAR(100) DEFAULT ''::character varying,
		"employeraddress1" VARCHAR(100) DEFAULT ''::character varying,
		"employeraddress2" VARCHAR(100) DEFAULT ''::character varying,
		"employercity" VARCHAR(100) DEFAULT ''::character varying,
		"employerstate" VARCHAR(100) DEFAULT ''::character varying,
		"employerzip" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosiscode1" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosiscode2" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosiscode3" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosiscode4" VARCHAR(100) DEFAULT ''::character varying,
		"dateofbirth" VARCHAR(100) DEFAULT ''::character varying,
		"injurydate" VARCHAR(100) DEFAULT ''::character varying,
		"reviewstate" VARCHAR(100) DEFAULT ''::character varying,
		"reviewzip" VARCHAR(100) DEFAULT ''::character varying,
		"lineid" INT4 DEFAULT 0,
		"fstype" VARCHAR(100) DEFAULT ''::character varying,
		"dateofservice" VARCHAR(100) DEFAULT ''::character varying,
		"cptcode" VARCHAR(100) DEFAULT ''::character varying,
		"modifier1" VARCHAR(100) DEFAULT ''::character varying,
		"modifier2" VARCHAR(100) DEFAULT ''::character varying,
		"units" NUMERIC(9 , 2) DEFAULT 0.0,
		"providercharge" NUMERIC(9 , 2) DEFAULT 0.0,
		"stateallowance" NUMERIC(9 , 2) DEFAULT 0.0,
		"mcmcallowance" NUMERIC(9 , 2) DEFAULT 0.0,
		"networkallowance" NUMERIC(9 , 2) DEFAULT 0.0,
		"networksavings" NUMERIC(9 , 2) DEFAULT 0.0,
		"reasoncode1" VARCHAR(100) DEFAULT ''::character varying,
		"reasoncode2" VARCHAR(100) DEFAULT ''::character varying,
		"reasoncode3" VARCHAR(100) DEFAULT ''::character varying,
		"reasoncode4" VARCHAR(100) DEFAULT ''::character varying,
		"rejectdescription" VARCHAR(100) DEFAULT ''::character varying,
		"statusid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tnid_widgettrack" (
		"widgettrackid" SERIAL DEFAULT nextval('tnid_widgettrack_widgettrackid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"requestingurl" VARCHAR(100) DEFAULT ''::character varying,
		"keyid" VARCHAR(100) DEFAULT ''::character varying,
		"zipcode" VARCHAR(100) DEFAULT ''::character varying,
		"cptwizardid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."ttemp" (
		"ttempid" SERIAL DEFAULT nextval('ttemp_ttempid_seq'::regclass) NOT NULL,
		"text" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tdisclosurecategorytypeli" (
		"disclosurecategoryid" INT4 NOT NULL,
		"disclosurecategoryshort" VARCHAR(100) DEFAULT ''::character varying,
		"disclosurecategorylong" VARCHAR(1000) DEFAULT ''::character varying
	);

CREATE TABLE "public"."treadphysicianstatusli" (
		"readphysicianstatusid" INT4 NOT NULL,
		"readphysicianstatuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_feeschedule" (
		"feescheduleid" SERIAL DEFAULT nextval('tnim2_feeschedule_feescheduleid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"feeschedulerefid" INT4 DEFAULT 0,
		"cpt" VARCHAR(100) DEFAULT ''::character varying,
		"amount" NUMERIC(9 , 2) DEFAULT 0.0,
		"stateid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tcompanysecuritygrouplu" (
		"lookupid" SERIAL DEFAULT nextval('tcompanysecuritygrouplu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"companyid" INT4 DEFAULT 0,
		"securitygroupid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tscheduledappointmentstatusli" (
		"scheduledappointmentstatusid" INT4 NOT NULL,
		"statusshort" VARCHAR(4) DEFAULT ''::character varying,
		"statuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_il" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim3_referral" (
		"referralid" SERIAL DEFAULT nextval('tnim3_referral_referralid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"referringphysicianid" INT4 DEFAULT 0,
		"attendingphysicianid" INT4 DEFAULT 0,
		"referraltypeid" INT4 DEFAULT 0,
		"referralstatusid" INT4 DEFAULT 0,
		"preauthorizationconfirmation" VARCHAR(100) DEFAULT ''::character varying,
		"authorizationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"receivedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"referraldate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nextactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"nextactiontaskid" INT4 DEFAULT 0,
		"preparedby" VARCHAR(100) DEFAULT ''::character varying,
		"referencephone" VARCHAR(100) DEFAULT ''::character varying,
		"referencefax" VARCHAR(100) DEFAULT ''::character varying,
		"referenceemail" VARCHAR(100) DEFAULT ''::character varying,
		"orderfileid" INT4 DEFAULT 0,
		"rxfileid" INT4 DEFAULT 0,
		"comments" VARCHAR(9000) DEFAULT ''::text,
		"auditnotes" VARCHAR(9000) DEFAULT ''::character varying,
		"providerpatientid" VARCHAR(100) DEFAULT ''::character varying,
		"urrecfileid" INT4 DEFAULT 0,
		"referralmethod" VARCHAR(50) DEFAULT ''::character varying,
		"referredbycontactid" INT4 DEFAULT 0,
		"referralsourceother" INT4 DEFAULT 0,
		"referralsourceotherdesc" VARCHAR(500) DEFAULT ''::character varying,
		"referralweblink" VARCHAR(100) DEFAULT ''::character varying,
		"referralnidpromoid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."SelectMRI_qUserUnion" (
		"BranchID" VARCHAR(255),
		"SelectMRI_ID" VARCHAR(255),
		"ContactLastName" VARCHAR(255),
		"ContactFirstName" VARCHAR(255),
		"UniqueCreateDate" VARCHAR(255),
		"UniqueModifyDate" VARCHAR(255),
		"LogonUserName" VARCHAR(255),
		"ContactEmail" VARCHAR(255),
		"ContactPhone" VARCHAR(255),
		"ContactFax" VARCHAR(255),
		"ContactAddress1" VARCHAR(255),
		"ContactAddress2" VARCHAR(255),
		"ContactCity" VARCHAR(255),
		"ContactStateID" VARCHAR(255),
		"ContactZIP" VARCHAR(255),
		"MELicense" VARCHAR(255),
		"SelectMRI_Notes" VARCHAR(255),
		"SelectMRI_UserType" VARCHAR(255)
	);

CREATE TABLE "public"."tcountryli" (
		"countryid" INT4 NOT NULL,
		"countrycode" VARCHAR(15) DEFAULT ''::character varying,
		"countrylong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tothercertification" (
		"othercertid" SERIAL DEFAULT nextval('tothercertification_othercertid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"certificationtype" VARCHAR(100) DEFAULT ''::character varying,
		"certificationnumber" VARCHAR(100) DEFAULT ''::character varying,
		"expirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"doculinkid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ma" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tdegreetypeli" (
		"degreeid" INT4 NOT NULL,
		"degreelong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcasestatusli" (
		"casestatusid" INT4 NOT NULL,
		"casestatusshort" VARCHAR(10) DEFAULT ''::character varying,
		"casestatuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_ar" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tformli" (
		"formid" SERIAL DEFAULT nextval('tformli_formid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"formcode" INT4 DEFAULT 0,
		"formname" VARCHAR(20) DEFAULT ''::character varying,
		"filename" VARCHAR(200) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tpracticemaster_copy" (
		"practiceid" SERIAL DEFAULT nextval('tpracticemaster_practiceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(500) DEFAULT ''::character varying,
		"practicename" VARCHAR(500) DEFAULT ''::character varying,
		"departmentname" VARCHAR(200) DEFAULT ''::character varying,
		"typeofpractice" INT4 DEFAULT 0,
		"officefederaltaxid" VARCHAR(200) DEFAULT ''::character varying,
		"officetaxidnameaffiliation" VARCHAR(1000) DEFAULT ''::character varying,
		"officeaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"officeaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"officecity" VARCHAR(100) DEFAULT ''::character varying,
		"officestateid" INT4 DEFAULT 0,
		"officeprovince" VARCHAR(100) DEFAULT ''::character varying,
		"officezip" VARCHAR(50) DEFAULT ''::character varying,
		"officecountryid" INT4 DEFAULT 0,
		"officephone" VARCHAR(50) DEFAULT ''::character varying,
		"backofficephoneno" VARCHAR(50) DEFAULT ''::character varying,
		"officefaxno" VARCHAR(50) DEFAULT ''::character varying,
		"officeemail" VARCHAR(200) DEFAULT ''::character varying,
		"officemanagerfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"officemanagerlastname" VARCHAR(200) DEFAULT ''::character varying,
		"officemanagerphone" VARCHAR(50) DEFAULT ''::character varying,
		"officemanageremail" VARCHAR(200) DEFAULT ''::character varying,
		"answeringservice" INT4 DEFAULT 0,
		"answeringservicephone" VARCHAR(50) DEFAULT ''::character varying,
		"coverage247" INT4 DEFAULT 0,
		"minorityenterprise" INT4 DEFAULT 0,
		"languagesspokeninoffice" VARCHAR(500) DEFAULT ''::character varying,
		"acceptallnewpatients" INT4 DEFAULT 0,
		"acceptexistingpayorchange" INT4 DEFAULT 0,
		"acceptnewfromreferralonly" INT4 DEFAULT 0,
		"acceptnewmedicare" INT4 DEFAULT 0,
		"acceptnewmedicaid" INT4 DEFAULT 0,
		"practicelimitationage" VARCHAR(90) DEFAULT ''::character varying,
		"practicelimitationsex" VARCHAR(90) DEFAULT ''::character varying,
		"practicelimitationother" VARCHAR(90) DEFAULT ''::character varying,
		"billingpayableto" VARCHAR(200) DEFAULT ''::character varying,
		"billingfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"billinglastname" VARCHAR(200) DEFAULT ''::character varying,
		"billingaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"billingaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"billingcity" VARCHAR(200) DEFAULT ''::character varying,
		"billingstateid" INT4 DEFAULT 0,
		"billingprovince" VARCHAR(100) DEFAULT ''::character varying,
		"billingzip" VARCHAR(50) DEFAULT ''::character varying,
		"billingcountryid" INT4 DEFAULT 0,
		"billingphone" VARCHAR(50) DEFAULT ''::character varying,
		"billingfax" VARCHAR(50) DEFAULT ''::character varying,
		"credentialingcontactfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactlastname" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactcity" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactstateid" INT4 DEFAULT 0,
		"credentialingcontactprovince" VARCHAR(100) DEFAULT ''::character varying,
		"credentialingcontactzip" VARCHAR(50) DEFAULT ''::character varying,
		"credentialingcontactcountryid" INT4 DEFAULT 0,
		"credentialingcontactphone" VARCHAR(50) DEFAULT ''::character varying,
		"credentialingcontactfax" VARCHAR(50) DEFAULT ''::character varying,
		"credentiallingcontactemail" VARCHAR(200) DEFAULT ''::character varying,
		"officeworkhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"comments" VARCHAR(9000) DEFAULT ''::character varying,
		"anesthesialocal" INT4 DEFAULT 0,
		"anesthesiaregional" INT4 DEFAULT 0,
		"anesthesiaconscious" INT4 DEFAULT 0,
		"anesthesiageneral" INT4 DEFAULT 0,
		"minorsurgery" INT4 DEFAULT 0,
		"gynecology" INT4 DEFAULT 0,
		"xrayprocedures" INT4 DEFAULT 0,
		"drawblood" INT4 DEFAULT 0,
		"basiclab" INT4 DEFAULT 0,
		"ekg" INT4 DEFAULT 0,
		"minorlacerations" INT4 DEFAULT 0,
		"pulmonary" INT4 DEFAULT 0,
		"allergy" INT4 DEFAULT 0,
		"visualscreen" INT4 DEFAULT 0,
		"audiometry" INT4 DEFAULT 0,
		"sigmoidoscopy" INT4 DEFAULT 0,
		"immunizations" INT4 DEFAULT 0,
		"asthma" INT4 DEFAULT 0,
		"ivtreatment" INT4 DEFAULT 0,
		"osteopathic" INT4 DEFAULT 0,
		"hydration" INT4 DEFAULT 0,
		"cardiacstress" INT4 DEFAULT 0,
		"physicaltherapy" INT4 DEFAULT 0,
		"maternalhealth" INT4 DEFAULT 0,
		"chdp" INT4 DEFAULT 0,
		"officeservicescomments" VARCHAR(200) DEFAULT ''::character varying,
		"licensedisplayed" INT4 DEFAULT 0,
		"cprpresent" INT4 DEFAULT 0,
		"ambubagavailable" INT4 DEFAULT 0,
		"oxygenavailable" INT4 DEFAULT 0,
		"surgicalsuite" INT4 DEFAULT 0,
		"certtype" VARCHAR(90) DEFAULT ''::character varying,
		"labonsite" INT4 DEFAULT 0,
		"clianumber" VARCHAR(90) DEFAULT ''::character varying,
		"cliawaiver" VARCHAR(90) DEFAULT ''::character varying,
		"adaaccessibility" INT4 DEFAULT 0,
		"handicapaccessbuilding" INT4 DEFAULT 0,
		"handicapaccessramprail" INT4 DEFAULT 0,
		"handicapaccessparking" INT4 DEFAULT 0,
		"handicapaccesswheel" INT4 DEFAULT 0,
		"handicapaccessrail" INT4 DEFAULT 0,
		"handicapaccesselevator" INT4 DEFAULT 0,
		"handicapaccessbraille" INT4 DEFAULT 0,
		"handicapfountainphone" INT4 DEFAULT 0,
		"nearpublictransportation" INT4 DEFAULT 0,
		"disabledtty" INT4 DEFAULT 0,
		"disabledasl" INT4 DEFAULT 0,
		"childcareservices" INT4 DEFAULT 0,
		"safetyfireextinguisher" INT4 DEFAULT 0,
		"safetyextinguisherinspected" INT4 DEFAULT 0,
		"safetysmokedetectors" INT4 DEFAULT 0,
		"safetycorridorsclear" INT4 DEFAULT 0,
		"infectioncontrolhandwashing" INT4 DEFAULT 0,
		"infectioncontrolgloves" INT4 DEFAULT 0,
		"infectioncontrolbloodcleaned" INT4 DEFAULT 0,
		"facilitycomments" VARCHAR(200) DEFAULT ''::character varying,
		"medicalrecordsindividual" INT4 DEFAULT 0,
		"medicalrecordsinaccessible" INT4 DEFAULT 0,
		"medicalrecordsuniform" INT4 DEFAULT 0,
		"medicalrecordspatientsnameperpage" INT4 DEFAULT 0,
		"medicalrecordssignatureauthor" INT4 DEFAULT 0,
		"medicalrecordsentrydate" INT4 DEFAULT 0,
		"medicalrecordshistory" INT4 DEFAULT 0,
		"medicalrecordsadverse" INT4 DEFAULT 0,
		"medicalrecordscomments" VARCHAR(200) DEFAULT ''::character varying,
		"lastmodifieddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"allowphysedit" INT4 DEFAULT 0,
		"diagnosticultrasound" INT4 DEFAULT 0,
		"endoscopy" INT4 DEFAULT 0,
		"acceptreferralscomments" VARCHAR(200) DEFAULT ''::character varying,
		"patientbringcomments" VARCHAR(200) DEFAULT ''::character varying,
		"patientreferralquestions" VARCHAR(200) DEFAULT ''::character varying,
		"cliaexpirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"auditstatus" VARCHAR(100) DEFAULT ''::character varying,
		"auditdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"auditscore" INT4 DEFAULT 0,
		"contractingstatusid" INT4 DEFAULT 0,
		"ownername" VARCHAR(200) DEFAULT ''::character varying,
		"price_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"weburl" VARCHAR(300) DEFAULT ''::character varying,
		"npinumber" VARCHAR(100) DEFAULT ''::character varying,
		"statelicensenumber" VARCHAR(100) DEFAULT ''::character varying,
		"medicarelicensenumber" VARCHAR(100) DEFAULT ''::character varying,
		"walkinmrict" INT4 DEFAULT 0,
		"diagnostictechonly" INT4 DEFAULT 0,
		"dicomforward" INT4 DEFAULT 0,
		"diagnosticafterhours" INT4 DEFAULT 0,
		"malpracticepolicycarrier" VARCHAR(1000) DEFAULT ''::character varying,
		"malpracticepolicynumber" VARCHAR(100) DEFAULT ''::character varying,
		"malpracticepolicyexpdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"billingaddressname" VARCHAR(200) DEFAULT ''::character varying,
		"billingemail" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactaddressname" VARCHAR(200) DEFAULT ''::character varying,
		"rate_curbappeal" INT4 DEFAULT 0,
		"rate_accesssafety" INT4 DEFAULT 0,
		"rate_cleanliness" INT4 DEFAULT 0,
		"rate_appropriateness" INT4 DEFAULT 0,
		"rate_stafffriendly" INT4 DEFAULT 0,
		"rate_attentivetimely" INT4 DEFAULT 0,
		"feeschedulerefid" INT4 DEFAULT 0,
		"feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"initialcontractdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"contractdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pacssystemvendor" VARCHAR(100) DEFAULT ''::character varying,
		"rissystemvendor" VARCHAR(100) DEFAULT ''::character varying,
		"isbluestar" INT4 DEFAULT 0,
		"initialquota" INT4 DEFAULT 0,
		"paymenttermsnet" INT4 DEFAULT 0,
		"paymenttermsmethod" INT4 DEFAULT 0,
		"diagnosticmdarthrogram" INT4 DEFAULT 0,
		"officemdarthrogramhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"diagnosticmdmyelogram" INT4 DEFAULT 0,
		"officemdmyelogramhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"petlinqid" VARCHAR(10) DEFAULT ''::character varying,
		"doesaging" INT4 DEFAULT 0,
		"selectmri_id" INT4 DEFAULT 0,
		"selectmri_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"selectmri_qbid" VARCHAR(50) DEFAULT ''::character varying,
		"scheduling_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"nextimageteleradmember" INT4 DEFAULT 0,
		"breader" INT4 DEFAULT 0,
		"contractglobal" INT4 DEFAULT 0,
		"contracttechonly" INT4 DEFAULT 0,
		"contractprofessionalonly" INT4 DEFAULT 0,
		"feescheduleoverrefid" INT4 DEFAULT 0,
		"feeoverpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"faxreportreminderoverride" VARCHAR(50),
		"gh_price_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_feeschedulerefid" INT4 DEFAULT 0,
		"gh_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_feescheduleoverrefid" INT4 DEFAULT 0,
		"gh_feeoverpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_initialquota" INT4 DEFAULT 0,
		"gh_paymenttermsnet" INT4 DEFAULT 0,
		"gh_paymenttermsmethod" INT4 DEFAULT 0,
		"gh_doesaging" INT4 DEFAULT 0,
		"gh_diagnosticmdarthrogram" INT4 DEFAULT 0,
		"gh_diagnosticmdmyelogram" INT4 DEFAULT 0,
		"clienttypeid" INT4 DEFAULT 3,
		"terminated" INT4 DEFAULT 2,
		"terminateddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"price_mod_petct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_petct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_petct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_petct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_petct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_petct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"hascardiacmr" INT4 DEFAULT 0,
		"hascardiacct" INT4 DEFAULT 0
	);

CREATE TABLE "public"."terrorlog" (
		"errorlogid" SERIAL DEFAULT nextval('terrorlog_errorlogid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"errortypeid" INT4 DEFAULT 0,
		"indentifier" VARCHAR(100) DEFAULT ''::character varying,
		"errorcomments" VARCHAR(2000) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_backup_may15" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tsupporttypeli" (
		"supporttypeid" INT4 NOT NULL,
		"supporttypeshort" VARCHAR(10) DEFAULT ''::character varying,
		"supporttypelong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tsalestransaction" (
		"salestransactionid" SERIAL DEFAULT nextval('tsalestransaction_salestransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"salesid" INT4 DEFAULT 0,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"typedescription" VARCHAR(90) DEFAULT ''::character varying,
		"userid" INT4 DEFAULT 0,
		"refid" INT4 DEFAULT 0,
		"transactioncomments" VARCHAR(2000) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_netdevdatatable" (
		"netdevdatatableid" SERIAL DEFAULT nextval('tnim3_netdevdatatable_netdevdatatableid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"encounterid" INT4 DEFAULT 0,
		"statusid" INT4 DEFAULT 0,
		"timestarted" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone
	);

CREATE TABLE "public"."in_fs_feeschedule_md" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tfieldsecurity" (
		"fieldsecurityid" SERIAL DEFAULT nextval('tfieldsecurity_fieldsecurityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"tablename" VARCHAR(100) DEFAULT ''::character varying,
		"fieldname" VARCHAR(100) DEFAULT ''::character varying,
		"requiredid" INT4 DEFAULT 0,
		"trackedid" INT4 DEFAULT 0,
		"accesslevel" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tworkhistorytypeli" (
		"workhistorytypeid" INT4 NOT NULL,
		"workhistorytypelong" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tstateli" (
		"stateid" INT4 NOT NULL,
		"shortstate" VARCHAR(4) DEFAULT ''::character varying,
		"longstate" VARCHAR(50) DEFAULT ''::character varying
	);

CREATE TABLE "public"."temailtransaction" (
		"emailtransactionid" SERIAL DEFAULT nextval('temailtransaction_emailtransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(500) DEFAULT ''::character varying,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"emailto" VARCHAR(200) DEFAULT ''::character varying,
		"emailfrom" VARCHAR(200) DEFAULT ''::character varying,
		"emailbody" VARCHAR(900000) DEFAULT ''::character varying,
		"emailbodytype" VARCHAR(90) DEFAULT ''::character varying,
		"emailsubject" VARCHAR(200) DEFAULT ''::character varying,
		"emailimportance" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"virtualattachment" VARCHAR(500) DEFAULT ''::character varying,
		"emailcc1" VARCHAR(200) DEFAULT ''::character varying,
		"emailcc2" VARCHAR(200) DEFAULT ''::character varying,
		"emailcc3" VARCHAR(200) DEFAULT ''::character varying,
		"emailcc4" VARCHAR(200) DEFAULT ''::character varying,
		"emailcc5" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcontinuingeducation" (
		"continuingeducationid" SERIAL DEFAULT nextval('tcontinuingeducation_continuingeducationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"coursename" VARCHAR(50) DEFAULT ''::character varying,
		"categorytype" VARCHAR(50) DEFAULT ''::character varying,
		"credits" VARCHAR(20) DEFAULT ''::character varying,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"referencename" VARCHAR(50) DEFAULT ''::character varying,
		"referenceid" VARCHAR(50) DEFAULT ''::character varying,
		"schoolname" VARCHAR(100) DEFAULT ''::character varying,
		"schooladdress1" VARCHAR(50) DEFAULT ''::character varying,
		"schooladdress2" VARCHAR(20) DEFAULT ''::character varying,
		"schoolcity" VARCHAR(30) DEFAULT ''::character varying,
		"schoolstateid" INT4 DEFAULT 0,
		"schoolprovince" VARCHAR(100) DEFAULT ''::character varying,
		"schoolzip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"schoolphone" VARCHAR(50) DEFAULT ''::character varying,
		"schoolfax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"sponsorname" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tboardcertification" (
		"boardcertificationid" SERIAL DEFAULT nextval('tboardcertification_boardcertificationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"specialty" VARCHAR(100) DEFAULT ''::character varying,
		"subspecialty" VARCHAR(100) DEFAULT ''::character varying,
		"islisted" INT4 DEFAULT 0,
		"isprimary" INT4 DEFAULT 0,
		"specialtyboardcertified" INT4 DEFAULT 0,
		"boardname" VARCHAR(100) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"boarddateinitialcertified" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"boarddaterecertified" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"expirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"documentnumber" VARCHAR(100) DEFAULT ''::character varying,
		"ifnotcert" VARCHAR(200) DEFAULT ''::character varying,
		"certeligible" INT4 DEFAULT 0,
		"eligiblestartdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"eligibleexpirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"certplanningtotake" INT4 DEFAULT 0,
		"certplandate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"doculinkid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tboardnameli" (
		"boardnameid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"boardname" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tadminpracticelu_copy" (
		"lookupid" SERIAL DEFAULT nextval('tadminpracticelu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"adminid" INT4 DEFAULT 0,
		"practiceid" INT4 DEFAULT 0,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"relationshiptypeid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tmalpractice" (
		"malpracticeid" SERIAL DEFAULT nextval('tmalpractice_malpracticeid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"inicidentdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"suitdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientgender" INT4 DEFAULT 0,
		"patientage" VARCHAR(50) DEFAULT ''::character varying,
		"suitcity" VARCHAR(50) DEFAULT ''::character varying,
		"suitstate" INT4 DEFAULT 0,
		"inicidentlocation" VARCHAR(100) DEFAULT ''::character varying,
		"patientrelationship" VARCHAR(100) DEFAULT ''::character varying,
		"allegation" VARCHAR(200) DEFAULT ''::character varying,
		"wasinsured" INT4 DEFAULT 0,
		"insurancecompany" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactfirstname" VARCHAR(90) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(90) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"insuranceclaimnumber" VARCHAR(90) DEFAULT ''::character varying,
		"attorneynamefull" VARCHAR(100) DEFAULT ''::character varying,
		"attorneyphone" VARCHAR(50) DEFAULT ''::character varying,
		"describedisposition" VARCHAR(200) DEFAULT ''::character varying,
		"doculinkid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"casecloseddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"claimantname" VARCHAR(100) DEFAULT ''::character varying,
		"settlementamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"paymentmadeamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"casestatus" INT4 DEFAULT 0,
		"iswithprejudice" INT4 DEFAULT 0,
		"defendantstatus" INT4 DEFAULT 0,
		"defentdantstatusother" VARCHAR(200) DEFAULT ''::character varying,
		"nameofcodefendants" VARCHAR(200) DEFAULT ''::character varying,
		"iscaseinnpdb" INT4 DEFAULT 0,
		"courtcasenumber" VARCHAR(90) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tactivity" (
		"activityid" SERIAL DEFAULT nextval('tactivity_activityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"eventchildid" INT4 DEFAULT 0,
		"name" VARCHAR(20) DEFAULT ''::character varying,
		"activitytype" VARCHAR(20) DEFAULT ''::character varying,
		"completed" INT4 DEFAULT 0,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"description" VARCHAR(1000) DEFAULT ''::character varying,
		"taskfilereference" VARCHAR(90) DEFAULT ''::character varying,
		"tasklogicreference" VARCHAR(90) DEFAULT ''::character varying,
		"summary" VARCHAR(500) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"referenceid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."CPTGroup_BefPT" (
		"CPTGroup" VARCHAR(255) NOT NULL,
		"CPT" INT4 NOT NULL
	);

CREATE TABLE "public"."tplcli" (
		"plcid" INT4 NOT NULL,
		"plcshort" VARCHAR(10) DEFAULT ''::character varying,
		"plclong" VARCHAR(50) DEFAULT ''::character varying,
		"companyid" INT4 DEFAULT 0,
		"adminid" INT4 DEFAULT 0,
		"hcoid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."in_fs_feeschedule_mi" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tactivityreference" (
		"activityreferenceid" SERIAL DEFAULT nextval('tactivityreference_activityreferenceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"hcoid" INT4 DEFAULT 0,
		"activityreferencetype" VARCHAR(20) DEFAULT ''::character varying,
		"taskfilereference" VARCHAR(200) DEFAULT ''::character varying,
		"taskname" VARCHAR(200) DEFAULT ''::character varying,
		"tasklogicreference" VARCHAR(90) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tsalutationli" (
		"salutationid" INT4 NOT NULL,
		"salutationshort" VARCHAR(10) DEFAULT ''::character varying,
		"salutationlong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_netdevdatatablesupport" (
		"netdevdatatablesupportid" SERIAL DEFAULT nextval('tnim3_netdevdatatablesupport_netdevdatatablesupportid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"key" VARCHAR(100) DEFAULT ''::character varying,
		"value" VARCHAR(100) DEFAULT ''::character varying,
		"netdevdatatableid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tadminmaster" (
		"adminid" SERIAL DEFAULT nextval('tadminmaster_adminid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"name" VARCHAR(400) DEFAULT ''::character varying,
		"address1" VARCHAR(200) DEFAULT ''::character varying,
		"address2" VARCHAR(100) DEFAULT ''::character varying,
		"city" VARCHAR(100) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"alertemail" VARCHAR(200) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"contactfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(200) DEFAULT ''::character varying,
		"contactemail" VARCHAR(200) DEFAULT ''::character varying,
		"contactaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"contactaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"contactcity" VARCHAR(200) DEFAULT ''::character varying,
		"contactstateid" INT4 DEFAULT 0,
		"contactzip" VARCHAR(50) DEFAULT ''::character varying,
		"contactphone" VARCHAR(50) DEFAULT ''::character varying,
		"lastimportdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"comments" VARCHAR(9000) DEFAULT ''::character varying,
		"doculinkid" INT4 DEFAULT 0,
		"contactfax" VARCHAR(100),
		"fax" VARCHAR(100),
		"assignedtoid" INT4
	);

CREATE TABLE "public"."tprofessionalliability" (
		"professionalliabilityid" SERIAL DEFAULT nextval('tprofessionalliability_professionalliabilityid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"coveragetype" INT4 DEFAULT 0,
		"insurancecarrier" VARCHAR(100) DEFAULT ''::character varying,
		"policyholder" VARCHAR(100) DEFAULT ''::character varying,
		"agentname" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(50) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"contactname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"policynumber" VARCHAR(100) DEFAULT ''::character varying,
		"insuredfromdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"insuredtodate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"originaleffectivedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"terminationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"perclaimamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"aggregateamount" NUMERIC(9 , 2) DEFAULT 0.0,
		"descofsurcharge" VARCHAR(200) DEFAULT ''::character varying,
		"doculinkid" INT4 DEFAULT 0,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."ttransaction" (
		"transactionid" SERIAL DEFAULT nextval('ttransaction_transactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"action" VARCHAR(200) DEFAULT ''::character varying,
		"modifier" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."teventchild" (
		"eventchildid" SERIAL DEFAULT nextval('teventchild_eventchildid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"eventid" INT4 DEFAULT 0,
		"name" VARCHAR(20) DEFAULT ''::character varying,
		"eventtype" VARCHAR(20) DEFAULT ''::character varying,
		"completed" INT4 DEFAULT 0,
		"startdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"enddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"reminddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"summary" VARCHAR(1000) DEFAULT ''::character varying,
		"itemrefid" INT4 DEFAULT 0,
		"itemreftype" VARCHAR(200) DEFAULT ''::character varying,
		"companyname" VARCHAR(200) DEFAULT ''::character varying,
		"contactname" VARCHAR(90) DEFAULT ''::character varying,
		"email" VARCHAR(90) DEFAULT ''::character varying,
		"address1" VARCHAR(90) DEFAULT ''::character varying,
		"address2" VARCHAR(90) DEFAULT ''::character varying,
		"city" VARCHAR(90) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(90) DEFAULT ''::character varying,
		"zip" VARCHAR(15) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(50) DEFAULT ''::character varying,
		"fax" VARCHAR(50) DEFAULT ''::character varying,
		"mergegeneric1" VARCHAR(200) DEFAULT ''::character varying,
		"mergegeneric2" VARCHAR(200) DEFAULT ''::character varying,
		"mergegeneric3" VARCHAR(200) DEFAULT ''::character varying,
		"mergegeneric4" VARCHAR(200) DEFAULT ''::character varying,
		"mergegeneric5" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tct_modelli" (
		"ct_modelid" INT4 NOT NULL,
		"descriptionlong" VARCHAR(100) DEFAULT ''::character varying,
		"manufacturer" VARCHAR(100) DEFAULT ''::character varying,
		"model" VARCHAR(100) DEFAULT ''::character varying,
		"slices" INT4 DEFAULT 0,
		"images_transferred" INT4 DEFAULT 0,
		"in_seconds" INT4 DEFAULT 0
	);

CREATE TABLE "public"."in_fs_feeschedule_wi" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."in_fs_feeschedule_tx" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tcompanymaster" (
		"companyid" SERIAL DEFAULT nextval('tcompanymaster_companyid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"name" VARCHAR(100) DEFAULT ''::character varying,
		"address1" VARCHAR(50) DEFAULT ''::character varying,
		"address2" VARCHAR(20) DEFAULT ''::character varying,
		"city" VARCHAR(30) DEFAULT ''::character varying,
		"stateid" INT4 DEFAULT 0,
		"province" VARCHAR(100) DEFAULT ''::character varying,
		"zip" VARCHAR(10) DEFAULT ''::character varying,
		"countryid" INT4 DEFAULT 0,
		"phone" VARCHAR(20) DEFAULT ''::character varying,
		"contactfirstname" VARCHAR(50) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(75) DEFAULT ''::character varying,
		"contactaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"contactaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"contactcity" VARCHAR(30) DEFAULT ''::character varying,
		"contactstateid" INT4 DEFAULT 0,
		"contactzip" VARCHAR(10) DEFAULT ''::character varying,
		"contactphone" VARCHAR(20) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tcommtrackalertstatuscodeli" (
		"commtrackalertstatuscodeid" INT4 NOT NULL,
		"statusshort" VARCHAR(4) DEFAULT ''::character varying,
		"statuslong" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim3_cptgrouplist" (
		"lookupid" SERIAL DEFAULT nextval('tnim3_cptgrouplist_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"cptgroupid" INT4 DEFAULT 0,
		"cpt" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tactivityfilereference" (
		"activityfilereferenceid" SERIAL DEFAULT nextval('tactivityfilereference_activityfilereferenceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"hcoid" INT4 DEFAULT 0,
		"eventtype" VARCHAR(20) DEFAULT ''::character varying,
		"filereference" VARCHAR(200) DEFAULT ''::character varying,
		"tasklogicreference" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."thcophysiciantransaction" (
		"hcophysiciantransactionid" SERIAL DEFAULT nextval('thcophysiciantransaction_hcophysiciantransactionid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"lookupid" INT4 DEFAULT 0,
		"transactiondate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"actionid" INT4 DEFAULT 0,
		"userid" INT4 DEFAULT 0,
		"transactioncomments" VARCHAR(2000) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tadditionalinformation" (
		"additionalinformationid" SERIAL DEFAULT nextval('tadditionalinformation_additionalinformationid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"physicianid" INT4 DEFAULT 0,
		"subject" VARCHAR(100) DEFAULT ''::character varying,
		"notecontent" VARCHAR(200) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."in_fs_feeschedule_vt" (
		"fsid" VARCHAR(255),
		"cpt" VARCHAR(255),
		"mod" VARCHAR(255),
		"fee" VARCHAR(255),
		"rc" VARCHAR(255),
		"effdate" DATE
	);

CREATE TABLE "public"."tnim2_caseaccount" (
		"caseid" SERIAL DEFAULT nextval('tnim2_caseaccount_caseid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"casepass" VARCHAR(100) DEFAULT ''::character varying,
		"caseclaimnumber1" VARCHAR(100) DEFAULT ''::character varying,
		"caseclaimnumber2" VARCHAR(100) DEFAULT ''::character varying,
		"payername" VARCHAR(100) DEFAULT ''::character varying,
		"adjustername" VARCHAR(100) DEFAULT ''::character varying,
		"payeraddress1" VARCHAR(100) DEFAULT ''::character varying,
		"payeraddress2" VARCHAR(100) DEFAULT ''::character varying,
		"payercity" VARCHAR(100) DEFAULT ''::character varying,
		"payerstateid" INT4 DEFAULT 0,
		"payerzip" VARCHAR(50) DEFAULT ''::character varying,
		"payerphone" VARCHAR(50) DEFAULT ''::character varying,
		"payerfax" VARCHAR(50) DEFAULT ''::character varying,
		"payeremail" VARCHAR(200) DEFAULT ''::character varying,
		"employername" VARCHAR(100) DEFAULT ''::character varying,
		"employerphone" VARCHAR(100) DEFAULT ''::character varying,
		"dateofinjury" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"injurydescription" VARCHAR(4000) DEFAULT ''::character varying,
		"nursecasemanagername" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanagerid" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanagerpreparedby" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanagerphone" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanagerfax" VARCHAR(100) DEFAULT ''::character varying,
		"nursecasemanageremail" VARCHAR(200) DEFAULT ''::character varying,
		"patientfirstname" VARCHAR(100) DEFAULT ''::character varying,
		"patientlastname" VARCHAR(100) DEFAULT ''::character varying,
		"patientaccountnumber" VARCHAR(100) DEFAULT ''::character varying,
		"patientexpirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientisactive" INT4 DEFAULT 0,
		"patientdob" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"patientssn" VARCHAR(20) DEFAULT ''::character varying,
		"patientgender" VARCHAR(20) DEFAULT ''::character varying,
		"patientaddress1" VARCHAR(100) DEFAULT ''::character varying,
		"patientaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"patientcity" VARCHAR(100) DEFAULT ''::character varying,
		"patientstateid" INT4 DEFAULT 0,
		"patientzip" VARCHAR(50) DEFAULT ''::character varying,
		"patienthomephone" VARCHAR(50) DEFAULT ''::character varying,
		"patientworkphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientcellphone" VARCHAR(50) DEFAULT ''::character varying,
		"patientccaccountfullname" VARCHAR(100) DEFAULT ''::character varying,
		"patientccaccountnumber1" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccountnumber2" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccounttype" VARCHAR(20) DEFAULT ''::character varying,
		"patientccaccountexpmonth" VARCHAR(4) DEFAULT ''::character varying,
		"patientccaccountexpyear" VARCHAR(6) DEFAULT ''::character varying,
		"comments" VARCHAR(2000) DEFAULT ''::character varying,
		"adjusterid" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tuseraccount" (
		"userid" SERIAL DEFAULT nextval('tuseraccount_userid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(500) DEFAULT ''::character varying,
		"plcid" INT4 DEFAULT 0,
		"startpage" VARCHAR(100) DEFAULT ''::character varying,
		"accounttype" VARCHAR(20) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"genericsecuritygroupid" INT4 DEFAULT 0,
		"referenceid" INT4 DEFAULT 0,
		"accesstype" INT4 DEFAULT 0,
		"status" INT4 DEFAULT 0,
		"logonusername" VARCHAR(200) DEFAULT ''::character varying,
		"logonuserpassword" VARCHAR(100) DEFAULT ''::character varying,
		"attestkeyword1" VARCHAR(50) DEFAULT ''::character varying,
		"attestkeyword2" VARCHAR(50) DEFAULT ''::character varying,
		"attestkeywordtemp1" VARCHAR(50) DEFAULT ''::character varying,
		"attestkeywordtemp2" VARCHAR(50) DEFAULT ''::character varying,
		"contactfirstname" VARCHAR(50) DEFAULT ''::character varying,
		"contactlastname" VARCHAR(50) DEFAULT ''::character varying,
		"contactemail" VARCHAR(200) DEFAULT ''::character varying,
		"contactaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"contactaddress2" VARCHAR(100) DEFAULT ''::character varying,
		"contactcity" VARCHAR(30) DEFAULT ''::character varying,
		"contactstateid" INT4 DEFAULT 0,
		"contactprovince" VARCHAR(100) DEFAULT ''::character varying,
		"contactzip" VARCHAR(50) DEFAULT ''::character varying,
		"contactcountryid" INT4 DEFAULT 0,
		"contactphone" VARCHAR(50) DEFAULT ''::character varying,
		"secretquestion" VARCHAR(50) DEFAULT ''::character varying,
		"secretanswer" VARCHAR(50) DEFAULT ''::character varying,
		"creditcardfullname" VARCHAR(100) DEFAULT ''::character varying,
		"creditcardnumber" VARCHAR(20) DEFAULT ''::character varying,
		"creditcardtype" VARCHAR(20) DEFAULT ''::character varying,
		"creditcardexpmonth" VARCHAR(4) DEFAULT ''::character varying,
		"creditcardexpyear" VARCHAR(6) DEFAULT ''::character varying,
		"creditcardchargedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"creditcardpostdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"alertemail" VARCHAR(50) DEFAULT ''::character varying,
		"alertdays" INT4 DEFAULT 0,
		"billingcomments" VARCHAR(200) DEFAULT ''::character varying,
		"promocode" VARCHAR(20) DEFAULT ''::character varying,
		"companytype" INT4 DEFAULT 0,
		"companyname" VARCHAR(200) DEFAULT ''::character varying,
		"companyaddress1" VARCHAR(50) DEFAULT ''::character varying,
		"companyaddress2" VARCHAR(20) DEFAULT ''::character varying,
		"companycity" VARCHAR(30) DEFAULT ''::character varying,
		"companystateid" INT4 DEFAULT 0,
		"companyzip" VARCHAR(50) DEFAULT ''::character varying,
		"companyphone" VARCHAR(50) DEFAULT ''::character varying,
		"comments" VARCHAR(200) DEFAULT ''::character varying,
		"billingid" INT4 DEFAULT 0,
		"phdbacknowledgementstatus" INT4 DEFAULT 0,
		"phdbletterheadstatus" INT4 DEFAULT 0,
		"taxid" VARCHAR(50) DEFAULT ''::character varying,
		"notusingphdbid" INT4 DEFAULT 0,
		"notusingphdbcomments" VARCHAR(4000) DEFAULT ''::character varying,
		"payerid" INT4 DEFAULT 0,
		"contactmobile" VARCHAR(60) DEFAULT ''::character varying,
		"contactfax" VARCHAR(60) DEFAULT ''::character varying,
		"companyfax" VARCHAR(60) DEFAULT ''::character varying,
		"managerid" INT4 DEFAULT 0,
		"comm_email_requiresattachement" INT4 DEFAULT 0,
		"comm_alerts_leveluser" INT4 DEFAULT 0,
		"comm_alerts_levelmanager" INT4 DEFAULT 0,
		"comm_alerts_levelbranch" INT4 DEFAULT 0,
		"comm_report_leveluser" INT4 DEFAULT 0,
		"comm_report_levelmanager" INT4 DEFAULT 0,
		"comm_report_levelbranch" INT4 DEFAULT 0,
		"comm_alerts2_leveluser" INT4 DEFAULT 0,
		"comm_alerts2_levelmanager" INT4 DEFAULT 0,
		"comm_alerts2_levelbranch" INT4 DEFAULT 0,
		"usernpi" VARCHAR(50) DEFAULT ''::character varying,
		"userstatelicense" VARCHAR(50) DEFAULT ''::character varying,
		"userstatelicensedesc" VARCHAR(10) DEFAULT ''::character varying,
		"selectmri_id" INT4 DEFAULT 0,
		"selectmri_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"melicense" VARCHAR(50) DEFAULT ''::character varying,
		"selectmri_usertype" VARCHAR(20) DEFAULT ''::character varying,
		"importantnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"emailalertnotes_alert" VARCHAR(2000) DEFAULT ''::character varying,
		"importantnotes" VARCHAR(2000) DEFAULT ''::character varying,
		"nim_usertype" VARCHAR(20) DEFAULT ''::character varying,
		"totalmonthlymris" INT4 DEFAULT 0,
		"expectedmonthlymris" INT4 DEFAULT 0,
		"data_rank" INT4 DEFAULT 0,
		"data_referrals_3monthaverage" INT4 DEFAULT 0,
		"data_referrals_alltime" INT4 DEFAULT 0,
		"data_referrals_analysistotal" INT4 DEFAULT 0,
		"data_referrals_analysisturnaroundaverage_tosched" NUMERIC(9 , 2) DEFAULT 0.0,
		"data_referrals_analysiscostsavings" NUMERIC(9 , 2) DEFAULT 0.0,
		"internal_primarycontact" INT4 DEFAULT 0,
		"lastcontacted" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"dateofbirth" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"personalinsights" VARCHAR(5000) DEFAULT ''::character varying,
		"allowasprimarycontact" INT4 DEFAULT 0,
		"contactemail2" VARCHAR(75) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tnim2_document" (
		"documentid" SERIAL DEFAULT nextval('tnim2_document_documentid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"caseid" INT4 DEFAULT 0,
		"authorizationid" INT4 DEFAULT 0,
		"doctype" INT4 DEFAULT 0,
		"filename" VARCHAR(100) DEFAULT ''::character varying,
		"dcmfile" VARCHAR(100) DEFAULT ''::character varying,
		"reportsummary" VARCHAR(100) DEFAULT ''::character varying,
		"reporttext" VARCHAR(100) DEFAULT ''::character varying,
		"reportdictationfile" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosticphysician" VARCHAR(100) DEFAULT ''::character varying,
		"diagnosticpacs" VARCHAR(100) DEFAULT ''::character varying,
		"referringphysicianid" VARCHAR(100) DEFAULT ''::character varying,
		"comments" VARCHAR(100) DEFAULT ''::character varying
	);

CREATE TABLE "public"."qMedicare_Fee_2011_RC" (
		"rc" VARCHAR(255) NOT NULL,
		"hcpcs/cpt" VARCHAR(255) NOT NULL,
		"mod" VARCHAR(255) NOT NULL,
		"nonfac_fee" FLOAT4 NOT NULL,
		"year" INT4 NOT NULL
	);

CREATE TABLE "public"."tpracticemaster" (
		"practiceid" SERIAL DEFAULT nextval('tpracticemaster_practiceid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(500) DEFAULT ''::character varying,
		"practicename" VARCHAR(500) DEFAULT ''::character varying,
		"departmentname" VARCHAR(200) DEFAULT ''::character varying,
		"typeofpractice" INT4 DEFAULT 0,
		"officefederaltaxid" VARCHAR(200) DEFAULT ''::character varying,
		"officetaxidnameaffiliation" VARCHAR(1000) DEFAULT ''::character varying,
		"officeaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"officeaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"officecity" VARCHAR(100) DEFAULT ''::character varying,
		"officestateid" INT4 DEFAULT 0,
		"officeprovince" VARCHAR(100) DEFAULT ''::character varying,
		"officezip" VARCHAR(50) DEFAULT ''::character varying,
		"officecountryid" INT4 DEFAULT 0,
		"officephone" VARCHAR(50) DEFAULT ''::character varying,
		"backofficephoneno" VARCHAR(50) DEFAULT ''::character varying,
		"officefaxno" VARCHAR(50) DEFAULT ''::character varying,
		"officeemail" VARCHAR(200) DEFAULT ''::character varying,
		"officemanagerfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"officemanagerlastname" VARCHAR(200) DEFAULT ''::character varying,
		"officemanagerphone" VARCHAR(50) DEFAULT ''::character varying,
		"officemanageremail" VARCHAR(200) DEFAULT ''::character varying,
		"answeringservice" INT4 DEFAULT 0,
		"answeringservicephone" VARCHAR(50) DEFAULT ''::character varying,
		"coverage247" INT4 DEFAULT 0,
		"minorityenterprise" INT4 DEFAULT 0,
		"languagesspokeninoffice" VARCHAR(500) DEFAULT ''::character varying,
		"acceptallnewpatients" INT4 DEFAULT 0,
		"acceptexistingpayorchange" INT4 DEFAULT 0,
		"acceptnewfromreferralonly" INT4 DEFAULT 0,
		"acceptnewmedicare" INT4 DEFAULT 0,
		"acceptnewmedicaid" INT4 DEFAULT 0,
		"practicelimitationage" VARCHAR(90) DEFAULT ''::character varying,
		"practicelimitationsex" VARCHAR(90) DEFAULT ''::character varying,
		"practicelimitationother" VARCHAR(90) DEFAULT ''::character varying,
		"billingpayableto" VARCHAR(200) DEFAULT ''::character varying,
		"billingfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"billinglastname" VARCHAR(200) DEFAULT ''::character varying,
		"billingaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"billingaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"billingcity" VARCHAR(200) DEFAULT ''::character varying,
		"billingstateid" INT4 DEFAULT 0,
		"billingprovince" VARCHAR(100) DEFAULT ''::character varying,
		"billingzip" VARCHAR(50) DEFAULT ''::character varying,
		"billingcountryid" INT4 DEFAULT 0,
		"billingphone" VARCHAR(50) DEFAULT ''::character varying,
		"billingfax" VARCHAR(50) DEFAULT ''::character varying,
		"credentialingcontactfirstname" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactlastname" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactaddress1" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactaddress2" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactcity" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactstateid" INT4 DEFAULT 0,
		"credentialingcontactprovince" VARCHAR(100) DEFAULT ''::character varying,
		"credentialingcontactzip" VARCHAR(50) DEFAULT ''::character varying,
		"credentialingcontactcountryid" INT4 DEFAULT 0,
		"credentialingcontactphone" VARCHAR(50) DEFAULT ''::character varying,
		"credentialingcontactfax" VARCHAR(50) DEFAULT ''::character varying,
		"credentiallingcontactemail" VARCHAR(200) DEFAULT ''::character varying,
		"officeworkhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeworkhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officeafterhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"comments" VARCHAR(9000) DEFAULT ''::character varying,
		"anesthesialocal" INT4 DEFAULT 0,
		"anesthesiaregional" INT4 DEFAULT 0,
		"anesthesiaconscious" INT4 DEFAULT 0,
		"anesthesiageneral" INT4 DEFAULT 0,
		"minorsurgery" INT4 DEFAULT 0,
		"gynecology" INT4 DEFAULT 0,
		"xrayprocedures" INT4 DEFAULT 0,
		"drawblood" INT4 DEFAULT 0,
		"basiclab" INT4 DEFAULT 0,
		"ekg" INT4 DEFAULT 0,
		"minorlacerations" INT4 DEFAULT 0,
		"pulmonary" INT4 DEFAULT 0,
		"allergy" INT4 DEFAULT 0,
		"visualscreen" INT4 DEFAULT 0,
		"audiometry" INT4 DEFAULT 0,
		"sigmoidoscopy" INT4 DEFAULT 0,
		"immunizations" INT4 DEFAULT 0,
		"asthma" INT4 DEFAULT 0,
		"ivtreatment" INT4 DEFAULT 0,
		"osteopathic" INT4 DEFAULT 0,
		"hydration" INT4 DEFAULT 0,
		"cardiacstress" INT4 DEFAULT 0,
		"physicaltherapy" INT4 DEFAULT 0,
		"maternalhealth" INT4 DEFAULT 0,
		"chdp" INT4 DEFAULT 0,
		"officeservicescomments" VARCHAR(200) DEFAULT ''::character varying,
		"licensedisplayed" INT4 DEFAULT 0,
		"cprpresent" INT4 DEFAULT 0,
		"ambubagavailable" INT4 DEFAULT 0,
		"oxygenavailable" INT4 DEFAULT 0,
		"surgicalsuite" INT4 DEFAULT 0,
		"certtype" VARCHAR(90) DEFAULT ''::character varying,
		"labonsite" INT4 DEFAULT 0,
		"clianumber" VARCHAR(90) DEFAULT ''::character varying,
		"cliawaiver" VARCHAR(90) DEFAULT ''::character varying,
		"adaaccessibility" INT4 DEFAULT 0,
		"handicapaccessbuilding" INT4 DEFAULT 0,
		"handicapaccessramprail" INT4 DEFAULT 0,
		"handicapaccessparking" INT4 DEFAULT 0,
		"handicapaccesswheel" INT4 DEFAULT 0,
		"handicapaccessrail" INT4 DEFAULT 0,
		"handicapaccesselevator" INT4 DEFAULT 0,
		"handicapaccessbraille" INT4 DEFAULT 0,
		"handicapfountainphone" INT4 DEFAULT 0,
		"nearpublictransportation" INT4 DEFAULT 0,
		"disabledtty" INT4 DEFAULT 0,
		"disabledasl" INT4 DEFAULT 0,
		"childcareservices" INT4 DEFAULT 0,
		"safetyfireextinguisher" INT4 DEFAULT 0,
		"safetyextinguisherinspected" INT4 DEFAULT 0,
		"safetysmokedetectors" INT4 DEFAULT 0,
		"safetycorridorsclear" INT4 DEFAULT 0,
		"infectioncontrolhandwashing" INT4 DEFAULT 0,
		"infectioncontrolgloves" INT4 DEFAULT 0,
		"infectioncontrolbloodcleaned" INT4 DEFAULT 0,
		"facilitycomments" VARCHAR(200) DEFAULT ''::character varying,
		"medicalrecordsindividual" INT4 DEFAULT 0,
		"medicalrecordsinaccessible" INT4 DEFAULT 0,
		"medicalrecordsuniform" INT4 DEFAULT 0,
		"medicalrecordspatientsnameperpage" INT4 DEFAULT 0,
		"medicalrecordssignatureauthor" INT4 DEFAULT 0,
		"medicalrecordsentrydate" INT4 DEFAULT 0,
		"medicalrecordshistory" INT4 DEFAULT 0,
		"medicalrecordsadverse" INT4 DEFAULT 0,
		"medicalrecordscomments" VARCHAR(200) DEFAULT ''::character varying,
		"lastmodifieddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"allowphysedit" INT4 DEFAULT 0,
		"diagnosticultrasound" INT4 DEFAULT 0,
		"endoscopy" INT4 DEFAULT 0,
		"acceptreferralscomments" VARCHAR(200) DEFAULT ''::character varying,
		"patientbringcomments" VARCHAR(200) DEFAULT ''::character varying,
		"patientreferralquestions" VARCHAR(200) DEFAULT ''::character varying,
		"cliaexpirationdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"auditstatus" VARCHAR(100) DEFAULT ''::character varying,
		"auditdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"auditscore" INT4 DEFAULT 0,
		"contractingstatusid" INT4 DEFAULT 0,
		"ownername" VARCHAR(200) DEFAULT ''::character varying,
		"price_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"weburl" VARCHAR(300) DEFAULT ''::character varying,
		"npinumber" VARCHAR(100) DEFAULT ''::character varying,
		"statelicensenumber" VARCHAR(100) DEFAULT ''::character varying,
		"medicarelicensenumber" VARCHAR(100) DEFAULT ''::character varying,
		"walkinmrict" INT4 DEFAULT 0,
		"diagnostictechonly" INT4 DEFAULT 0,
		"dicomforward" INT4 DEFAULT 0,
		"diagnosticafterhours" INT4 DEFAULT 0,
		"malpracticepolicycarrier" VARCHAR(1000) DEFAULT ''::character varying,
		"malpracticepolicynumber" VARCHAR(100) DEFAULT ''::character varying,
		"malpracticepolicyexpdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"billingaddressname" VARCHAR(200) DEFAULT ''::character varying,
		"billingemail" VARCHAR(200) DEFAULT ''::character varying,
		"credentialingcontactaddressname" VARCHAR(200) DEFAULT ''::character varying,
		"rate_curbappeal" INT4 DEFAULT 0,
		"rate_accesssafety" INT4 DEFAULT 0,
		"rate_cleanliness" INT4 DEFAULT 0,
		"rate_appropriateness" INT4 DEFAULT 0,
		"rate_stafffriendly" INT4 DEFAULT 0,
		"rate_attentivetimely" INT4 DEFAULT 0,
		"feeschedulerefid" INT4 DEFAULT 0,
		"feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"initialcontractdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"contractdate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"pacssystemvendor" VARCHAR(100) DEFAULT ''::character varying,
		"rissystemvendor" VARCHAR(100) DEFAULT ''::character varying,
		"isbluestar" INT4 DEFAULT 0,
		"initialquota" INT4 DEFAULT 0,
		"paymenttermsnet" INT4 DEFAULT 0,
		"paymenttermsmethod" INT4 DEFAULT 0,
		"diagnosticmdarthrogram" INT4 DEFAULT 0,
		"officemdarthrogramhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdarthrogramhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"diagnosticmdmyelogram" INT4 DEFAULT 0,
		"officemdmyelogramhoursopenmonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopentuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopenwednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopenthursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopenfriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopensaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursopensunday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosemonday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosetuesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosewednesday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosethursday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosefriday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosesaturday" VARCHAR(10) DEFAULT ''::character varying,
		"officemdmyelogramhoursclosesunday" VARCHAR(10) DEFAULT ''::character varying,
		"petlinqid" VARCHAR(10) DEFAULT ''::character varying,
		"doesaging" INT4 DEFAULT 0,
		"selectmri_id" INT4 DEFAULT 0,
		"selectmri_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"selectmri_qbid" VARCHAR(50) DEFAULT ''::character varying,
		"scheduling_notes" VARCHAR(5000) DEFAULT ''::character varying,
		"nextimageteleradmember" INT4 DEFAULT 0,
		"breader" INT4 DEFAULT 0,
		"contractglobal" INT4 DEFAULT 0,
		"contracttechonly" INT4 DEFAULT 0,
		"contractprofessionalonly" INT4 DEFAULT 0,
		"feescheduleoverrefid" INT4 DEFAULT 0,
		"feeoverpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_mri_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_ct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"faxreportreminderoverride" VARCHAR(50),
		"gh_price_mod_mri_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_w" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_w_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_mri_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_w_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_ct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_feeschedulerefid" INT4 DEFAULT 0,
		"gh_feepercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_feescheduleoverrefid" INT4 DEFAULT 0,
		"gh_feeoverpercentage" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_initialquota" INT4 DEFAULT 0,
		"gh_paymenttermsnet" INT4 DEFAULT 0,
		"gh_paymenttermsmethod" INT4 DEFAULT 0,
		"gh_doesaging" INT4 DEFAULT 0,
		"gh_diagnosticmdarthrogram" INT4 DEFAULT 0,
		"gh_diagnosticmdmyelogram" INT4 DEFAULT 0,
		"clienttypeid" INT4 DEFAULT 3,
		"terminated" INT4 DEFAULT 2,
		"terminateddate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"price_mod_petct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_petct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"price_mod_petct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_petct_wwo" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_petct_wwo_tc" NUMERIC(9 , 2) DEFAULT 0.0,
		"gh_price_mod_petct_wwo_26" NUMERIC(9 , 2) DEFAULT 0.0,
		"hascardiacmr" INT4 DEFAULT 0,
		"hascardiacct" INT4 DEFAULT 0
	);

CREATE TABLE "public"."tgroupsecurityitemsli" (
		"groupsecurityitemsid" INT4 NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(100) DEFAULT ''::character varying,
		"securitygroupid" INT4 DEFAULT 0,
		"groupsecurityitem" VARCHAR(200) DEFAULT ''::character varying,
		"groupsecuritydesc" VARCHAR(200) DEFAULT ''::character varying
	);

CREATE TABLE "public"."tadminpracticelu" (
		"lookupid" SERIAL DEFAULT nextval('tadminpracticelu_lookupid_seq'::regclass) NOT NULL,
		"uniquecreatedate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifydate" TIMESTAMP DEFAULT '1800-01-01 00:00:00'::timestamp without time zone,
		"uniquemodifycomments" VARCHAR(400) DEFAULT ''::character varying,
		"adminid" INT4 DEFAULT 0,
		"practiceid" INT4 DEFAULT 0,
		"comments" VARCHAR(1000) DEFAULT ''::character varying,
		"relationshiptypeid" INT4 DEFAULT 0
	);

CREATE VIEW "public"."vMQ_Services-NoVoid" (UA_Sched_LogonUserName, UA_Sched_FirstName, UA_Sched_LastName, PayerID, PayerName, CaseClaimNumber, UA_Adj_LogonUserName, UA_Adj_FirstName, UA_Adj_LastName, PatientFirstName, PatientLastName, UA_RefDr_LogonUserName, UA_RefDr_FirstName, UA_RefDr_LastName, ScanPass, ReportFileID, iReadingRadiologist, EncounterStatusID, EncounterStatus, iPracticeID, PracticeName, PracticeZIP, dPaymentToProvider1, dPaymentToProvider2, dPaymentToProvider3, CPT, BillAmount, AllowAmount, AllowAmountAdjustment, ReceivedAmount, PaidOutAmount, ReceiveDate, display_ReceiveDate, iMonth_ReceiveDate, iDay_ReceiveDate, iDOW_ReceiveDate, iYear_ReceiveDate, DateOfService, display_DateOfService, iMonth_DateOfService, iDay_DateOfService, iDOW_DateOfService, iYear_DateOfService, AppointmentTime, display_AppointmentTime, iMonth_AppointmentTime, iDay_AppointmentTime, iDOW_AppointmentTime, iYear_AppointmentTime, SentTo_Bill_Pay, display_SentTo_Bill_Pay, iMonth_SentTo_Bill_Pay, iDay_SentTo_Bill_Pay, iDOW_SentTo_Bill_Pay, iYear_SentTo_Bill_Pay, Rec_Bill_Pro, display_Rec_Bill_Pro, iMonth_Rec_Bill_Pro, iDay_Rec_Bill_Pro, iDOW_Rec_Bill_Pro, iYear_Rec_Bill_Pro, TimeTrack_ReqRec, display_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, TimeTrack_ReqCreated, display_TimeTrack_ReqCreated, iMonth_TimeTrack_ReqCreated, iDay_TimeTrack_ReqCreated, iDOW_TimeTrack_ReqCreated, iYear_TimeTrack_ReqCreated, TimeTrack_ReqProc, display_TimeTrack_ReqProc, iMonth_TimeTrack_ReqProc, iDay_TimeTrack_ReqProc, iDOW_TimeTrack_ReqProc, iYear_TimeTrack_ReqProc, TimeTrack_ReqSched, display_TimeTrack_ReqSched, iMonth_TimeTrack_ReqSched, iDay_TimeTrack_ReqSched, iDOW_TimeTrack_ReqSched, iYear_TimeTrack_ReqSched, TimeTrack_ReqDelivered, display_TimeTrack_ReqDelivered, iMonth_TimeTrack_ReqDelivered, iDay_TimeTrack_ReqDelivered, iDOW_TimeTrack_ReqDelivered, iYear_TimeTrack_ReqDelivered, TimeTrack_ReqPaidIN, display_TimeTrack_ReqPaidIN, iMonth_TimeTrack_ReqPaidIN, iDay_TimeTrack_ReqPaidIN, iDOW_TimeTrack_ReqPaidIN, iYear_TimeTrack_ReqPaidIN, TimeTrack_ReqPaidOut, display_TimeTrack_ReqPaidOut, iMonth_TimeTrack_ReqPaidOut, iDay_TimeTrack_ReqPaidOut, iDOW_TimeTrack_ReqPaidOut, iYear_TimeTrack_ReqPaidOut, TimeTrack_ReqApproved, display_TimeTrack_ReqApproved, iMonth_TimeTrack_ReqApproved, iDay_TimeTrack_ReqApproved, iDOW_TimeTrack_ReqApproved, iYear_TimeTrack_ReqApproved, TimeTrack_ReqInitialAppointment, display_TimeTrack_ReqInitialAppointment, iMonth_TimeTrack_ReqInitialAppointment, iDay_TimeTrack_ReqInitialAppointment, iDOW_TimeTrack_ReqInitialAppointment, iYear_TimeTrack_ReqInitialAppointment, TimeTrack_ReqRxReview, display_TimeTrack_ReqRxReview, iMonth_TimeTrack_ReqRxReview, iDay_TimeTrack_ReqRxReview, iDOW_TimeTrack_ReqRxReview, iYear_TimeTrack_ReqRxReview) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."testview" (caseid, assignedtoid, scanpass, payername, adjuster_first, adjuster_last, patient_first, patient_last, patient_state, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."jan_sp" (Parent_PayerName, payer_office, adjuster_first, adjuster_last, january_scanpass) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."getRawPowerBi" (encounterid, region_owner, salesdivision, ref_rec_date, ref_rec_receiveyear, ref_rec_receivemonth, ref_rec_receiveday, scanpass, encounter_status, encounterstatusid, line_of_business, void_reason, voidleakreasonid, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, pt_home_phone, patientcellphone, patientemail, patientdob, pt_doi, payertypeid, payerid, parentpayerid, parent, payername, employername, schd_first, schd_last, referral_method, stat, senttoadj, encounter_type, caseclaimnumber, appointmenttime, dateofservice, dosreceiveyear, dosreceivemonth, dosreceiveday, time_rxrev, time_sched, time_appt_to_rprv, time_create_to_rp, referralid, referralby_first, referralby_last, adj_first, adj_last, adj_phone, adj_fax, adj_email, ncm_first, ncm_last, ncm_phone, ncm_fax, ncm_email, adm1_first, adm1_last, adm1_phone, adm1_fax, adm1_email, cpt, cptbodypart, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip, prac_ph, prac_fax, contract_status, pid, courtesy, courtesyid, retro, in_netdev, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3amount, paidtoprovidercheck3date, receivedamount, receivedcheck1amount, receivedcheck1date, receivedcheck2amount, receivedcheck2date, receivedcheck3amount, receivedcheck3date, allowamount, billamount, payer_fs, istatus, reference_feescheduleamount, reference_ucamount, reference_providercontractamount, reference_payerbillamount, reference_payercontractamount, paidoutamount, allowamountadjustment, noshow, patient_to_ic_miles) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."Active_NOW_Terminated_Practices" (practiceid, initialcontractdate, officeaddress1, officeaddress2, officecity, officestate, officezip, officefederaltaxid, npinumber, terminated_date) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."ttop40" (payername, payerid, fourmonthago, threemonthago, twomonthsago, lastmonth, thismonth, projected, total) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."needs rx review" (caseid, assignedtoid, scanpass, payername, adjuster_first, adjuster_last, patient_first, patient_last, patient_state, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vNIM_Daily_Division" (iMonth_ReceiveDate, iDay_ReceiveDate, iYear_ReceiveDate, ScanPass, sdivision, AssignedToID) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_B2A" (iYear, iMonth, Date_Full, Contact_Full, Contact_Type, PrimaryContact_Full, PayerName, SalesDivision, ParentPayerName, ScanPass, Count_L1, Count_L2, Count_Referrals, Capacity, Expected) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."incomplete data" (scanpass, caseid, assignedtoid, payername, adjuster_first, adjuster_last, patientfirstname, patientlastname, patient_state, hours, status, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ5_Services_Transactions" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, ServiceID, Service_StatusID, Service_Status, Service_TypeID, Service_CPT, Service_CPT_Qty, Service_CPT_Qty_Bill, Service_CPT_Qty_Pay, Service_Ref_Provider_Contracted_Amount_Unit, Service_Ref_FeeSchedule_Amount_Unit, Service_Ref_UC_Amount_Unit, Service_Ref_Payer_Bill_Amount_Unit, Service_Ref_Payer_Contract_Amount_Unit, Service_Ref_Provider_Contracted_Amount_Total, Service_Ref_Provider_Billed_Amount_Total, Service_Ref_FeeSchedule_Amount_TotalBill, Service_Ref_FeeSchedule_Amount_TotalPay, Service_Ref_UC_Amount_TotalBill, Service_Ref_UC_Amount_TotalPay, Service_Ref_Payer_Bill_Amount_Total, Service_Ref_Payer_Contract_Amount_Total, Service_CPTModifier, Service_CPTBodyPart, Service_ICD_1, Service_ICD_2, Service_ICD_3, Service_ICD_4, Service_CPTText, V1_Service_BillAmount, V1_Service_AllowAmount, V1_Service_AllowAmountAdjustment, V1_Service_ReceivedAmount, V1_Service_PaidOutAmount, V1_Service_ReceivedAmount_CheckCalc, ServiceTransaction_Type, ServiceTransaction_Amount, ServiceTransaction_Ref_Number, ServiceTransaction_exporteddate, ServiceTransaction_exporteddate_display, ServiceTransaction_exporteddate_iMonth, ServiceTransaction_exporteddate_iDay, ServiceTransaction_exporteddate_iDOW, ServiceTransaction_exporteddate_iYear, ServiceTransaction_referencedate, ServiceTransaction_referencedate_display, ServiceTransaction_referencedate_iMonth, ServiceTransaction_referencedate_iDay, ServiceTransaction_referencedate_iDOW, ServiceTransaction_referencedate_iYear) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Practices_TIN_List" (practiceid, practicename, officeaddress1, officeaddress2, officecity, officestate, officezip, officecounty, ContractStatus, officefederaltaxid, npinumber, statelicensenumber, officephone) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vSummary of Referrals6" (Case Create Date, Case#, Patient Last Name, Patient First Name, Procedure Code, Description, Date of Service, Payor Name, Referring Dr Last Name, Referring Dr First Name, Imaging Center, Billed, Allowed, Paid, Paid Date, Check#, Paid to IC) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Services" (AssignedToID, UA_Sched_LogonUserName, UA_Sched_FirstName, UA_Sched_LastName, PayerID, PayerName, CaseClaimNumber, UA_Adj_LogonUserName, UA_Adj_FirstName, UA_Adj_LastName, PatientFirstName, PatientLastName, UA_RefDr_LogonUserName, UA_RefDr_FirstName, UA_RefDr_LastName, ScanPass, ReportFileID, iReadingRadiologist, EncounterStatusID, EncounterStatus, iPracticeID, PracticeName, PracticeZIP, dPaymentToProvider1, dPaymentToProvider2, dPaymentToProvider3, CPT, BillAmount, AllowAmount, AllowAmountAdjustment, ReceivedAmount, PaidOutAmount, ReceiveDate, display_ReceiveDate, iMonth_ReceiveDate, iDay_ReceiveDate, iDOW_ReceiveDate, iYear_ReceiveDate, DateOfService, display_DateOfService, iMonth_DateOfService, iDay_DateOfService, iDOW_DateOfService, iYear_DateOfService, AppointmentTime, display_AppointmentTime, iMonth_AppointmentTime, iDay_AppointmentTime, iDOW_AppointmentTime, iYear_AppointmentTime, SentTo_Bill_Pay, display_SentTo_Bill_Pay, iMonth_SentTo_Bill_Pay, iDay_SentTo_Bill_Pay, iDOW_SentTo_Bill_Pay, iYear_SentTo_Bill_Pay, Rec_Bill_Pro, display_Rec_Bill_Pro, iMonth_Rec_Bill_Pro, iDay_Rec_Bill_Pro, iDOW_Rec_Bill_Pro, iYear_Rec_Bill_Pro, TimeTrack_ReqRec, display_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, TimeTrack_ReqCreated, display_TimeTrack_ReqCreated, iMonth_TimeTrack_ReqCreated, iDay_TimeTrack_ReqCreated, iDOW_TimeTrack_ReqCreated, iYear_TimeTrack_ReqCreated, TimeTrack_ReqProc, display_TimeTrack_ReqProc, iMonth_TimeTrack_ReqProc, iDay_TimeTrack_ReqProc, iDOW_TimeTrack_ReqProc, iYear_TimeTrack_ReqProc, TimeTrack_ReqSched, display_TimeTrack_ReqSched, iMonth_TimeTrack_ReqSched, iDay_TimeTrack_ReqSched, iDOW_TimeTrack_ReqSched, iYear_TimeTrack_ReqSched, TimeTrack_ReqDelivered, display_TimeTrack_ReqDelivered, iMonth_TimeTrack_ReqDelivered, iDay_TimeTrack_ReqDelivered, iDOW_TimeTrack_ReqDelivered, iYear_TimeTrack_ReqDelivered, TimeTrack_ReqPaidIN, display_TimeTrack_ReqPaidIN, iMonth_TimeTrack_ReqPaidIN, iDay_TimeTrack_ReqPaidIN, iDOW_TimeTrack_ReqPaidIN, iYear_TimeTrack_ReqPaidIN, TimeTrack_ReqPaidOut, display_TimeTrack_ReqPaidOut, iMonth_TimeTrack_ReqPaidOut, iDay_TimeTrack_ReqPaidOut, iDOW_TimeTrack_ReqPaidOut, iYear_TimeTrack_ReqPaidOut, TimeTrack_ReqApproved, display_TimeTrack_ReqApproved, iMonth_TimeTrack_ReqApproved, iDay_TimeTrack_ReqApproved, iDOW_TimeTrack_ReqApproved, iYear_TimeTrack_ReqApproved, TimeTrack_ReqInitialAppointment, display_TimeTrack_ReqInitialAppointment, iMonth_TimeTrack_ReqInitialAppointment, iDay_TimeTrack_ReqInitialAppointment, iDOW_TimeTrack_ReqInitialAppointment, iYear_TimeTrack_ReqInitialAppointment, TimeTrack_ReqRxReview, display_TimeTrack_ReqRxReview, iMonth_TimeTrack_ReqRxReview, iDay_TimeTrack_ReqRxReview, iDOW_TimeTrack_ReqRxReview, iYear_TimeTrack_ReqRxReview, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ2_Practice_Services" (practiceid, officezip, contractingstatusid, contractglobal, contracttechonly, contractprofessionalonly, diagnosticmdarthrogram, diagnosticmdmyelogram, doesaging, hasus, hasmr, mr_open_class, mr_open_tesla, mr_trad_class, mr_trad_tesla, hasct, hasfl, hasnm, hasxr, hasemg, haspet) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."missing patient availability" (caseid, assignedtoid, payername, adjuster_first, adjuster_last, patient_first, patient_last, scanpass, patient_state, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."getCustomFGA" (encounterid, pt_lat, pt_lon, ic_lat, ic_lon, ref_rec_date, in_netdev, voidleakreasonlong, voidnotes, is_courtesy, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, payername, encounter_type, retro, caseclaimnumber, dateofservice, commtypeid, time_rxrev, time_sched, time_appt_to_rprv, time_create_to_rp, cpt, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."missing patient homephone" (caseid, assignedtoid, payername, adjuster_first, adjuster_last, patient_first, patient_last, scanpass, patient_state, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."monthlyschedulernumbers" (scheduler, mon, yyyy, count) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."report review" (caseid, assignedtoid, payername, adjuster_first, adjuster_last, patient_first, patient_last, patient_state, scanpass, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vEmail_Report1" (iday, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, PayerName, count) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."TysonReadyToBill" (ReferralID, hcfa_filename, report_filename) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Encounter_Void_BP" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, Service_iCount, Service_first_StatusID, Service_first_Status, Service_first_TypeID, Service_first_CPT, Service_first_QTY, Service_first_CPTModifier, Service_first_CPTBodyPart, Service_first_ICD_1, Service_first_ICD_2, Service_first_ICD_3, Service_first_ICD_4, Service_first_CPTText, Service_sum_BillAmount, Service_sum_AllowAmount, Service_sum_AllowAmountAdjustment, Service_sum_ReceivedAmount, Service_sum_PaidOutAmount, Service_sum_ReceivedAmount_CheckCalc) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."n4_worklist" (patient, payer, assignedtoid, patientstateid, receivedate, scanpass, caseid, encounterid, issched, dateofservice, iscritdata, isrxrw, isrxuploaded, isauthuploaded, isservicecomplete, isreportuploaded, ispostappointment, reportreminder, faxcount, sentrefrecvcomplete, sentspcomplete, sentreportcomplete, isnextactiondate, nextactiondate, nextactionnotes) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_Sch_CaseWorkload" (UA_Sched_FirstName, EncounterStatus, TotalCases, chartdisplay) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."all_payers" (row_to_json) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."test" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, ServiceID, Service_StatusID, Service_Status, Service_TypeID, Service_CPT, Service_CPT_Qty, Service_CPTModifier, Service_CPTBodyPart, Service_ICD_1, Service_ICD_2, Service_ICD_3, Service_ICD_4, Service_CPTText, Service_BillAmount, Service_AllowAmount, Service_AllowAmountAdjustment, Service_ReceivedAmount, Service_PaidOutAmount, Service_ReceivedAmount_CheckCalc) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Encounter_NoVoid_BP" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, Service_iCount, Service_first_StatusID, Service_first_Status, Service_first_TypeID, Service_first_CPT, Service_first_QTY, Service_first_CPTModifier, Service_first_CPTBodyPart, Service_first_ICD_1, Service_first_ICD_2, Service_first_ICD_3, Service_first_ICD_4, Service_first_CPTText, Service_sum_BillAmount, Service_sum_AllowAmount, Service_sum_AllowAmountAdjustment, Service_sum_ReceivedAmount, Service_sum_PaidOutAmount, Service_sum_ReceivedAmount_CheckCalc, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName, Service_modality) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Services" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, ServiceID, Service_StatusID, Service_Status, Service_TypeID, Service_CPT, Service_CPT_Qty, Service_CPTModifier, Service_CPTBodyPart, Service_ICD_1, Service_ICD_2, Service_ICD_3, Service_ICD_4, Service_CPTText, Service_BillAmount, Service_AllowAmount, Service_AllowAmountAdjustment, Service_ReceivedAmount, Service_PaidOutAmount, Service_ReceivedAmount_CheckCalc, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName, Service_modality, Patient_Zip, Practice_Zip) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."SchedulerStats" (scheduler, assignedcases, scheduled) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vBilling_Report" (Receive Date, Encounter_Type, pricing_structure, PatientFirstName, PatientLastName, Claim Number, ScanPass, DOS, Encounter_DateOfService_iMonth, Encounter_DateOfService_iYear, Encounter_IsRetro, Payer, Parent_PayerName, UA_Adjuster_FirstName, UA_Adjuster_LastName, Provider, Report Received, Date Billed to Payer, Allow, Allow_Amt_Net, Date Paid by Payer, Days AR, Amount Received From Payer, AR Balance, AR Balance (Zero), AR Balance (Negative), Date Bill Rec'd From Provider, Amount Billed by Provider, Date Paid to Provider, Days AP From Bill Date, Expected Pay Out, Expected Profit, Actual Paid Out, Provider Check Date1, AP Balance, AP Balance (Zero), AP Balance (Negative), Actual Profit, Encounter Status, Encounter_IsCourtesy, Adjustment, Payer_SalesDivision, Payer_AcquisitionDivision, Practice_State, PayerState, PracticeID, name, adminid, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName, Service_modality) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vND_Overview" (stateid, shortstate, i_total, i_contracted, i_negotiate, avg_mri_w, min_mri_w, max_mri_w) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."mQ5" (salesdivision, receivedate, receiveyear, receivemonth, receiveday, scanpass, encounter_status, encounterstatusid, voidleakreasonid, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, pt_home_phone, patientcellphone, patientemail, patientdob, pt_doi, payertypeid, payerid, parentpayerid, parent, payername, employername, schd_first, schd_last, referral_method, stat, senttoadj, encounter_type, caseclaimnumber, timetrack_reqsched, appointmenttime, dateofservice, dosreceiveyear, dosreceivemonth, dosreceiveday, time_rxrev, time_sched, hrs_app_to_rprv, referralid, referredby_first, referredby_last, adj_first, adj_last, adj_phone, adj_fax, adj_email, ncm_first, ncm_last, ncm_phone, ncm_fax, ncm_email, adm1_first, adm1_last, adm1_phone, adm1_fax, adm1_email, adm2_first, adm2_last, ca2_phone, adm2_fax, adm2_email, adm3_first, adm3_last, ca3_phone, adm3_fax, adm3_email, adm4_first, adm4_last, ca4_phone, adm4_fax, adm4_email, md_first, md_last, md_phone, md_fax, md_email, cpt, cptbodypart, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip, prac_ph, prac_fax, pid, courtesy, courtesyid, retro, in_netdev, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3amount, paidtoprovidercheck3date, receivedamount, receivedcheck1amount, receivedcheck1date, receivedcheck2amount, receivedcheck2date, receivedcheck3amount, receivedcheck3date, allowamount, billamount, payer_fs) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_Sch_EncounterAnalysis_Missing_TT_RRec" (PayerID, PayerName, ScanPass, display_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, display_AppointmentTime, display_TimeTrack_ReqSched, display_TimeTrack_ReqDelivered) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."feb_sp" (adjusterid, feb_scanpass) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."tEmailT_Errors" (emailtransactionid, uniquecreatedate, transactiondate, actionid, emailto, emailsubject) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Encounters-NoVoid" (AssignedToID, UA_Sched_LogonUserName, UA_Sched_FirstName, UA_Sched_LastName, PayerID, PayerName, CaseClaimNumber, UA_Adj_LogonUserName, UA_Adj_FirstName, UA_Adj_LastName, PatientFirstName, PatientLastName, UA_RefDr_LogonUserName, UA_RefDr_FirstName, UA_RefDr_LastName, ScanPass, ReportFileID, iReadingRadiologist, EncounterStatusID, EncounterStatus, iPracticeID, PracticeName, PracticeZIP, dTotal_PaymentToProvider, iTotalServices, dSum_BillAmount, dSum_AllowAmount, dSum_AllowAmountAdjustment, dSum_ReceivedAmount, dSum_PaidOutAmount, ReceiveDate, display_ReceiveDate, iMonth_ReceiveDate, iDay_ReceiveDate, iDOW_ReceiveDate, iYear_ReceiveDate, DateOfService, display_DateOfService, iMonth_DateOfService, iDay_DateOfService, iDOW_DateOfService, iYear_DateOfService, AppointmentTime, display_AppointmentTime, iMonth_AppointmentTime, iDay_AppointmentTime, iDOW_AppointmentTime, iYear_AppointmentTime, SentTo_Bill_Pay, display_SentTo_Bill_Pay, iMonth_SentTo_Bill_Pay, iDay_SentTo_Bill_Pay, iDOW_SentTo_Bill_Pay, iYear_SentTo_Bill_Pay, Rec_Bill_Pro, display_Rec_Bill_Pro, iMonth_Rec_Bill_Pro, iDay_Rec_Bill_Pro, iDOW_Rec_Bill_Pro, iYear_Rec_Bill_Pro, TimeTrack_ReqRec, display_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, TimeTrack_ReqCreated, display_TimeTrack_ReqCreated, iMonth_TimeTrack_ReqCreated, iDay_TimeTrack_ReqCreated, iDOW_TimeTrack_ReqCreated, iYear_TimeTrack_ReqCreated, TimeTrack_ReqProc, display_TimeTrack_ReqProc, iMonth_TimeTrack_ReqProc, iDay_TimeTrack_ReqProc, iDOW_TimeTrack_ReqProc, iYear_TimeTrack_ReqProc, TimeTrack_ReqSched, display_TimeTrack_ReqSched, iMonth_TimeTrack_ReqSched, iDay_TimeTrack_ReqSched, iDOW_TimeTrack_ReqSched, iYear_TimeTrack_ReqSched, TimeTrack_ReqDelivered, display_TimeTrack_ReqDelivered, iMonth_TimeTrack_ReqDelivered, iDay_TimeTrack_ReqDelivered, iDOW_TimeTrack_ReqDelivered, iYear_TimeTrack_ReqDelivered, TimeTrack_ReqPaidIN, display_TimeTrack_ReqPaidIN, iMonth_TimeTrack_ReqPaidIN, iDay_TimeTrack_ReqPaidIN, iDOW_TimeTrack_ReqPaidIN, iYear_TimeTrack_ReqPaidIN, TimeTrack_ReqPaidOut, display_TimeTrack_ReqPaidOut, iMonth_TimeTrack_ReqPaidOut, iDay_TimeTrack_ReqPaidOut, iDOW_TimeTrack_ReqPaidOut, iYear_TimeTrack_ReqPaidOut, TimeTrack_ReqApproved, display_TimeTrack_ReqApproved, iMonth_TimeTrack_ReqApproved, iDay_TimeTrack_ReqApproved, iDOW_TimeTrack_ReqApproved, iYear_TimeTrack_ReqApproved, TimeTrack_ReqRxReview, display_TimeTrack_ReqRxReview, iMonth_TimeTrack_ReqRxReview, iDay_TimeTrack_ReqRxReview, iDOW_TimeTrack_ReqRxReview, iYear_TimeTrack_ReqRxReview, TimeTrack_ReqInitialAppointment, display_TimeTrack_ReqInitialAppointment, iMonth_TimeTrack_ReqInitialAppointment, iDay_TimeTrack_ReqInitialAppointment, iDOW_TimeTrack_ReqInitialAppointment, iYear_TimeTrack_ReqInitialAppointment, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vCaseDetails_1" (caseid, referralid, caseclaimnumber, adjusterid, vUA_Adjuster_LU, vUA_Adjuster_PayerID, vUA_Adjuster_FirstName, vUA_Adjuster_LastName, vUA_Adjuster_Phone, vUA_Adjuster_Fax, patientfirstname, patientlastname, patienthomephone, patientcellphone, referringphysicianid, vUA_RefDr_LU, vUA_RefDr_FirstName, vUA_RefDr_LastName, vUA_RefDr_Phone, vUA_RefDr_Fax, referraldate, encounterid, scanpass, serviceid, cpt, cptqty, cptbodypart) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."Active_New_Practices" (practiceid, officeaddress1, officeaddress2, officecity, officestate, officezip, officefederaltaxid, npinumber, effective_date) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."getBusinessDataSum" (encounterid, region_owner, salesdivision, ref_rec_date, ref_rec_receiveyear, ref_rec_receivemonth, ref_rec_receiveday, scanpass, encounter_status, encounterstatusid, line_of_business, void_reason, voidleakreasonid, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, pt_home_phone, patientcellphone, patientemail, patientdob, pt_doi, payertypeid, payerid, parentpayerid, parent, payername, employername, schd_first, schd_last, referral_method, stat, senttoadj, encounter_type, caseclaimnumber, appointmenttime, dateofservice, dosreceiveyear, dosreceivemonth, dosreceiveday, time_rxrev, time_sched, time_appt_to_rprv, time_create_to_rp, referralid, referralby_first, referralby_last, adj_first, adj_last, adj_phone, adj_fax, adj_email, ncm_first, ncm_last, ncm_phone, ncm_fax, ncm_email, adm1_first, adm1_last, adm1_phone, adm1_fax, adm1_email, cpt, cptbodypart, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip, prac_ph, prac_fax, contract_status, pid, courtesy, courtesyid, retro, in_netdev, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3amount, paidtoprovidercheck3date, receivedamount, receivedcheck1amount, receivedcheck1date, receivedcheck2amount, receivedcheck2date, receivedcheck3amount, receivedcheck3date, allow, billamt, payer_fs, fee_schedule_amount, uc_amount, provider_contractamount, payer_billamount, payer_contractamount, paid_out_amount, allow_amount_adjustment, istatus, noshow, unscheduledClient, patient_to_ic_miles) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."tTop40_java" (payername, payerid, fourmonthago, threemonthago, twomonthsago, lastmonth, thismonth, projected, change, trailing 3 months) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Services2-NoVoid" (AssignedToID, UA_Sched_LogonUserName, UA_Sched_FirstName, UA_Sched_LastName, PayerID, PayerName, CaseClaimNumber, UA_Adj_UserID, UA_Adj_LogonUserName, UA_Adj_FirstName, UA_Adj_LastName, PatientFirstName, PatientLastName, UA_RefDr_UserID, UA_RefDr_LogonUserName, UA_RefDr_FirstName, UA_RefDr_LastName, ScanPass, ReportFileID, iReadingRadiologist, EncounterStatusID, EncounterStatus, iPracticeID, PracticeName, PracticeZIP, dPaymentToProvider1, dPaymentToProvider2, dPaymentToProvider3, CPT, BillAmount, AllowAmount, AllowAmountAdjustment, ReceivedAmount, PaidOutAmount, ReceiveDate, display_ReceiveDate, iMonth_ReceiveDate, iDay_ReceiveDate, iDOW_ReceiveDate, iYear_ReceiveDate, DateOfService, display_DateOfService, iMonth_DateOfService, iDay_DateOfService, iDOW_DateOfService, iYear_DateOfService, AppointmentTime, display_AppointmentTime, iMonth_AppointmentTime, iDay_AppointmentTime, iDOW_AppointmentTime, iYear_AppointmentTime, SentTo_Bill_Pay, display_SentTo_Bill_Pay, iMonth_SentTo_Bill_Pay, iDay_SentTo_Bill_Pay, iDOW_SentTo_Bill_Pay, iYear_SentTo_Bill_Pay, Rec_Bill_Pro, display_Rec_Bill_Pro, iMonth_Rec_Bill_Pro, iDay_Rec_Bill_Pro, iDOW_Rec_Bill_Pro, iYear_Rec_Bill_Pro, TimeTrack_ReqRec, display_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, TimeTrack_ReqCreated, display_TimeTrack_ReqCreated, iMonth_TimeTrack_ReqCreated, iDay_TimeTrack_ReqCreated, iDOW_TimeTrack_ReqCreated, iYear_TimeTrack_ReqCreated, TimeTrack_ReqProc, display_TimeTrack_ReqProc, iMonth_TimeTrack_ReqProc, iDay_TimeTrack_ReqProc, iDOW_TimeTrack_ReqProc, iYear_TimeTrack_ReqProc, TimeTrack_ReqSched, display_TimeTrack_ReqSched, iMonth_TimeTrack_ReqSched, iDay_TimeTrack_ReqSched, iDOW_TimeTrack_ReqSched, iYear_TimeTrack_ReqSched, TimeTrack_ReqDelivered, display_TimeTrack_ReqDelivered, iMonth_TimeTrack_ReqDelivered, iDay_TimeTrack_ReqDelivered, iDOW_TimeTrack_ReqDelivered, iYear_TimeTrack_ReqDelivered, TimeTrack_ReqPaidIN, display_TimeTrack_ReqPaidIN, iMonth_TimeTrack_ReqPaidIN, iDay_TimeTrack_ReqPaidIN, iDOW_TimeTrack_ReqPaidIN, iYear_TimeTrack_ReqPaidIN, TimeTrack_ReqPaidOut, display_TimeTrack_ReqPaidOut, iMonth_TimeTrack_ReqPaidOut, iDay_TimeTrack_ReqPaidOut, iDOW_TimeTrack_ReqPaidOut, iYear_TimeTrack_ReqPaidOut, TimeTrack_ReqApproved, display_TimeTrack_ReqApproved, iMonth_TimeTrack_ReqApproved, iDay_TimeTrack_ReqApproved, iDOW_TimeTrack_ReqApproved, iYear_TimeTrack_ReqApproved, TimeTrack_ReqInitialAppointment, display_TimeTrack_ReqInitialAppointment, iMonth_TimeTrack_ReqInitialAppointment, iDay_TimeTrack_ReqInitialAppointment, iDOW_TimeTrack_ReqInitialAppointment, iYear_TimeTrack_ReqInitialAppointment, TimeTrack_ReqRxReview, display_TimeTrack_ReqRxReview, iMonth_TimeTrack_ReqRxReview, iDay_TimeTrack_ReqRxReview, iDOW_TimeTrack_ReqRxReview, iYear_TimeTrack_ReqRxReview) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Filter_Service_BP" (serviceid, servicestatusid) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."util_report" (Parent_PayerID, PayerID, adjusterid, Parent_PayerName, PayerName, adjuster_first, adjuster_last, scanpass, receive_month, receive_day, receive_year, Encounter_Type, Service_modality, paid_to_provider, paidout_amount, allow_amount, sum_adjustment, received_amount, sum_received_amount) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vSummary of Referrals7 by Patient Name" (Case Create Date, Case#, Patient Last Name, Patient First Name, Procedure Code, Description, Date of Service, Payor Name, Referring Dr Last Name, Referring Dr First Name, Imaging Center, Billed, Allowed, Paid, Paid Date, Check#, Paid to IC Amount, Paid To IC Check Number, Paid To IC Date) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."provider_list" (provider_id, location_name, addr1, addr2, city, prac_state, zip, phone, taxid, contract_date, term_date, provider_npid, location_npid, lic_statecode, specialty) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."nim_network" (practiceid, practicename, ownername, relationshiptypeid, officeaddress1, officeaddress2, officecity, sOfficeState, officezip, officephone, officefaxno, officeemail, officefederaltaxid, npinumber, contractingstatusid, Contract_Status, price_mod_mri_wo, price_mod_mri_w, price_mod_mri_wwo, price_mod_ct_wo, price_mod_ct_w, price_mod_ct_wwo, count_mod_mri_wo, count_mod_mri_w, count_mod_mri_wwo, count_mod_ct_wo, count_mod_ct_w, count_mod_ct_wwo, feescheduleoverrefid, feepercentage, feeoverpercentage, paymenttermsnet, initialcontractdate, billingpayableto, billingfirstname, billinglastname, billingaddress1, billingaddress2, billingcity, billingstateid, billingzip, billingphone, billingfax, Account_Name, Account_Contact_Firstname, Account_Contact_Lastname, Account_Contact_Add1, Account_Contact_Add2, Account_Contact_City, shortstate, Account_Contact_Zip, Account_Contact_Phone, Account_Contact_Fax, Account_Contact__Email, price_mod_mri_w_26, price_mod_mri_wo_26, price_mod_mri_wwo_26, price_mod_ct_w_26, price_mod_ct_wo_26, price_mod_ct_wwo_26, price_mod_mri_w_tc, price_mod_mri_wo_tc, price_mod_mri_wwo_tc, price_mod_ct_w_tc, price_mod_ct_wo_tc, price_mod_ct_wwo_tc, MR, CT, BMD, FL, MG, NM, US, XR, PET/CT) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."getPowerBiResults" (encounterid, salesdivision, ref_rec_date, ref_rec_receiveyear, ref_rec_receivemonth, ref_rec_receiveday, scanpass, encounter_status, encounterstatusid, line_of_business, voidleakreasonid, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, pt_home_phone, patientcellphone, patientemail, patientdob, pt_doi, payertypeid, payerid, parentpayerid, parent, payername, employername, schd_first, schd_last, referral_method, stat, senttoadj, encounter_type, caseclaimnumber, timetrack_reqsched, appointmenttime, dateofservice, dosreceiveyear, dosreceivemonth, dosreceiveday, time_rxrev, time_sched, hrs_app_to_rprv, referralid, referredby_first, referredby_last, adj_first, adj_last, adj_phone, adj_fax, adj_email, ncm_first, ncm_last, ncm_phone, ncm_fax, ncm_email, adm1_first, adm1_last, adm1_phone, adm1_fax, adm1_email, adm2_first, adm2_last, md_first, md_last, md_phone, cpt, cptbodypart, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip, prac_ph, prac_fax, pid, courtesy, courtesyid, retro, in_netdev, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3amount, paidtoprovidercheck3date, receivedamount, receivedcheck1amount, receivedcheck1date, receivedcheck2amount, receivedcheck2date, receivedcheck3amount, receivedcheck3date, allowamount, billamount, payer_fs) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_NIM-Daily" (payername, cTodayAll, cTodayMR, cTodayCT, cTodayOther, cMonth, cMonthCT, cMonthMR, cMonthOther, cAll, cAll2, cAllMR, cAllCT, cAllOther) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."billing_report_no_cs_with_adjustments_pp" (Receive Date, Encounter_Type, PatientFirstName, PatientLastName, Claim Number, ScanPass, DOS, Payer, Provider, Report Received, Date Billed to Payer, Allow, Date Paid by Payer, Amount Received From Payer, Date Bill Rec'd From Provider, Date Paid to Provider, Paid to Provider, Encounter Status, Encounter_IsCourtesy, Adjustment, Payer_SalesDivision, Payer_AcquisitionDivision, Practice_State, PayerState) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."marty_query" (createdate, scanpass, encounter_status, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, pt_home_phone, patientcellphone, patientemail, patientdob, pt_doi, payername, employername, schd_first, schd_last, referral_method, stat, senttoadj, encounter_type, caseclaimnumber, timetrack_reqsched, appointmenttime, dateofservice, time_rxrev, time_sched, hrs_app_to_rprv, referralid, referredby_first, referredby_last, adj_first, adj_last, adj_phone, adj_fax, adj_email, ncm_first, ncm_last, ncm_phone, ncm_fax, ncm_email, adm1_first, adm1_last, adm1_phone, adm1_fax, adm1_email, adm2_first, adm2_last, ca2_phone, adm2_fax, adm2_email, adm3_first, adm3_last, ca3_phone, adm3_fax, adm3_email, adm4_first, adm4_last, ca4_phone, adm4_fax, adm4_email, md_first, md_last, md_phone, md_fax, md_email, cpt, cptbodypart, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip, prac_ph, prac_fax, pid, courtesy, retro, in_netdev, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3amount, paidtoprovidercheck3date, receivedamount, receivedcheck1amount, receivedcheck1date, receivedcheck2amount, receivedcheck2date, receivedcheck3amount, receivedcheck3date, allowamount, billamount, payer_fs, pricing_structure) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."unpaid_or_underpaid_cliams_by_adjuster" (Receive Date, PayerName, Adjuster First Name, Adjuster Last Name, PatientFirstName, PatientLastName, CaseClaimNumber, Report Received, Date of Service, Date Billed, Bill, Allow, Paid, Adjustment) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Services_NoVoid_BP" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, ServiceID, Service_StatusID, Service_Status, Service_TypeID, Service_CPT, Service_CPT_Qty, Service_CPTModifier, Service_CPTBodyPart, Service_ICD_1, Service_ICD_2, Service_ICD_3, Service_ICD_4, Service_CPTText, Service_BillAmount, Service_AllowAmount, Service_AllowAmountAdjustment, Service_ReceivedAmount, Service_PaidOutAmount, Service_ReceivedAmount_CheckCalc, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName, Service_modality, Patient_Zip, Practice_Zip) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_ByPayerByDayByMod" (PayerName, iYear_ReceiveDate, iMonth_ReceiveDate, iDay_ReceiveDate, themodality, count, PayerID) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."tnim3_dualprovider_v_payer" (caseid, referringid, attendingid) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vActivity All Payers April 2010" (Claim #, Service, Patient Name, Adjuster Name, Payer Name, Referral Date, Appointment Set, Report Completed) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."nim_monthly_numbers" (parent, branch, scanpass, patientid, practice_name, practice_city, practice_state, practice_zip, referralreceive_date, scheduled_date, appointment_date, report_date, scheduler, image_type, appointment_status) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vActivity View All Payers March 2010" (Claim #, Service, Patient Name, Adjuster Name, Payer Name, Referral Date, Appointment Set, Report Completed) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."next action alerts" (caseid, assignedtoid, payername, adjuster_first, adjuster_last, patient_first, patient_last, patient_state, scanpass, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vSummary of Referrals7 by Create Date" (Case Create Date, Case#, Patient Last Name, Patient First Name, Procedure Code, Description, Date of Service, Payor Name, Referring Dr Last Name, Referring Dr First Name, Imaging Center, Billed, Allowed, Paid, Paid Date, Check#, Paid to IC Amount, Paid To IC Check Number, Paid To IC Date) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."ReferralByContact" (userid, referralsource, rrole, salesdivision, lday, lweek, lmonth, email, fax, phone, payername, address) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_Sch_EncounterAnalysis" (PayerID, PayerName, ScanPass, display_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, display_AppointmentTime, display_TimeTrack_ReqSched, display_TimeTrack_ReqDelivered, timeto_schedule, timeto_appointment, timeto_report) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."rising_HCFA_View" (scanpass, patientfirstname, patientlastname, patientaddress1, patientaddress2, patientcity, patientzip, patienthomephone, patientcellphone, patientdob, dateofinjury, cptbodypart, patientssn, payername, contactfirstname, contactlastname, usernpi, dcpt1, dcpt2, dcpt3, dcpt4, appointmenttime, cpt, cptmodifier, price_mod_ct_w, price_mod_ct_wo, price_mod_ct_wwo, price_mod_mri_w, price_mod_mri_wo, price_mod_mri_wwo, officefederaltaxid, practicename, officeaddress1, officeaddress2, officecity, ofc_state, officezip, billingaddress1, billingaddress2, billingcity, billing_state, billingzip, npinumber) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vCaseCount_AdjusterID" (adjusterid, iCount) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."abcd" (payerid, payername, Jan_Total, Feb_Total, Mar_Total, Apr_Total, May_Total, Jun_Total, Jul_Total, Aug_Total, Sep_Total, Oct_Total, Nov_Total, Dec_Total) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vCaseDetails_2" (caseid, referralid, caseclaimnumber, adjusterid, vUA_Adjuster_LU, vUA_Adjuster_PayerID, vUA_Adjuster_FirstName, vUA_Adjuster_LastName, vUA_Adjuster_Phone, vUA_Adjuster_Fax, patientfirstname, patientlastname, patienthomephone, patientcellphone, referringphysicianid, vUA_RefDr_LU, vUA_RefDr_FirstName, vUA_RefDr_LastName, vUA_RefDr_Phone, vUA_RefDr_Fax, referraldate, encounterid, scanpass) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."ActivePractices_In_Network" (practiceid, officeaddress1, officeaddress2, officecity, officestate, officezip, officefederaltaxid, npinumber, effective_date) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_NIMCRM_CountByUser" (adjuster_userid, PrimaryContact_Full, count) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_Total_ByPayer_ByMod" (PayerID, PayerName, themodality, sum) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."getPbi" (encounterid, region_owner, salesdivision, ref_rec_date, ref_rec_receiveyear, ref_rec_receivemonth, ref_rec_receiveday, scanpass, encounter_status, encounterstatusid, line_of_business, void_reason, voidleakreasonid, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, pt_home_phone, patientcellphone, patientemail, patientdob, pt_doi, payertypeid, payerid, parentpayerid, parent, payername, employername, schd_first, schd_last, referral_method, stat, senttoadj, encounter_type, caseclaimnumber, appointmenttime, dateofservice, dosreceiveyear, dosreceivemonth, dosreceiveday, time_rxrev, time_sched, time_appt_to_rprv, time_create_to_rp, referralid, referralby_first, referralby_last, adj_first, adj_last, adj_phone, adj_fax, adj_email, ncm_first, ncm_last, ncm_phone, ncm_fax, ncm_email, adm1_first, adm1_last, adm1_phone, adm1_fax, adm1_email, cpt, cptbodypart, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip, prac_ph, prac_fax, contract_status, pid, courtesy, courtesyid, retro, in_netdev, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3amount, paidtoprovidercheck3date, receivedamount, receivedcheck1amount, receivedcheck1date, receivedcheck2amount, receivedcheck2date, receivedcheck3amount, receivedcheck3date, allow, billamt, payer_fs, fee_schedule_amount, uc_amount, provider_contractamount, payer_billamount, payer_contractamount, paid_out_amount, allow_amount_adjustment, istatus, noshow, unscheduledClient, patient_to_ic_miles) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ2_Practices" (practiceid, practicename, officeaddress1, officeaddress2, officecity, officestate, officezip, officecounty, officephone, officefaxno, officeemail, officefederaltaxid, officetaxidnameaffiliation, billingpayableto, billingaddress1, billingaddress2, billingcity, billingstate, billingzip, billingphone, billingfax, npinumber, statelicensenumber, medicarelicensenumber, adminid, admin_name, RelationshipType, ContractStatus, isbluestar, contractglobal, contracttechonly, contractprofessionalonly, price_mod_mri_wo, price_mod_mri_w, price_mod_mri_wwo, price_mod_ct_wo, price_mod_ct_w, price_mod_ct_wwo, feeschedulerefid, feepercentage, feescheduleoverrefid, feeoverpercentage, diagnosticmdarthrogram, diagnosticmdmyelogram, doesaging, hasmr, hasct, hasemg, hasus, hasfl, hasnm, hasxr, haspet) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."missing rx" (caseid, assignedtoid, payername, adjuster_first, adjuster_last, patient_first, patient_last, scanpass, patient_state, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vEmail_Pending" (emailtransactionid, uniquecreatedate, transactiondate, actionid, emailto, emailsubject) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Services_Void_BP" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, ServiceID, Service_StatusID, Service_Status, Service_TypeID, Service_CPT, Service_CPT_Qty, Service_CPTModifier, Service_CPTBodyPart, Service_ICD_1, Service_ICD_2, Service_ICD_3, Service_ICD_4, Service_CPTText, Service_BillAmount, Service_AllowAmount, Service_AllowAmountAdjustment, Service_ReceivedAmount, Service_PaidOutAmount, Service_ReceivedAmount_CheckCalc) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Practices" (contractingstatusid, descriptionlong, practiceid, practicename, officeaddress1, officecity, sOfficeState, officezip, officephone, officefaxno, officeemail, price_mod_mri_w, price_mod_mri_wo, price_mod_mri_wwo, price_mod_ct_w, price_mod_ct_wo, price_mod_ct_wwo, rate_curbappeal, rate_accesssafety, rate_cleanliness, rate_appropriateness, rate_stafffriendly, rate_attentivetimely, contractdate) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_Total_ByDrPayer" (Dr First Name, Dr Last Name, Count, Last ScanPass, Last Patient First Name, Last Patient Last Name, imonth, iyear, PayerName) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."amerisys_performance" (Vendor_Company_Name, Month_Year_Activity, Location, AmeriSys_Payor_ID, AmeriSys_Payor_ID_Name, Referral_Source_Name, Description_of_Service, AmeriSys_Claim_ID, Date_of_Injury, IW_Last_Name, First_Name, Date_of_Birth, Home_Work_Address_1, Address_2, City, State, Zip_Code, Referring_NPI, Rendering_NPI, Diagnosis_ICD9, Diagnosis_Descriptor, Which_Body_Part, Quantity, Facility_Address_1, Facility_Address_2, Facility_City, Facility_State, Facility_Zip_Code, Date_Service_Requested, Date_Initial_Contact_made_with_IW, Date_Initial_Appointment_Offered, Schedule_Date_of_Appointment, Schedule_Time_of_Appointment, Actual_Date_Service_Provided, Actual_Time_Service_Provided, Day_of_Final_Service_Delivery_Date, Date_Report_Provided_to_Referral_Source, Invoice, Usual_Customary_or_State_Fee_Schedule, Vendor_Contracted_Price) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_EncounterService_PayoutMax_Date" (encounterid, maxfirstdate) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."ota_at_a_glance" (scanpass, schedfirst, schedlast, courtesy, netdev_status, opendate, closedate, comments, patientfirstname, patientlastname, payername, cpt, body_part, days, hours, mins) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."ready to schedule" (scanpass, caseid, assignedtoid, payername, adjuster_first, adjuster_last, patientfirstname, patientlastname, patient_state, hours, status, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Cases_NoVoid" (caseclaimnumber, patientfirstname, patientlastname, caseid, parentpayerid) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_NID_Parter_Payouts" (PayerID, PayerName, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iYear, Service_sum_AllowAmount, Service_sum_PaidOutAmount, isnid, commissionhighpercentageself, commissionhighpercentageparent, commissionnotes, Commission_Self_Amount, Commission_Parent_Amount, Parent_PayerID, Parent_PayerName) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."all_active_needs_to_be_scheduled" (scanpass, uniquecreatedate, sched_first, sched_last, payername, adjuster_first, adjuster_last, patientfirstname, patientlastname, patient_state, cpt_code, body_part) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vLinkedUsersCount_CaseAccount" (userid, count_as_adjuster, count_as_ncm, count_as_admin1, count_as_admin2, count_as_admin3, count_as_admin4) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."dailyNumbers" (Referral_ReceiveDate, Encounter_ScanPass, PayerName, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Encounter_NoVoid_BP_Plus_Checks" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, Service_iCount, Service_first_StatusID, Service_first_Status, Service_first_TypeID, Service_first_CPT, Service_first_QTY, Service_first_CPTModifier, Service_first_CPTBodyPart, Service_first_ICD_1, Service_first_ICD_2, Service_first_ICD_3, Service_first_ICD_4, Service_first_CPTText, Service_sum_BillAmount, Service_sum_AllowAmount, Service_sum_AllowAmountAdjustment, Service_sum_ReceivedAmount, Service_sum_PaidOutAmount, Service_sum_ReceivedAmount_CheckCalc, paidtoprovidercheck1number, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2number, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3number, paidtoprovidercheck3amount, paidtoprovidercheck3date) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ3_Practice_Services" (practiceid, officezip, contractingstatusid, contractglobal, contracttechonly, contractprofessionalonly, diagnosticmdarthrogram, diagnosticmdmyelogram, doesaging, hasus, hasus_acr, hasus_acrverified, hasus_acrverifieddate, hasus_acrexpirationdate, hasmr, hasmr_acr, hasmr_acrverified, hasmr_acrverifieddate, hasmr_acrexpirationdate, mr_open_class, mr_open_tesla, mr_trad_class, mr_trad_tesla, hasct, hasct_acr, hasct_acrverified, hasct_acrverifieddate, hasct_acrexpirationdate, hasfl, hasfl_acr, hasfl_acrverified, hasfl_acrverifieddate, hasfl_acrexpirationdate, hasnm, hasnm_acr, hasnm_acrverified, hasnm_acrverifieddate, hasnm_acrexpirationdate, hasxr, hasxr_acr, hasxr_acrverified, hasxr_acrverifieddate, hasxr_acrexpirationdate, hasemg, haspet, haspet_acr, haspet_acrverified, haspet_acrverifieddate, haspet_acrexpirationdate, officefaxno) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Services_with_FS" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, ServiceID, Service_StatusID, Service_Status, Service_TypeID, Service_CPT, Service_CPT_Qty, Service_CPTModifier, Service_CPTBodyPart, Service_ICD_1, Service_ICD_2, Service_ICD_3, Service_ICD_4, Service_CPTText, Service_BillAmount, Service_AllowAmount, Service_AllowAmountAdjustment, Service_ReceivedAmount, Service_PaidOutAmount, Service_ReceivedAmount_CheckCalc, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName, Service_modality, Patient_Zip, Practice_Zip, StateFS, Medicare, Walmart, NSP) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."monthYearSideView" (mon, yyyy, count) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Practice_Services" (practiceid, officezip, contractingstatusid, contractglobal, contracttechonly, contractprofessionalonly, diagnosticmdarthrogram, diagnosticmdmyelogram, doesaging, hasus, hasmr, hasct, hasfl, hasnm, hasxr, hasemg, haspet) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."referral not approved" (caseid, assignedtoid, payer, adjuster_first, adjuster_last, patient_first, patient_last, scanpass, patient_state, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ4_Encounter_NoVoid_BP_InteractionsCount" (CaseID, CaseCode, CaseClaimNumber, PayerID, PayerName, Payer_TypeID, Payer_SalesDivision, Payer_AcquisitionDivision, PayerCity, PayerState, Parent_PayerID, Parent_PayerName, ParentPayer_TypeID, ParentPayer_SalesDivision, ParentPayer_AcquisitionDivsion, UA_AssignedTo_UserID, UA_AssignedTo_UserName, UA_AssignedTo_FirstName, UA_AssignedTo_LastName, UA_AssignedTo_Email, UA_Adjuster_UserID, UA_Adjuster_FirstName, UA_Adjuster_LastName, UA_Adjuster_Email, UA_NCM_UserID, UA_NCM_FirstName, UA_NCM_LastName, UA_NCM_Email, UA_ReferralSource_UserID, UA_ReferralSource_FirstName, UA_ReferralSource_LastName, UA_ReferralSource_Email, EmployerName, PatientFirstName, PatientLastName, DateOfInjury, DateOfInjury_display, DateOfInjury_iMonth, DateOfInjury_iDay, DateOfInjury_iDOW, DateOfInjury_iYear, PatientDOB, PatientDOB_display, PatientDOB_iMonth, PatientDOB_iDay, PatientDOB_iDOW, PatientDOB_iYear, PatientAddress1, PatientAddress2, PatientCity, PatientState, PatientHomePhone, PatientCellPhone, PreScreen_IsClaus, PreScreen_ReqOpenModality, PreScreen_HasImplants, PreScreen_HasMetal, PreScreen_Allergies, PreScreen_HasRecentSurgery, PreScreen_PreviousMRIs, PreScreen_OtherCondition, PreScreen_IsPregnant, PreScreen_Height, PreScreen_Weight, ReferralID, ReferralStatusID, Referral_ReceiveDate, Referral_ReceiveDate_display, Referral_ReceiveDate_iMonth, Referral_ReceiveDate_iDay, Referral_ReceiveDate_iDOW, Referral_ReceiveDate_iYear, Referral_OrderFileID, Referral_RxFileID, UA_ReferringDr_UserID, UA_ReferringDr_FirstName, UA_ReferringDr_LastName, UA_ReferringDr_Email, EncounterID, Encounter_StatusID, Encounter_Status, Encounter_Type, Encounter_ScanPass, Encounter_DateOfService, Encounter_DateOfService_display, Encounter_DateOfService_iMonth, Encounter_DateOfService_iDay, Encounter_DateOfService_iDOW, Encounter_DateOfService_iYear, Encounter_ReportFileID, Encounter_VoidLeakReason, Encounter_ExportPaymentToQB, Encounter_IsSTAT, Encounter_IsRetro, Encounter_ReqFilms, Encounter_HasBeenRescheduled, Encounter_ReqAging, Encounter_IsCourtesy, Encounter_HCFA_ToPayerFileID, Encounter_HCFA_FromProvider_FileID, Encounter_SentTo_Bill_Pay, Encounter_SentTo_Bill_Pay_display, Encounter_SentTo_Bill_Pay_iMonth, Encounter_SentTo_Bill_Pay_iDay, Encounter_SentTo_Bill_Pay_iDOW, Encounter_SentTo_Bill_Pay_iYear, Encounter_Rec_Bill_Pro, Encounter_Rec_Bill_Pro_display, Encounter_Rec_Bill_Pro_iMonth, Encounter_Rec_Bill_Pro_iDay, Encounter_Rec_Bill_Pro_iDOW, Encounter_Rec_Bill_Pro_iYear, Encounter_TimeTrack_ReqRec, Encounter_TimeTrack_ReqRec_display, Encounter_TimeTrack_ReqRec_iMonth, Encounter_TimeTrack_ReqRec_iDay, Encounter_TimeTrack_ReqRec_iDOW, Encounter_TimeTrack_ReqRec_iYear, Encounter_TimeTrack_ReqCreated, Encounter_TimeTrack_ReqCreated_display, Encounter_TimeTrack_ReqCreated_iMonth, Encounter_TimeTrack_ReqCreated_iDay, Encounter_TimeTrack_ReqCreated_iDOW, Encounter_TimeTrack_ReqCreated_iYear, Encounter_TimeTrack_ReqProc, Encounter_TimeTrack_ReqProc_display, Encounter_TimeTrack_ReqProc_iMonth, Encounter_TimeTrack_ReqProc_iDay, Encounter_TimeTrack_ReqProc_iDOW, Encounter_TimeTrack_ReqProc_iYear, Encounter_TimeTrack_ReqSched, Encounter_TimeTrack_ReqSched_display, Encounter_TimeTrack_ReqSched_iMonth, Encounter_TimeTrack_ReqSched_iDay, Encounter_TimeTrack_ReqSched_iDOW, Encounter_TimeTrack_ReqSched_iYear, Encounter_TimeTrack_ReqDelivered, Encounter_TimeTrack_ReqDelivered_display, Encounter_TimeTrack_ReqDelivered_iMonth, Encounter_TimeTrack_ReqDelivered_iDay, Encounter_TimeTrack_ReqDelivered_iDOW, Encounter_TimeTrack_ReqDelivered_iYear, Encounter_TimeTrack_ReqPaidIN, Encounter_TimeTrack_ReqPaidIN_display, Encounter_TimeTrack_ReqPaidIN_iMonth, Encounter_TimeTrack_ReqPaidIN_iDay, Encounter_TimeTrack_ReqPaidIN_iDOW, Encounter_TimeTrack_ReqPaidIN_iYear, Encounter_TimeTrack_ReqPaidOut, Encounter_TimeTrack_ReqPaidOut_display, Encounter_TimeTrack_ReqPaidOut_iMonth, Encounter_TimeTrack_ReqPaidOut_iDay, Encounter_TimeTrack_ReqPaidOut_iDOW, Encounter_TimeTrack_ReqPaidOut_iYear, Encounter_TimeTrack_ReqApproved, Encounter_TimeTrack_ReqApproved_display, Encounter_TimeTrack_ReqApproved_iMonth, Encounter_TimeTrack_ReqApproved_iDay, Encounter_TimeTrack_ReqApproved_iDOW, Encounter_TimeTrack_ReqApproved_iYear, Encounter_TimeTrack_ReqInitialAppointment, Encounter_TimeTrack_ReqInitialAppointment_display, Encounter_TimeTrack_ReqInitialAppointment_iMonth, Encounter_TimeTrack_ReqInitialAppointment_iDay, Encounter_TimeTrack_ReqInitialAppointment_iDOW, Encounter_TimeTrack_ReqInitialAppointment_iYear, Encounter_TimeTrack_ReqRxReview, Encounter_TimeTrack_ReqRxReview_display, Encounter_TimeTrack_ReqRxReview_iMonth, Encounter_TimeTrack_ReqRxReview_iDay, Encounter_TimeTrack_ReqRxReview_iDOW, Encounter_TimeTrack_ReqRxReview_iYear, Encounter_TimeTrack_ReqRpReview, Encounter_TimeTrack_ReqRpReview_display, Encounter_TimeTrack_ReqRpReview_iMonth, Encounter_TimeTrack_ReqRpReview_iDay, Encounter_TimeTrack_ReqRpReview_iDOW, Encounter_TimeTrack_ReqRpReview_iYear, AppointmentID, Encounter_PaidToProvider_CheckCalc, Appointment_ProviderID, Appointment_AppointmentTime, Appointment_AppointmentTime_display, Appointment_AppointmentTime_iMonth, Appointment_AppointmentTime_iDay, Appointment_AppointmentTime_iDOW, Appointment_AppointmentTime_iYear, Appointment_StatusID, PracticeID, Practice_Name, Practice_City, Practice_State, Practice_IsBlueStar, Practice_PetLinQID, Practice_SelectMRI_ID, Service_iCount, Service_first_StatusID, Service_first_Status, Service_first_TypeID, Service_first_CPT, Service_first_QTY, Service_first_CPTModifier, Service_first_CPTBodyPart, Service_first_ICD_1, Service_first_ICD_2, Service_first_ICD_3, Service_first_ICD_4, Service_first_CPTText, Service_sum_BillAmount, Service_sum_AllowAmount, Service_sum_AllowAmountAdjustment, Service_sum_ReceivedAmount, Service_sum_PaidOutAmount, Service_sum_ReceivedAmount_CheckCalc, Adjuster_FullID, PrimaryContact_Full, Interactions_Count) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_Sch_TotalCasesByMonth2010" (iYear_TimeTrack_ReqRec, sMonth_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, count) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Services2" (AssignedToID, UA_Sched_LogonUserName, UA_Sched_FirstName, UA_Sched_LastName, PayerID, PayerName, CaseClaimNumber, UA_Adj_UserID, UA_Adj_LogonUserName, UA_Adj_FirstName, UA_Adj_LastName, PatientFirstName, PatientLastName, UA_RefDr_UserID, UA_RefDr_LogonUserName, UA_RefDr_FirstName, UA_RefDr_LastName, ScanPass, ReportFileID, iReadingRadiologist, EncounterStatusID, EncounterStatus, iPracticeID, PracticeName, PracticeZIP, dPaymentToProvider1, dPaymentToProvider2, dPaymentToProvider3, CPT, BillAmount, AllowAmount, AllowAmountAdjustment, ReceivedAmount, PaidOutAmount, ReceiveDate, display_ReceiveDate, iMonth_ReceiveDate, iDay_ReceiveDate, iDOW_ReceiveDate, iYear_ReceiveDate, DateOfService, display_DateOfService, iMonth_DateOfService, iDay_DateOfService, iDOW_DateOfService, iYear_DateOfService, AppointmentTime, display_AppointmentTime, iMonth_AppointmentTime, iDay_AppointmentTime, iDOW_AppointmentTime, iYear_AppointmentTime, SentTo_Bill_Pay, display_SentTo_Bill_Pay, iMonth_SentTo_Bill_Pay, iDay_SentTo_Bill_Pay, iDOW_SentTo_Bill_Pay, iYear_SentTo_Bill_Pay, Rec_Bill_Pro, display_Rec_Bill_Pro, iMonth_Rec_Bill_Pro, iDay_Rec_Bill_Pro, iDOW_Rec_Bill_Pro, iYear_Rec_Bill_Pro, TimeTrack_ReqRec, display_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, TimeTrack_ReqCreated, display_TimeTrack_ReqCreated, iMonth_TimeTrack_ReqCreated, iDay_TimeTrack_ReqCreated, iDOW_TimeTrack_ReqCreated, iYear_TimeTrack_ReqCreated, TimeTrack_ReqProc, display_TimeTrack_ReqProc, iMonth_TimeTrack_ReqProc, iDay_TimeTrack_ReqProc, iDOW_TimeTrack_ReqProc, iYear_TimeTrack_ReqProc, TimeTrack_ReqSched, display_TimeTrack_ReqSched, iMonth_TimeTrack_ReqSched, iDay_TimeTrack_ReqSched, iDOW_TimeTrack_ReqSched, iYear_TimeTrack_ReqSched, TimeTrack_ReqDelivered, display_TimeTrack_ReqDelivered, iMonth_TimeTrack_ReqDelivered, iDay_TimeTrack_ReqDelivered, iDOW_TimeTrack_ReqDelivered, iYear_TimeTrack_ReqDelivered, TimeTrack_ReqPaidIN, display_TimeTrack_ReqPaidIN, iMonth_TimeTrack_ReqPaidIN, iDay_TimeTrack_ReqPaidIN, iDOW_TimeTrack_ReqPaidIN, iYear_TimeTrack_ReqPaidIN, TimeTrack_ReqPaidOut, display_TimeTrack_ReqPaidOut, iMonth_TimeTrack_ReqPaidOut, iDay_TimeTrack_ReqPaidOut, iDOW_TimeTrack_ReqPaidOut, iYear_TimeTrack_ReqPaidOut, TimeTrack_ReqApproved, display_TimeTrack_ReqApproved, iMonth_TimeTrack_ReqApproved, iDay_TimeTrack_ReqApproved, iDOW_TimeTrack_ReqApproved, iYear_TimeTrack_ReqApproved, TimeTrack_ReqInitialAppointment, display_TimeTrack_ReqInitialAppointment, iMonth_TimeTrack_ReqInitialAppointment, iDay_TimeTrack_ReqInitialAppointment, iDOW_TimeTrack_ReqInitialAppointment, iYear_TimeTrack_ReqInitialAppointment, TimeTrack_ReqRxReview, display_TimeTrack_ReqRxReview, iMonth_TimeTrack_ReqRxReview, iDay_TimeTrack_ReqRxReview, iDOW_TimeTrack_ReqRxReview, iYear_TimeTrack_ReqRxReview) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."getBusinessData" (encounterid, region_owner, salesdivision, ref_rec_date, ref_rec_receiveyear, ref_rec_receivemonth, ref_rec_receiveday, scanpass, encounter_status, encounterstatusid, line_of_business, void_reason, voidleakreasonid, pt_first, pt_last, pt_addrs1, pt_addrs2, pt_city, shortstate, pt_zip, pt_home_phone, patientcellphone, patientemail, patientdob, pt_doi, payertypeid, payerid, parentpayerid, parent, payername, employername, schd_first, schd_last, referral_method, stat, senttoadj, encounter_type, caseclaimnumber, appointmenttime, dateofservice, dosreceiveyear, dosreceivemonth, dosreceiveday, time_rxrev, time_sched, time_appt_to_rprv, time_create_to_rp, referralid, referralby_first, referralby_last, adj_first, adj_last, adj_phone, adj_fax, adj_email, ncm_first, ncm_last, ncm_phone, ncm_fax, ncm_email, adm1_first, adm1_last, adm1_phone, adm1_fax, adm1_email, cpt, cptbodypart, cptmodifier, practicename, prac_addrs1, prac_addrs2, prac_city, prac_state, prac_zip, prac_ph, prac_fax, contract_status, pid, courtesy, courtesyid, retro, in_netdev, paidtoprovidercheck1amount, paidtoprovidercheck1date, paidtoprovidercheck2amount, paidtoprovidercheck2date, paidtoprovidercheck3amount, paidtoprovidercheck3date, receivedamount, receivedcheck1amount, receivedcheck1date, receivedcheck2amount, receivedcheck2date, receivedcheck3amount, receivedcheck3date, allowamount, billamount, payer_fs, istatus, reference_feescheduleamount, reference_ucamount, reference_providercontractamount, reference_payerbillamount, reference_payercontractamount, paidoutamount, allowamountadjustment, noshow, patient_to_ic_miles) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vMQ_Encounters" (AssignedToID, UA_Sched_LogonUserName, UA_Sched_FirstName, UA_Sched_LastName, PayerID, PayerName, CaseClaimNumber, UA_Adj_LogonUserName, UA_Adj_FirstName, UA_Adj_LastName, PatientFirstName, PatientLastName, UA_RefDr_LogonUserName, UA_RefDr_FirstName, UA_RefDr_LastName, ScanPass, ReportFileID, iReadingRadiologist, EncounterStatusID, EncounterStatus, iPracticeID, PracticeName, PracticeZIP, dTotal_PaymentToProvider, iTotalServices, dSum_BillAmount, dSum_AllowAmount, dSum_AllowAmountAdjustment, dSum_ReceivedAmount, dSum_PaidOutAmount, ReceiveDate, display_ReceiveDate, iMonth_ReceiveDate, iDay_ReceiveDate, iDOW_ReceiveDate, iYear_ReceiveDate, DateOfService, display_DateOfService, iMonth_DateOfService, iDay_DateOfService, iDOW_DateOfService, iYear_DateOfService, AppointmentTime, display_AppointmentTime, iMonth_AppointmentTime, iDay_AppointmentTime, iDOW_AppointmentTime, iYear_AppointmentTime, SentTo_Bill_Pay, display_SentTo_Bill_Pay, iMonth_SentTo_Bill_Pay, iDay_SentTo_Bill_Pay, iDOW_SentTo_Bill_Pay, iYear_SentTo_Bill_Pay, Rec_Bill_Pro, display_Rec_Bill_Pro, iMonth_Rec_Bill_Pro, iDay_Rec_Bill_Pro, iDOW_Rec_Bill_Pro, iYear_Rec_Bill_Pro, TimeTrack_ReqRec, display_TimeTrack_ReqRec, iMonth_TimeTrack_ReqRec, iDay_TimeTrack_ReqRec, iDOW_TimeTrack_ReqRec, iYear_TimeTrack_ReqRec, TimeTrack_ReqCreated, display_TimeTrack_ReqCreated, iMonth_TimeTrack_ReqCreated, iDay_TimeTrack_ReqCreated, iDOW_TimeTrack_ReqCreated, iYear_TimeTrack_ReqCreated, TimeTrack_ReqProc, display_TimeTrack_ReqProc, iMonth_TimeTrack_ReqProc, iDay_TimeTrack_ReqProc, iDOW_TimeTrack_ReqProc, iYear_TimeTrack_ReqProc, TimeTrack_ReqSched, display_TimeTrack_ReqSched, iMonth_TimeTrack_ReqSched, iDay_TimeTrack_ReqSched, iDOW_TimeTrack_ReqSched, iYear_TimeTrack_ReqSched, TimeTrack_ReqDelivered, display_TimeTrack_ReqDelivered, iMonth_TimeTrack_ReqDelivered, iDay_TimeTrack_ReqDelivered, iDOW_TimeTrack_ReqDelivered, iYear_TimeTrack_ReqDelivered, TimeTrack_ReqPaidIN, display_TimeTrack_ReqPaidIN, iMonth_TimeTrack_ReqPaidIN, iDay_TimeTrack_ReqPaidIN, iDOW_TimeTrack_ReqPaidIN, iYear_TimeTrack_ReqPaidIN, TimeTrack_ReqPaidOut, display_TimeTrack_ReqPaidOut, iMonth_TimeTrack_ReqPaidOut, iDay_TimeTrack_ReqPaidOut, iDOW_TimeTrack_ReqPaidOut, iYear_TimeTrack_ReqPaidOut, TimeTrack_ReqApproved, display_TimeTrack_ReqApproved, iMonth_TimeTrack_ReqApproved, iDay_TimeTrack_ReqApproved, iDOW_TimeTrack_ReqApproved, iYear_TimeTrack_ReqApproved, TimeTrack_ReqInitialAppointment, display_TimeTrack_ReqInitialAppointment, iMonth_TimeTrack_ReqInitialAppointment, iDay_TimeTrack_ReqInitialAppointment, iDOW_TimeTrack_ReqInitialAppointment, iYear_TimeTrack_ReqInitialAppointment, TimeTrack_ReqRxReview, display_TimeTrack_ReqRxReview, iMonth_TimeTrack_ReqRxReview, iDay_TimeTrack_ReqRxReview, iDOW_TimeTrack_ReqRxReview, iYear_TimeTrack_ReqRxReview, UA_CaseAdmin_FirstName, UA_CaseAdmin_LastName) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."vRP_CaseSummary" (display_ReceiveDate, UA_Sched_LogonUserName, UA_Sched_FirstName, UA_Sched_LastName, PayerName, CaseClaimNumber, UA_Adj_FirstName, UA_Adj_LastName, PatientFirstName, PatientLastName, UA_RefDr_FirstName, UA_RefDr_LastName, ScanPass, EncounterStatus, iPracticeID, PracticeName, PracticeZIP, display_TimeTrack_ReqRec, display_TimeTrack_ReqRxReview, display_TimeTrack_ReqSched, display_AppointmentTime, display_TimeTrack_ReqInitialAppointment, display_TimeTrack_ReqDelivered) 
WITH CASCADED CHECK OPTION;

CREATE VIEW "public"."no report" (caseid, assignedtoid, payername, adjuster_first, adjuster_last, patient_first, patient_last, patient_state, scanpass, hours, stat) 
WITH CASCADED CHECK OPTION;

CREATE UNIQUE INDEX "public"."tcompanysecuritygrouplu_pkey" ON "public"."tcompanysecuritygrouplu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."thcouseraccountlu_pkey" ON "public"."thcouseraccountlu" ("lookupid" ASC);

CREATE INDEX "public"."tnim3_patient_zip" ON "public"."tnim3_patientaccount" ("patientzip" ASC);

CREATE UNIQUE INDEX "public"."nid_api_token_copy_pkey1" ON "public"."rest_api_token" ("token" ASC);

CREATE UNIQUE INDEX "public"."tadminmaster_pkey" ON "public"."tadminmaster" ("adminid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_feeschedule_pkey" ON "public"."tnim3_feeschedule" ("feescheduleid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_referralintake_pkey" ON "public"."tnim3_referralintake" ("referralintakeid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_referral_pkey" ON "public"."tnim3_referral" ("referralid" ASC);

CREATE INDEX "public"."practice_zip" ON "public"."tpracticemaster" ("officezip" ASC);

CREATE UNIQUE INDEX "public"."tnim2_icmaster_pkey" ON "public"."tnim2_icmaster" ("icid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_service_pkey" ON "public"."tnim3_service" ("serviceid" ASC);

CREATE UNIQUE INDEX "public"."tdruglist_pkey" ON "public"."tdruglist" ("druglistid" ASC);

CREATE UNIQUE INDEX "public"."treadstatusli_pkey" ON "public"."treadstatusli" ("readstatusid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_salesprospect_pkey" ON "public"."tnim2_salesprospect" ("salesprospectid" ASC);

CREATE INDEX "public"."vmq4_services_index" ON "public"."vmq4_services" (null);

CREATE UNIQUE INDEX "public"."tencounterstatusli_pkey" ON "public"."tencounterstatusli" ("encounterstatusid" ASC);

CREATE UNIQUE INDEX "public"."tuseraccount_pkey" ON "public"."tuseraccount" ("userid" ASC);

CREATE UNIQUE INDEX "public"."tspecialtystatusli_pkey" ON "public"."tspecialtystatusli" ("specialtystatusid" ASC);

CREATE UNIQUE INDEX "public"."tphysiciancategoryli_pkey" ON "public"."tphysiciancategoryli" ("physiciancategoryid" ASC);

CREATE INDEX "public"."tnim3_referral_case" ON "public"."tnim3_referral" ("caseid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_netdevdatatable_pkey" ON "public"."tnim3_netdevdatatable" ("netdevdatatableid" ASC);

CREATE UNIQUE INDEX "public"."tattestr_pkey" ON "public"."tattestr" ("attestrid" ASC);

CREATE INDEX "public"."nim3_appointment_providerid_istatus_appointment_time" ON "public"."tnim3_appointment" ("providerid" ASC, "istatus" ASC, "appointmenttime" ASC);

CREATE UNIQUE INDEX "public"."treadmaster_pkey" ON "public"."treadmaster" ("readid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_servicebillingtransaction_pkey" ON "public"."tnim3_servicebillingtransaction" ("servicebillingtransactionid" ASC);

CREATE UNIQUE INDEX "public"."talliedhealthprofessional_pkey" ON "public"."talliedhealthprofessional" ("alliedid" ASC);

CREATE UNIQUE INDEX "public"."tsecuritygroupmaster_pkey" ON "public"."tsecuritygroupmaster" ("securitygroupid" ASC);

CREATE UNIQUE INDEX "public"."tprofessionalmisconduct_pkey" ON "public"."tprofessionalmisconduct" ("professionalmisconductid" ASC);

CREATE UNIQUE INDEX "public"."tprofessionalliability_pkey" ON "public"."tprofessionalliability" ("professionalliabilityid" ASC);

CREATE UNIQUE INDEX "public"."tsalutationli_pkey" ON "public"."tsalutationli" ("salutationid" ASC);

CREATE UNIQUE INDEX "public"."tfacilityaffiliation_pkey" ON "public"."tfacilityaffiliation" ("affiliationid" ASC);

CREATE UNIQUE INDEX "public"."taudit_copy_pkey" ON "public"."audittemp" ("auditid" ASC);

CREATE UNIQUE INDEX "public"."tupdateitem_pkey" ON "public"."tupdateitem" ("updateitemid" ASC);

CREATE UNIQUE INDEX "public"."tprivileger_pkey" ON "public"."tprivileger" ("privilegerid" ASC);

CREATE UNIQUE INDEX "public"."tpracticeactivity_pkey" ON "public"."tpracticeactivity" ("practiceactivityid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_jobtype_pkey" ON "public"."tnim3_jobtype" ("jobtypeid" ASC);

CREATE INDEX "public"."fce_referral2_id" ON "public"."fce_referral2" ("id" ASC);

CREATE UNIQUE INDEX "public"."terrorlog_pkey" ON "public"."terrorlog" ("errorlogid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_netdevdatatablesupport_pkey" ON "public"."tnim3_netdevdatatablesupport" ("netdevdatatablesupportid" ASC);

CREATE UNIQUE INDEX "public"."tcompanyadminlu_pkey" ON "public"."tcompanyadminlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."trislinqtransaction_pkey" ON "public"."trislinqtransaction" ("rislinqtransactionid" ASC);

CREATE UNIQUE INDEX "public"."ttimetrack_rcodeli_pkey" ON "public"."ttimetrack_rcodeli" ("timetrack_rcodeid" ASC);

CREATE UNIQUE INDEX "public"."tcompanyuseraccountlu_pkey" ON "public"."tcompanyuseraccountlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."talertdatetrack_pkey" ON "public"."talertdatetrack" ("alertdateid" ASC);

CREATE INDEX "public"."tnim3_case_payer" ON "public"."tnim3_caseaccount" ("payerid" ASC);

CREATE UNIQUE INDEX "public"."tphysicianmaster_pkey" ON "public"."tphysicianmaster" ("physicianid" ASC);

CREATE UNIQUE INDEX "public"."tgenderli_pkey" ON "public"."tgenderli" ("genderid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_employeraccount_pkey" ON "public"."tnim3_employeraccount" ("employerid" ASC);

CREATE INDEX "public"."practice_zip_copy" ON "public"."tpracticemaster_copy" ("officezip" ASC);

CREATE UNIQUE INDEX "public"."tnim2_salestransaction_pkey" ON "public"."tnim2_salestransaction" ("salestransactionid" ASC);

CREATE UNIQUE INDEX "public"."tencountertypeli_pkey" ON "public"."tencountertypeli" ("encountertypeid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_caseaccountuseraccountlu_pkey" ON "public"."tnim3_caseaccountuseraccountlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."thcomaster_pkey" ON "public"."thcomaster" ("hcoid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_authorization_pkey" ON "public"."tnim2_authorization" ("authorizationid" ASC);

CREATE UNIQUE INDEX "public"."tothercertification_pkey" ON "public"."tothercertification" ("othercertid" ASC);

CREATE UNIQUE INDEX "public"."tuserstatusli_pkey" ON "public"."tuserstatusli" ("userstatusid" ASC);

CREATE UNIQUE INDEX "public"."tadditionalinformation_pkey" ON "public"."tadditionalinformation" ("additionalinformationid" ASC);

CREATE UNIQUE INDEX "public"."tcompanymaster_pkey" ON "public"."tcompanymaster" ("companyid" ASC);

CREATE INDEX "public"."tnim3_patient_lastname" ON "public"."tnim3_patientaccount" ("patientlastname" ASC);

CREATE UNIQUE INDEX "public"."tnim2_salescontactsalesprospectlu_pkey" ON "public"."tnim2_salescontactsalesprospectlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tpeerreference_pkey" ON "public"."tpeerreference" ("referenceid" ASC);

CREATE INDEX "public"."tnim3_case_adjusterid" ON "public"."tnim3_caseaccount" ("adjusterid" ASC);

CREATE UNIQUE INDEX "public"."thcocontractstatusli_pkey" ON "public"."thcocontractstatusli" ("hcocontractstatusid" ASC);

CREATE UNIQUE INDEX "public"."thcoprivilegequestionlu_pkey" ON "public"."thcoprivilegequestionlu" ("lookupid" ASC);

CREATE INDEX "public"."tnim3_patient_firstname" ON "public"."tnim3_patientaccount" ("patientfirstname" ASC);

CREATE UNIQUE INDEX "public"."tboardcertification_pkey" ON "public"."tboardcertification" ("boardcertificationid" ASC);

CREATE UNIQUE INDEX "public"."tsupporttypeli_pkey" ON "public"."tsupporttypeli" ("supporttypeid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_eligibilityreference_pkey" ON "public"."tnim3_eligibilityreference" ("eligibilityreferenceid" ASC);

CREATE UNIQUE INDEX "public"."tcptwizard_pkey" ON "public"."tcptwizard" ("cptwizardid" ASC);

CREATE UNIQUE INDEX "public"."tphysicianuseraccountlu_pkey" ON "public"."tphysicianuseraccountlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tstateli_pkey" ON "public"."tstateli" ("stateid" ASC);

CREATE UNIQUE INDEX "public"."zip_code_pkey" ON "public"."zip_code_old1" ("id" ASC);

CREATE UNIQUE INDEX "public"."tadminphysicianlu_pkey" ON "public"."tadminphysicianlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tspecialtyli_pkey" ON "public"."tspecialtyli" ("specialtyid" ASC);

CREATE UNIQUE INDEX "public"."atpa_compare_pkey" ON "public"."atpa_compare" ("id" ASC);

CREATE UNIQUE INDEX "public"."payerexceptionlist_pkey" ON "public"."payerexceptionlist" ("payerexceptionlistid" ASC);

CREATE UNIQUE INDEX "public"."thospitalli_pkey" ON "public"."thospitalli" ("hospitalid" ASC);

CREATE INDEX "public"."practice_zip_contractstatus" ON "public"."tpracticemaster" ("officezip" ASC, "contractingstatusid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_salescontact_pkey" ON "public"."tnim2_salescontact" ("salescontactid" ASC);

CREATE UNIQUE INDEX "public"."tformdisclosurequestionlu_pkey" ON "public"."tformdisclosurequestionlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tcommtrackalertstatuscodeli_pkey" ON "public"."tcommtrackalertstatuscodeli" ("commtrackalertstatuscodeid" ASC);

CREATE UNIQUE INDEX "public"."tmodalitytypeli_pkey" ON "public"."tmodalitytypeli" ("modalitytypeid" ASC);

CREATE UNIQUE INDEX "public"."tpracticecontractstatusli_pkey" ON "public"."tpracticecontractstatusli" ("practicecontractstatusid" ASC);

CREATE UNIQUE INDEX "public"."tcasestatusli_pkey" ON "public"."tcasestatusli" ("casestatusid" ASC);

CREATE UNIQUE INDEX "public"."tupdateitemcategoryli_pkey" ON "public"."tupdateitemcategoryli" ("updateitemcategoryid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_billingentity_pkey" ON "public"."tnim3_billingentity" ("billingentityid" ASC);

CREATE UNIQUE INDEX "public"."thcophysicianstanding_pkey" ON "public"."thcophysicianstanding" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tworkhistorytypeli_pkey" ON "public"."tworkhistorytypeli" ("workhistorytypeid" ASC);

CREATE UNIQUE INDEX "public"."tplcli_pkey" ON "public"."tplcli" ("plcid" ASC);

CREATE UNIQUE INDEX "public"."thcodepartmentli_pkey" ON "public"."thcodepartmentli" ("hcodepartmentid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_payermaster_pkey" ON "public"."tnim2_payermaster" ("payerid" ASC);

CREATE UNIQUE INDEX "public"."tgroupsecurity_pkey" ON "public"."tgroupsecurity" ("groupsecurityid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_schedulingtransaction_pkey" ON "public"."tnim3_schedulingtransaction" ("schedulingtransactionid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_directreferralsource_pkey" ON "public"."tnim3_directreferralsource" ("directreferralsourceid" ASC);

CREATE UNIQUE INDEX "public"."tmri_modelli_pkey" ON "public"."tmri_modelli" ("mri_modelid" ASC);

CREATE UNIQUE INDEX "public"."tcoveringphysicians_pkey" ON "public"."tcoveringphysicians" ("coveringphysicianid" ASC);

CREATE UNIQUE INDEX "public"."tcaseaccountstatusli_pkey" ON "public"."tcaseaccountstatusli" ("casestatusid" ASC);

CREATE UNIQUE INDEX "public"."tccardtransaction_pkey" ON "public"."tccardtransaction" ("ccardtransactionid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_billingentity_pkey" ON "public"."tnim2_billingentity" ("billingentityid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_caseaccount_pkey" ON "public"."tnim2_caseaccount" ("caseid" ASC);

CREATE UNIQUE INDEX "public"."taudit_pkey" ON "public"."taudit" ("auditid" ASC);

CREATE UNIQUE INDEX "public"."nidvote_pkey" ON "public"."nidvote" ("nidvote" ASC);

CREATE UNIQUE INDEX "public"."thcophysiciantransaction_pkey" ON "public"."thcophysiciantransaction" ("hcophysiciantransactionid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_directpatientcard_pkey" ON "public"."tnim3_directpatientcard" ("directpatientcardid" ASC);

CREATE UNIQUE INDEX "public"."thcoprivilegecategorytypelu_pkey" ON "public"."thcoprivilegecategorytypelu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tyesnoli_pkey" ON "public"."tyesnoli" ("yesnoid" ASC);

CREATE UNIQUE INDEX "public"."tpracticetypeli_pkey" ON "public"."tpracticetypeli" ("practicetypeid" ASC);

CREATE UNIQUE INDEX "public"."ttemp_pkey" ON "public"."ttemp" ("ttempid" ASC);

CREATE UNIQUE INDEX "public"."tmalpractice_pkey" ON "public"."tmalpractice" ("malpracticeid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_salesportal_pkey" ON "public"."tnim3_salesportal" ("salesportalid" ASC);

CREATE UNIQUE INDEX "public"."tnim_authorization_pkey" ON "public"."tnim_authorization" ("authorizationid" ASC);

CREATE INDEX "public"."practice_zip_contractstatus_copy" ON "public"."tpracticemaster_copy" ("officezip" ASC, "contractingstatusid" ASC);

CREATE UNIQUE INDEX "public"."tsuppuecn_pkey" ON "public"."tsuppuecn" ("suppuecnid" ASC);

CREATE UNIQUE INDEX "public"."thcophysicianlu_pkey" ON "public"."thcophysicianlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."treadphysicianstatusli_pkey" ON "public"."treadphysicianstatusli" ("readphysicianstatusid" ASC);

CREATE UNIQUE INDEX "public"."tinsuranceli_pkey" ON "public"."tinsuranceli" ("insuranceid" ASC);

CREATE UNIQUE INDEX "public"."tschoolli_pkey" ON "public"."tschoolli" ("schoolid" ASC);

CREATE UNIQUE INDEX "public"."tadminpracticelu_pkey" ON "public"."tadminpracticelu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tphdbhospitalli_pkey" ON "public"."tphdbhospitalli" ("hospitalid" ASC);

CREATE UNIQUE INDEX "public"."temailtransaction_pkey" ON "public"."temailtransaction" ("emailtransactionid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_appointment_pkey" ON "public"."tnim3_appointment" ("appointmentid" ASC);

CREATE UNIQUE INDEX "public"."tgroupsecurityitemsli_pkey" ON "public"."tgroupsecurityitemsli" ("groupsecurityitemsid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_payermaster_pkey" ON "public"."tnim3_payermaster" ("payerid" ASC);

CREATE UNIQUE INDEX "public"."tuseraccesstypeli_pkey" ON "public"."tuseraccesstypeli" ("useraccesstypeid" ASC);

CREATE UNIQUE INDEX "public"."tworkhistory_pkey" ON "public"."tworkhistory" ("workhistoryid" ASC);

CREATE UNIQUE INDEX "public"."teventmaster_pkey" ON "public"."teventmaster" ("eventid" ASC);

CREATE UNIQUE INDEX "public"."tcallschedule_pkey" ON "public"."tcallschedule" ("callscheduleid" ASC);

CREATE UNIQUE INDEX "public"."tprofessionalsociety_pkey" ON "public"."tprofessionalsociety" ("professionalsocietyid" ASC);

CREATE UNIQUE INDEX "public"."tlicenseboardli_pkey" ON "public"."tlicenseboardli" ("licenseboardid" ASC);

CREATE UNIQUE INDEX "public"."tbillingaccount_pkey" ON "public"."tbillingaccount" ("billingid" ASC);

CREATE UNIQUE INDEX "public"."tuaalertsli_pkey" ON "public"."tuaalertsli" ("uaalertsid" ASC);

CREATE UNIQUE INDEX "public"."tprivilegequestionli_pkey" ON "public"."tprivilegequestionli" ("questionid" ASC);

CREATE UNIQUE INDEX "public"."tuseraccount_copy_pkey" ON "public"."tuseraccount_copy" ("userid" ASC);

CREATE UNIQUE INDEX "public"."nidnamecontest_pkey" ON "public"."nidnamecontest" ("nidid" ASC);

CREATE UNIQUE INDEX "public"."tcourtesyli_pkey" ON "public"."tcourtesyli" ("courtesyid" ASC);

CREATE UNIQUE INDEX "public"."tpromocodeli_pkey" ON "public"."tpromocodeli" ("promocodeid" ASC);

CREATE UNIQUE INDEX "public"."nid_referral_transaction_pkey" ON "public"."nid_referral_transaction" ("referraltoken" ASC);

CREATE UNIQUE INDEX "public"."tprofessionaleducation_pkey" ON "public"."tprofessionaleducation" ("professionaleducationid" ASC);

CREATE UNIQUE INDEX "public"."tcoveragetypeli_pkey" ON "public"."tcoveragetypeli" ("coverageid" ASC);

CREATE UNIQUE INDEX "public"."tcommunitytopic_pkey" ON "public"."tcommunitytopic" ("communitytopicid" ASC);

CREATE UNIQUE INDEX "public"."tlicensetypeli_pkey" ON "public"."tlicensetypeli" ("licensetypeid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_appointment_pkey" ON "public"."tnim2_appointment" ("appointmentid" ASC);

CREATE UNIQUE INDEX "public"."nid_api_token_pkey" ON "public"."nid_api_token" ("token" ASC);

CREATE UNIQUE INDEX "public"."tnim3_documentblob_pkey" ON "public"."tnim3_documentblob" ("documentid" ASC);

CREATE UNIQUE INDEX "public"."tcvohcolu_pkey" ON "public"."tcvohcolu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_payermaster_copy1_pkey" ON "public"."tnim3_payermaster_copy" ("payerid" ASC);

CREATE UNIQUE INDEX "public"."tnim_report_pkey" ON "public"."tnim_report" ("reportid" ASC);

CREATE UNIQUE INDEX "public"."tappointmenttypeli_pkey" ON "public"."tappointmenttypeli" ("appointmenttypeid" ASC);

CREATE UNIQUE INDEX "public"."thcodivisionli_pkey" ON "public"."thcodivisionli" ("hcodivisionid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_document_pkey" ON "public"."tnim2_document" ("documentid" ASC);

CREATE UNIQUE INDEX "public"."tsuppinterplan_pkey" ON "public"."tsuppinterplan" ("suppinterplanid" ASC);

CREATE UNIQUE INDEX "public"."tcontinuingeducation_pkey" ON "public"."tcontinuingeducation" ("continuingeducationid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_cptgroup_pkey" ON "public"."tnim3_cptgroup" ("cptgroupid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_feescheduleref_pkey" ON "public"."tnim2_feescheduleref" ("feeschedulerefid" ASC);

CREATE UNIQUE INDEX "public"."thcotransaction_pkey" ON "public"."thcotransaction" ("hcotransactionid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_cptgroup_pkey" ON "public"."tnim2_cptgroup" ("cptgroupid" ASC);

CREATE UNIQUE INDEX "public"."tprivilegecategorytypeli_pkey" ON "public"."tprivilegecategorytypeli" ("privilegecategoryid" ASC);

CREATE UNIQUE INDEX "public"."tnim_icmaster_pkey" ON "public"."tnim_icmaster" ("icid" ASC);

CREATE UNIQUE INDEX "public"."tdisclosurequestionli_pkey" ON "public"."tdisclosurequestionli" ("questionid" ASC);

CREATE UNIQUE INDEX "public"."tnim_appointmentlu_pkey" ON "public"."tnim_appointmentlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."thcodistrictli_pkey" ON "public"."thcodistrictli" ("hcodistrictid" ASC);

CREATE UNIQUE INDEX "public"."tcommunityforum_pkey" ON "public"."tcommunityforum" ("communityforumid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_userpracticelu_pkey" ON "public"."tnim3_userpracticelu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_nidpromo_pkey" ON "public"."tnim3_nidpromo" ("nidpromoid" ASC);

CREATE UNIQUE INDEX "public"."tresourcetypeli_pkey" ON "public"."tresourcetypeli" ("resourcetypeid" ASC);

CREATE UNIQUE INDEX "public"."tactivityreference_pkey" ON "public"."tactivityreference" ("activityreferenceid" ASC);

CREATE INDEX "public"."tnim3_case_caseclaimnumber" ON "public"."tnim3_caseaccount" ("caseclaimnumber" ASC);

CREATE UNIQUE INDEX "public"."tnid_widgettrack_pkey" ON "public"."tnid_widgettrack" ("widgettrackid" ASC);

CREATE UNIQUE INDEX "public"."nid_api_token_copy_pkey" ON "public"."fce_referral2" ("id" ASC);

CREATE UNIQUE INDEX "public"."tnim2_caseaccountuseraccountlu_pkey" ON "public"."tnim2_caseaccountuseraccountlu" ("lookupid" ASC);

CREATE INDEX "public"."nim3_modality_practiceid" ON "public"."tnim3_modality" ("practiceid" ASC);

CREATE UNIQUE INDEX "public"."tmessage_pkey" ON "public"."tmessage" ("messageid" ASC);

CREATE UNIQUE INDEX "public"."tactivityfilereference_pkey" ON "public"."tactivityfilereference" ("activityfilereferenceid" ASC);

CREATE UNIQUE INDEX "public"."faxreceive_pkey" ON "public"."faxreceive" ("faxid" ASC);

CREATE UNIQUE INDEX "public"."bunchcare_eligibility_pkey" ON "public"."bunchcare_eligibility" ("caseid" ASC, "carriercaseid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_patientaccount_pkey" ON "public"."tnim3_patientaccount" ("patientid" ASC);

CREATE UNIQUE INDEX "public"."tsalesmanmaster_pkey" ON "public"."tsalesmanmaster" ("salesmanid" ASC);

CREATE UNIQUE INDEX "public"."tdefendantstatusli_pkey" ON "public"."tdefendantstatusli" ("defendantstatusid" ASC);

CREATE UNIQUE INDEX "public"."tscheduledappointmentstatusli_pkey" ON "public"."tscheduledappointmentstatusli" ("scheduledappointmentstatusid" ASC);

CREATE UNIQUE INDEX "public"."tdocumentmanagement_pkey" ON "public"."tdocumentmanagement" ("documentid" ASC);

CREATE UNIQUE INDEX "public"."teventeventlu_pkey" ON "public"."teventeventlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."thcodisclosurequestionlu_pkey" ON "public"."thcodisclosurequestionlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tcountryli_pkey" ON "public"."tcountryli" ("countryid" ASC);

CREATE UNIQUE INDEX "public"."tformli_pkey" ON "public"."tformli" ("formid" ASC);

CREATE UNIQUE INDEX "public"."tprofessionalassociationsli_pkey" ON "public"."tprofessionalassociationsli" ("professionalassociationsid" ASC);

CREATE UNIQUE INDEX "public"."texperiencetypeli_pkey" ON "public"."texperiencetypeli" ("experienceid" ASC);

CREATE UNIQUE INDEX "public"."tphysicianformlu_pkey" ON "public"."tphysicianformlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tdegreetypeli_pkey" ON "public"."tdegreetypeli" ("degreeid" ASC);

CREATE UNIQUE INDEX "public"."tupdateitemstatusli_pkey" ON "public"."tupdateitemstatusli" ("updateitemstatusid" ASC);

CREATE UNIQUE INDEX "public"."thcoeventattesttransaction_pkey" ON "public"."thcoeventattesttransaction" ("hcoeventattesttransactionid" ASC);

CREATE UNIQUE INDEX "public"."tdrugmedicaredata_pkey" ON "public"."tdrugmedicaredata" ("drugmedicaredataid" ASC);

CREATE UNIQUE INDEX "public"."tmanagedcareplan_pkey" ON "public"."tmanagedcareplan" ("planid" ASC);

CREATE UNIQUE INDEX "public"."teventchild_pkey" ON "public"."teventchild" ("eventchildid" ASC);

CREATE UNIQUE INDEX "public"."tboardnameli_pkey" ON "public"."tboardnameli" ("boardnameid" ASC);

CREATE UNIQUE INDEX "public"."tnim_patientaccount_pkey" ON "public"."tnim_patientaccount" ("patientid" ASC);

CREATE UNIQUE INDEX "public"."tstandardaccesstypeli_pkey" ON "public"."tstandardaccesstypeli" ("standardaccesstypeid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_caseaccount_pkey" ON "public"."tnim3_caseaccount" ("caseid" ASC);

CREATE UNIQUE INDEX "public"."tadminpracticerelationshiptypeli_pkey" ON "public"."tadminpracticerelationshiptypeli" ("adminpracticerelationshiptypeid" ASC);

CREATE INDEX "public"."tnim3_encounter_referral" ON "public"."tnim3_encounter" ("referralid" ASC);

CREATE UNIQUE INDEX "public"."tfieldsecurity_pkey" ON "public"."tfieldsecurity" ("fieldsecurityid" ASC);

CREATE INDEX "public"."zipcode_county" ON "public"."zip_code_old1" ("county" ASC);

CREATE UNIQUE INDEX "public"."tcptli_pkey" ON "public"."tcptli" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tphysicianpracticelu_pkey" ON "public"."tphysicianpracticelu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tpracticemaster_pkey" ON "public"."tpracticemaster" ("practiceid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_encounter_pkey" ON "public"."tnim3_encounter" ("encounterid" ASC);

CREATE INDEX "public"."tnim3_service_encounter" ON "public"."tnim3_service" ("encounterid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_document_pkey" ON "public"."tnim3_document" ("documentid" ASC);

CREATE UNIQUE INDEX "public"."tsalesaccount_pkey" ON "public"."tsalesaccount" ("salesid" ASC);

CREATE UNIQUE INDEX "public"."tsalestransaction_pkey" ON "public"."tsalestransaction" ("salestransactionid" ASC);

CREATE UNIQUE INDEX "public"."tdisclosurecategorytypeli_pkey" ON "public"."tdisclosurecategorytypeli" ("disclosurecategoryid" ASC);

CREATE UNIQUE INDEX "public"."tsalesstatusli_pkey" ON "public"."tsalesstatusli" ("salesstatusid" ASC);

CREATE UNIQUE INDEX "public"."ttransaction_pkey" ON "public"."ttransaction" ("transactionid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_weblinktracker_pkey" ON "public"."tnim3_weblinktracker" ("weblinktrackerid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_commtrack_pkey" ON "public"."tnim3_commtrack" ("commtrackid" ASC);

CREATE UNIQUE INDEX "public"."tvoidleakreasonli_pkey" ON "public"."tvoidleakreasonli" ("voidleakreasonid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_feescheduleref_pkey" ON "public"."tnim3_feescheduleref" ("feeschedulerefid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_commtrack_pkey" ON "public"."tnim2_commtrack" ("commtrackid" ASC);

CREATE UNIQUE INDEX "public"."tappointmentstatusli_pkey" ON "public"."tappointmentstatusli" ("appointmentstatusid" ASC);

CREATE UNIQUE INDEX "public"."texperience_pkey" ON "public"."texperience" ("experienceid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_feeschedule_pkey" ON "public"."tnim2_feeschedule" ("feescheduleid" ASC);

CREATE UNIQUE INDEX "public"."tnim2_cptgrouplist_pkey" ON "public"."tnim2_cptgrouplist" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tnim_payermaster_pkey" ON "public"."tnim_payermaster" ("payerid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_modality_pkey" ON "public"."tnim3_modality" ("modalityid" ASC);

CREATE UNIQUE INDEX "public"."tapprovaltypeli_pkey" ON "public"."tapprovaltypeli" ("approvaltypeid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_cptgrouplist_pkey" ON "public"."tnim3_cptgrouplist" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tpracticemaster_copy_pkey" ON "public"."tpracticemaster_copy" ("practiceid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_netdevcommtrack_pkey" ON "public"."tnim3_netdevcommtrack" ("netdevcommtrackid" ASC);

CREATE UNIQUE INDEX "public"."tbillingtransaction_pkey" ON "public"."tbillingtransaction" ("billingtransactionid" ASC);

CREATE UNIQUE INDEX "public"."thcophysicianstatusli_pkey" ON "public"."thcophysicianstatusli" ("hcophysicianstatusid" ASC);

CREATE UNIQUE INDEX "public"."tmcmc_retros_pkey" ON "public"."tmcmc_retros" ("mcmcid" ASC);

CREATE UNIQUE INDEX "public"."tdocumenttypeli_pkey" ON "public"."tdocumenttypeli" ("documenttypeid" ASC);

CREATE UNIQUE INDEX "public"."tnpdbfoltypeli_pkey" ON "public"."tnpdbfoltypeli" ("npdbfolid" ASC);

CREATE UNIQUE INDEX "public"."tnim3_commreference_pkey" ON "public"."tnim3_commreference" ("commreferenceid" ASC);

CREATE INDEX "public"."zipcode_zip" ON "public"."zip_code_old1" ("zip_code" ASC);

CREATE UNIQUE INDEX "public"."tphysicianeventlu_pkey" ON "public"."tphysicianeventlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tsupportbase_pkey" ON "public"."tsupportbase" ("supportbaseid" ASC);

CREATE UNIQUE INDEX "public"."tlicenseregistration_pkey" ON "public"."tlicenseregistration" ("licenseregistrationid" ASC);

CREATE UNIQUE INDEX "public"."tct_modelli_pkey" ON "public"."tct_modelli" ("ct_modelid" ASC);

CREATE UNIQUE INDEX "public"."tactivity_pkey" ON "public"."tactivity" ("activityid" ASC);

CREATE UNIQUE INDEX "public"."treadphysicianlu_pkey" ON "public"."treadphysicianlu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tresource_pkey" ON "public"."tresource" ("resourceid" ASC);

CREATE UNIQUE INDEX "public"."tadminpracticelu_copy_pkey" ON "public"."tadminpracticelu_copy" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tservicebillingtransactiontypeli_pkey" ON "public"."tservicebillingtransactiontypeli" ("servicebillingtransactiontypeid" ASC);

CREATE UNIQUE INDEX "public"."tcvomaster_pkey" ON "public"."tcvomaster" ("cvoid" ASC);

CREATE INDEX "public"."cid" ON "public"."tnim3_commtrack" ("caseid" DESC, "referralid" DESC, "encounterid" DESC);

CREATE INDEX "public"."tnim3_case_patientaccountid" ON "public"."tnim3_caseaccount" ("patientaccountid" ASC);

CREATE UNIQUE INDEX "public"."tservicestatusli_pkey" ON "public"."tservicestatusli" ("servicestatusid" ASC);

CREATE UNIQUE INDEX "public"."tcompanyhcolu_pkey" ON "public"."tcompanyhcolu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tysonbilling_pkey" ON "public"."tysonbilling" ("tysonbillingid" ASC);

CREATE UNIQUE INDEX "public"."tcommtracktypeli_pkey" ON "public"."tcommtracktypeli" ("commtracktypeid" ASC);

CREATE INDEX "public"."tnim3_patient_payerid" ON "public"."tnim3_patientaccount" ("payerid" ASC);

CREATE UNIQUE INDEX "public"."tsalesmansaleslu_pkey" ON "public"."tsalesmansaleslu" ("lookupid" ASC);

CREATE UNIQUE INDEX "public"."tsupppractoptometry_pkey" ON "public"."tsupppractoptometry" ("supppractoptometryid" ASC);

ALTER TABLE "public"."tnim3_userpracticelu" ADD CONSTRAINT "tnim3_userpracticelu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tpromocodeli" ADD CONSTRAINT "tpromocodeli_pkey" PRIMARY KEY ("promocodeid");

ALTER TABLE "public"."tpracticecontractstatusli" ADD CONSTRAINT "tpracticecontractstatusli_pkey" PRIMARY KEY ("practicecontractstatusid");

ALTER TABLE "public"."tnim3_caseaccountuseraccountlu" ADD CONSTRAINT "tnim3_caseaccountuseraccountlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tspecialtystatusli" ADD CONSTRAINT "tspecialtystatusli_pkey" PRIMARY KEY ("specialtystatusid");

ALTER TABLE "public"."tsalesstatusli" ADD CONSTRAINT "tsalesstatusli_pkey" PRIMARY KEY ("salesstatusid");

ALTER TABLE "public"."tccardtransaction" ADD CONSTRAINT "tccardtransaction_pkey" PRIMARY KEY ("ccardtransactionid");

ALTER TABLE "public"."thcoprivilegecategorytypelu" ADD CONSTRAINT "thcoprivilegecategorytypelu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim2_feeschedule" ADD CONSTRAINT "tnim2_feeschedule_pkey" PRIMARY KEY ("feescheduleid");

ALTER TABLE "public"."tbillingtransaction" ADD CONSTRAINT "tbillingtransaction_pkey" PRIMARY KEY ("billingtransactionid");

ALTER TABLE "public"."teventchild" ADD CONSTRAINT "teventchild_pkey" PRIMARY KEY ("eventchildid");

ALTER TABLE "public"."tdrugmedicaredata" ADD CONSTRAINT "tdrugmedicaredata_pkey" PRIMARY KEY ("drugmedicaredataid");

ALTER TABLE "public"."tsalesmansaleslu" ADD CONSTRAINT "tsalesmansaleslu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."nidnamecontest" ADD CONSTRAINT "nidnamecontest_pkey" PRIMARY KEY ("nidid");

ALTER TABLE "public"."tcommtrackalertstatuscodeli" ADD CONSTRAINT "tcommtrackalertstatuscodeli_pkey" PRIMARY KEY ("commtrackalertstatuscodeid");

ALTER TABLE "public"."tmodalitytypeli" ADD CONSTRAINT "tmodalitytypeli_pkey" PRIMARY KEY ("modalitytypeid");

ALTER TABLE "public"."tsalesaccount" ADD CONSTRAINT "tsalesaccount_pkey" PRIMARY KEY ("salesid");

ALTER TABLE "public"."tappointmenttypeli" ADD CONSTRAINT "tappointmenttypeli_pkey" PRIMARY KEY ("appointmenttypeid");

ALTER TABLE "public"."tuseraccount" ADD CONSTRAINT "tuseraccount_pkey" PRIMARY KEY ("userid");

ALTER TABLE "public"."tlicenseboardli" ADD CONSTRAINT "tlicenseboardli_pkey" PRIMARY KEY ("licenseboardid");

ALTER TABLE "public"."tnim3_commreference" ADD CONSTRAINT "tnim3_commreference_pkey" PRIMARY KEY ("commreferenceid");

ALTER TABLE "public"."tdocumentmanagement" ADD CONSTRAINT "tdocumentmanagement_pkey" PRIMARY KEY ("documentid");

ALTER TABLE "public"."tnim2_feescheduleref" ADD CONSTRAINT "tnim2_feescheduleref_pkey" PRIMARY KEY ("feeschedulerefid");

ALTER TABLE "public"."tct_modelli" ADD CONSTRAINT "tct_modelli_pkey" PRIMARY KEY ("ct_modelid");

ALTER TABLE "public"."tbillingaccount" ADD CONSTRAINT "tbillingaccount_pkey" PRIMARY KEY ("billingid");

ALTER TABLE "public"."tfacilityaffiliation" ADD CONSTRAINT "tfacilityaffiliation_pkey" PRIMARY KEY ("affiliationid");

ALTER TABLE "public"."tnim2_appointment" ADD CONSTRAINT "tnim2_appointment_pkey" PRIMARY KEY ("appointmentid");

ALTER TABLE "public"."tnim3_netdevdatatablesupport" ADD CONSTRAINT "tnim3_netdevdatatablesupport_pkey" PRIMARY KEY ("netdevdatatablesupportid");

ALTER TABLE "public"."atpa_compare" ADD CONSTRAINT "atpa_compare_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."tactivityreference" ADD CONSTRAINT "tactivityreference_pkey" PRIMARY KEY ("activityreferenceid");

ALTER TABLE "public"."tresourcetypeli" ADD CONSTRAINT "tresourcetypeli_pkey" PRIMARY KEY ("resourcetypeid");

ALTER TABLE "public"."teventeventlu" ADD CONSTRAINT "teventeventlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tactivity" ADD CONSTRAINT "tactivity_pkey" PRIMARY KEY ("activityid");

ALTER TABLE "public"."tcommunityforum" ADD CONSTRAINT "tcommunityforum_pkey" PRIMARY KEY ("communityforumid");

ALTER TABLE "public"."tactivityfilereference" ADD CONSTRAINT "tactivityfilereference_pkey" PRIMARY KEY ("activityfilereferenceid");

ALTER TABLE "public"."thospitalli" ADD CONSTRAINT "thospitalli_pkey" PRIMARY KEY ("hospitalid");

ALTER TABLE "public"."tnim2_salescontact" ADD CONSTRAINT "tnim2_salescontact_pkey" PRIMARY KEY ("salescontactid");

ALTER TABLE "public"."tprofessionalliability" ADD CONSTRAINT "tprofessionalliability_pkey" PRIMARY KEY ("professionalliabilityid");

ALTER TABLE "public"."thcodepartmentli" ADD CONSTRAINT "thcodepartmentli_pkey" PRIMARY KEY ("hcodepartmentid");

ALTER TABLE "public"."tadminpracticelu_copy" ADD CONSTRAINT "tadminpracticelu_copy_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."thcouseraccountlu" ADD CONSTRAINT "thcouseraccountlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim3_weblinktracker" ADD CONSTRAINT "tnim3_weblinktracker_pkey" PRIMARY KEY ("weblinktrackerid");

ALTER TABLE "public"."tmri_modelli" ADD CONSTRAINT "tmri_modelli_pkey" PRIMARY KEY ("mri_modelid");

ALTER TABLE "public"."thcophysicianlu" ADD CONSTRAINT "thcophysicianlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tcompanysecuritygrouplu" ADD CONSTRAINT "tcompanysecuritygrouplu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim3_directreferralsource" ADD CONSTRAINT "tnim3_directreferralsource_pkey" PRIMARY KEY ("directreferralsourceid");

ALTER TABLE "public"."tnim3_caseaccount" ADD CONSTRAINT "tnim3_caseaccount_pkey" PRIMARY KEY ("caseid");

ALTER TABLE "public"."treadphysicianlu" ADD CONSTRAINT "treadphysicianlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim3_nidpromo" ADD CONSTRAINT "tnim3_nidpromo_pkey" PRIMARY KEY ("nidpromoid");

ALTER TABLE "public"."tstandardaccesstypeli" ADD CONSTRAINT "tstandardaccesstypeli_pkey" PRIMARY KEY ("standardaccesstypeid");

ALTER TABLE "public"."tcoveringphysicians" ADD CONSTRAINT "tcoveringphysicians_pkey" PRIMARY KEY ("coveringphysicianid");

ALTER TABLE "public"."tnim2_payermaster" ADD CONSTRAINT "tnim2_payermaster_pkey" PRIMARY KEY ("payerid");

ALTER TABLE "public"."thcodistrictli" ADD CONSTRAINT "thcodistrictli_pkey" PRIMARY KEY ("hcodistrictid");

ALTER TABLE "public"."ttemp" ADD CONSTRAINT "ttemp_pkey" PRIMARY KEY ("ttempid");

ALTER TABLE "public"."tvoidleakreasonli" ADD CONSTRAINT "tvoidleakreasonli_pkey" PRIMARY KEY ("voidleakreasonid");

ALTER TABLE "public"."rest_api_token" ADD CONSTRAINT "nid_api_token_copy_pkey1" PRIMARY KEY ("token");

ALTER TABLE "public"."tuserstatusli" ADD CONSTRAINT "tuserstatusli_pkey" PRIMARY KEY ("userstatusid");

ALTER TABLE "public"."tnim3_jobtype" ADD CONSTRAINT "tnim3_jobtype_pkey" PRIMARY KEY ("jobtypeid");

ALTER TABLE "public"."tmcmc_retros" ADD CONSTRAINT "tmcmc_retros_pkey" PRIMARY KEY ("mcmcid");

ALTER TABLE "public"."tnim3_modality" ADD CONSTRAINT "tnim3_modality_pkey" PRIMARY KEY ("modalityid");

ALTER TABLE "public"."tnim3_salesportal" ADD CONSTRAINT "tnim3_salesportal_pkey" PRIMARY KEY ("salesportalid");

ALTER TABLE "public"."tencounterstatusli" ADD CONSTRAINT "tencounterstatusli_pkey" PRIMARY KEY ("encounterstatusid");

ALTER TABLE "public"."tmalpractice" ADD CONSTRAINT "tmalpractice_pkey" PRIMARY KEY ("malpracticeid");

ALTER TABLE "public"."tupdateitem" ADD CONSTRAINT "tupdateitem_pkey" PRIMARY KEY ("updateitemid");

ALTER TABLE "public"."tnim2_billingentity" ADD CONSTRAINT "tnim2_billingentity_pkey" PRIMARY KEY ("billingentityid");

ALTER TABLE "public"."terrorlog" ADD CONSTRAINT "terrorlog_pkey" PRIMARY KEY ("errorlogid");

ALTER TABLE "public"."thcoprivilegequestionlu" ADD CONSTRAINT "thcoprivilegequestionlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tapprovaltypeli" ADD CONSTRAINT "tapprovaltypeli_pkey" PRIMARY KEY ("approvaltypeid");

ALTER TABLE "public"."texperience" ADD CONSTRAINT "texperience_pkey" PRIMARY KEY ("experienceid");

ALTER TABLE "public"."tnim2_salestransaction" ADD CONSTRAINT "tnim2_salestransaction_pkey" PRIMARY KEY ("salestransactionid");

ALTER TABLE "public"."bunchcare_eligibility" ADD CONSTRAINT "bunchcare_eligibility_pkey" PRIMARY KEY ("caseid", "carriercaseid");

ALTER TABLE "public"."tsuppuecn" ADD CONSTRAINT "tsuppuecn_pkey" PRIMARY KEY ("suppuecnid");

ALTER TABLE "public"."tphysiciancategoryli" ADD CONSTRAINT "tphysiciancategoryli_pkey" PRIMARY KEY ("physiciancategoryid");

ALTER TABLE "public"."tgroupsecurityitemsli" ADD CONSTRAINT "tgroupsecurityitemsli_pkey" PRIMARY KEY ("groupsecurityitemsid");

ALTER TABLE "public"."tnid_widgettrack" ADD CONSTRAINT "tnid_widgettrack_pkey" PRIMARY KEY ("widgettrackid");

ALTER TABLE "public"."thcophysicianstatusli" ADD CONSTRAINT "thcophysicianstatusli_pkey" PRIMARY KEY ("hcophysicianstatusid");

ALTER TABLE "public"."tnim3_netdevcommtrack" ADD CONSTRAINT "tnim3_netdevcommtrack_pkey" PRIMARY KEY ("netdevcommtrackid");

ALTER TABLE "public"."tformli" ADD CONSTRAINT "tformli_pkey" PRIMARY KEY ("formid");

ALTER TABLE "public"."tphysicianeventlu" ADD CONSTRAINT "tphysicianeventlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim3_document" ADD CONSTRAINT "tnim3_document_pkey" PRIMARY KEY ("documentid");

ALTER TABLE "public"."thcomaster" ADD CONSTRAINT "thcomaster_pkey" PRIMARY KEY ("hcoid");

ALTER TABLE "public"."tcommtracktypeli" ADD CONSTRAINT "tcommtracktypeli_pkey" PRIMARY KEY ("commtracktypeid");

ALTER TABLE "public"."tnim3_billingentity" ADD CONSTRAINT "tnim3_billingentity_pkey" PRIMARY KEY ("billingentityid");

ALTER TABLE "public"."tnim3_cptgroup" ADD CONSTRAINT "tnim3_cptgroup_pkey" PRIMARY KEY ("cptgroupid");

ALTER TABLE "public"."talliedhealthprofessional" ADD CONSTRAINT "talliedhealthprofessional_pkey" PRIMARY KEY ("alliedid");

ALTER TABLE "public"."thcocontractstatusli" ADD CONSTRAINT "thcocontractstatusli_pkey" PRIMARY KEY ("hcocontractstatusid");

ALTER TABLE "public"."audittemp" ADD CONSTRAINT "taudit_copy_pkey" PRIMARY KEY ("auditid");

ALTER TABLE "public"."tadminphysicianlu" ADD CONSTRAINT "tadminphysicianlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tsalesmanmaster" ADD CONSTRAINT "tsalesmanmaster_pkey" PRIMARY KEY ("salesmanid");

ALTER TABLE "public"."tcvohcolu" ADD CONSTRAINT "tcvohcolu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tpracticetypeli" ADD CONSTRAINT "tpracticetypeli_pkey" PRIMARY KEY ("practicetypeid");

ALTER TABLE "public"."talertdatetrack" ADD CONSTRAINT "talertdatetrack_pkey" PRIMARY KEY ("alertdateid");

ALTER TABLE "public"."tfieldsecurity" ADD CONSTRAINT "tfieldsecurity_pkey" PRIMARY KEY ("fieldsecurityid");

ALTER TABLE "public"."tnim_report" ADD CONSTRAINT "tnim_report_pkey" PRIMARY KEY ("reportid");

ALTER TABLE "public"."tscheduledappointmentstatusli" ADD CONSTRAINT "tscheduledappointmentstatusli_pkey" PRIMARY KEY ("scheduledappointmentstatusid");

ALTER TABLE "public"."tyesnoli" ADD CONSTRAINT "tyesnoli_pkey" PRIMARY KEY ("yesnoid");

ALTER TABLE "public"."tnim3_payermaster_copy" ADD CONSTRAINT "tnim3_payermaster_copy1_pkey" PRIMARY KEY ("payerid");

ALTER TABLE "public"."tphdbhospitalli" ADD CONSTRAINT "tphdbhospitalli_pkey" PRIMARY KEY ("hospitalid");

ALTER TABLE "public"."tmanagedcareplan" ADD CONSTRAINT "tmanagedcareplan_pkey" PRIMARY KEY ("planid");

ALTER TABLE "public"."tdegreetypeli" ADD CONSTRAINT "tdegreetypeli_pkey" PRIMARY KEY ("degreeid");

ALTER TABLE "public"."thcophysicianstanding" ADD CONSTRAINT "thcophysicianstanding_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim2_caseaccountuseraccountlu" ADD CONSTRAINT "tnim2_caseaccountuseraccountlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim3_cptgrouplist" ADD CONSTRAINT "tnim3_cptgrouplist_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim3_referralintake" ADD CONSTRAINT "tnim3_referralintake_pkey" PRIMARY KEY ("referralintakeid");

ALTER TABLE "public"."tplcli" ADD CONSTRAINT "tplcli_pkey" PRIMARY KEY ("plcid");

ALTER TABLE "public"."ttimetrack_rcodeli" ADD CONSTRAINT "ttimetrack_rcodeli_pkey" PRIMARY KEY ("timetrack_rcodeid");

ALTER TABLE "public"."tcallschedule" ADD CONSTRAINT "tcallschedule_pkey" PRIMARY KEY ("callscheduleid");

ALTER TABLE "public"."tupdateitemstatusli" ADD CONSTRAINT "tupdateitemstatusli_pkey" PRIMARY KEY ("updateitemstatusid");

ALTER TABLE "public"."tnpdbfoltypeli" ADD CONSTRAINT "tnpdbfoltypeli_pkey" PRIMARY KEY ("npdbfolid");

ALTER TABLE "public"."tothercertification" ADD CONSTRAINT "tothercertification_pkey" PRIMARY KEY ("othercertid");

ALTER TABLE "public"."tstateli" ADD CONSTRAINT "tstateli_pkey" PRIMARY KEY ("stateid");

ALTER TABLE "public"."tnim3_commtrack" ADD CONSTRAINT "tnim3_commtrack_pkey" PRIMARY KEY ("commtrackid");

ALTER TABLE "public"."tprivilegecategorytypeli" ADD CONSTRAINT "tprivilegecategorytypeli_pkey" PRIMARY KEY ("privilegecategoryid");

ALTER TABLE "public"."tdruglist" ADD CONSTRAINT "tdruglist_pkey" PRIMARY KEY ("druglistid");

ALTER TABLE "public"."tsalutationli" ADD CONSTRAINT "tsalutationli_pkey" PRIMARY KEY ("salutationid");

ALTER TABLE "public"."fce_referral2" ADD CONSTRAINT "nid_api_token_copy_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."tnim3_servicebillingtransaction" ADD CONSTRAINT "tnim3_servicebillingtransaction_pkey" PRIMARY KEY ("servicebillingtransactionid");

ALTER TABLE "public"."tpeerreference" ADD CONSTRAINT "tpeerreference_pkey" PRIMARY KEY ("referenceid");

ALTER TABLE "public"."tnim2_document" ADD CONSTRAINT "tnim2_document_pkey" PRIMARY KEY ("documentid");

ALTER TABLE "public"."ttransaction" ADD CONSTRAINT "ttransaction_pkey" PRIMARY KEY ("transactionid");

ALTER TABLE "public"."tnim3_netdevdatatable" ADD CONSTRAINT "tnim3_netdevdatatable_pkey" PRIMARY KEY ("netdevdatatableid");

ALTER TABLE "public"."tprofessionalsociety" ADD CONSTRAINT "tprofessionalsociety_pkey" PRIMARY KEY ("professionalsocietyid");

ALTER TABLE "public"."tnim_payermaster" ADD CONSTRAINT "tnim_payermaster_pkey" PRIMARY KEY ("payerid");

ALTER TABLE "public"."tattestr" ADD CONSTRAINT "tattestr_pkey" PRIMARY KEY ("attestrid");

ALTER TABLE "public"."tpracticeactivity" ADD CONSTRAINT "tpracticeactivity_pkey" PRIMARY KEY ("practiceactivityid");

ALTER TABLE "public"."tschoolli" ADD CONSTRAINT "tschoolli_pkey" PRIMARY KEY ("schoolid");

ALTER TABLE "public"."tphysicianuseraccountlu" ADD CONSTRAINT "tphysicianuseraccountlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tsupppractoptometry" ADD CONSTRAINT "tsupppractoptometry_pkey" PRIMARY KEY ("supppractoptometryid");

ALTER TABLE "public"."tcaseaccountstatusli" ADD CONSTRAINT "tcaseaccountstatusli_pkey" PRIMARY KEY ("casestatusid");

ALTER TABLE "public"."temailtransaction" ADD CONSTRAINT "temailtransaction_pkey" PRIMARY KEY ("emailtransactionid");

ALTER TABLE "public"."tphysicianpracticelu" ADD CONSTRAINT "tphysicianpracticelu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tboardcertification" ADD CONSTRAINT "tboardcertification_pkey" PRIMARY KEY ("boardcertificationid");

ALTER TABLE "public"."thcodivisionli" ADD CONSTRAINT "thcodivisionli_pkey" PRIMARY KEY ("hcodivisionid");

ALTER TABLE "public"."tnim3_feeschedule" ADD CONSTRAINT "tnim3_feeschedule_pkey" PRIMARY KEY ("feescheduleid");

ALTER TABLE "public"."tcvomaster" ADD CONSTRAINT "tcvomaster_pkey" PRIMARY KEY ("cvoid");

ALTER TABLE "public"."tnim3_directpatientcard" ADD CONSTRAINT "tnim3_directpatientcard_pkey" PRIMARY KEY ("directpatientcardid");

ALTER TABLE "public"."treadstatusli" ADD CONSTRAINT "treadstatusli_pkey" PRIMARY KEY ("readstatusid");

ALTER TABLE "public"."thcodisclosurequestionlu" ADD CONSTRAINT "thcodisclosurequestionlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tcompanyuseraccountlu" ADD CONSTRAINT "tcompanyuseraccountlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim2_salesprospect" ADD CONSTRAINT "tnim2_salesprospect_pkey" PRIMARY KEY ("salesprospectid");

ALTER TABLE "public"."texperiencetypeli" ADD CONSTRAINT "texperiencetypeli_pkey" PRIMARY KEY ("experienceid");

ALTER TABLE "public"."tgroupsecurity" ADD CONSTRAINT "tgroupsecurity_pkey" PRIMARY KEY ("groupsecurityid");

ALTER TABLE "public"."tworkhistorytypeli" ADD CONSTRAINT "tworkhistorytypeli_pkey" PRIMARY KEY ("workhistorytypeid");

ALTER TABLE "public"."thcophysiciantransaction" ADD CONSTRAINT "thcophysiciantransaction_pkey" PRIMARY KEY ("hcophysiciantransactionid");

ALTER TABLE "public"."tprofessionaleducation" ADD CONSTRAINT "tprofessionaleducation_pkey" PRIMARY KEY ("professionaleducationid");

ALTER TABLE "public"."tdisclosurequestionli" ADD CONSTRAINT "tdisclosurequestionli_pkey" PRIMARY KEY ("questionid");

ALTER TABLE "public"."tadminpracticerelationshiptypeli" ADD CONSTRAINT "tadminpracticerelationshiptypeli_pkey" PRIMARY KEY ("adminpracticerelationshiptypeid");

ALTER TABLE "public"."tsupportbase" ADD CONSTRAINT "tsupportbase_pkey" PRIMARY KEY ("supportbaseid");

ALTER TABLE "public"."tuseraccount_copy" ADD CONSTRAINT "tuseraccount_copy_pkey" PRIMARY KEY ("userid");

ALTER TABLE "public"."treadphysicianstatusli" ADD CONSTRAINT "treadphysicianstatusli_pkey" PRIMARY KEY ("readphysicianstatusid");

ALTER TABLE "public"."tdocumenttypeli" ADD CONSTRAINT "tdocumenttypeli_pkey" PRIMARY KEY ("documenttypeid");

ALTER TABLE "public"."tnim3_appointment" ADD CONSTRAINT "tnim3_appointment_pkey" PRIMARY KEY ("appointmentid");

ALTER TABLE "public"."tpracticemaster_copy" ADD CONSTRAINT "tpracticemaster_copy_pkey" PRIMARY KEY ("practiceid");

ALTER TABLE "public"."tnim_patientaccount" ADD CONSTRAINT "tnim_patientaccount_pkey" PRIMARY KEY ("patientid");

ALTER TABLE "public"."tnim3_schedulingtransaction" ADD CONSTRAINT "tnim3_schedulingtransaction_pkey" PRIMARY KEY ("schedulingtransactionid");

ALTER TABLE "public"."tuaalertsli" ADD CONSTRAINT "tuaalertsli_pkey" PRIMARY KEY ("uaalertsid");

ALTER TABLE "public"."tcountryli" ADD CONSTRAINT "tcountryli_pkey" PRIMARY KEY ("countryid");

ALTER TABLE "public"."tnim2_commtrack" ADD CONSTRAINT "tnim2_commtrack_pkey" PRIMARY KEY ("commtrackid");

ALTER TABLE "public"."nid_referral_transaction" ADD CONSTRAINT "nid_referral_transaction_pkey" PRIMARY KEY ("referraltoken");

ALTER TABLE "public"."nid_api_token" ADD CONSTRAINT "nid_api_token_pkey" PRIMARY KEY ("token");

ALTER TABLE "public"."trislinqtransaction" ADD CONSTRAINT "trislinqtransaction_pkey" PRIMARY KEY ("rislinqtransactionid");

ALTER TABLE "public"."thcotransaction" ADD CONSTRAINT "thcotransaction_pkey" PRIMARY KEY ("hcotransactionid");

ALTER TABLE "public"."tdefendantstatusli" ADD CONSTRAINT "tdefendantstatusli_pkey" PRIMARY KEY ("defendantstatusid");

ALTER TABLE "public"."payerexceptionlist" ADD CONSTRAINT "payerexceptionlist_pkey" PRIMARY KEY ("payerexceptionlistid");

ALTER TABLE "public"."tcompanymaster" ADD CONSTRAINT "tcompanymaster_pkey" PRIMARY KEY ("companyid");

ALTER TABLE "public"."tnim_authorization" ADD CONSTRAINT "tnim_authorization_pkey" PRIMARY KEY ("authorizationid");

ALTER TABLE "public"."tprivileger" ADD CONSTRAINT "tprivileger_pkey" PRIMARY KEY ("privilegerid");

ALTER TABLE "public"."tphysicianformlu" ADD CONSTRAINT "tphysicianformlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tadminpracticelu" ADD CONSTRAINT "tadminpracticelu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tsuppinterplan" ADD CONSTRAINT "tsuppinterplan_pkey" PRIMARY KEY ("suppinterplanid");

ALTER TABLE "public"."tprivilegequestionli" ADD CONSTRAINT "tprivilegequestionli_pkey" PRIMARY KEY ("questionid");

ALTER TABLE "public"."tnim3_service" ADD CONSTRAINT "tnim3_service_pkey" PRIMARY KEY ("serviceid");

ALTER TABLE "public"."tboardnameli" ADD CONSTRAINT "tboardnameli_pkey" PRIMARY KEY ("boardnameid");

ALTER TABLE "public"."tadminmaster" ADD CONSTRAINT "tadminmaster_pkey" PRIMARY KEY ("adminid");

ALTER TABLE "public"."tcasestatusli" ADD CONSTRAINT "tcasestatusli_pkey" PRIMARY KEY ("casestatusid");

ALTER TABLE "public"."tcoveragetypeli" ADD CONSTRAINT "tcoveragetypeli_pkey" PRIMARY KEY ("coverageid");

ALTER TABLE "public"."tnim3_feescheduleref" ADD CONSTRAINT "tnim3_feescheduleref_pkey" PRIMARY KEY ("feeschedulerefid");

ALTER TABLE "public"."tnim_icmaster" ADD CONSTRAINT "tnim_icmaster_pkey" PRIMARY KEY ("icid");

ALTER TABLE "public"."tnim2_salescontactsalesprospectlu" ADD CONSTRAINT "tnim2_salescontactsalesprospectlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tnim_appointmentlu" ADD CONSTRAINT "tnim_appointmentlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tlicensetypeli" ADD CONSTRAINT "tlicensetypeli_pkey" PRIMARY KEY ("licensetypeid");

ALTER TABLE "public"."tnim3_payermaster" ADD CONSTRAINT "tnim3_payermaster_pkey" PRIMARY KEY ("payerid");

ALTER TABLE "public"."tservicestatusli" ADD CONSTRAINT "tservicestatusli_pkey" PRIMARY KEY ("servicestatusid");

ALTER TABLE "public"."tnim2_cptgroup" ADD CONSTRAINT "tnim2_cptgroup_pkey" PRIMARY KEY ("cptgroupid");

ALTER TABLE "public"."tencountertypeli" ADD CONSTRAINT "tencountertypeli_pkey" PRIMARY KEY ("encountertypeid");

ALTER TABLE "public"."teventmaster" ADD CONSTRAINT "teventmaster_pkey" PRIMARY KEY ("eventid");

ALTER TABLE "public"."tnim2_icmaster" ADD CONSTRAINT "tnim2_icmaster_pkey" PRIMARY KEY ("icid");

ALTER TABLE "public"."tcompanyadminlu" ADD CONSTRAINT "tcompanyadminlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tdisclosurecategorytypeli" ADD CONSTRAINT "tdisclosurecategorytypeli_pkey" PRIMARY KEY ("disclosurecategoryid");

ALTER TABLE "public"."tuseraccesstypeli" ADD CONSTRAINT "tuseraccesstypeli_pkey" PRIMARY KEY ("useraccesstypeid");

ALTER TABLE "public"."faxreceive" ADD CONSTRAINT "faxreceive_pkey" PRIMARY KEY ("faxid");

ALTER TABLE "public"."tadditionalinformation" ADD CONSTRAINT "tadditionalinformation_pkey" PRIMARY KEY ("additionalinformationid");

ALTER TABLE "public"."tcourtesyli" ADD CONSTRAINT "tcourtesyli_pkey" PRIMARY KEY ("courtesyid");

ALTER TABLE "public"."tgenderli" ADD CONSTRAINT "tgenderli_pkey" PRIMARY KEY ("genderid");

ALTER TABLE "public"."tysonbilling" ADD CONSTRAINT "tysonbilling_pkey" PRIMARY KEY ("tysonbillingid");

ALTER TABLE "public"."tnim3_patientaccount" ADD CONSTRAINT "tnim3_patientaccount_pkey" PRIMARY KEY ("patientid");

ALTER TABLE "public"."treadmaster" ADD CONSTRAINT "treadmaster_pkey" PRIMARY KEY ("readid");

ALTER TABLE "public"."tnim2_cptgrouplist" ADD CONSTRAINT "tnim2_cptgrouplist_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tsupporttypeli" ADD CONSTRAINT "tsupporttypeli_pkey" PRIMARY KEY ("supporttypeid");

ALTER TABLE "public"."tformdisclosurequestionlu" ADD CONSTRAINT "tformdisclosurequestionlu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."zip_code_old1" ADD CONSTRAINT "zip_code_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."tcontinuingeducation" ADD CONSTRAINT "tcontinuingeducation_pkey" PRIMARY KEY ("continuingeducationid");

ALTER TABLE "public"."tnim2_authorization" ADD CONSTRAINT "tnim2_authorization_pkey" PRIMARY KEY ("authorizationid");

ALTER TABLE "public"."tsalestransaction" ADD CONSTRAINT "tsalestransaction_pkey" PRIMARY KEY ("salestransactionid");

ALTER TABLE "public"."tcompanyhcolu" ADD CONSTRAINT "tcompanyhcolu_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."thcoeventattesttransaction" ADD CONSTRAINT "thcoeventattesttransaction_pkey" PRIMARY KEY ("hcoeventattesttransactionid");

ALTER TABLE "public"."tnim3_referral" ADD CONSTRAINT "tnim3_referral_pkey" PRIMARY KEY ("referralid");

ALTER TABLE "public"."tresource" ADD CONSTRAINT "tresource_pkey" PRIMARY KEY ("resourceid");

ALTER TABLE "public"."tnim2_caseaccount" ADD CONSTRAINT "tnim2_caseaccount_pkey" PRIMARY KEY ("caseid");

ALTER TABLE "public"."taudit" ADD CONSTRAINT "taudit_pkey" PRIMARY KEY ("auditid");

ALTER TABLE "public"."tlicenseregistration" ADD CONSTRAINT "tlicenseregistration_pkey" PRIMARY KEY ("licenseregistrationid");

ALTER TABLE "public"."nidvote" ADD CONSTRAINT "nidvote_pkey" PRIMARY KEY ("nidvote");

ALTER TABLE "public"."tnim3_documentblob" ADD CONSTRAINT "tnim3_documentblob_pkey" PRIMARY KEY ("documentid");

ALTER TABLE "public"."tservicebillingtransactiontypeli" ADD CONSTRAINT "tservicebillingtransactiontypeli_pkey" PRIMARY KEY ("servicebillingtransactiontypeid");

ALTER TABLE "public"."tupdateitemcategoryli" ADD CONSTRAINT "tupdateitemcategoryli_pkey" PRIMARY KEY ("updateitemcategoryid");

ALTER TABLE "public"."tnim3_encounter" ADD CONSTRAINT "tnim3_encounter_pkey" PRIMARY KEY ("encounterid");

ALTER TABLE "public"."tcommunitytopic" ADD CONSTRAINT "tcommunitytopic_pkey" PRIMARY KEY ("communitytopicid");

ALTER TABLE "public"."tprofessionalassociationsli" ADD CONSTRAINT "tprofessionalassociationsli_pkey" PRIMARY KEY ("professionalassociationsid");

ALTER TABLE "public"."tmessage" ADD CONSTRAINT "tmessage_pkey" PRIMARY KEY ("messageid");

ALTER TABLE "public"."tnim3_eligibilityreference" ADD CONSTRAINT "tnim3_eligibilityreference_pkey" PRIMARY KEY ("eligibilityreferenceid");

ALTER TABLE "public"."tpracticemaster" ADD CONSTRAINT "tpracticemaster_pkey" PRIMARY KEY ("practiceid");

ALTER TABLE "public"."tphysicianmaster" ADD CONSTRAINT "tphysicianmaster_pkey" PRIMARY KEY ("physicianid");

ALTER TABLE "public"."tcptwizard" ADD CONSTRAINT "tcptwizard_pkey" PRIMARY KEY ("cptwizardid");

ALTER TABLE "public"."tsecuritygroupmaster" ADD CONSTRAINT "tsecuritygroupmaster_pkey" PRIMARY KEY ("securitygroupid");

ALTER TABLE "public"."tnim3_employeraccount" ADD CONSTRAINT "tnim3_employeraccount_pkey" PRIMARY KEY ("employerid");

ALTER TABLE "public"."tprofessionalmisconduct" ADD CONSTRAINT "tprofessionalmisconduct_pkey" PRIMARY KEY ("professionalmisconductid");

ALTER TABLE "public"."tcptli" ADD CONSTRAINT "tcptli_pkey" PRIMARY KEY ("lookupid");

ALTER TABLE "public"."tinsuranceli" ADD CONSTRAINT "tinsuranceli_pkey" PRIMARY KEY ("insuranceid");

ALTER TABLE "public"."tworkhistory" ADD CONSTRAINT "tworkhistory_pkey" PRIMARY KEY ("workhistoryid");

ALTER TABLE "public"."tspecialtyli" ADD CONSTRAINT "tspecialtyli_pkey" PRIMARY KEY ("specialtyid");

ALTER TABLE "public"."tappointmentstatusli" ADD CONSTRAINT "tappointmentstatusli_pkey" PRIMARY KEY ("appointmentstatusid");


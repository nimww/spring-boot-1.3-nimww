package com.wwpc.spring.persistence.services;

import org.springframework.data.repository.CrudRepository;


import com.wwpc.model.feeschedule.RegionCode;

public interface RepositoryRegionCode extends CrudRepository<RegionCode, Long> {
	
	RegionCode findById(Long id);

}

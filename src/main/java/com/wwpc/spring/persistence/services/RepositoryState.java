package com.wwpc.spring.persistence.services;

import org.springframework.data.repository.CrudRepository;

import com.wwpc.model.feeschedule.State;




public interface RepositoryState extends CrudRepository<State, Long> {
	
	State findById(Long id);

}

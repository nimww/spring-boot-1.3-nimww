package com.wwpc.spring.persistence.services;

import org.springframework.data.repository.CrudRepository;

import com.wwpc.model.feeschedule.Cpt;


public interface RepositoryCPT extends CrudRepository<Cpt, Long> {
	
	Cpt findById(Long id);

}

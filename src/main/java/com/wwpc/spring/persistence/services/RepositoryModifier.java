package com.wwpc.spring.persistence.services;

import org.springframework.data.repository.CrudRepository;


import com.wwpc.model.feeschedule.Modifier;

public interface RepositoryModifier extends CrudRepository<Modifier, Long> {
	
	Modifier findById(Long id);

}

package com.wwpc.spring.persistence.services;

import org.springframework.data.repository.CrudRepository;

import com.wwpc.model.feeschedule.Zip;

public interface RespositoryZip extends CrudRepository<Zip, Long> {
	
	Zip findById(Long id);

}

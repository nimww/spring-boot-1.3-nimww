package com.wwpc.spring.persistence.services;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.wwpc.model.Case.CaseAccount;


public interface RepositoryCaseAccount extends CrudRepository<CaseAccount, Long> {

	CaseAccount findById(Long id);

    // List<CaseAccount> findByLastName(String lastName);
}

package com.wwpc.spring.test;

import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.wwpc.application.PersistenceJPAConfig;
import com.wwpc.model.Case.CaseAccount;
import com.wwpc.model.Case.CaseAdmin;
import com.wwpc.model.Case.NurseCaseManager;
import com.wwpc.model.adjuster.Adjuster;
import com.wwpc.model.patient.Patient;
import com.wwpc.model.payer.Payer;
import com.wwpc.model.referral.Referral;
import com.wwpc.model.referral.ReferringParty;
import com.wwpc.model.schedule.PrimaryScheduler;
import com.wwpc.spring.persistence.services.RepositoryCaseAccount;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(PersistenceJPAConfig.class)

@Transactional
public class CaseAccountTests extends AbstractTransactionalJUnit4SpringContextTests {

	
	
	static
	{
		
		//ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("app-contextConfig.xml");
		//MyApplicationContextInitializer MyApplicationContextInitializer = new MyApplicationContextInitializer();
		Logger rootLogger = Logger.getRootLogger();
	    rootLogger.setLevel(Level.ALL);
	    //rootLogger.addAppender((Appender) new ConsoleAppender());
	}
    @Autowired
    RepositoryCaseAccount repositoryCaseAccount;
    
    @Test
	public void CaseAccountLoadTest(){
    	
    	CaseAccount caseAccount = new CaseAccount();
    	
    	 Referral referral = new Referral();
    	 referral.setCaseAccount(caseAccount);
    	 
    	 Payer payer = new Payer();
    	 payer.setCaseAccount(caseAccount);
    	 
    	 Adjuster adjuster = new Adjuster();
    	 adjuster.setCaseAccount(caseAccount);
    	 
    	 NurseCaseManager nurseCaseManager = new NurseCaseManager();
    	 nurseCaseManager.setCaseAccount(caseAccount);
    	 
    	 Patient patient = new Patient();
    	 patient.setCaseAccount(caseAccount);
    	 
    	 CaseAdmin caseAdmin = new CaseAdmin();
    	 caseAdmin.setCaseAccount(caseAccount);
    	 
    	 PrimaryScheduler primaryScheduler = new PrimaryScheduler();
    	 primaryScheduler.setCaseAccount(caseAccount);
    	 
    	 ReferringParty referringParty = new ReferringParty();
    	 referringParty.setCaseAccount(caseAccount);
    	 
    	 repositoryCaseAccount.save(caseAccount);
    	 
    	 Long identifer = (long) 1;
    	 CaseAccount ca = repositoryCaseAccount.findById(identifer);
    	 
    	 
    	
    	
    	
    }
    
    

    

}
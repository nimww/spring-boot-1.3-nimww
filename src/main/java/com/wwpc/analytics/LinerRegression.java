/*package com.wwpc.analytics;

import org.rosuda.JRI.Rengine;

public class LinerRegression {
      // Create an R vector in the form of a string.
        String javaVector = "c(1,2,3,4,5)";
 
        // Start Rengine.
        Rengine engine = new Rengine(new String[] { "--no-save" }, false, null);
 
        // The vector that was created in JAVA context is stored in 'rVector' which is a variable in R context.
        engine.eval("rVector=" + javaVector);
        
        //Calculate MEAN of vector using R syntax.
        engine.eval("meanVal=mean(rVector)");
        
        //Retrieve MEAN value
        double mean = engine.eval("meanVal").asDouble();
        
        //Print output values
        System.out.println("Mean of given vector is=" + mean);
 
    }


x=c(2,2,3,4,3,6,5,4,5,7,6,9)
y=c(2,3,3,4,5,5,6,7,7,7,8,9)

plot(x,y)

theta0=10
theta1=10
alpha=0.0001
initialJ=100000
learningIterations=200000

J=function(x,y,theta0,theta1){
    m=length(x)
    sum=0
    for(i in 1:m){
        sum=sum+((theta0+theta1*x[i]-y[i])^2)
    }
    sum=sum/(2*m)
    return(sum)
}

updateTheta=function(x,y,theta0,theta1){
    sum0=0
    sum1=0
    m=length(x)
    for(i in 1:m){
        sum0=sum0+(theta0+theta1*x[i]-y[i])
        sum1=sum1+((theta0+theta1*x[i]-y[i])*x[i])
    }
    sum0=sum0/m
    sum1=sum1/m
    theta0=theta0-(alpha*sum0)
    theta1=theta1-(alpha*sum1)
    
    return(c(theta0,theta1))
}    

for(i in 1:learningIterations){
    thetas=updateTheta(x,y,theta0,theta1)
    tempSoln=0
    tempSoln=J(x,y,theta0,theta1)
    if(tempSoln<initialJ){
        initialJ=tempSoln
    }
    if(tempSoln>initialJ){
        break
    }
    
    theta0=thetas[1]
    theta1=thetas[2]
    #print(thetas)
    #print(initialJ)
    plot(x,y)
    lines(x,(theta0+theta1*x), col="red")
    
}
lines(x,(theta0+theta1*x), 




}*/
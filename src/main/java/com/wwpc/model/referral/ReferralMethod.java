package com.wwpc.model.referral;

import javax.persistence.Embeddable;

@Embeddable
public enum ReferralMethod {

	none("None"),
	phone("Phone"),
	email("Email"),
	fax("Fax"),
	onlinePublic("Online (public)"),
	onlineSecure("Online (secure)"),
	other("other"),
	nid("NID Reservation");
	
	private String method;
	
	private ReferralMethod(String s){
		this.method = s;
	}

	public String getMethod() {
		return method;
	}
}
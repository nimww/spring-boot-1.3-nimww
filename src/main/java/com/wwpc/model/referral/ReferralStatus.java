package com.wwpc.model.referral;

import javax.persistence.Embeddable;

@Embeddable
public enum ReferralStatus {
	
	Approved(1),
	Denied(2),
	FlagForReview(3),
	Closed(4),
	Void(5),
	Pending(6),
	NA(0);

	private int status;

	private ReferralStatus(int i) {
		this.status = i;
	}

	public int getStatus() {
		return status;
	}
}
// <select name="ReferralStatusID" id="ReferralStatusID">
// <option value="0">NA</option>
// <option selected="" value="1">Approved</option>
// <option value="2">Denied</option>
// <option value="3">Flag for Review</option>
// <option value="4">Closed</option>
// <option value="5">Void</option>
// <option value="6">Pending</option>
// </select>
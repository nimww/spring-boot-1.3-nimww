package com.wwpc.model.referral;

import javax.persistence.Embeddable;


@Embeddable
public class DirectReferral {
	
	private String referralWeblink;
	private Integer referralPromoId;
	
	
	public String getReferralWeblink() {
		return referralWeblink;
	}
	public void setReferralWeblink(String referralWeblink) {
		this.referralWeblink = referralWeblink;
	}
	public Integer getReferralPromoId() {
		return referralPromoId;
	}
	public void setReferralPromoId(Integer referralPromoId) {
		this.referralPromoId = referralPromoId;
	}

}

package com.wwpc.model.referral;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import com.wwpc.model.AbstractEntity;
import com.wwpc.model.adjuster.Adjuster;
import com.wwpc.model.Case.CaseAccount;
import com.wwpc.model.encounter.Encounter;
import com.wwpc.model.practice.Physician;
import com.wwpc.model.referral.*;







	@Entity
	
	public class Referral extends AbstractEntity implements Serializable {
		
		@OneToMany(targetEntity=Referral.class, cascade=CascadeType.ALL, fetch = FetchType.EAGER)
		private List<Encounter> encounters = new ArrayList<Encounter>();
		
				
		
		@OneToMany(targetEntity=Log.class, cascade = CascadeType.ALL, fetch=FetchType.EAGER)
		private Set <Log> logs = new HashSet<Log>(0);
		
		
		@JoinColumn(name = "id", insertable=false, updatable=false)
		@ManyToOne(optional = false)
		private CaseAccount caseAccount;
		
		
		@OneToOne(targetEntity=Referral.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
		private Physician referringPhysician;
		
		
		@Embedded
		private ReferralStatus referralStatus;
		
		// is this the same as create date
		@Temporal(TemporalType.DATE)
		private Date receiveDate;
		
		//private List<Document> documents = new ArrayList<Document>();
		private String notes;
		
		@Embedded
		private ReferralMethod referralMethod;
		@Embedded
		private DirectReferral directReferral;
		public List<Encounter> getEncounters() {
			return encounters;
		}
		public Set<Log> getLogs() {
			return logs;
		}
		public CaseAccount getCaseAccount() {
			return caseAccount;
		}
		public Physician getReferringPhysician() {
			return referringPhysician;
		}
		public ReferralStatus getReferralStatus() {
			return referralStatus;
		}
		public Date getReceiveDate() {
			return receiveDate;
		}
		public String getNotes() {
			return notes;
		}
		public ReferralMethod getReferralMethod() {
			return referralMethod;
		}
		public DirectReferral getDirectReferral() {
			return directReferral;
		}
		public void setEncounters(List<Encounter> encounters) {
			this.encounters = encounters;
		}
		public void setLogs(Set<Log> logs) {
			this.logs = logs;
		}
		public void setCaseAccount(CaseAccount caseAccount) {
			this.caseAccount = caseAccount;
		}
		public void setReferringPhysician(Physician referringPhysician) {
			this.referringPhysician = referringPhysician;
		}
		public void setReferralStatus(ReferralStatus referralStatus) {
			this.referralStatus = referralStatus;
		}
		public void setReceiveDate(Date receiveDate) {
			this.receiveDate = receiveDate;
		}
		public void setNotes(String notes) {
			this.notes = notes;
		}
		public void setReferralMethod(ReferralMethod referralMethod) {
			this.referralMethod = referralMethod;
		}
		public void setDirectReferral(DirectReferral directReferral) {
			this.directReferral = directReferral;
		}
		
		

}

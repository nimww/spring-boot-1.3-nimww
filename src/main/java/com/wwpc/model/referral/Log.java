package com.wwpc.model.referral;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


import com.wwpc.model.AbstractEntity;




@Entity
public class Log extends AbstractEntity implements Serializable {
	
	
	@ManyToOne(optional = false)
	private Referral referral; 
	
	
	private String log = null;
	
	public Log() {
	}
	
	public Log(String log) {
		this.log = log;
	}
	
	
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}

}

package com.wwpc.model.Case;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;

@Entity
public class NurseCaseManager extends AbstractEntity {
	
	@OneToOne(optional = false)
	private CaseAccount caseAccount;

	public CaseAccount getCaseAccount() {
		return caseAccount;
	}

	public void setCaseAccount(CaseAccount caseAccount) {
		this.caseAccount = caseAccount;
	}

}

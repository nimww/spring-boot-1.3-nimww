package com.wwpc.model.Case;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;

@Entity
public class CaseAdmin extends AbstractEntity {
	
	@ManyToOne(optional = false)
	private CaseAccount caseAccount;

	public CaseAccount getCaseAccount() {
		return caseAccount;
	}

	public void setCaseAccount(CaseAccount caseAccount) {
		this.caseAccount = caseAccount;
	}

}

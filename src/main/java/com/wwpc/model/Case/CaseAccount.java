package com.wwpc.model.Case;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


import com.wwpc.model.AbstractEntity;
import com.wwpc.model.adjuster.Adjuster;
import com.wwpc.model.encounter.Encounter;
import com.wwpc.model.patient.Patient;
import com.wwpc.model.payer.Payer;
import com.wwpc.model.referral.Referral;
import com.wwpc.model.referral.ReferringParty;
import com.wwpc.model.schedule.PrimaryScheduler;


@Entity
public class CaseAccount extends AbstractEntity{
	
	
	@OneToMany(targetEntity=Referral.class, mappedBy = "caseAccount",  cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
	private Set <Referral> referrals = new HashSet<Referral>(0);
	
		
	@OneToOne(targetEntity=Payer.class, mappedBy="caseAccount", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
	private Payer payer;
	
	@OneToOne(targetEntity=Adjuster.class, mappedBy="caseAccount", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Adjuster adjuster;
	
	@OneToOne(targetEntity=NurseCaseManager.class, mappedBy="caseAccount", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private NurseCaseManager nurseCaseManager; // Nerce Case Manager
	
	@OneToOne(targetEntity=Patient.class, mappedBy="caseAccount", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Patient patient;
	
	@OneToMany(targetEntity=CaseAdmin.class, mappedBy ="caseAccount", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private Set <CaseAdmin> caseAdmins = new HashSet<CaseAdmin>(0);
	
	
	@OneToOne(targetEntity=PrimaryScheduler.class, mappedBy="caseAccount", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private PrimaryScheduler primaryScheduler;
	
	@OneToOne(targetEntity=ReferringParty.class, mappedBy="caseAccount", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private ReferringParty referringParty;

	public Set<Referral> getReferrals() {
		return referrals;
	}

	public Payer getPayer() {
		return payer;
	}

	public Adjuster getAdjuster() {
		return adjuster;
	}

	public NurseCaseManager getNurseCaseManager() {
		return nurseCaseManager;
	}

	public Patient getPatient() {
		return patient;
	}

	public Set<CaseAdmin> getCaseAdmins() {
		return caseAdmins;
	}

	public PrimaryScheduler getPrimaryScheduler() {
		return primaryScheduler;
	}

	public ReferringParty getReferringParty() {
		return referringParty;
	}

	public void setReferrals(Set<Referral> referrals) {
		this.referrals = referrals;
	}

	public void setPayer(Payer payer) {
		this.payer = payer;
	}

	public void setAdjuster(Adjuster adjuster) {
		this.adjuster = adjuster;
	}

	public void setNurseCaseManager(NurseCaseManager nurseCaseManager) {
		this.nurseCaseManager = nurseCaseManager;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void setCaseAdmins(Set<CaseAdmin> caseAdmins) {
		this.caseAdmins = caseAdmins;
	}

	public void setPrimaryScheduler(PrimaryScheduler primaryScheduler) {
		this.primaryScheduler = primaryScheduler;
	}

	public void setReferringParty(ReferringParty referringParty) {
		this.referringParty = referringParty;
	}
	
	
	
	
	
	
	
	
	
	
}

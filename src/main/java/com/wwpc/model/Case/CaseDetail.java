package com.wwpc.model.Case;

import java.util.Date;

import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.Case.CaseAccount;


public class CaseDetail extends AbstractEntity{

	
	
	
	private String carrier;
	private String adjuster;
	private String adjusterPayer;
	private String patientAddress1;
	private String patientAddress2;
	private String patientCity;
	private String patientState;
	private String patientZip;
	private String patientPhone;
	private String patientMobile;
	private Date patientDOB;
	private String patientSSN;
	private Date patientDOI;
	private String caseNotes;
	private String payerNotes;
	
}

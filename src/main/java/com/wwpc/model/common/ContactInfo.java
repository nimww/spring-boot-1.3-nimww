package com.wwpc.model.common;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Embeddable
public class ContactInfo {
	
	@Size(min=2, max=30)
	String FirstName = null;
	@Size(min=2, max=40)
	String LastName = null;
	
	String gender = null;
	
	@Embedded
	Address address;
	
	@Pattern(regexp="\\(\\d{3}\\)\\d{3}-\\d{4}")
	String homePhone;

}

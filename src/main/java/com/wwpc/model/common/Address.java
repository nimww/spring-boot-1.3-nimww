package com.wwpc.model.common;

import javax.persistence.Embeddable;

import javax.validation.constraints.*;

@Embeddable
public class Address implements Comparable
{
    
	@Size(min=2, max=4)
    private String surname = null;
	@Size(min=2, max=30)
    private String firstName = null;
	@Size(min=2, max=30)
	private String lastName = null;
	@Size(min=2, max=40)
    private String street = null;
	
    private String district = null;
    @Size(min=2, max=40)
    private String city = null;
    
    @Size(min=2, max=30)
    private String postCode = null;

    public Address (String surname, String firstname, String street, String district, String city, String postcode)
    {
        
        this.surname = surname;
        this.firstName = firstname;
        this.street = street;
        this.district = district;
        this.city = city;
        this.postCode = postcode;
    }

    

    public String getSurname ()
    {
        return this.surname;
    }

    public String getFirstname ()
    {
        return this.firstName;
    }

    public String getStreet ()
    {
        return this.street;
    }

    public String getDistrict ()
    {
        return this.district;
    }

    public String getCity ()
    {
        return this.city;
    }

    public String getPostcode ()
    {
        return this.postCode;
    }

    public String getFullname ()
    {
        return this.firstName + " " + this.surname;
    }

    public String getFulladdress ()
    {
        return this.street + " " + this.district + " " + this.city + " " + this.postCode;
    }

    public int compareTo (Object object)
    {
        Address address = (Address) object;
        int compare = this.surname.compareToIgnoreCase (address.surname);
        if (compare == 0)
        {
            compare = this.firstName.compareToIgnoreCase (address.firstName);
        }
        return (compare);
    }

}

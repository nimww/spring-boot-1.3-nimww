package com.wwpc.model.schedule;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.Case.CaseAccount;

@Entity
public class PrimaryScheduler extends AbstractEntity {
	
	@OneToOne(optional = false)
	private CaseAccount caseAccount;

	public CaseAccount getCaseAccount() {
		return caseAccount;
	}

	public void setCaseAccount(CaseAccount caseAccount) {
		this.caseAccount = caseAccount;
	}
	
	

}

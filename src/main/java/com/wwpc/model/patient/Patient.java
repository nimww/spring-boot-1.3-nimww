package com.wwpc.model.patient;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.wwpc.model.common.Address;
import com.wwpc.model.AbstractEntity;
import com.wwpc.model.Case.CaseAccount;

import javax.validation.constraints.*;
@Entity
public class Patient extends AbstractEntity {
	
	@OneToOne(optional = false)
	private CaseAccount caseAccount;
	
	@Size(min=2, max=30)
	String FirstName = null;
	@Size(min=2, max=40)
	String LastName = null;
	
	String gender = null;
	
	@Embedded
	Address address;
	
	@Pattern(regexp="\\(\\d{3}\\)\\d{3}-\\d{4}")
	String homePhone;
	
	
	@Past
	Date DOB = null;
	
	String height = null;
	String weight = null;
	public CaseAccount getCaseAccount() {
		return caseAccount;
	}
	public String getFirstName() {
		return FirstName;
	}
	public String getLastName() {
		return LastName;
	}
	public String getGender() {
		return gender;
	}
	public Address getAddress() {
		return address;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public Date getDOB() {
		return DOB;
	}
	public String getHeight() {
		return height;
	}
	public String getWeight() {
		return weight;
	}
	public void setCaseAccount(CaseAccount caseAccount) {
		this.caseAccount = caseAccount;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public void setDOB(Date dOB) {
		DOB = dOB;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	
	
	

}

package com.wwpc.model.practice;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.referral.Referral;

@Entity
public class Practice extends AbstractEntity implements Serializable {
	
	
	
	@OneToMany(targetEntity=Physician.class, cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private Set <Physician> physician = new HashSet<Physician>(0);
	
	/*PracticeName
	TypeOfPractice
    OfficeFederalTaxID
    OfficeTaxIDNameAffiliation
    OfficeAddress1
    OfficeCity
    OfficeZIP
    OfficeCountryID
    OfficePhone
    OfficeFaxNo
    OfficeManagerFirstName
    OfficeManagerLastName
    OfficeManagerPhone
    AnsweringService
    Coverage247
    BillingPayableTo
    BillingFirstName
    BillingLastName
    BillingAddress1
    BillingCity
    BillingZIP
    BillingCountryID
    BillingPhone
    BillingFax
    CredentialingContactFirstName
    CredentialingContactLastName
    CredentialingContactAddress1
    CredentialingContactCity
    CredentialingContactZIP
    CredentialingContactCountryID
    CredentialingContactPhone
    CredentialingContactFax
    CredentiallingContactEmail
    Price_Mod_MRI_W
    Price_Mod_MRI_WO
    Price_Mod_MRI_WWO
    Price_Mod_CT_W
    Price_Mod_CT_WO
    Price_Mod_CT_WWO
    IsBlueStar
    DiagnosticMDArthrogram
    DiagnosticMDMyelogram
    DoesAging
    GH_Price_Mod_MRI_W
    GH_Price_Mod_MRI_WO
    GH_Price_Mod_MRI_WWO
    GH_Price_Mod_CT_W
    GH_Price_Mod_CT_WO
    GH_Price_Mod_CT_WWO
    GH_DoesAging
    GH_DiagnosticMDArthrogram
    GH_DiagnosticMDMyelogram*/

}

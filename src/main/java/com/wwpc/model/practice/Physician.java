package com.wwpc.model.practice;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.referral.Referral;

@Entity
public class Physician extends AbstractEntity {
	
	
	
	@OneToOne(targetEntity=Physician.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Referral referral;
	
	@OneToOne(targetEntity=Practice.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Practice practice;

}

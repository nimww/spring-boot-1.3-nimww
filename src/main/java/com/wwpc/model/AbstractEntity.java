package com.wwpc.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;


@MappedSuperclass
public class AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue Long id;
	
	@CreatedDate//
	// @Type(type = "org.jadira.usertype.dateandtime.threetenbp.PersistentZonedDateTime")//
	ZonedDateTime createdDate;

	@LastModifiedDate//
	// @Type(type = "org.jadira.usertype.dateandtime.threetenbp.PersistentZonedDateTime")//
	ZonedDateTime modifiedDate;
}

package com.wwpc.model.hcfa;
/**
 * 
 * @author hoppho
 *
 */
public class HcfaModel {
	private String insuredEmployerName;
	private String employment;
	private String chargesDollar1;
	private String chargesDollar2;
	private String BillingProviderPhoneNumber;
	private String chargesDollar5;
	private String chargesDollar6;
	private String federalTaxIdNumber;
	private String chargesDollar3;
	private String chargesDollar4;
	private String insuredCity;
	private String chargeDescA2;
	private String chargesCent4;
	private String chargeDescA3;
	private String chargesCent3;
	private String chargesCent6;
	private String chargesCent5;
	private String chargeDescA1;
	private String totalChargeCent;
	private String patientFullName;
	private String chargesCent2;
	private String chargesCent1;
	private String modifier3;
	private String modifier4;
	private String modifier5;
	private String modifier6;
	private String chargeDescA6;
	private String insuredDOBDD;
	private String insuredFullName;
	private String chargeDescA4;
	private String modifier1;
	private String chargeDescA5;
	private String modifier2;
	private String insuredAddress;
	private String referringProviderNPI;
	private String BalanceCent;
	private String patientEmployersName;
	private String otherInsuredDateOfBirth;
	private String dateOfServiceFromMM;
	private String autoAccident;
	private String dateOfServiceFromDD3;
	private String dateOfServiceFromDD2;
	private String dateOfServiceFromDD5;
	private String dateOfServiceFromDD4;
	private String dateOfServiceToMM4;
	private String patientRelationsihpToInsured;
	private String dateOfServiceFromDD6;
	private String dateOfServiceToMM3;
	private String dateOfServiceToMM6;
	private String dateOfServiceToMM5;
	private String placeOfService3;
	private String placeOfService4;
	private String billingProviderAddress;
	private String placeOfService5;
	private String dateOfServiceToMM1;
	private String insuredDOBMM;
	private String federalTIN;
	private String placeOfService1;
	private String dateOfServiceFromDD1;
	private String placeOfService2;
	private String dateOfServiceFromMM5;
	private String dateOfServiceFromMM4;
	private String dateOfInjuryDD;
	private String dateOfServiceFromMM3;
	private String otherInsuredsDOBYY;
	private String dateOfServiceFromMM2;
	private String totalChargeDollar;
	private String dateOfServiceFromMM1;
	private String AmountPaidDollar;
	private String placeOfService;
	private String dateOfServiceFromYY1;
	private String referringProviderFullName;
	private String insuredIdNumber;
	private String dateOfServiceFromYY6;
	private String patientDOBDD;
	private String dateOfServiceFromYY3;
	private String dateOfServiceFromYY2;
	private String dateOfServiceFromYY5;
	private String dateOfInjuryYY;
	private String dateOfServiceFromYY4;
	private String otherInsuredsDOBDD;
	private String chargeDescB1;
	private String insuredInsurancePlanName;
	private String diagnosisPointer6;
	private String chargeDescB2;
	private String serviceFacilityLocationAddress;
	private String chargeDescB3;
	private String diagnosisPointer5;
	private String chargeDescB4;
	private String diagnosisPointer4;
	private String chargeDescB5;
	private String diagnosisPointer3;
	private String chargeDescB6;
	private String diagnosisPointer2;
	private String patientDOBMM;
	private String diagnosisPointer1;
	private String diagA1;
	private String patientAddress;
	private String diagA3;
	private String otherInsuredsFullName;
	private String dateOfServiceToYY2;
	private String diagA2;
	private String dateOfServiceToYY1;
	private String diagA4;
	private String dateOfServiceToYY6;
	private String dateOfServiceToDD;
	private String BillingProviderPhoneArea;
	private String dateOfServiceToYY5;
	private String insuredPhoneNumber;
	private String dateOfServiceToYY4;
	private String dateOfServiceToYY3;
	private String insuredPolicyGroup;
	private String patientZip;
	private String dateOfServiceToDD2;
	private String dateOfServiceToDD1;
	private String serviceFacilityNPI;
	private String healthGroup;
	private String dateOfServiceToDD6;
	private String dateOfServiceToDD5;
	private String dateOfServiceToDD4;
	private String otherInsuredsPolicyNumber;
	private String acceptAssignment;
	private String anotherHealthBenefitPlan;
	private String patientCity;
	private String insuredZip;
	private String state5;
	private String state7;
	private String AmountPaidCent;
	private String serviceFacilityLocationCityStateZip;
	private String otherAccident;
	private String dateOfInjuryMM;
	private String patientInsurancePlanName;
	private String patientDOBYY;
	private String insuredSex;
	private String patientStatus;
	private String BalanceDollar;
	private String patientPhoneNumber;
	private String patientSex;
	private String insuredDOBYY;
	private String units5;
	private String employmentstate;
	private String dateOfServiceToMM;
	private String units6;
	private String insuredPhoneArea;
	private String units3;
	private String units4;
	private String units1;
	private String units2;
	private String diagB4;
	private String patientPhoneArea;
	private String diagB3;
	private String diagB2;
	private String diagB1;
	private String billingProviderNPI;
	private String billToName;
	private String billingProviderCityStateZip;
	private String billingProviderName;
	private String serviceFacilityName;
	private String scanPass;
	private String cpt1;
	private String cpt2;
	private String cpt5;
	private String cpt6;
	private String otherInsuredsDOBMM;
	private String cpt3;
	private String cpt4;
	public String getInsuredEmployerName() {
		return insuredEmployerName;
	}
	public void setInsuredEmployerName(String insuredEmployerName) {
		this.insuredEmployerName = insuredEmployerName;
	}
	public String getEmployment() {
		return employment;
	}
	public void setEmployment(String employment) {
		this.employment = employment;
	}
	public String getChargesDollar1() {
		return chargesDollar1;
	}
	public void setChargesDollar1(String chargesDollar1) {
		this.chargesDollar1 = chargesDollar1;
	}
	public String getChargesDollar2() {
		return chargesDollar2;
	}
	public void setChargesDollar2(String chargesDollar2) {
		this.chargesDollar2 = chargesDollar2;
	}
	public String getBillingProviderPhoneNumber() {
		return BillingProviderPhoneNumber;
	}
	public void setBillingProviderPhoneNumber(String billingProviderPhoneNumber) {
		BillingProviderPhoneNumber = billingProviderPhoneNumber;
	}
	public String getChargesDollar5() {
		return chargesDollar5;
	}
	public void setChargesDollar5(String chargesDollar5) {
		this.chargesDollar5 = chargesDollar5;
	}
	public String getChargesDollar6() {
		return chargesDollar6;
	}
	public void setChargesDollar6(String chargesDollar6) {
		this.chargesDollar6 = chargesDollar6;
	}
	public String getFederalTaxIdNumber() {
		return federalTaxIdNumber;
	}
	public void setFederalTaxIdNumber(String federalTaxIdNumber) {
		this.federalTaxIdNumber = federalTaxIdNumber;
	}
	public String getChargesDollar3() {
		return chargesDollar3;
	}
	public void setChargesDollar3(String chargesDollar3) {
		this.chargesDollar3 = chargesDollar3;
	}
	public String getChargesDollar4() {
		return chargesDollar4;
	}
	public void setChargesDollar4(String chargesDollar4) {
		this.chargesDollar4 = chargesDollar4;
	}
	public String getInsuredCity() {
		return insuredCity;
	}
	public void setInsuredCity(String insuredCity) {
		this.insuredCity = insuredCity;
	}
	public String getChargeDescA2() {
		return chargeDescA2;
	}
	public void setChargeDescA2(String chargeDescA2) {
		this.chargeDescA2 = chargeDescA2;
	}
	public String getChargesCent4() {
		return chargesCent4;
	}
	public void setChargesCent4(String chargesCent4) {
		this.chargesCent4 = chargesCent4;
	}
	public String getChargeDescA3() {
		return chargeDescA3;
	}
	public void setChargeDescA3(String chargeDescA3) {
		this.chargeDescA3 = chargeDescA3;
	}
	public String getChargesCent3() {
		return chargesCent3;
	}
	public void setChargesCent3(String chargesCent3) {
		this.chargesCent3 = chargesCent3;
	}
	public String getChargesCent6() {
		return chargesCent6;
	}
	public void setChargesCent6(String chargesCent6) {
		this.chargesCent6 = chargesCent6;
	}
	public String getChargesCent5() {
		return chargesCent5;
	}
	public void setChargesCent5(String chargesCent5) {
		this.chargesCent5 = chargesCent5;
	}
	public String getChargeDescA1() {
		return chargeDescA1;
	}
	public void setChargeDescA1(String chargeDescA1) {
		this.chargeDescA1 = chargeDescA1;
	}
	public String getTotalChargeCent() {
		return totalChargeCent;
	}
	public void setTotalChargeCent(String totalChargeCent) {
		this.totalChargeCent = totalChargeCent;
	}
	public String getPatientFullName() {
		return patientFullName;
	}
	public void setPatientFullName(String patientFullName) {
		this.patientFullName = patientFullName;
	}
	public String getChargesCent2() {
		return chargesCent2;
	}
	public void setChargesCent2(String chargesCent2) {
		this.chargesCent2 = chargesCent2;
	}
	public String getChargesCent1() {
		return chargesCent1;
	}
	public void setChargesCent1(String chargesCent1) {
		this.chargesCent1 = chargesCent1;
	}
	public String getModifier3() {
		return modifier3;
	}
	public void setModifier3(String modifier3) {
		this.modifier3 = modifier3;
	}
	public String getModifier4() {
		return modifier4;
	}
	public void setModifier4(String modifier4) {
		this.modifier4 = modifier4;
	}
	public String getModifier5() {
		return modifier5;
	}
	public void setModifier5(String modifier5) {
		this.modifier5 = modifier5;
	}
	public String getModifier6() {
		return modifier6;
	}
	public void setModifier6(String modifier6) {
		this.modifier6 = modifier6;
	}
	public String getChargeDescA6() {
		return chargeDescA6;
	}
	public void setChargeDescA6(String chargeDescA6) {
		this.chargeDescA6 = chargeDescA6;
	}
	public String getInsuredDOBDD() {
		return insuredDOBDD;
	}
	public void setInsuredDOBDD(String insuredDOBDD) {
		this.insuredDOBDD = insuredDOBDD;
	}
	public String getInsuredFullName() {
		return insuredFullName;
	}
	public void setInsuredFullName(String insuredFullName) {
		this.insuredFullName = insuredFullName;
	}
	public String getChargeDescA4() {
		return chargeDescA4;
	}
	public void setChargeDescA4(String chargeDescA4) {
		this.chargeDescA4 = chargeDescA4;
	}
	public String getModifier1() {
		return modifier1;
	}
	public void setModifier1(String modifier1) {
		this.modifier1 = modifier1;
	}
	public String getChargeDescA5() {
		return chargeDescA5;
	}
	public void setChargeDescA5(String chargeDescA5) {
		this.chargeDescA5 = chargeDescA5;
	}
	public String getModifier2() {
		return modifier2;
	}
	public void setModifier2(String modifier2) {
		this.modifier2 = modifier2;
	}
	public String getInsuredAddress() {
		return insuredAddress;
	}
	public void setInsuredAddress(String insuredAddress) {
		this.insuredAddress = insuredAddress;
	}
	public String getReferringProviderNPI() {
		return referringProviderNPI;
	}
	public void setReferringProviderNPI(String referringProviderNPI) {
		this.referringProviderNPI = referringProviderNPI;
	}
	public String getBalanceCent() {
		return BalanceCent;
	}
	public void setBalanceCent(String balanceCent) {
		BalanceCent = balanceCent;
	}
	public String getPatientEmployersName() {
		return patientEmployersName;
	}
	public void setPatientEmployersName(String patientEmployersName) {
		this.patientEmployersName = patientEmployersName;
	}
	public String getOtherInsuredDateOfBirth() {
		return otherInsuredDateOfBirth;
	}
	public void setOtherInsuredDateOfBirth(String otherInsuredDateOfBirth) {
		this.otherInsuredDateOfBirth = otherInsuredDateOfBirth;
	}
	public String getDateOfServiceFromMM() {
		return dateOfServiceFromMM;
	}
	public void setDateOfServiceFromMM(String dateOfServiceFromMM) {
		this.dateOfServiceFromMM = dateOfServiceFromMM;
	}
	public String getAutoAccident() {
		return autoAccident;
	}
	public void setAutoAccident(String autoAccident) {
		this.autoAccident = autoAccident;
	}
	public String getDateOfServiceFromDD3() {
		return dateOfServiceFromDD3;
	}
	public void setDateOfServiceFromDD3(String dateOfServiceFromDD3) {
		this.dateOfServiceFromDD3 = dateOfServiceFromDD3;
	}
	public String getDateOfServiceFromDD2() {
		return dateOfServiceFromDD2;
	}
	public void setDateOfServiceFromDD2(String dateOfServiceFromDD2) {
		this.dateOfServiceFromDD2 = dateOfServiceFromDD2;
	}
	public String getDateOfServiceFromDD5() {
		return dateOfServiceFromDD5;
	}
	public void setDateOfServiceFromDD5(String dateOfServiceFromDD5) {
		this.dateOfServiceFromDD5 = dateOfServiceFromDD5;
	}
	public String getDateOfServiceFromDD4() {
		return dateOfServiceFromDD4;
	}
	public void setDateOfServiceFromDD4(String dateOfServiceFromDD4) {
		this.dateOfServiceFromDD4 = dateOfServiceFromDD4;
	}
	public String getDateOfServiceToMM4() {
		return dateOfServiceToMM4;
	}
	public void setDateOfServiceToMM4(String dateOfServiceToMM4) {
		this.dateOfServiceToMM4 = dateOfServiceToMM4;
	}
	public String getPatientRelationsihpToInsured() {
		return patientRelationsihpToInsured;
	}
	public void setPatientRelationsihpToInsured(String patientRelationsihpToInsured) {
		this.patientRelationsihpToInsured = patientRelationsihpToInsured;
	}
	public String getDateOfServiceFromDD6() {
		return dateOfServiceFromDD6;
	}
	public void setDateOfServiceFromDD6(String dateOfServiceFromDD6) {
		this.dateOfServiceFromDD6 = dateOfServiceFromDD6;
	}
	public String getDateOfServiceToMM3() {
		return dateOfServiceToMM3;
	}
	public void setDateOfServiceToMM3(String dateOfServiceToMM3) {
		this.dateOfServiceToMM3 = dateOfServiceToMM3;
	}
	public String getDateOfServiceToMM6() {
		return dateOfServiceToMM6;
	}
	public void setDateOfServiceToMM6(String dateOfServiceToMM6) {
		this.dateOfServiceToMM6 = dateOfServiceToMM6;
	}
	public String getDateOfServiceToMM5() {
		return dateOfServiceToMM5;
	}
	public void setDateOfServiceToMM5(String dateOfServiceToMM5) {
		this.dateOfServiceToMM5 = dateOfServiceToMM5;
	}
	public String getPlaceOfService3() {
		return placeOfService3;
	}
	public void setPlaceOfService3(String placeOfService3) {
		this.placeOfService3 = placeOfService3;
	}
	public String getPlaceOfService4() {
		return placeOfService4;
	}
	public void setPlaceOfService4(String placeOfService4) {
		this.placeOfService4 = placeOfService4;
	}
	public String getBillingProviderAddress() {
		return billingProviderAddress;
	}
	public void setBillingProviderAddress(String billingProviderAddress) {
		this.billingProviderAddress = billingProviderAddress;
	}
	public String getPlaceOfService5() {
		return placeOfService5;
	}
	public void setPlaceOfService5(String placeOfService5) {
		this.placeOfService5 = placeOfService5;
	}
	public String getDateOfServiceToMM1() {
		return dateOfServiceToMM1;
	}
	public void setDateOfServiceToMM1(String dateOfServiceToMM1) {
		this.dateOfServiceToMM1 = dateOfServiceToMM1;
	}
	public String getInsuredDOBMM() {
		return insuredDOBMM;
	}
	public void setInsuredDOBMM(String insuredDOBMM) {
		this.insuredDOBMM = insuredDOBMM;
	}
	public String getFederalTIN() {
		return federalTIN;
	}
	public void setFederalTIN(String federalTIN) {
		this.federalTIN = federalTIN;
	}
	public String getPlaceOfService1() {
		return placeOfService1;
	}
	public void setPlaceOfService1(String placeOfService1) {
		this.placeOfService1 = placeOfService1;
	}
	public String getDateOfServiceFromDD1() {
		return dateOfServiceFromDD1;
	}
	public void setDateOfServiceFromDD1(String dateOfServiceFromDD1) {
		this.dateOfServiceFromDD1 = dateOfServiceFromDD1;
	}
	public String getPlaceOfService2() {
		return placeOfService2;
	}
	public void setPlaceOfService2(String placeOfService2) {
		this.placeOfService2 = placeOfService2;
	}
	public String getDateOfServiceFromMM5() {
		return dateOfServiceFromMM5;
	}
	public void setDateOfServiceFromMM5(String dateOfServiceFromMM5) {
		this.dateOfServiceFromMM5 = dateOfServiceFromMM5;
	}
	public String getDateOfServiceFromMM4() {
		return dateOfServiceFromMM4;
	}
	public void setDateOfServiceFromMM4(String dateOfServiceFromMM4) {
		this.dateOfServiceFromMM4 = dateOfServiceFromMM4;
	}
	public String getDateOfInjuryDD() {
		return dateOfInjuryDD;
	}
	public void setDateOfInjuryDD(String dateOfInjuryDD) {
		this.dateOfInjuryDD = dateOfInjuryDD;
	}
	public String getDateOfServiceFromMM3() {
		return dateOfServiceFromMM3;
	}
	public void setDateOfServiceFromMM3(String dateOfServiceFromMM3) {
		this.dateOfServiceFromMM3 = dateOfServiceFromMM3;
	}
	public String getOtherInsuredsDOBYY() {
		return otherInsuredsDOBYY;
	}
	public void setOtherInsuredsDOBYY(String otherInsuredsDOBYY) {
		this.otherInsuredsDOBYY = otherInsuredsDOBYY;
	}
	public String getDateOfServiceFromMM2() {
		return dateOfServiceFromMM2;
	}
	public void setDateOfServiceFromMM2(String dateOfServiceFromMM2) {
		this.dateOfServiceFromMM2 = dateOfServiceFromMM2;
	}
	public String getTotalChargeDollar() {
		return totalChargeDollar;
	}
	public void setTotalChargeDollar(String totalChargeDollar) {
		this.totalChargeDollar = totalChargeDollar;
	}
	public String getDateOfServiceFromMM1() {
		return dateOfServiceFromMM1;
	}
	public void setDateOfServiceFromMM1(String dateOfServiceFromMM1) {
		this.dateOfServiceFromMM1 = dateOfServiceFromMM1;
	}
	public String getAmountPaidDollar() {
		return AmountPaidDollar;
	}
	public void setAmountPaidDollar(String amountPaidDollar) {
		AmountPaidDollar = amountPaidDollar;
	}
	public String getPlaceOfService() {
		return placeOfService;
	}
	public void setPlaceOfService(String placeOfService) {
		this.placeOfService = placeOfService;
	}
	public String getDateOfServiceFromYY1() {
		return dateOfServiceFromYY1;
	}
	public void setDateOfServiceFromYY1(String dateOfServiceFromYY1) {
		this.dateOfServiceFromYY1 = dateOfServiceFromYY1;
	}
	public String getReferringProviderFullName() {
		return referringProviderFullName;
	}
	public void setReferringProviderFullName(String referringProviderFullName) {
		this.referringProviderFullName = referringProviderFullName;
	}
	public String getInsuredIdNumber() {
		return insuredIdNumber;
	}
	public void setInsuredIdNumber(String insuredIdNumber) {
		this.insuredIdNumber = insuredIdNumber;
	}
	public String getDateOfServiceFromYY6() {
		return dateOfServiceFromYY6;
	}
	public void setDateOfServiceFromYY6(String dateOfServiceFromYY6) {
		this.dateOfServiceFromYY6 = dateOfServiceFromYY6;
	}
	public String getPatientDOBDD() {
		return patientDOBDD;
	}
	public void setPatientDOBDD(String patientDOBDD) {
		this.patientDOBDD = patientDOBDD;
	}
	public String getDateOfServiceFromYY3() {
		return dateOfServiceFromYY3;
	}
	public void setDateOfServiceFromYY3(String dateOfServiceFromYY3) {
		this.dateOfServiceFromYY3 = dateOfServiceFromYY3;
	}
	public String getDateOfServiceFromYY2() {
		return dateOfServiceFromYY2;
	}
	public void setDateOfServiceFromYY2(String dateOfServiceFromYY2) {
		this.dateOfServiceFromYY2 = dateOfServiceFromYY2;
	}
	public String getDateOfServiceFromYY5() {
		return dateOfServiceFromYY5;
	}
	public void setDateOfServiceFromYY5(String dateOfServiceFromYY5) {
		this.dateOfServiceFromYY5 = dateOfServiceFromYY5;
	}
	public String getDateOfInjuryYY() {
		return dateOfInjuryYY;
	}
	public void setDateOfInjuryYY(String dateOfInjuryYY) {
		this.dateOfInjuryYY = dateOfInjuryYY;
	}
	public String getDateOfServiceFromYY4() {
		return dateOfServiceFromYY4;
	}
	public void setDateOfServiceFromYY4(String dateOfServiceFromYY4) {
		this.dateOfServiceFromYY4 = dateOfServiceFromYY4;
	}
	public String getOtherInsuredsDOBDD() {
		return otherInsuredsDOBDD;
	}
	public void setOtherInsuredsDOBDD(String otherInsuredsDOBDD) {
		this.otherInsuredsDOBDD = otherInsuredsDOBDD;
	}
	public String getChargeDescB1() {
		return chargeDescB1;
	}
	public void setChargeDescB1(String chargeDescB1) {
		this.chargeDescB1 = chargeDescB1;
	}
	public String getInsuredInsurancePlanName() {
		return insuredInsurancePlanName;
	}
	public void setInsuredInsurancePlanName(String insuredInsurancePlanName) {
		this.insuredInsurancePlanName = insuredInsurancePlanName;
	}
	public String getDiagnosisPointer6() {
		return diagnosisPointer6;
	}
	public void setDiagnosisPointer6(String diagnosisPointer6) {
		this.diagnosisPointer6 = diagnosisPointer6;
	}
	public String getChargeDescB2() {
		return chargeDescB2;
	}
	public void setChargeDescB2(String chargeDescB2) {
		this.chargeDescB2 = chargeDescB2;
	}
	public String getServiceFacilityLocationAddress() {
		return serviceFacilityLocationAddress;
	}
	public void setServiceFacilityLocationAddress(String serviceFacilityLocationAddress) {
		this.serviceFacilityLocationAddress = serviceFacilityLocationAddress;
	}
	public String getChargeDescB3() {
		return chargeDescB3;
	}
	public void setChargeDescB3(String chargeDescB3) {
		this.chargeDescB3 = chargeDescB3;
	}
	public String getDiagnosisPointer5() {
		return diagnosisPointer5;
	}
	public void setDiagnosisPointer5(String diagnosisPointer5) {
		this.diagnosisPointer5 = diagnosisPointer5;
	}
	public String getChargeDescB4() {
		return chargeDescB4;
	}
	public void setChargeDescB4(String chargeDescB4) {
		this.chargeDescB4 = chargeDescB4;
	}
	public String getDiagnosisPointer4() {
		return diagnosisPointer4;
	}
	public void setDiagnosisPointer4(String diagnosisPointer4) {
		this.diagnosisPointer4 = diagnosisPointer4;
	}
	public String getChargeDescB5() {
		return chargeDescB5;
	}
	public void setChargeDescB5(String chargeDescB5) {
		this.chargeDescB5 = chargeDescB5;
	}
	public String getDiagnosisPointer3() {
		return diagnosisPointer3;
	}
	public void setDiagnosisPointer3(String diagnosisPointer3) {
		this.diagnosisPointer3 = diagnosisPointer3;
	}
	public String getChargeDescB6() {
		return chargeDescB6;
	}
	public void setChargeDescB6(String chargeDescB6) {
		this.chargeDescB6 = chargeDescB6;
	}
	public String getDiagnosisPointer2() {
		return diagnosisPointer2;
	}
	public void setDiagnosisPointer2(String diagnosisPointer2) {
		this.diagnosisPointer2 = diagnosisPointer2;
	}
	public String getPatientDOBMM() {
		return patientDOBMM;
	}
	public void setPatientDOBMM(String patientDOBMM) {
		this.patientDOBMM = patientDOBMM;
	}
	public String getDiagnosisPointer1() {
		return diagnosisPointer1;
	}
	public void setDiagnosisPointer1(String diagnosisPointer1) {
		this.diagnosisPointer1 = diagnosisPointer1;
	}
	public String getDiagA1() {
		return diagA1;
	}
	public void setDiagA1(String diagA1) {
		this.diagA1 = diagA1;
	}
	public String getPatientAddress() {
		return patientAddress;
	}
	public void setPatientAddress(String patientAddress) {
		this.patientAddress = patientAddress;
	}
	public String getDiagA3() {
		return diagA3;
	}
	public void setDiagA3(String diagA3) {
		this.diagA3 = diagA3;
	}
	public String getOtherInsuredsFullName() {
		return otherInsuredsFullName;
	}
	public void setOtherInsuredsFullName(String otherInsuredsFullName) {
		this.otherInsuredsFullName = otherInsuredsFullName;
	}
	public String getDateOfServiceToYY2() {
		return dateOfServiceToYY2;
	}
	public void setDateOfServiceToYY2(String dateOfServiceToYY2) {
		this.dateOfServiceToYY2 = dateOfServiceToYY2;
	}
	public String getDiagA2() {
		return diagA2;
	}
	public void setDiagA2(String diagA2) {
		this.diagA2 = diagA2;
	}
	public String getDateOfServiceToYY1() {
		return dateOfServiceToYY1;
	}
	public void setDateOfServiceToYY1(String dateOfServiceToYY1) {
		this.dateOfServiceToYY1 = dateOfServiceToYY1;
	}
	public String getDiagA4() {
		return diagA4;
	}
	public void setDiagA4(String diagA4) {
		this.diagA4 = diagA4;
	}
	public String getDateOfServiceToYY6() {
		return dateOfServiceToYY6;
	}
	public void setDateOfServiceToYY6(String dateOfServiceToYY6) {
		this.dateOfServiceToYY6 = dateOfServiceToYY6;
	}
	public String getDateOfServiceToDD() {
		return dateOfServiceToDD;
	}
	public void setDateOfServiceToDD(String dateOfServiceToDD) {
		this.dateOfServiceToDD = dateOfServiceToDD;
	}
	public String getBillingProviderPhoneArea() {
		return BillingProviderPhoneArea;
	}
	public void setBillingProviderPhoneArea(String billingProviderPhoneArea) {
		BillingProviderPhoneArea = billingProviderPhoneArea;
	}
	public String getDateOfServiceToYY5() {
		return dateOfServiceToYY5;
	}
	public void setDateOfServiceToYY5(String dateOfServiceToYY5) {
		this.dateOfServiceToYY5 = dateOfServiceToYY5;
	}
	public String getInsuredPhoneNumber() {
		return insuredPhoneNumber;
	}
	public void setInsuredPhoneNumber(String insuredPhoneNumber) {
		this.insuredPhoneNumber = insuredPhoneNumber;
	}
	public String getDateOfServiceToYY4() {
		return dateOfServiceToYY4;
	}
	public void setDateOfServiceToYY4(String dateOfServiceToYY4) {
		this.dateOfServiceToYY4 = dateOfServiceToYY4;
	}
	public String getDateOfServiceToYY3() {
		return dateOfServiceToYY3;
	}
	public void setDateOfServiceToYY3(String dateOfServiceToYY3) {
		this.dateOfServiceToYY3 = dateOfServiceToYY3;
	}
	public String getInsuredPolicyGroup() {
		return insuredPolicyGroup;
	}
	public void setInsuredPolicyGroup(String insuredPolicyGroup) {
		this.insuredPolicyGroup = insuredPolicyGroup;
	}
	public String getPatientZip() {
		return patientZip;
	}
	public void setPatientZip(String patientZip) {
		this.patientZip = patientZip;
	}
	public String getDateOfServiceToDD2() {
		return dateOfServiceToDD2;
	}
	public void setDateOfServiceToDD2(String dateOfServiceToDD2) {
		this.dateOfServiceToDD2 = dateOfServiceToDD2;
	}
	public String getDateOfServiceToDD1() {
		return dateOfServiceToDD1;
	}
	public void setDateOfServiceToDD1(String dateOfServiceToDD1) {
		this.dateOfServiceToDD1 = dateOfServiceToDD1;
	}
	public String getServiceFacilityNPI() {
		return serviceFacilityNPI;
	}
	public void setServiceFacilityNPI(String serviceFacilityNPI) {
		this.serviceFacilityNPI = serviceFacilityNPI;
	}
	public String getHealthGroup() {
		return healthGroup;
	}
	public void setHealthGroup(String healthGroup) {
		this.healthGroup = healthGroup;
	}
	public String getDateOfServiceToDD6() {
		return dateOfServiceToDD6;
	}
	public void setDateOfServiceToDD6(String dateOfServiceToDD6) {
		this.dateOfServiceToDD6 = dateOfServiceToDD6;
	}
	public String getDateOfServiceToDD5() {
		return dateOfServiceToDD5;
	}
	public void setDateOfServiceToDD5(String dateOfServiceToDD5) {
		this.dateOfServiceToDD5 = dateOfServiceToDD5;
	}
	public String getDateOfServiceToDD4() {
		return dateOfServiceToDD4;
	}
	public void setDateOfServiceToDD4(String dateOfServiceToDD4) {
		this.dateOfServiceToDD4 = dateOfServiceToDD4;
	}
	public String getOtherInsuredsPolicyNumber() {
		return otherInsuredsPolicyNumber;
	}
	public void setOtherInsuredsPolicyNumber(String otherInsuredsPolicyNumber) {
		this.otherInsuredsPolicyNumber = otherInsuredsPolicyNumber;
	}
	public String getAcceptAssignment() {
		return acceptAssignment;
	}
	public void setAcceptAssignment(String acceptAssignment) {
		this.acceptAssignment = acceptAssignment;
	}
	public String getAnotherHealthBenefitPlan() {
		return anotherHealthBenefitPlan;
	}
	public void setAnotherHealthBenefitPlan(String anotherHealthBenefitPlan) {
		this.anotherHealthBenefitPlan = anotherHealthBenefitPlan;
	}
	public String getPatientCity() {
		return patientCity;
	}
	public void setPatientCity(String patientCity) {
		this.patientCity = patientCity;
	}
	public String getInsuredZip() {
		return insuredZip;
	}
	public void setInsuredZip(String insuredZip) {
		this.insuredZip = insuredZip;
	}
	public String getState5() {
		return state5;
	}
	public void setState5(String state5) {
		this.state5 = state5;
	}
	public String getState7() {
		return state7;
	}
	public void setState7(String state7) {
		this.state7 = state7;
	}
	public String getAmountPaidCent() {
		return AmountPaidCent;
	}
	public void setAmountPaidCent(String amountPaidCent) {
		AmountPaidCent = amountPaidCent;
	}
	public String getServiceFacilityLocationCityStateZip() {
		return serviceFacilityLocationCityStateZip;
	}
	public void setServiceFacilityLocationCityStateZip(String serviceFacilityLocationCityStateZip) {
		this.serviceFacilityLocationCityStateZip = serviceFacilityLocationCityStateZip;
	}
	public String getOtherAccident() {
		return otherAccident;
	}
	public void setOtherAccident(String otherAccident) {
		this.otherAccident = otherAccident;
	}
	public String getDateOfInjuryMM() {
		return dateOfInjuryMM;
	}
	public void setDateOfInjuryMM(String dateOfInjuryMM) {
		this.dateOfInjuryMM = dateOfInjuryMM;
	}
	public String getPatientInsurancePlanName() {
		return patientInsurancePlanName;
	}
	public void setPatientInsurancePlanName(String patientInsurancePlanName) {
		this.patientInsurancePlanName = patientInsurancePlanName;
	}
	public String getPatientDOBYY() {
		return patientDOBYY;
	}
	public void setPatientDOBYY(String patientDOBYY) {
		this.patientDOBYY = patientDOBYY;
	}
	public String getInsuredSex() {
		return insuredSex;
	}
	public void setInsuredSex(String insuredSex) {
		this.insuredSex = insuredSex;
	}
	public String getPatientStatus() {
		return patientStatus;
	}
	public void setPatientStatus(String patientStatus) {
		this.patientStatus = patientStatus;
	}
	public String getBalanceDollar() {
		return BalanceDollar;
	}
	public void setBalanceDollar(String balanceDollar) {
		BalanceDollar = balanceDollar;
	}
	public String getPatientPhoneNumber() {
		return patientPhoneNumber;
	}
	public void setPatientPhoneNumber(String patientPhoneNumber) {
		this.patientPhoneNumber = patientPhoneNumber;
	}
	public String getPatientSex() {
		return patientSex;
	}
	public void setPatientSex(String patientSex) {
		this.patientSex = patientSex;
	}
	public String getInsuredDOBYY() {
		return insuredDOBYY;
	}
	public void setInsuredDOBYY(String insuredDOBYY) {
		this.insuredDOBYY = insuredDOBYY;
	}
	public String getUnits5() {
		return units5;
	}
	public void setUnits5(String units5) {
		this.units5 = units5;
	}
	public String getEmploymentstate() {
		return employmentstate;
	}
	public void setEmploymentstate(String employmentstate) {
		this.employmentstate = employmentstate;
	}
	public String getDateOfServiceToMM() {
		return dateOfServiceToMM;
	}
	public void setDateOfServiceToMM(String dateOfServiceToMM) {
		this.dateOfServiceToMM = dateOfServiceToMM;
	}
	public String getUnits6() {
		return units6;
	}
	public void setUnits6(String units6) {
		this.units6 = units6;
	}
	public String getInsuredPhoneArea() {
		return insuredPhoneArea;
	}
	public void setInsuredPhoneArea(String insuredPhoneArea) {
		this.insuredPhoneArea = insuredPhoneArea;
	}
	public String getUnits3() {
		return units3;
	}
	public void setUnits3(String units3) {
		this.units3 = units3;
	}
	public String getUnits4() {
		return units4;
	}
	public void setUnits4(String units4) {
		this.units4 = units4;
	}
	public String getUnits1() {
		return units1;
	}
	public void setUnits1(String units1) {
		this.units1 = units1;
	}
	public String getUnits2() {
		return units2;
	}
	public void setUnits2(String units2) {
		this.units2 = units2;
	}
	public String getDiagB4() {
		return diagB4;
	}
	public void setDiagB4(String diagB4) {
		this.diagB4 = diagB4;
	}
	public String getPatientPhoneArea() {
		return patientPhoneArea;
	}
	public void setPatientPhoneArea(String patientPhoneArea) {
		this.patientPhoneArea = patientPhoneArea;
	}
	public String getDiagB3() {
		return diagB3;
	}
	public void setDiagB3(String diagB3) {
		this.diagB3 = diagB3;
	}
	public String getDiagB2() {
		return diagB2;
	}
	public void setDiagB2(String diagB2) {
		this.diagB2 = diagB2;
	}
	public String getDiagB1() {
		return diagB1;
	}
	public void setDiagB1(String diagB1) {
		this.diagB1 = diagB1;
	}
	public String getBillingProviderNPI() {
		return billingProviderNPI;
	}
	public void setBillingProviderNPI(String billingProviderNPI) {
		this.billingProviderNPI = billingProviderNPI;
	}
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	public String getBillingProviderCityStateZip() {
		return billingProviderCityStateZip;
	}
	public void setBillingProviderCityStateZip(String billingProviderCityStateZip) {
		this.billingProviderCityStateZip = billingProviderCityStateZip;
	}
	public String getBillingProviderName() {
		return billingProviderName;
	}
	public void setBillingProviderName(String billingProviderName) {
		this.billingProviderName = billingProviderName;
	}
	public String getServiceFacilityName() {
		return serviceFacilityName;
	}
	public void setServiceFacilityName(String serviceFacilityName) {
		this.serviceFacilityName = serviceFacilityName;
	}
	public String getScanPass() {
		return scanPass;
	}
	public void setScanPass(String scanPass) {
		this.scanPass = scanPass;
	}
	public String getCpt1() {
		return cpt1;
	}
	public void setCpt1(String cpt1) {
		this.cpt1 = cpt1;
	}
	public String getCpt2() {
		return cpt2;
	}
	public void setCpt2(String cpt2) {
		this.cpt2 = cpt2;
	}
	public String getCpt5() {
		return cpt5;
	}
	public void setCpt5(String cpt5) {
		this.cpt5 = cpt5;
	}
	public String getCpt6() {
		return cpt6;
	}
	public void setCpt6(String cpt6) {
		this.cpt6 = cpt6;
	}
	public String getOtherInsuredsDOBMM() {
		return otherInsuredsDOBMM;
	}
	public void setOtherInsuredsDOBMM(String otherInsuredsDOBMM) {
		this.otherInsuredsDOBMM = otherInsuredsDOBMM;
	}
	public String getCpt3() {
		return cpt3;
	}
	public void setCpt3(String cpt3) {
		this.cpt3 = cpt3;
	}
	public String getCpt4() {
		return cpt4;
	}
	public void setCpt4(String cpt4) {
		this.cpt4 = cpt4;
	}

}

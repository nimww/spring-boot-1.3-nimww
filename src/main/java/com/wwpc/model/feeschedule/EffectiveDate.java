package com.wwpc.model.feeschedule;

import java.util.Date;

import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.wwpc.model.AbstractEntity;

public class EffectiveDate extends AbstractEntity{
	
	@ManyToOne(optional = false)
	private Cpt cpt;
	
	@Temporal( TemporalType.DATE )
	Date effectiveDate;

}

package com.wwpc.model.feeschedule;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.wwpc.model.AbstractEntity;
@Entity
public class State extends AbstractEntity {

	@ManyToOne(optional = false)
	private RegionCode regionCode;
}

package com.wwpc.model.feeschedule;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.payer.Payer;

@Entity
public class Modifier extends AbstractEntity {

	@OneToMany(targetEntity=Cpt.class, mappedBy = "modifier",  cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private Set <Cpt> cpts = new HashSet<Cpt>(0);
	
	/*@OneToOne(targetEntity=Cpt.class, mappedBy="cpt", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Cpt cpt;*/
	
	String modifier;
}

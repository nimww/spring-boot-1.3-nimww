package com.wwpc.model.feeschedule;


import java.io.Serializable;
import javax.persistence.*;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.referral.Referral;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * The persistent class for the RegionCode database table.
 * 
 */
@Entity
public class RegionCode extends AbstractEntity {
	
	@OneToMany(targetEntity=Cpt.class, mappedBy = "regionCode",  cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private Set <Cpt> cpts = new HashSet<Cpt>(0);
	
	@ManyToOne(targetEntity=State.class,   cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private Set <State> states = new HashSet<State>(0);
	
	@OneToMany(targetEntity=Zip.class, mappedBy = "regionCode",  cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private Set <Zip> zips = new HashSet<Zip>(0);
	
	
	

}

package com.wwpc.model.feeschedule;



import javax.persistence.*;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.payer.Payer;
import com.wwpc.model.referral.Referral;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

	
	@Entity
	public class FeeData extends AbstractEntity {
		
		
		
		@OneToMany(targetEntity=Cpt.class,  mappedBy = "feeData", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
		private Set <Cpt> cpts = new HashSet<Cpt>(0);
		
		private BigInteger fee;

		public BigInteger getFee() {
			return fee;
		}

		public void setFee(BigInteger fee) {
			this.fee = fee;
		}

		
	}

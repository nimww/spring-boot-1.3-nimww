package com.wwpc.model.feeschedule;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.payer.Payer;

@Entity
public class Zip extends AbstractEntity {
	
	String zip;
	
	@ManyToOne(optional = false)
	RegionCode regionCode;

}

package com.wwpc.model.encounter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.Case.CaseAccount;
import com.wwpc.model.Case.CaseAdmin;
import com.wwpc.model.referral.Log;
import com.wwpc.model.referral.Referral;

public class Encounter extends AbstractEntity implements Serializable {
	
	
	@OneToMany(targetEntity=Log.class, mappedBy ="encounter", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private Set <Log> logs = new HashSet<Log>(0);
			
	@OneToOne(optional = false)
	private Referral referral;
	
	@Embedded
	private EncounterType encounterType;
	@Embedded
	private EncounterStatus encounterStatus;

}

package com.wwpc.model.encounter;

import javax.persistence.Embeddable;

@Embeddable
public enum EncounterStatus {
	
	Active(1),
	InBilling(9),
	Closed(6),
	Void(5);
	
	private int status;
	
	private EncounterStatus(int i){
		this.status = i;
	}
	
	public int getStatus(){
		return status;
	}
}


//<select name="CaseStatusID" id="CaseStatusID">
//<option value="-2">-All</option>
//<option value="-1" selected="">-All Active</option>
//<option value="0">NA</option>
//<option value="1">Active</option>
//<option value="4">Active: Flag</option>
//<option value="7">Active: Flag: Hold-Bill</option>
//<option value="12">Active: Flag: Mgr Escalate</option>
//<option value="11">Active: Flag: Mgr Rp Review</option>
//<option value="8">Active: Flag: Mgr Rx Review</option>
//<option value="9">Billing: In Progress</option>
//<option value="6">Closed</option>
//<option value="5">Void</option>
//<option value="10">Void: Leak</option>
//<option value="3">Void: No-Show</option>
//<option value="2">Void: Rescan</option>
//</select>



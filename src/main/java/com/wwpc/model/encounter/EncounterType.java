package com.wwpc.model.encounter;

import javax.persistence.Embeddable;

@Embeddable
public enum EncounterType {
	
	None,
	Single,
	Multi,
	Other,
	MR,
	CT,
	EMG,
	NCV,
	PET,
	Lab;

}

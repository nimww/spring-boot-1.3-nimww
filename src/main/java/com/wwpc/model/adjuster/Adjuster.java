package com.wwpc.model.adjuster;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.wwpc.model.AbstractEntity;
import com.wwpc.model.Case.CaseAccount;

@Entity
public class Adjuster extends AbstractEntity {
	
	@OneToOne(optional = false)
	private CaseAccount caseAccount;

	public CaseAccount getCaseAccount() {
		return caseAccount;
	}

	public void setCaseAccount(CaseAccount caseAccount) {
		this.caseAccount = caseAccount;
	}

}

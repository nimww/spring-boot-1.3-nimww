It is also possible to run a packaged application with remote debugging support enabled. This allows you to attach a debugger to your packaged application:

$ java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n \
       -jar target/myproject-0.0.1-SNAPSHOT.jar





Spring-Boot-JSF-Example
=======================

Looked around and saw on stack overflow that no one had a working example of JSF and Spring boot working together. So I added this example so that I could use it to answer some stack-overflow questions

To run use 

```
Windows:
gradlew run 

Other:
sh gradlew run
```

After that visit these 2 urls

JSF Example [http://localhost:8080/test.xhtml](http://localhost:8080/test.xhtml)

http://localhost:8080/facesTemplateIndex.xhtml

http://localhost:8080/NoFaces_index.xhtml

http://localhost:8080/index.xhtml
http://localhost:8080/index.html


and 

Spring MVC Greeting Service [http://localhost:8080/greeting](http://localhost:8080/greeting) http://localhost:8080/index.xhtml

There was a common belief that because JSF and Spring MVC were their own view technologies and that they could not be used together, but this is incorrect and is part of this example.


javax.el.PropertyNotFoundException: Property 'initRandomCarPool' not found on type hello.CarPool
        at javax.el.BeanELResolver$BeanProperties.get(BeanELResolver.java:266)
        at javax.el.BeanELResolver$BeanProperties.access$300(BeanELResolver.java:243)
        at javax.el.BeanELResolver.property(BeanELResolver.java:353)
        at javax.el.BeanELResolver.getValue(BeanELResolver.java:97)
        at com.sun.faces.el.DemuxCompositeELResolver._getValue(DemuxCompositeELResolver.java:176)
